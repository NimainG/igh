import os
import json
import requests
import uuid
import random

from flask import Flask, request, Response, jsonify
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from multiprocessing import Process
from sqlalchemy import func
from os import path

from datetime import datetime
from calendar import monthrange
from dateutil.relativedelta import relativedelta
from sqlalchemy import extract
from sqlalchemy.orm.attributes import flag_modified

from app.config import activateConfig

from app.models import db, DeviceDataBatch

load_dotenv()

active_config = activateConfig(os.getenv('ENV') or 'production')

def correct_wd():
    app = Flask(__name__)
    app.config.from_object(activateConfig(active_config))
    app.app_context().push()
    db.init_app(app)

    x=0
    _data = DeviceDataBatch\
        .query\
        .all()
    for _d in _data:
        _batch = _d.batch

        try:
            if 'wd' in _batch:
                print(_batch['wd'])
                _batch['wd'] = round(float(str(_batch['wd']).replace('"','')), 2)
                print(_batch['wd'])
                print("======")
                _d.batch = _batch
                flag_modified(_d, "batch")
                # if x >10;
                #     break
                #
                # x = x + 1
                db.session.commit()
        except Exception as e:
            print(str(_batch['wd']) + " failed to correct. :: " + str(e));

if __name__ == '__main__':
    p1 = Process(target=correct_wd)
    p1.start()
