from __future__ import print_function

import os
import json
import requests
import uuid
import random
import gspread

from flask import Flask, request, Response, jsonify
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from multiprocessing import Process
from sqlalchemy import func
from os import path
from oauth2client.service_account import ServiceAccountCredentials
from googleapiclient.discovery import build

from datetime import datetime
from calendar import monthrange
from dateutil.relativedelta import relativedelta
from sqlalchemy import extract, Integer

from app.config import activateConfig

from app.models import db, Customer, User, Role, UserRole, Greenhouse, \
    CustomerGreenhouse, Farm, CustomerDevice, Device, DeviceDataBatch, Service, \
    CustomerUser, DeviceData

load_dotenv()

active_config = activateConfig('production')

def batch_exists(date_time_created, batch, source):
    status = False

    _batch_exist = db\
        .session\
        .query(DeviceDataBatch, DeviceData)\
        .join(DeviceData, DeviceData.batch_id == DeviceDataBatch.id)\
        .filter(DeviceDataBatch.batch == batch)\
        .filter(DeviceDataBatch.data_source == source)\
        .filter(DeviceDataBatch.created_at == date_time_created)\
        .filter(DeviceDataBatch.status > active_config.STATUS_DELETED)\
        .first()
    if _batch_exist:
        status = True

    return status

def save_batch(date_time_created, batch, meta_data, source):
    _attached_device = None
    if 'id' in batch:
        _attached_device = db\
            .session\
            .query(Device, CustomerDevice)\
            .outerjoin(CustomerDevice, CustomerDevice.device_id == Device.id)\
            .filter(Device.serial == batch['id'])\
            .filter(Device.status > active_config.STATUS_DELETED)\
            .first()

    _data_batch = DeviceDataBatch(
        uid=uuid.uuid4(),
        revision=0,
        status=active_config.STATUS_ACTIVE,
        created_at=date_time_created,
        batch=batch,
        meta_data=meta_data,
        data_source=source
    )
    db.session.add(_data_batch)
    db.session.commit()

    if _data_batch:
        customer_id = None
        device_id = None
        if _attached_device and _attached_device.CustomerDevice.customer_id:
            customer_id = _attached_device.CustomerDevice.customer_id
            device_id = _attached_device.Device.id

            _device_data = DeviceData(
                uid=uuid.uuid4(),
                revision=0,
                status=active_config.STATUS_ACTIVE,
                created_at=date_time_created,
                device_id=device_id,
                customer_id=customer_id,
                batch_id=_data_batch.id,
            )
            db.session.add(_device_data)
            db.session.commit()

    else:
        print(str(batch) + " FAILED to save!")

def import_data():
    app = Flask(__name__)
    app.config.from_object(activateConfig(active_config))
    app.app_context().push()
    db.init_app(app)

    scope = [
        "https://www.googleapis.com/auth/spreadsheets"
    ]
    SPREADSHEET_ID = '1JFT05kMx_7rHp81TTORO6_p1sA7A4Tm71N5krWlXatI'

    creds = ServiceAccountCredentials.from_json_keyfile_name('./g_6acf4cc02ef4.json', scope)

    service = build('sheets', 'v4', credentials=creds)

    sheet = service.spreadsheets()

    result = sheet.values().get(spreadsheetId=SPREADSHEET_ID, range='sheet1').execute()
    vals = result["values"]

    headers = vals[0]

    vals.pop(0)

    data_count = 0
    for val in vals:
        date_time_created = None
        to_ingest = {}
        pos = 0
        for header in headers:
            if header.lower() == 'dt':
                date_time_created = val[pos]
            elif header.lower() == 'id' and val[pos].lower() == "igh001":
                to_ingest[header.lower()] = "IGH45RT578UGF43"
            elif header.lower() == 'npk':
                to_ingest[header.lower()] = val[pos].split("-")
            else:
                to_ingest[header.lower()] = val[pos]

            pos += 1

        if date_time_created:
            if not batch_exists(date_time_created, to_ingest, active_config.DATA_G_SHEET):
                save_batch(date_time_created, to_ingest, {}, active_config.DATA_G_SHEET)
                data_count += 1

    print("DONE! Imported: " + str(data_count) + " messages from GoogleSheet")

if __name__ == '__main__':
    p1 = Process(target=import_data)
    p1.start()
