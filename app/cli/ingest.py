import os
import json
import requests
import uuid
import random

from flask import Flask, request, Response, jsonify
from flask_sqlalchemy import SQLAlchemy
from dotenv import load_dotenv
from multiprocessing import Process
from sqlalchemy import func
from os import path

from datetime import datetime
from calendar import monthrange
from dateutil.relativedelta import relativedelta
from sqlalchemy import extract

from app.config import activateConfig

from app.models import db, Customer, User, Role, UserRole, Greenhouse, \
    CustomerGreenhouse, Farm, CustomerDevice, Device, DeviceDataBatch, Service, \
    CustomerUser, DeviceData

load_dotenv()

active_config = activateConfig(os.getenv('ENV') or 'production')

user_id = 1

device_serial = 'IGH45RT578UGF43'
customer_id = 1

def import_data():
    app = Flask(__name__)
    app.config.from_object(activateConfig(active_config))
    app.app_context().push()
    db.init_app(app)

    data_batch = []

    eval_date = datetime(2020, 1, 1, 0, 0, 0)
    end_date = datetime(2020, 3, 31, 0, 0, 0)

    files_source = './raw/data_imports/jan_mar_20_'

    data_files = [
        'co2',
        'npk',
        'st',
        'at',
        'li',
        'sm',
        'ah',
        'wd',
        'spid',
        'sbv',
        'bv',
        'vp',
        'ssig',
        'csig',
        'smsc',
    ]

    while eval_date <= end_date:
        this_month_days = monthrange(int(eval_date.strftime('%Y')), int(eval_date.strftime('%-m')))[1]
        __d = {}
        __d['date'] = eval_date.strftime('%Y-%m-%d')
        for _source in data_files:
            __d[_source] = []
            _f_path = files_source + _source + '.json'
            if path.exists(_f_path):
                f = open(_f_path)
                _data = json.load(f)
                f.close()
                for _d in _data:
                    if _d["FIELD1"] == str(eval_date.strftime('%b-%y')):
                        if eval_date <= end_date:
                            day = 1
                            while day <= this_month_days:
                                eval_time = 1
                                while eval_time <= 24:
                                    if _d["TIME"] == eval_time:
                                        if str(eval_date.strftime('%-d')) == str(day):
                                            _eval_time = eval_time - 1
                                            _eval_time = '0' + str(_eval_time) if _eval_time < 10 else str(_eval_time)
                                            _package = {
                                                _eval_time: _d[str(day)]
                                            }
                                            __d[_source].append(_package)
                                    eval_time += 1
                                day += 1

        data_batch.append(__d)
        eval_date += relativedelta(days=1)

    to_db = []

    for dta in data_batch:
        _to_storage = {}
        h = 0
        while h < 24:
            _h = h
            _h = '0' + str(_h) if _h < 10 else str(_h)
            _h_index = dta['date'] + ' ' + _h + ':00:00'

            _to_storage[_h_index] = {}

            _to_storage[_h_index]["id"] = device_serial

            for df in data_files:
                for _d in dta[df]:
                    if _h in _d:
                        if df == 'npk':
                            _npk_ingest = [0,0,0]
                            _npk = _d[_h].split("-")
                            if len(_npk) == 3:
                                _npk_ingest = _npk
                            _to_storage[_h_index][df] = _npk_ingest
                        else:
                            _to_storage[_h_index][df] = _d[_h]

            _to_storage[_h_index]["sp1id"] = "ighp001"
            _to_storage[_h_index]["sp2id"] = "ighp002"
            _to_storage[_h_index]["sp1bv"] = 3
            _to_storage[_h_index]["sp2bv"] = 3.1
            _to_storage[_h_index]["vp"] = 1
            _to_storage[_h_index]["ssig"] = 49
            _to_storage[_h_index]["csig"] = 100
            _to_storage[_h_index]["smsc"] = 74

            # Store now
            _attached_device = Device\
                .query\
                .filter(Device.serial == device_serial)\
                .filter(Device.status > active_config.STATUS_DELETED)\
                .first()
            if _attached_device:
                _batch_exist = db\
                    .session\
                    .query(DeviceDataBatch, DeviceData)\
                    .join(DeviceData, DeviceData.batch_id == DeviceDataBatch.id)\
                    .filter(DeviceData.device_id == _attached_device.id)\
                    .filter(DeviceDataBatch.batch == _to_storage[_h_index])\
                    .filter(DeviceDataBatch.created_at == _h_index)\
                    .filter(DeviceDataBatch.status > active_config.STATUS_DELETED)\
                    .first()
                if not _batch_exist:

                    _data_batch = DeviceDataBatch(
                        uid=uuid.uuid4(),
                        revision=0,
                        status=active_config.STATUS_ACTIVE,
                        created_at=_h_index,
                        batch=_to_storage[_h_index]
                    )
                    db.session.add(_data_batch)
                    db.session.commit()

                    if _data_batch:
                        _device_data = DeviceData(
                            uid=uuid.uuid4(),
                            revision=0,
                            status=active_config.STATUS_ACTIVE,
                            created_at=_h_index,
                            device_id=_attached_device.id,
                            customer_id=customer_id,
                            batch_id=_data_batch.id,
                        )
                        db.session.add(_device_data)
                        db.session.commit()

            h += 1

if __name__ == '__main__':
    p1 = Process(target=import_data)
    p1.start()
