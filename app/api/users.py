from flask import request, json, Response, Blueprint
from ..models import User
from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException
from werkzeug.security import check_password_hash, generate_password_hash

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from flask import Response
from app.models import Auth
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from flask_mail import Mail,Message 
from app import mail


from app.config import activateConfig

from app.core import alert

from app.core import user
from app.core.user import invites
from app.core.user import auth
from app.core.user import realms
from app.core.user import configs
from app.core.user import user_roles
from app.core.user import igh
from app.core.user import customers
from app.core.user import partners

from re import DEBUG
from app import *

from app.helper import validateEmailAddress, valid_phone_number, send_account_activation_mail

api = Blueprint('dashboard', __name__, template_folder='templates', static_folder='static',  static_url_path='/igh/static')
api.config = {}

load_dotenv()
