import os
import smtplib
import requests
import re
import datetime

from flask import Flask
from flask import request
from flask import jsonify
from email.utils import parseaddr
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from app.models import db
from flask_wtf import Form
from dotenv import load_dotenv

from app.config import activateConfig
from app.core.mail import send_mail

from app.models import User, UserRole, Role, CustomerUser, Media

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def status_name(status=None):
    _status_name = "-"
    if status == config.STATUS_ACTIVE:
        _status_name = "Active"

    if status == config.STATUS_INACTIVE:
        _status_name = "Inactive"

    if status == config.STATUS_DELETED:
        _status_name = "Deleted"

    return _status_name

def creator_detail(creator_id):
    if creator_id:
        user = User\
            .query\
            .filter(User.id == creator_id)\
            .filter(User.status > config.STATUS_DELETED)\
            .first()
        if user:
            return {
                "first_name": user.first_name,
                "last_name": user.last_name,
                "phone_number": user.phone_number,
                "email_address": user.email_address
            }

    return {}

def fetch_realm_name(realm_id):
    realm = ""

    if realm_id == config.USER_IGH:
        realm = "IGH"

    if realm_id == config.USER_CUSTOMER:
        realm = "Customer"

    if realm_id == config.USER_PARTNER:
        realm = "Partner"

    return realm



# def send_email_api(emailFrom, emailTo, emailSubject, message):
#     return requests.post(
#         "https://api.mailgun.net/v3/" + config.MG_IGH_DOMAIN + "/messages",
#         auth=("api", config.MAILGUN_API_KEY),
#         data={"from": emailFrom,
#               "to": emailTo,
#               "subject": emailSubject,
#               "html": message})

# def send_email(emailFrom, emailTo, emailSubject, message):
#     if '@' in parseaddr(emailFrom)[1] and '@' in parseaddr(emailTo)[1] and emailSubject != "" and message != "":
#         msg = MIMEMultipart('alternative')
#         msg['Subject'] = emailSubject
#         msg['From'] = config.EMAIL_FROM
#         msg['To'] = emailTo
#         msg.attach(MIMEText(message, 'html'))

#         try:
#             instance = smtplib.SMTP(config.EMAIL_SERVER, config.EMAIL_SERVER_PORT)
#             instance.ehlo()
#             instance.starttls()
#             instance.login(config.SMTP_USERNAME, config.SMTP_PASSWORD)
#             instance.sendmail(emailFrom, emailTo, msg.as_string())
#             instance.quit()
#             return "Sent"
#         except Exception as e:
#             return "Failed: " + str(e)

#     return "Failed: Invalid properties"

def send_account_activation_mail(activation_code=None, first_name=None, email_address=None):

    if activation_code is not None and first_name is not None and email_address is not None:
        subject = "Account Activation"
        message = """\
                        <!DOCTYPE html>
                        <html>
                            <head>
                                <meta charset="utf-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                <title>""" + config.COMPANY_NAME + """</title>
                                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                            </head>
                            <body class="hold-transition skin-purple sidebar-mini">
                                <section class="content">
                                    <p>Dear """ + first_name +""",</p>
                                    <p>An account has been created for you in the """ + config.APP_NAME + """.</p>
                                    <p>Use the following link to activate your account by choosing a password.</p>
                                    <p><a href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/activate/""" + activation_code + """' target='mails'>""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/activate/""" + activation_code + """</a></p>
                                    <p>If the link does not respond, you can copy and paste it in your browser to access the activation page.</p>
                                    <p>Regards<br /><br />""" + config.CO_TEAM + """</p>
                                    <p><small><strong>Powered by """ + config.SIGN_OFF + """</strong></small></p>
                                </section>
                            </body>
                        </html>
                    """

        #mail_resp = send_email_api(config.MAILS_FROM, email_address, subject, message)
        mail_resp = send_mail(email_address,subject,config.MAILS_FROM,message)
        return mail_resp

    return "Not sent"

def password_reset_request(email_address=None, first_name=None, reset_code=None):

    if email_address is not None and first_name is not None and reset_code is not None:
        subject = "Password Reset Request"
        message = """\
                        <!DOCTYPE html>
                        <html>
                            <head>
                                <meta charset="utf-8">
                                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                                <title>""" + config.COMPANY_NAME + """</title>
                                <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
                            </head>
                            <body class="hold-transition skin-purple sidebar-mini">
                                <section class="content">
                                    <p>Dear """ + first_name + """,</p>
                                    <p>A password request has been received for your account. If you did not send it, please ignore this email. If you did send the request, use the code below to reset your password from the app.</p>
                                    <p><a href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/reset/""" + reset_code + """' target='mails'>""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/reset/""" + reset_code + """</a></p>
                                    <p>If the link does not respond, you can copy and paste it in your browser to access the reset page.</p>
                                    <p>Regards<br /><br />""" + config.CO_TEAM + """</p>
                                    <p><small><strong>Powered by """ + config.SIGN_OFF + """</strong></small></p>
                                </section>
                            </body>
                        </html>
                    """

        #mail_resp = send_email_api(config.MAILS_FROM, email_address, subject, message)
        #send_mail(_props['to'],_props['subject'],email_from,message)  
        mail_resp = send_mail(email_address,subject,config.MAILS_FROM,message)
        return mail_resp

    return False

def validateEmailAddress(emailAddress):
    if emailAddress is not None and '@' in parseaddr(emailAddress)[1]:
        return True

    return False

def valid_phone_number(phone_number):
    pattern = re.compile("^[\dA-Z]{3}-[\dA-Z]{3}-[\dA-Z]{4}$", re.IGNORECASE)
    return pattern.match(phone_number) is not None

def valid_uuid(uid):
    regex = re.compile('^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z', re.I)
    match = regex.match(str(uid))
    return bool(match)

def valid_date(dt):
    try:
        valid_date = datetime.datetime.strptime(dt, '%Y-%m-%d %H:%M:%S')
        return valid_date
    except:
        pass

    return bool(False)

def billing_name(id):
    if id == config.BILLING_FREE:
        return "Free"

    if id == config.BILLING_MONTHLY:
        return "Monthly"

    if id == config.BILLING_QUARTERLY:
        return "Quarterly"

    if id == config.BILLING_YEARLY:
        return "Yearly"

    return "-"

def person_info(person_id):
    _response = {}
    _user = User\
        .query\
        .filter(User.id == person_id)\
        .filter(User.status > config.STATUS_DELETED)\
        .first()
    if _user:
        _avatar = {}
        if _user.avatar_media_id:
            _media = Media\
                .query\
                .filter(Media.id == _user.avatar_media_id)\
                .filter(Media.status > config.STATUS_DELETED)\
                .first()
            if _media:
                _avatar = {
                    "file": _media.file_url,
                    "caption": _media.caption
                }

        _response = {
            "id": _user.uid,
            "email_address": _user.email_address,
            "phone_number": _user.phone_number,
            "first_name": _user.first_name,
            "last_name": _user.last_name,
            "avatar": _avatar,
            "status": "Active" if _user.status == config.STATUS_ACTIVE else "Inactive",
            "created": _user.created_at
        }

    return jsonify(_response)

def contact_person(contact_person_id):
    _response = {}
    _user = db\
        .session\
        .query(User, UserRole, Role, CustomerUser)\
        .join(UserRole, UserRole.user_id == User.id)\
        .join(Role, Role.id == UserRole.role_id)\
        .outerjoin(CustomerUser, CustomerUser.user_id == User.id)\
        .filter(User.id == contact_person_id)\
        .filter(User.status > config.STATUS_DELETED)\
        .first()
    if _user:
        _response = {
            "id": _user.User.uid,
            "first_name": _user.User.first_name,
            "last_name": _user.User.last_name,
            "phone_number": _user.User.phone_number,
            "email_address": _user.User.email_address,
            "id_number": _user.CustomerUser.id_number if _user.CustomerUser and _user.CustomerUser.id_number is not None else "-",
            "role": _user.Role.name,
            "status": "Active" if _user.User.status == config.STATUS_ACTIVE else "Inactive",
            "created": _user.User.created_at,
            "updated": _user.User.modified_at,
        }

    return _response
