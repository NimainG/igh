import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Shadenet

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shadenet_obj(shadenet=False, real_id=False):
    _shadenet = {}
    if shadenet:
        if real_id:
            _shadenet["real_id"] = shadenet.id

        _shadenet["id"] = str(shadenet.uid)
        _shadenet["shadenet_ref"] = shadenet.shadenet_ref
        _shadenet["size"] = shadenet.size
        _shadenet["info_link"] = shadenet.info_link
        _shadenet["created_by"] = creator_detail(shadenet.creator_id)
        _shadenet["status"] = status_name(shadenet.status)
        _shadenet["created"] = shadenet.created_at.strftime('%d %B %Y')
        _shadenet["modified"] = shadenet.modified_at

    return _shadenet

def fetch_all():
    response = []

    shadenets = db\
        .session\
        .query(Shadenet)\
        .filter(Shadenet.status > config.STATUS_DELETED)\
        .all()

    for shadenet in shadenets:
        response.append(shadenet_obj(shadenet))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shadenet = db\
            .session\
            .query(Shadenet)\
            .filter(Shadenet.uid == uid)\
            .filter(Shadenet.status > config.STATUS_DELETED)\
            .first()
        if shadenet:
            response = shadenet_obj(shadenet, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shadenet = db\
            .session\
            .query(Shadenet)\
            .filter(Shadenet.id == id)\
            .filter(Shadenet.status > config.STATUS_DELETED)\
            .first()
        if shadenet:
            response = shadenet_obj(shadenet)

    return jsonify(response)

def add_new(shadenet_data=None, return_obj=False):

    try:
        data = json.loads(shadenet_data)
        if data:

            if 'shadenet_ref' in data and 'creator_id' in data:

                _shadenet_ref = data['shadenet_ref']
                _size = data['size']  if 'size' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _shadenet_ref and _creator:
                    new_shadenet = Shadenet(
                        uid = uuid.uuid4(),
                        
                        status = config.STATUS_ACTIVE,
                        shadenet_ref = _shadenet_ref,
                        size = _size,
                        info_link = _info_link,
                        creator_id = _creator
                    )
                    db.session.add(new_shadenet)
                    db.session.commit()
                    if new_shadenet:
                        if return_obj:
                            return jsonify(fetch_one(new_shadenet.uid)), 200

                        return jsonify({"info": "Shadenet added successfully!"}), 200

        return jsonify({"error": "Shadenet not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shadenet_id, shadenet_data=None, return_obj=False):

    try:
        data = json.loads(shadenet_data)
        if data:

            if valid_uuid(shadenet_id):

                _size = data['size']  if 'size' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                if _name:
                    _shadenet = Shadenet\
                        .query\
                        .filter(Shadenet.uid == str(shadenet_id))\
                        .first()
                    if _shadenet:
                        _shadenet.size = _size
                        _shadenet.info_link = _info_link
                        db.session.commit()

                        return jsonify({"info": "Shadenet edited successfully!"}), 200

        return jsonify({"error": "Shadenet not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(shadenet_id):

    try:

        if valid_uuid(shadenet_id):
            _shadenet= Shadenet\
                .query\
                .filter(Shadenet.uid == shadenet_id)\
                .first()
            if _shadenet:
                _shadenet.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Shadenet activated successfully!"}), 200

        return jsonify({"error": "Shadenet not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shadenet_id):

    try:

        if valid_uuid(shadenet_id) :
            _shadenet = Shadenet\
                .query\
                .filter(Shadenet.uid == shadenet_id)\
                .first()
            if _shadenet:
                _shadenet.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shadenet deactivated successfully!"}), 200

        return jsonify({"error": "Shadenet not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
