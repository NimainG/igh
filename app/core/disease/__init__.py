import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Disease

from app.core import user

from app.core.disease import photos as disease_photos


load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def disease_obj(disease=False, real_id=False):
    _disease = {}
    if disease:
        if real_id:
            _disease["real_id"] = disease.id

        _disease["id"] = str(disease.uid)
        _disease["name"] = disease.name
        _disease["description"] = disease.description
        _disease["photos"] = disease_photos.fetch_by_disease(disease.id).get_json()
        _disease["created_by"] = creator_detail(disease.creator_id)
        _disease["status"] = status_name(disease.status)
        _disease["created"] = disease.created_at

    return _disease

def fetch_all():
    response = []

    diseases = db\
        .session\
        .query(Disease)\
        .filter(Disease.status > config.STATUS_DELETED)\
        .all()

    for disease in diseases:
        response.append(disease_obj(disease))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        disease = db\
            .session\
            .query(Disease)\
            .filter(Disease.uid == uid)\
            .filter(Disease.status > config.STATUS_DELETED)\
            .first()
        if disease:
            response = disease_obj(disease, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        disease = db\
            .session\
            .query(Disease)\
            .filter(Disease.id == id)\
            .filter(Disease.status > config.STATUS_DELETED)\
            .first()
        if disease:
            response = disease_obj(disease)

    return jsonify(response)

def add_new(disease_data=None, return_obj=False):

    try:
        data = json.loads(disease_data)
        if data:

            if 'name' in data and 'description' in data and 'creator' in data:

                _name = data['name']
                _description = data['description']
                _creator = None

                if valid_uuid(data['creator']):
                    _creator = user.fetch_one(data['creator'], True).get_json()['real_id']

                if _name and _description and _creator:
                    new_disease = Disease(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        description = _description,
                        creator_id = _creator
                    )
                    db.session.add(new_disease)
                    db.session.commit()
                    if new_disease:
                        if return_obj:
                            return jsonify(fetch_one(new_disease.uid)), 200

                        return jsonify({"info": "Disease added successfully!"}), 200

        return jsonify({"error": "Disease not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(disease_id, disease_data=None, return_obj=False):

    try:
        data = json.loads(disease_data)
        if data:

            if valid_uuid(disease_id) and 'name' in data and 'description' in data:

                _name = data['name']
                _description = data['description']
                _creator = data['creator_id']

                if _name and _description and _creator:
                    _disease = Disease\
                        .query\
                        .filter(Disease.uid == str(disease_id))\
                        .first()
                    if _disease:
                        _disease.name = _name
                        _disease.description = _description
                        db.session.commit()

                        return jsonify({"info": "Disease edited successfully!"}), 200

        return jsonify({"error": "Disease not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(disease_id):

    try:

        if valid_uuid(disease_id) :
            _disease = Disease\
                .query\
                .filter(Disease.uid == disease_id)\
                .first()
            if _disease:
                _disease.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Disease deactivated successfully!"}), 200

        return jsonify({"error": "Disease not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
