import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import DiseasePhoto

from app.core import media
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def disease_photo_obj(disease_photo=False, real_id=False):
    _disease_photo = {}
    if disease_photo:
        if real_id:
            _disease_photo["real_id"] = disease_photo.id

        _disease_photo["id"] = str(disease_photo.uid)
        _disease_photo["photo"] = media.fetch_by_id(disease_photo.photo_media_id).get_json()
        _disease_photo["created_by"] = creator_detail(disease_photo.creator_id)
        _disease_photo["status"] = status_name(disease_photo.status)
        _disease_photo["created"] = disease_photo.created_at
        _disease_photo["modified"] = disease_photo.modified_at

    return _disease_photo

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        disease_photo = db\
            .session\
            .query(DiseasePhoto)\
            .filter(DiseasePhoto.uid == uid)\
            .filter(DiseasePhoto.status > config.STATUS_DELETED)\
            .first()
        if disease_photo:
            response = disease_photo_obj(disease_photo, real_id)

    return jsonify(response)

def fetch_by_disease(disease_id):
    response = {}
    if disease_id:
        disease_photo = db\
            .session\
            .query(DiseasePhoto)\
            .filter(DiseasePhoto.disease_id == disease_id)\
            .filter(DiseasePhoto.status > config.STATUS_DELETED)\
            .first()
        if disease_photo:
            response = disease_photo_obj(disease_photo)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        disease_photo = db\
            .session\
            .query(DiseasePhoto)\
            .filter(DiseasePhoto.id == id)\
            .filter(DiseasePhoto.status > config.STATUS_DELETED)\
            .first()
        if disease_photo:
            response = disease_photo_obj(disease_photo)

    return jsonify(response)

def add_new(disease_photo_data=None, return_obj=False):

    try:
        data = json.loads(disease_photo_data)
        if data:

            if 'photo_media_id' in data and 'disease_id' in data and 'creator_id' in data :

                _photo_media = None
                _disease_id = data['disease_id']
                _creator = None

                if valid_uuid(data['photo_media_id']):
                    _photo_media = media.fetch_one(data['photo_media_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _photo_media and _disease_id and _creator:
                    new_disease_photo = DiseasePhoto(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        photo_media_id = _photo_media,
                        disease_id = _disease_id,
                        creator_id = _creator
                    )
                    db.session.add(new_disease_photo)
                    db.session.commit()
                    if new_disease_photo:
                        if return_obj:
                            return jsonify(fetch_one(new_disease_photo.uid)), 200

                        return jsonify({"info": "Disease photo added successfully!"}), 200

        return jsonify({"error": "Disease photo not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(disease_photo_id):

    try:
        if valid_uuid(disease_photo_id) :
            _disease_photo = DiseasePhoto\
                .query\
                .filter(DiseasePhoto.uid == disease_photo_id)\
                .first()
            if _disease_photo:
                _disease_photo.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Disease photo deactivated successfully!"}), 200

        return jsonify({"error": "Disease photo not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
