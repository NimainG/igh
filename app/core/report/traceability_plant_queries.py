import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import TraceabilityPlantQuery

from app.core import user

from app.core.report import traceability

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def traceability_pq_obj(traceability_pq=False, real_id=False):
    _traceability_pq = {}
    if traceability_pq:
        if real_id:
            _traceability_pq["real_id"] = traceability_pq.id

        _traceability_pq["id"] = str(traceability_pq.uid)
        _traceability_pq["planting_date"] = traceability_pq.planting_date
        _traceability_pq["crop_variety"] = traceability_pq.crop_variety
        _traceability_pq["traceability_report_id"] = traceability.fetch_by_id(traceability_pq.traceability_report_id).get_json()['uid']
        _traceability_pq["created_by"] = creator_detail(traceability_pq.creator_id)
        _traceability_pq["status"] = status_name(traceability_pq.status)
        _traceability_pq["created"] = traceability_pq.created_at
        _traceability_pq["modified"] = traceability_pq.modified_at

    return _traceability_pq

def fetch_all():
    response = []

    traceabilities = db\
        .session\
        .query(TraceabilityPlantQuery)\
        .filter(TraceabilityPlantQuery.status > config.STATUS_DELETED)\
        .all()

    for traceability_pq in traceabilities:
        response.append(traceability_pq_obj(traceability_pq))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        traceability_pq = db\
            .session\
            .query(TraceabilityPlantQuery)\
            .filter(TraceabilityPlantQuery.uid == uid)\
            .filter(TraceabilityPlantQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_pq:
            response = traceability_pq_obj(traceability_pq, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        traceability_pq = db\
            .session\
            .query(TraceabilityPlantQuery)\
            .filter(TraceabilityPlantQuery.id == id)\
            .filter(TraceabilityPlantQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_pq:
            response = traceability_pq_obj(traceability_pq)

    return jsonify(response)

def fetch_by_traceability_report(traceability_report_id=0):
    response = []
    if traceability_report_id:
        traceabilities = db\
            .session\
            .query(TraceabilityPlantQuery)\
            .filter(TraceabilityPlantQuery.traceability_report_id == traceability_report_id)\
            .filter(TraceabilityPlantQuery.status > config.STATUS_DELETED)\
            .all()
        for traceability_pq in traceabilities:
            response.append(traceability_pq_obj(traceability_pq))

    return jsonify(response)

def add_new(traceability_pq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_pq_data)
        if data:

            _planting_date = None
            if 'planting_date' in data:
                _planting_date = data['planting_date']

            _crop_variety = None
            if 'crop_variety' in data:
                _crop_variety = data['crop_variety']

            _traceability_report_id = None
            if valid_uuid(data['traceability_report_id']):
                _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _traceability_report_id and _creator_id:
                new_traceability_pq = TraceabilityPlantQuery(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    planting_date = _planting_date,
                    crop_variety = _crop_variety,
                    traceability_report_id = _traceability_report_id,
                    creator_id = _creator_id
                )
                db.session.add(new_traceability_pq)
                db.session.commit()
                if new_traceability_pq:
                    if return_obj:
                        return jsonify(fetch_one(new_traceability_pq.uid)), 200

                    return jsonify({"info": "Traceability plant queries report added successfully!"}), 200

        return jsonify({"error": "Traceability plant queries report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(traceability_pq_id, traceability_pq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_pq_data)
        if data:

            if valid_uuid(traceability_pq_id):

                _planting_date = None
                if 'planting_date' in data:
                    _planting_date = data['planting_date']

                _crop_variety = None
                if 'crop_variety' in data:
                    _crop_variety = data['crop_variety']

                _traceability_report_id = None
                if valid_uuid(data['traceability_report_id']):
                    _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

                if _traceability_report_id:
                    _traceability_pq = TraceabilityPlantQuery\
                        .query\
                        .filter(TraceabilityPlantQuery.uid == str(traceability_pq_id))\
                        .first()
                    if _traceability_pq:
                        _traceability_pq.planting_date = _planting_date
                        _traceability_pq.crop_variety = _crop_variety
                        _traceability_pq.traceability_report_id = _traceability_report_id
                        db.session.commit()

                        return jsonify({"info": "Traceability plant queries report edited successfully!"}), 200

        return jsonify({"error": "Traceability plant queries report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(traceability_pq_id):

    try:

        if valid_uuid(traceability_pq_id) :
            _traceability_pq = TraceabilityPlantQuery\
                .query\
                .filter(TraceabilityPlantQuery.uid == traceability_pq_id)\
                .first()
            if _traceability_pq:
                _traceability_pq.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability plant queries report deactivated successfully!"}), 200

        return jsonify({"error": "Traceability plant queries report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
