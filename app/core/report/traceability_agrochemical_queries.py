import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import TraceabilityAgrochemicalQuery

from app.core import user

from app.core.report import traceability

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def traceability_aq_obj(traceability_aq=False, real_id=False):
    _traceability_aq = {}
    if traceability_aq:
        if real_id:
            _traceability_aq["real_id"] = traceability_aq.id

        _traceability_aq["id"] = str(traceability_aq.uid)
        _traceability_aq["name"] = traceability_aq.name
        _traceability_aq["component"] = traceability_aq.component
        _traceability_aq["application_date"] = traceability_aq.application_date
        _traceability_aq["traceability_report_id"] = traceability.fetch_by_id(traceability_aq.traceability_report_id).get_json()['uid']
        _traceability_aq["created_by"] = creator_detail(traceability_aq.creator_id)
        _traceability_aq["status"] = status_name(traceability_aq.status)
        _traceability_aq["created"] = traceability_aq.created_at
        _traceability_aq["modified"] = traceability_aq.modified_at

    return _traceability_aq

def fetch_all():
    response = []

    traceabilities = db\
        .session\
        .query(TraceabilityAgrochemicalQuery)\
        .filter(TraceabilityAgrochemicalQuery.status > config.STATUS_DELETED)\
        .all()

    for traceability_aq in traceabilities:
        response.append(traceability_aq_obj(traceability_aq))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        traceability_aq = db\
            .session\
            .query(TraceabilityAgrochemicalQuery)\
            .filter(TraceabilityAgrochemicalQuery.uid == uid)\
            .filter(TraceabilityAgrochemicalQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_aq:
            response = traceability_aq_obj(traceability_aq, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        traceability_aq = db\
            .session\
            .query(TraceabilityAgrochemicalQuery)\
            .filter(TraceabilityAgrochemicalQuery.id == id)\
            .filter(TraceabilityAgrochemicalQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_aq:
            response = traceability_aq_obj(traceability_aq)

    return jsonify(response)

def fetch_by_traceability_report(traceability_report_id=0):
    response = []
    if traceability_report_id:
        traceabilities = db\
            .session\
            .query(TraceabilityAgrochemicalQuery)\
            .filter(TraceabilityAgrochemicalQuery.traceability_report_id == traceability_report_id)\
            .filter(TraceabilityAgrochemicalQuery.status > config.STATUS_DELETED)\
            .all()
        for traceability_aq in traceabilities:
            response.append(traceability_aq_obj(traceability_aq))

    return jsonify(response)

def add_new(traceability_aq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_aq_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _component = None
            if 'component' in data:
                _component = data['component']

            _application_date = None
            if 'application_date' in data:
                _application_date = data['application_date']

            _traceability_report_id = None
            if valid_uuid(data['traceability_report_id']):
                _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _traceability_report_id and _creator_id:
                new_traceability_aq = TraceabilityAgrochemicalQuery(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    component = _component,
                    application_date = _application_date,
                    traceability_report_id = _traceability_report_id,
                    creator_id = _creator_id
                )
                db.session.add(new_traceability_aq)
                db.session.commit()
                if new_traceability_aq:
                    if return_obj:
                        return jsonify(fetch_one(new_traceability_aq.uid)), 200

                    return jsonify({"info": "Traceability agrochemical queries report added successfully!"}), 200

        return jsonify({"error": "Traceability agrochemical queries report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(traceability_aq_id, traceability_aq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_aq_data)
        if data:

            if valid_uuid(traceability_aq_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _component = None
                if 'component' in data:
                    _component = data['component']

                _application_date = None
                if 'application_date' in data:
                    _application_date = data['application_date']

                _traceability_report_id = None
                if valid_uuid(data['traceability_report_id']):
                    _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

                if _traceability_report_id:
                    _traceability_aq = TraceabilityAgrochemicalQuery\
                        .query\
                        .filter(TraceabilityAgrochemicalQuery.uid == str(traceability_aq_id))\
                        .first()
                    if _traceability_aq:
                        _traceability_aq.name = _name
                        _traceability_aq.component = _component
                        _traceability_aq.application_date = _application_date
                        _traceability_aq.traceability_report_id = _traceability_report_id
                        db.session.commit()

                        return jsonify({"info": "Traceability agrochemical queries report edited successfully!"}), 200

        return jsonify({"error": "Traceability agrochemical queries report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(traceability_aq_id):

    try:

        if valid_uuid(traceability_aq_id) :
            _traceability_aq = TraceabilityAgrochemicalQuery\
                .query\
                .filter(TraceabilityAgrochemicalQuery.uid == traceability_aq_id)\
                .first()
            if _traceability_aq:
                _traceability_aq.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability agrochemical queries report deactivated successfully!"}), 200

        return jsonify({"error": "Traceability agrochemical queries report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
