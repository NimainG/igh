import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import PreharvestChecklist

from app.core import user
from app.core import media
from app.core import customer
from app.core import report

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def pc_obj(pc=False, real_id=False):
    _pc = {}
    if pc:
        if real_id:
            _pc["real_id"] = pc.id

        _pc["id"] = str(pc.uid)
        _pc["animal_intrusion"] = pc.animal_intrusion
        _pc["crew_washed_hands"] = pc.crew_washed_hands
        _pc["crew_clothing_is_clean"] = pc.crew_clothing_is_clean
        _pc["crew_without_jewellery"] = pc.crew_without_jewellery
        _pc["packing_material_clean"] = pc.packing_material_clean
        _pc["customer_shield_id"] = customer.shields.fetch_by_id(pc.customer_shield_id).get_json()['uid']
        _pc["report_id"] = report.fetch_by_id(pc.report_id).get_json()['uid']
        _pc["customer_id"] = customer.fetch_by_id(pc.customer_id).get_json()['uid']
        _pc["created_by"] = creator_detail(pc.creator_id)
        _pc["status"] = status_name(pc.status)
        _pc["created"] = pc.created_at
        _pc["modified"] = pc.modified_at

    return _pc

def fetch_all():
    response = []

    pcs = db\
        .session\
        .query(PreharvestChecklist)\
        .filter(PreharvestChecklist.status > config.STATUS_DELETED)\
        .all()

    for pc in pcs:
        response.append(pc_obj(pc))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        pc = db\
            .session\
            .query(PreharvestChecklist)\
            .filter(PreharvestChecklist.uid == uid)\
            .filter(PreharvestChecklist.status > config.STATUS_DELETED)\
            .first()
        if pc:
            response = pc_obj(pc, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        pc = db\
            .session\
            .query(PreharvestChecklist)\
            .filter(PreharvestChecklist.id == id)\
            .filter(PreharvestChecklist.status > config.STATUS_DELETED)\
            .first()
        if pc:
            response = pc_obj(pc)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        pcs = db\
            .session\
            .query(PreharvestChecklist)\
            .filter(PreharvestChecklist.customer_shield_id == customer_shield_id)\
            .filter(PreharvestChecklist.status > config.STATUS_DELETED)\
            .all()
        for pc in pcs:
            response.append(pc_obj(pc))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        pcs = db\
            .session\
            .query(PreharvestChecklist)\
            .filter(PreharvestChecklist.report_id == report_id)\
            .filter(PreharvestChecklist.status > config.STATUS_DELETED)\
            .all()
        for pc in pcs:
            response.append(pc_obj(pc))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        pcs = db\
            .session\
            .query(PreharvestChecklist)\
            .filter(PreharvestChecklist.customer_id == customer_id)\
            .filter(PreharvestChecklist.status > config.STATUS_DELETED)\
            .all()
        for pc in pcs:
            response.append(pc_obj(pc))

    return jsonify(response)

def add_new(pc_data=None, return_obj=False):

    try:
        data = json.loads(pc_data)
        if data:

            _animal_intrusion = None
            if 'animal_intrusion' in data:
                _animal_intrusion = data['animal_intrusion']

            _crew_washed_hands = None
            if 'crew_washed_hands' in data:
                _crew_washed_hands = data['crew_washed_hands']

            _crew_clothing_is_clean = None
            if 'crew_clothing_is_clean' in data:
                _crew_clothing_is_clean = data['crew_clothing_is_clean']

            _crew_without_jewellery = None
            if 'crew_without_jewellery' in data:
                _crew_without_jewellery = data['crew_without_jewellery']

            _packing_material_clean = None
            if 'packing_material_clean' in data:
                _packing_material_clean = data['packing_material_clean']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_pc = PreharvestChecklist(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    animal_intrusion = _animal_intrusion,
                    crew_washed_hands = _crew_washed_hands,
                    crew_clothing_is_clean = _crew_clothing_is_clean,
                    crew_without_jewellery = _crew_without_jewellery,
                    packing_material_clean = _packing_material_clean,
                    customer_shield_id = _customer_shield_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_pc)
                db.session.commit()
                if new_pc:
                    if return_obj:
                        return jsonify(fetch_one(new_pc.uid)), 200

                    return jsonify({"info": "Preharvest checklist added successfully!"}), 200

        return jsonify({"error": "Preharvest checklist not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(pc_id, pc_data=None, return_obj=False):

    try:
        data = json.loads(pc_data)
        if data:

            if valid_uuid(pc_id):

                _animal_intrusion = None
                if 'animal_intrusion' in data:
                    _animal_intrusion = data['animal_intrusion']

                _crew_washed_hands = None
                if 'crew_washed_hands' in data:
                    _crew_washed_hands = data['crew_washed_hands']

                _crew_clothing_is_clean = None
                if 'crew_clothing_is_clean' in data:
                    _crew_clothing_is_clean = data['crew_clothing_is_clean']

                _crew_without_jewellery = None
                if 'crew_without_jewellery' in data:
                    _crew_without_jewellery = data['crew_without_jewellery']

                _packing_material_clean = None
                if 'packing_material_clean' in data:
                    _packing_material_clean = data['packing_material_clean']

                _pc = PreharvestChecklist\
                    .query\
                    .filter(PreharvestChecklist.uid == str(pc_id))\
                    .first()
                if _pc:
                    _pc.animal_intrusion = _animal_intrusion
                    _pc.crew_washed_hands = _crew_washed_hands
                    _pc.crew_clothing_is_clean = _crew_clothing_is_clean
                    _pc.crew_without_jewellery = _crew_without_jewellery
                    _pc.packing_material_clean = _packing_material_clean
                    db.session.commit()

                    return jsonify({"info": "Preharvest checklist edited successfully!"}), 200

        return jsonify({"error": "Preharvest checklist not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(pc_id):

    try:

        if valid_uuid(pc_id) :
            _pc = PreharvestChecklist\
                .query\
                .filter(PreharvestChecklist.uid == pc_id)\
                .first()
            if _pc:
                _pc.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Preharvest checklist deactivated successfully!"}), 200

        return jsonify({"error": "Preharvest checklist not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
