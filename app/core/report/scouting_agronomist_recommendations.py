import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ScoutingAgronomistRecommendation

from app.core import user
from app.core import media
from app.core import customer
from app.core import report
from app.core import supply

from app.core.setting import scouting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def scouting_ar_obj(scouting_ar=False, real_id=False):
    _scouting_ar = {}
    if scouting_ar:
        if real_id:
            _scouting_ar["real_id"] = scouting_ar.id

        _scouting_ar["id"] = str(scouting_ar.uid)
        _scouting_ar["spray"] = scouting_ar.spray
        _scouting_ar["agrochem"] = scouting_ar.agrochem
        _scouting_ar["active_ingredient"] = scouting_ar.active_ingredient
        _scouting_ar["agrochem_quantity"] = scouting_ar.agrochem_quantity
        _scouting_ar["water_amount"] = scouting_ar.water_amount
        _scouting_ar["application_rate"] = scouting_ar.application_rate
        _scouting_ar["application_start_date"] = scouting_ar.application_start_date
        _scouting_ar["application_method_id"] = supply.application_methods.fetch_by_id(scouting_ar.application_method_id).get_json()['uid']
        _scouting_ar["scouting_id"] = scouting.fetch_by_id(scouting_ar.scouting_id).get_json()['uid']
        _scouting_ar["report_id"] = report.fetch_by_id(scouting_ar.report_id).get_json()['uid']
        _scouting_ar["customer_id"] = customer.fetch_by_id(scouting_ar.customer_id).get_json()['uid']
        _scouting_ar["created_by"] = creator_detail(scouting_ar.creator_id)
        _scouting_ar["status"] = status_name(scouting_ar.status)
        _scouting_ar["created"] = scouting_ar.created_at
        _scouting_ar["modified"] = scouting_ar.modified_at

    return _scouting_ar

def fetch_all():
    response = []

    scouting_ars = db\
        .session\
        .query(ScoutingAgronomistRecommendation)\
        .filter(ScoutingAgronomistRecommendation.status > config.STATUS_DELETED)\
        .all()

    for scouting_ar in scouting_ars:
        response.append(scouting_ar_obj(scouting_ar))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        scouting_ar = db\
            .session\
            .query(ScoutingAgronomistRecommendation)\
            .filter(ScoutingAgronomistRecommendation.uid == uid)\
            .filter(ScoutingAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if scouting_ar:
            response = scouting_ar_obj(scouting_ar, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        scouting_ar = db\
            .session\
            .query(ScoutingAgronomistRecommendation)\
            .filter(ScoutingAgronomistRecommendation.id == id)\
            .filter(ScoutingAgronomistRecommendation.status > config.STATUS_DELETED)\
            .first()
        if scouting_ar:
            response = scouting_ar_obj(scouting_ar)

    return jsonify(response)

def fetch_by_scouting(scouting_id=0):
    response = []
    if scouting_id:
        scouting_ars = db\
            .session\
            .query(ScoutingAgronomistRecommendation)\
            .filter(ScoutingAgronomistRecommendation.scouting_id == scouting_id)\
            .filter(ScoutingAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for scouting_ar in scouting_ars:
            response.append(scouting_ar_obj(scouting_ar))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        scouting_ars = db\
            .session\
            .query(ScoutingAgronomistRecommendation)\
            .filter(ScoutingAgronomistRecommendation.report_id == report_id)\
            .filter(ScoutingAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for scouting_ar in scouting_ars:
            response.append(scouting_ar_obj(scouting_ar))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        scouting_ars = db\
            .session\
            .query(ScoutingAgronomistRecommendation)\
            .filter(ScoutingAgronomistRecommendation.customer_id == customer_id)\
            .filter(ScoutingAgronomistRecommendation.status > config.STATUS_DELETED)\
            .all()
        for scouting_ar in scouting_ars:
            response.append(scouting_ar_obj(scouting_ar))

    return jsonify(response)

def add_new(scouting_ar_data=None, return_obj=False):

    try:
        data = json.loads(scouting_ar_data)
        if data:

            _spray = 0
            if 'spray' in data:
                _spray = data['spray']

            _agrochem = None
            if 'agrochem' in data:
                _agrochem = data['agrochem']

            _active_ingredient = None
            if 'active_ingredient' in data:
                _active_ingredient = data['active_ingredient']

            _agrochem_quantity = None
            if 'agrochem_quantity' in data:
                _agrochem_quantity = data['agrochem_quantity']

            _water_amount = None
            if 'water_amount' in data:
                _water_amount = data['water_amount']

            _application_rate = None
            if 'application_rate' in data:
                _application_rate = data['application_rate']

            _application_start_date = None
            if 'application_start_date' in data:
                _application_start_date = data['application_start_date']

            _application_method_id = None
            if valid_uuid(data['application_method_id']):
                _application_method_id = supply.application_methods.fetch_one(data['application_method_id'], True).get_json()['real_id']

            _scouting_id = None
            if valid_uuid(data['scouting_id']):
                _scouting_id = scouting.fetch_one(data['scouting_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_scouting_ar = ScoutingAgronomistRecommendation(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    spray = _spray,
                    agrochem = _agrochem,
                    active_ingredient = _active_ingredient,
                    agrochem_quantity = _agrochem_quantity,
                    water_amount = _water_amount,
                    application_rate = _application_rate,
                    application_method_id = _application_method_id,
                    application_start_date = _application_start_date,
                    scouting_id = _scouting_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_scouting_ar)
                db.session.commit()
                if new_scouting_ar:
                    if return_obj:
                        return jsonify(fetch_one(new_scouting_ar.uid)), 200

                    return jsonify({"info": "Scouting agronomist recommendation report added successfully!"}), 200

        return jsonify({"error": "Scouting agronomist recommendation report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(scouting_ar_id, scouting_ar_data=None, return_obj=False):

    try:
        data = json.loads(scouting_ar_data)
        if data:

            if valid_uuid(scouting_ar_id):

                _spray = 0
                if 'spray' in data:
                    _spray = data['spray']

                _agrochem = None
                if 'agrochem' in data:
                    _agrochem = data['agrochem']

                _active_ingredient = None
                if 'active_ingredient' in data:
                    _active_ingredient = data['active_ingredient']

                _agrochem_quantity = None
                if 'agrochem_quantity' in data:
                    _agrochem_quantity = data['agrochem_quantity']

                _water_amount = None
                if 'water_amount' in data:
                    _water_amount = data['water_amount']

                _application_rate = None
                if 'application_rate' in data:
                    _application_rate = data['application_rate']

                _application_start_date = None
                if 'application_start_date' in data:
                    _application_start_date = data['application_start_date']

                _application_method_id = None
                if valid_uuid(data['application_method_id']):
                    _application_method_id = supply.application_methods.fetch_one(data['application_method_id'], True).get_json()['real_id']

                _scouting_ar = ScoutingAgronomistRecommendation\
                    .query\
                    .filter(ScoutingAgronomistRecommendation.uid == str(scouting_ar_id))\
                    .first()
                if _scouting_ar:
                    _scouting_ar.spray = _spray
                    _scouting_ar.agrochem = _agrochem
                    _scouting_ar.active_ingredient = _active_ingredient
                    _scouting_ar.agrochem_quantity = _agrochem_quantity
                    _scouting_ar.water_amount = _water_amount
                    _scouting_ar.application_rate = _application_rate
                    _scouting_ar.application_method_id = _application_method_id
                    _scouting_ar.application_start_date = _application_start_date
                    db.session.commit()

                    return jsonify({"info": "Scouting agronomist recommendation report edited successfully!"}), 200

        return jsonify({"error": "Scouting agronomist recommendation report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(scouting_ar_id):

    try:

        if valid_uuid(scouting_ar_id) :
            _scouting_ar = ScoutingAgronomistRecommendation\
                .query\
                .filter(ScoutingAgronomistRecommendation.uid == scouting_ar_id)\
                .first()
            if _scouting_ar:
                _scouting_ar.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Scouting agronomist recommendation report deactivated successfully!"}), 200

        return jsonify({"error": "Scouting agronomist recommendation report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
