import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import OtherReport

from app.core import user
from app.core import farm
from app.core import customer
from app.core import report

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def other_obj(other=False, real_id=False):
    _other = {}
    if other:
        if real_id:
            _other["real_id"] = other.id

        _other["id"] = str(other.uid)
        _other["notes"] = other.notes
        _other["farm_action"] = farm.actions.fetch_by_id(other.farm_action_id).get_json()
        _other["customer_shield_id"] = customer.shields.fetch_by_id(other.customer_shield_id).get_json()['uid']
        _other["report_id"] = report.fetch_by_id(other.report_id).get_json()['uid']
        _other["customer_id"] = customer.fetch_by_id(other.customer_id).get_json()['uid']
        _other["created_by"] = creator_detail(other.creator_id)
        _other["status"] = status_name(other.status)
        _other["created"] = other.created_at
        _other["modified"] = other.modified_at

    return _other

def fetch_all():
    response = []

    others = db\
        .session\
        .query(OtherReport)\
        .filter(OtherReport.status > config.STATUS_DELETED)\
        .all()

    for other in others:
        response.append(other_obj(other))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        other = db\
            .session\
            .query(OtherReport)\
            .filter(OtherReport.uid == uid)\
            .filter(OtherReport.status > config.STATUS_DELETED)\
            .first()
        if other:
            response = other_obj(other, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        other = db\
            .session\
            .query(OtherReport)\
            .filter(OtherReport.id == id)\
            .filter(OtherReport.status > config.STATUS_DELETED)\
            .first()
        if other:
            response = other_obj(other)

    return jsonify(response)

def fetch_by_farm_action(farm_action_id=0):
    response = []
    if farm_action_id:
        others = db\
            .session\
            .query(OtherReport)\
            .filter(OtherReport.farm_action_id == farm_action_id)\
            .filter(OtherReport.status > config.STATUS_DELETED)\
            .all()
        for other in others:
            response.append(other_obj(other))

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        others = db\
            .session\
            .query(OtherReport)\
            .filter(OtherReport.customer_shield_id == customer_shield_id)\
            .filter(OtherReport.status > config.STATUS_DELETED)\
            .all()
        for other in others:
            response.append(other_obj(other))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        others = db\
            .session\
            .query(OtherReport)\
            .filter(OtherReport.report_id == report_id)\
            .filter(OtherReport.status > config.STATUS_DELETED)\
            .all()
        for other in others:
            response.append(other_obj(other))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        others = db\
            .session\
            .query(OtherReport)\
            .filter(OtherReport.customer_id == customer_id)\
            .filter(OtherReport.status > config.STATUS_DELETED)\
            .all()
        for other in others:
            response.append(other_obj(other))

    return jsonify(response)

def add_new(other_data=None, return_obj=False):

    try:
        data = json.loads(other_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _farm_action_id = None
            if valid_uuid(data['farm_action_id']):
                _farm_action_id = farm.actions.fetch_one(data['farm_action_id'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if report_id and _customer_id and _creator_id:
                new_other = OtherReport(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    farm_action_id = _farm_action_id,
                    customer_shield_id = _customer_shield_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_other)
                db.session.commit()
                if new_other:
                    if return_obj:
                        return jsonify(fetch_one(new_other.uid)), 200

                    return jsonify({"info": "Other report added successfully!"}), 200

        return jsonify({"error": "Other report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(other_id, other_data=None, return_obj=False):

    try:
        data = json.loads(other_data)
        if data:

            if valid_uuid(other_id):

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                _farm_action_id = None
                if valid_uuid(data['farm_action_id']):
                    _farm_action_id = farm.actions.fetch_one(data['farm_action_id'], True).get_json()['real_id']

                _customer_shield_id = None
                if valid_uuid(data['customer_shield_id']):
                    _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

                _other = OtherReport\
                    .query\
                    .filter(OtherReport.uid == str(other_id))\
                    .first()
                if _other:
                    _other.notes = _notes
                    _other.farm_action_id = _farm_action_id
                    _other.customer_shield_id = _customer_shield_id
                    db.session.commit()

                    return jsonify({"info": "Other report edited successfully!"}), 200

        return jsonify({"error": "Other report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(other_id):

    try:

        if valid_uuid(other_id) :
            _other = OtherReport\
                .query\
                .filter(OtherReport.uid == other_id)\
                .first()
            if _other:
                _other.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Other report deactivated successfully!"}), 200

        return jsonify({"error": "Other report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
