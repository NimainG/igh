import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import WastePollutionManagement

from app.core import user
from app.core import media
from app.core import customer
from app.core import report

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def wpm_obj(wpm=False, real_id=False):
    _wpm = {}
    if wpm:
        if real_id:
            _wpm["real_id"] = wpm.id

        _wpm["id"] = str(wpm.uid)
        _wpm["residue"] = wpm.residue
        _wpm["source"] = wpm.source
        _wpm["eliminated_amount"] = wpm.eliminated_amount
        _wpm["reduced_amount"] = wpm.reduced_amount
        _wpm["recycled_amount"] = wpm.recycled_amount
        _wpm["customer_shield_id"] = customer.shields.fetch_by_id(wpm.customer_shield_id).get_json()['uid']
        _wpm["report_id"] = report.fetch_by_id(wpm.report_id).get_json()['uid']
        _wpm["customer_id"] = customer.fetch_by_id(wpm.customer_id).get_json()['uid']
        _wpm["created_by"] = creator_detail(wpm.creator_id)
        _wpm["status"] = status_name(wpm.status)
        _wpm["created"] = wpm.created_at
        _wpm["modified"] = wpm.modified_at

    return _wpm

def fetch_all():
    response = []

    wpms = db\
        .session\
        .query(WastePollutionManagement)\
        .filter(WastePollutionManagement.status > config.STATUS_DELETED)\
        .all()

    for wpm in wpms:
        response.append(wpm_obj(wpm))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        wpm = db\
            .session\
            .query(WastePollutionManagement)\
            .filter(WastePollutionManagement.uid == uid)\
            .filter(WastePollutionManagement.status > config.STATUS_DELETED)\
            .first()
        if wpm:
            response = wpm_obj(wpm, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        wpm = db\
            .session\
            .query(WastePollutionManagement)\
            .filter(WastePollutionManagement.id == id)\
            .filter(WastePollutionManagement.status > config.STATUS_DELETED)\
            .first()
        if wpm:
            response = wpm_obj(wpm)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        wpms = db\
            .session\
            .query(WastePollutionManagement)\
            .filter(WastePollutionManagement.customer_shield_id == customer_shield_id)\
            .filter(WastePollutionManagement.status > config.STATUS_DELETED)\
            .all()
        for wpm in wpms:
            response.append(wpm_obj(wpm))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        wpms = db\
            .session\
            .query(WastePollutionManagement)\
            .filter(WastePollutionManagement.report_id == report_id)\
            .filter(WastePollutionManagement.status > config.STATUS_DELETED)\
            .all()
        for wpm in wpms:
            response.append(wpm_obj(wpm))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        wpms = db\
            .session\
            .query(WastePollutionManagement)\
            .filter(WastePollutionManagement.customer_id == customer_id)\
            .filter(WastePollutionManagement.status > config.STATUS_DELETED)\
            .all()
        for wpm in wpms:
            response.append(wpm_obj(wpm))

    return jsonify(response)

def add_new(wpm_data=None, return_obj=False):

    try:
        data = json.loads(wpm_data)
        if data:

            _residue = None
            if 'residue' in data:
                _residue = data['residue']

            _source = None
            if 'source' in data:
                _source = data['source']

            _eliminated_amount = None
            if 'eliminated_amount' in data:
                _eliminated_amount = data['eliminated_amount']

            _reduced_amount = None
            if 'reduced_amount' in data:
                _reduced_amount = data['reduced_amount']

            _recycled_amount = None
            if 'recycled_amount' in data:
                _recycled_amount = data['recycled_amount']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _residue and report_id and _customer_id and _creator_id:
                new_wpm = WastePollutionManagement(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    residue = _residue,
                    source = _source,
                    eliminated_amount = _eliminated_amount,
                    reduced_amount = _reduced_amount,
                    recycled_amount = _recycled_amount,
                    customer_shield_id = _customer_shield_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_wpm)
                db.session.commit()
                if new_wpm:
                    if return_obj:
                        return jsonify(fetch_one(new_wpm.uid)), 200

                    return jsonify({"info": "Waste pollution management added successfully!"}), 200

        return jsonify({"error": "Waste pollution management not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(wpm_id, wpm_data=None, return_obj=False):

    try:
        data = json.loads(wpm_data)
        if data:

            if valid_uuid(wpm_id):

                _residue = None
                if 'residue' in data:
                    _residue = data['residue']

                _source = None
                if 'source' in data:
                    _source = data['source']

                _eliminated_amount = None
                if 'eliminated_amount' in data:
                    _eliminated_amount = data['eliminated_amount']

                _reduced_amount = None
                if 'reduced_amount' in data:
                    _reduced_amount = data['reduced_amount']

                _recycled_amount = None
                if 'recycled_amount' in data:
                    _recycled_amount = data['recycled_amount']

                if _residue:
                    _wpm = WastePollutionManagement\
                        .query\
                        .filter(WastePollutionManagement.uid == str(wpm_id))\
                        .first()
                    if _wpm:
                        _wpm.residue = _residue
                        _wpm.source = _source
                        _wpm.eliminated_amount = _eliminated_amount
                        _wpm.reduced_amount = _reduced_amount
                        _wpm.recycled_amount = _recycled_amount
                        db.session.commit()

                        return jsonify({"info": "Waste pollution management edited successfully!"}), 200

        return jsonify({"error": "Waste pollution management not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(wpm_id):

    try:

        if valid_uuid(wpm_id) :
            _wpm = WastePollutionManagement\
                .query\
                .filter(WastePollutionManagement.uid == wpm_id)\
                .first()
            if _wpm:
                _wpm.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Waste pollution management deactivated successfully!"}), 200

        return jsonify({"error": "Waste pollution management not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
