import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Scouting

from app.core import user
from app.core import media
from app.core import customer
from app.core import report
from app.core import planting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def scouting_obj(scouting=False, real_id=False):
    _scouting = {}
    if scouting:
        if real_id:
            _scouting["real_id"] = scouting.id

        _scouting["id"] = str(scouting.uid)
        _scouting["leaf_miners"] = scouting.leaf_miners
        _scouting["ball_worms"] = scouting.ball_worms
        _scouting["thrips"] = scouting.thrips
        _scouting["caterpillars"] = scouting.caterpillars
        _scouting["mites"] = scouting.mites
        _scouting["scales"] = scouting.scales
        _scouting["bean_flies"] = scouting.bean_flies
        _scouting["white_flies"] = scouting.white_flies
        _scouting["cut_worms"] = scouting.cut_worms
        _scouting["other_insects"] = scouting.other_insects
        _scouting["aschochtya"] = scouting.aschochtya
        _scouting["botrytis"] = scouting.botrytis
        _scouting["downy"] = scouting.downy
        _scouting["powdery"] = scouting.powdery
        _scouting["leaf_spots"] = scouting.leaf_spots
        _scouting["halo_blight"] = scouting.halo_blight
        _scouting["other_diseases"] = scouting.other_diseases
        _scouting["weeds"] = scouting.weeds
        _scouting["customer_shield_id"] = customer.shields.fetch_by_id(scouting.customer_shield_id).get_json()['uid']
        _scouting["report_id"] = report.fetch_by_id(scouting.report_id).get_json()['uid']
        _scouting["customer_id"] = customer.fetch_by_id(scouting.customer_id).get_json()['uid']
        _scouting["created_by"] = creator_detail(scouting.creator_id)
        _scouting["status"] = status_name(scouting.status)
        _scouting["created"] = scouting.created_at
        _scouting["modified"] = scouting.modified_at

    return _scouting

def fetch_all():
    response = []

    scoutings = db\
        .session\
        .query(Scouting)\
        .filter(Scouting.status > config.STATUS_DELETED)\
        .all()

    for scouting in scoutings:
        response.append(scouting_obj(scouting))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        scouting = db\
            .session\
            .query(Scouting)\
            .filter(Scouting.uid == uid)\
            .filter(Scouting.status > config.STATUS_DELETED)\
            .first()
        if scouting:
            response = scouting_obj(scouting, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        scouting = db\
            .session\
            .query(Scouting)\
            .filter(Scouting.id == id)\
            .filter(Scouting.status > config.STATUS_DELETED)\
            .first()
        if scouting:
            response = scouting_obj(scouting)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=0):
    response = []
    if customer_shield_id:
        scoutings = db\
            .session\
            .query(Scouting)\
            .filter(Scouting.customer_shield_id == customer_shield_id)\
            .filter(Scouting.status > config.STATUS_DELETED)\
            .all()
        for scouting in scoutings:
            response.append(scouting_obj(scouting))

    return jsonify(response)

def fetch_by_report(report_id=0):
    response = []
    if report_id:
        scoutings = db\
            .session\
            .query(Scouting)\
            .filter(Scouting.report_id == report_id)\
            .filter(Scouting.status > config.STATUS_DELETED)\
            .all()
        for scouting in scoutings:
            response.append(scouting_obj(scouting))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = []
    if customer_id:
        scoutings = db\
            .session\
            .query(Scouting)\
            .filter(Scouting.customer_id == customer_id)\
            .filter(Scouting.status > config.STATUS_DELETED)\
            .all()
        for scouting in scoutings:
            response.append(scouting_obj(scouting))

    return jsonify(response)

def add_new(scouting_data=None, return_obj=False):

    try:
        data = json.loads(scouting_data)
        if data:

            _leaf_miners = None
            if 'leaf_miners' in data:
                _leaf_miners = data['leaf_miners']

            _ball_worms = None
            if 'ball_worms' in data:
                _ball_worms = data['ball_worms']

            _thrips = None
            if 'thrips' in data:
                _thrips = data['thrips']

            _caterpillars = None
            if 'caterpillars' in data:
                _caterpillars = data['caterpillars']

            _mites = None
            if 'mites' in data:
                _mites = data['mites']

            _scales = None
            if 'scales' in data:
                _scales = data['scales']

            _bean_flies = None
            if 'bean_flies' in data:
                _bean_flies = data['bean_flies']

            _white_flies = None
            if 'white_flies' in data:
                _white_flies = data['white_flies']

            _cut_worms = None
            if 'cut_worms' in data:
                _cut_worms = data['cut_worms']

            _other_insects = None
            if 'other_insects' in data:
                _other_insects = data['other_insects']

            _aschochtya = None
            if 'aschochtya' in data:
                _aschochtya = data['aschochtya']

            _botrytis = None
            if 'botrytis' in data:
                _botrytis = data['botrytis']

            _downy = None
            if 'downy' in data:
                _downy = data['downy']

            _powdery = None
            if 'powdery' in data:
                _powdery = data['powdery']

            _leaf_spots = None
            if 'leaf_spots' in data:
                _leaf_spots = data['leaf_spots']

            _halo_blight = None
            if 'halo_blight' in data:
                _halo_blight = data['halo_blight']

            _other_diseases = None
            if 'other_diseases' in data:
                _other_diseases = data['other_diseases']

            _weeds = None
            if 'weeds' in data:
                _weeds = data['weeds']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _report_id = None
            if valid_uuid(data['report_id']):
                _report_id = report.fetch_one(data['report_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _report_id and _customer_id and _creator_id:
                new_scouting = Scouting(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    leaf_miners = _leaf_miners,
                    ball_worms = _ball_worms,
                    thrips = _thrips,
                    caterpillars = _caterpillars,
                    mites = _mites,
                    scales = _scales,
                    bean_flies = _bean_flies,
                    white_flies = _white_flies,
                    cut_worms = _cut_worms,
                    other_insects = _other_insects,
                    aschochtya = _aschochtya,
                    botrytis = _botrytis,
                    downy = _downy,
                    powdery = _powdery,
                    leaf_spots = _leaf_spots,
                    halo_blight = _halo_blight,
                    other_diseases = _other_diseases,
                    weeds = _weeds,
                    customer_shield_id = _customer_shield_id,
                    report_id = _report_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_scouting)
                db.session.commit()
                if new_scouting:
                    if return_obj:
                        return jsonify(fetch_one(new_scouting.uid)), 200

                    return jsonify({"info": "Scouting report added successfully!"}), 200

        return jsonify({"error": "Scouting report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(scouting_id, scouting_data=None, return_obj=False):

    try:
        data = json.loads(scouting_data)
        if data:

            if valid_uuid(scouting_id):

                _leaf_miners = None
                if 'leaf_miners' in data:
                    _leaf_miners = data['leaf_miners']

                _ball_worms = None
                if 'ball_worms' in data:
                    _ball_worms = data['ball_worms']

                _thrips = None
                if 'thrips' in data:
                    _thrips = data['thrips']

                _caterpillars = None
                if 'caterpillars' in data:
                    _caterpillars = data['caterpillars']

                _mites = None
                if 'mites' in data:
                    _mites = data['mites']

                _scales = None
                if 'scales' in data:
                    _scales = data['scales']

                _bean_flies = None
                if 'bean_flies' in data:
                    _bean_flies = data['bean_flies']

                _white_flies = None
                if 'white_flies' in data:
                    _white_flies = data['white_flies']

                _cut_worms = None
                if 'cut_worms' in data:
                    _cut_worms = data['cut_worms']

                _other_insects = None
                if 'other_insects' in data:
                    _other_insects = data['other_insects']

                _aschochtya = None
                if 'aschochtya' in data:
                    _aschochtya = data['aschochtya']

                _botrytis = None
                if 'botrytis' in data:
                    _botrytis = data['botrytis']

                _downy = None
                if 'downy' in data:
                    _downy = data['downy']

                _powdery = None
                if 'powdery' in data:
                    _powdery = data['powdery']

                _leaf_spots = None
                if 'leaf_spots' in data:
                    _leaf_spots = data['leaf_spots']

                _halo_blight = None
                if 'halo_blight' in data:
                    _halo_blight = data['halo_blight']

                _other_diseases = None
                if 'other_diseases' in data:
                    _other_diseases = data['other_diseases']

                _weeds = None
                if 'weeds' in data:
                    _weeds = data['weeds']

                _scouting = Scouting\
                    .query\
                    .filter(Scouting.uid == str(scouting_id))\
                    .first()
                if _scouting:
                    _scouting.leaf_miners = _leaf_miners
                    _scouting.ball_worms = _ball_worms
                    _scouting.thrips = _thrips
                    _scouting.caterpillars = _caterpillars
                    _scouting.mites = _mites
                    _scouting.scales = _scales
                    _scouting.bean_flies = _bean_flies
                    _scouting.white_flies = _white_flies
                    _scouting.cut_worms = _cut_worms
                    _scouting.other_insects = _other_insects
                    _scouting.aschochtya = _aschochtya
                    _scouting.botrytis = _botrytis
                    _scouting.downy = _downy
                    _scouting.powdery = _powdery
                    _scouting.leaf_spots = _leaf_spots
                    _scouting.halo_blight = _halo_blight
                    _scouting.other_diseases = _other_diseases
                    _scouting.weeds = _weeds
                    db.session.commit()

                    return jsonify({"info": "Scouting report edited successfully!"}), 200

        return jsonify({"error": "Scouting report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(scouting_id):

    try:

        if valid_uuid(scouting_id) :
            _scouting = Scouting\
                .query\
                .filter(Scouting.uid == scouting_id)\
                .first()
            if _scouting:
                _scouting.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Scouting report deactivated successfully!"}), 200

        return jsonify({"error": "Scouting report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
