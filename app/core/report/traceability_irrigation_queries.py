import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import TraceabilityIrrigationQuery

from app.core import user

from app.core.report import traceability

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def traceability_iq_obj(traceability_iq=False, real_id=False):
    _traceability_iq = {}
    if traceability_iq:
        if real_id:
            _traceability_iq["real_id"] = traceability_iq.id

        _traceability_iq["id"] = str(traceability_iq.uid)
        _traceability_iq["monthly_irrigation"] = traceability_iq.monthly_irrigation
        _traceability_iq["traceability_report_id"] = traceability.fetch_by_id(traceability_iq.traceability_report_id).get_json()['uid']
        _traceability_iq["created_by"] = creator_detail(traceability_iq.creator_id)
        _traceability_iq["status"] = status_name(traceability_iq.status)
        _traceability_iq["created"] = traceability_iq.created_at
        _traceability_iq["modified"] = traceability_iq.modified_at

    return _traceability_iq

def fetch_all():
    response = []

    traceabilities = db\
        .session\
        .query(TraceabilityIrrigationQuery)\
        .filter(TraceabilityIrrigationQuery.status > config.STATUS_DELETED)\
        .all()

    for traceability_iq in traceabilities:
        response.append(traceability_iq_obj(traceability_iq))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        traceability_iq = db\
            .session\
            .query(TraceabilityIrrigationQuery)\
            .filter(TraceabilityIrrigationQuery.uid == uid)\
            .filter(TraceabilityIrrigationQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_iq:
            response = traceability_iq_obj(traceability_iq, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        traceability_iq = db\
            .session\
            .query(TraceabilityIrrigationQuery)\
            .filter(TraceabilityIrrigationQuery.id == id)\
            .filter(TraceabilityIrrigationQuery.status > config.STATUS_DELETED)\
            .first()
        if traceability_iq:
            response = traceability_iq_obj(traceability_iq)

    return jsonify(response)

def fetch_by_traceability_report(traceability_report_id=0):
    response = []
    if traceability_report_id:
        traceabilities = db\
            .session\
            .query(TraceabilityIrrigationQuery)\
            .filter(TraceabilityIrrigationQuery.traceability_report_id == traceability_report_id)\
            .filter(TraceabilityIrrigationQuery.status > config.STATUS_DELETED)\
            .all()
        for traceability_iq in traceabilities:
            response.append(traceability_iq_obj(traceability_iq))

    return jsonify(response)

def add_new(traceability_iq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_iq_data)
        if data:

            _monthly_irrigation = None
            if 'monthly_irrigation' in data:
                _monthly_irrigation = data['monthly_irrigation']

            _traceability_report_id = None
            if valid_uuid(data['traceability_report_id']):
                _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _traceability_report_id and _creator_id:
                new_traceability_iq = TraceabilityIrrigationQuery(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    monthly_irrigation = _monthly_irrigation,
                    traceability_report_id = _traceability_report_id,
                    creator_id = _creator_id
                )
                db.session.add(new_traceability_iq)
                db.session.commit()
                if new_traceability_iq:
                    if return_obj:
                        return jsonify(fetch_one(new_traceability_iq.uid)), 200

                    return jsonify({"info": "Traceability irrigation queries report added successfully!"}), 200

        return jsonify({"error": "Traceability irrigation queries report not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(traceability_iq_id, traceability_iq_data=None, return_obj=False):

    try:
        data = json.loads(traceability_iq_data)
        if data:

            if valid_uuid(traceability_iq_id):

                _monthly_irrigation = None
                if 'monthly_irrigation' in data:
                    _monthly_irrigation = data['monthly_irrigation']

                _traceability_report_id = None
                if valid_uuid(data['traceability_report_id']):
                    _traceability_report_id = traceability.fetch_one(data['traceability_report_id'], True).get_json()['real_id']

                if _traceability_report_id:
                    _traceability_iq = TraceabilityIrrigationQuery\
                        .query\
                        .filter(TraceabilityIrrigationQuery.uid == str(traceability_iq_id))\
                        .first()
                    if _traceability_iq:
                        _traceability_iq.monthly_irrigation = _monthly_irrigation
                        _traceability_iq.traceability_report_id = _traceability_report_id
                        db.session.commit()

                        return jsonify({"info": "Traceability irrigation queries report edited successfully!"}), 200

        return jsonify({"error": "Traceability irrigation queries report not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(traceability_iq_id):

    try:

        if valid_uuid(traceability_iq_id) :
            _traceability_iq = TraceabilityIrrigationQuery\
                .query\
                .filter(TraceabilityIrrigationQuery.uid == traceability_iq_id)\
                .first()
            if _traceability_iq:
                _traceability_iq.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability irrigation queries report deactivated successfully!"}), 200

        return jsonify({"error": "Traceability irrigation queries report not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
