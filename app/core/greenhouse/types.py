import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import GreenhouseType

from app.core import user
from app.core import media

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_type_obj(greenhouse_type=False, real_id=False):
    _greenhouse_type = {}
    if greenhouse_type:
        if real_id:
            _greenhouse_type["real_id"] = greenhouse_type.id

        _greenhouse_type["id"] = str(greenhouse_type.uid)
        _greenhouse_type["name"] = greenhouse_type.name
        _greenhouse_type["size"] = greenhouse_type.size
        _greenhouse_type["description"] = greenhouse_type.description
        _greenhouse_type["info_link"] = greenhouse_type.info_link
        _greenhouse_type["photo"] = media.fetch_by_id(greenhouse_type.photo_media_id).get_json()
        _greenhouse_type["created_by"] = creator_detail(greenhouse_type.creator_id)
        _greenhouse_type["status"] = status_name(greenhouse_type.status)
        _greenhouse_type["created"] = greenhouse_type.created_at.strftime('%d %B %Y')
        _greenhouse_type["modified"] = greenhouse_type.modified_at

    return _greenhouse_type

def fetch_all():
    response = []

    greenhouse_types = db\
        .session\
        .query(GreenhouseType)\
        .filter(GreenhouseType.status > config.STATUS_DELETED)\
        .all()

    for greenhouse_type in greenhouse_types:
        response.append(greenhouse_type_obj(greenhouse_type))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse_type = db\
            .session\
            .query(GreenhouseType)\
            .filter(GreenhouseType.uid == uid)\
            .filter(GreenhouseType.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_type:
            response = greenhouse_type_obj(greenhouse_type, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse_type = db\
            .session\
            .query(GreenhouseType)\
            .filter(GreenhouseType.id == id)\
            .filter(GreenhouseType.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_type:
            response = greenhouse_type_obj(greenhouse_type)

    return jsonify(response)

def add_new(greenhouse_type_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_type_data)
        if data:

            if 'name' in data and 'creator_id' in data:

                _name = data['name']
                _size = data['size'] if 'size' in data else None
                _description = data['description'] if 'description' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _photo_media = None
                _creator = None

                if valid_uuid(data['photo_media_id']):
                    _photo_media = media.fetch_one(data['photo_media_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name  and _creator:
                    new_greenhouse_type = GreenhouseType(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        size = _size,
                        description = _description,
                        info_link = _info_link,
                        photo_media_id = _photo_media,
                        creator_id = _creator
                    )
                    db.session.add(new_greenhouse_type)
                    db.session.commit()
                    if new_greenhouse_type:
                        if return_obj:
                            return jsonify(fetch_one(new_greenhouse_type.uid)), 200

                        return jsonify({"info": "Greenhouse type added successfully!"}), 200

        return jsonify({"error": "Greenhouse type not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(greenhouse_type_id, greenhouse_type_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_type_data)
        if data:

            if valid_uuid(greenhouse_type_id) and 'name' in data:

                _name = data['name']
                _size = data['size'] if 'size' in data else None
                _description = data['description'] if 'description' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _photo_media = None

                if valid_uuid(data['cover_photo']):
                    _photo_media = media.fetch_one(data['cover_photo'], True).get_json()['real_id']

                if _name:
                    _greenhouse_type = GreenhouseType\
                        .query\
                        .filter(GreenhouseType.uid == str(greenhouse_type_id))\
                        .first()
                    if _greenhouse_type:
                        _greenhouse_type.name = _name
                        _greenhouse_type.size = _size
                        _greenhouse_type.description = _description
                        _greenhouse_type.info_link = _info_link
                        _greenhouse_type.photo_media = _photo_media
                        db.session.commit()

                        return jsonify({"info": "Greenhouse type edited successfully!"}), 200

        return jsonify({"error": "Greenhouse type not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(greenhouse_type_id):

    try:

        if valid_uuid(greenhouse_type_id) :
            _greenhouse_type = GreenhouseType\
                .query\
                .filter(GreenhouseType.uid == greenhouse_type_id)\
                .first()
            if _greenhouse_type:
                _greenhouse_type.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Greenhouse type deactivated successfully!"}), 200

        return jsonify({"error": "Greenhouse type not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
