import email
import os
# import smtplib
import json
from re import DEBUG
from app import *


from flask import jsonify,Flask
from email.utils import parseaddr
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from flask_mail import Mail,Message
from app import mail







from app.config import activateConfig


config = activateConfig(os.getenv('ENV') or 'production')



#mail= Mail()
#mail.init_app(app)


def send_mail(emailTo,subject,message):
    try:
        
        if '@' in parseaddr(emailTo)[1] and subject != "" and message != "":
            # body = """ \
            #             <!DOCTYPE html>
            #             <html>
            #                 <head>
            #                     <meta charset="utf-8">
            #                     <meta http-equiv="X-UA-Compatible" content="IE=edge">
            #                     <title>""" + config.COMPANY_NAME + """</title>
            #                     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            #                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/css/bootstrap.min.css'>
            #                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/AdminLTE.min.css'>
            #                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/skins/_all-skins.min.css'>
            #                 </head>
            #                 <body class="hold-transition skin-purple sidebar-mini">
            #                     <section class="content">""" +  + """</section>
            #                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/plugins/jQuery/jQuery-2.1.4.min.js'></script>
            #                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/js/bootstrap.min.js'></script>
            #                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/js/app.min.js'></script>
            #                 </body>
            #             </html>
            #         """
            msg = Message(subject,sender = 'nimain@sinsignal.com',recipients=[emailTo])
            print('Error with receipant',subject,emailTo)
            msg.html = message
            mail.send(msg)
            
            
            return jsonify({"info": "Mail sent successfully!"}), 200

        return jsonify({"error": "Failed: Invalid properties"}), 405

    except Exception as e:
        return jsonify({"error": "Failed: " + str(e)}), 406

def send_user_invite(props=None):
    
    try:
        _props = json.loads(props)
        if 'to' in _props \
            and 'subject' in _props \
            and 'invite' in _props \
            and 'names' in _props['invite'] \
            and 'code' in _props['invite'] \
            and 'org' in _props['invite']:

            # email_from = config.SMTP_USERNAME
            # if 'from' in _props and _props['from'] != "":
            #     email_from = _props['from']

            message = "<p>Dear " + _props['invite']['names'] + ",</p> \
                <p>You have been invited to join " + _props['invite']['code'] + " on " + config.APP_NAME + ".  \
                Use the following link to accept the invitation.</p> \
                <p><a href='" + config.HTTP_SCHEME + "://" + config.APP_DOMAIN + "/invite/" + _props['invite']['code'] + "' target='mails'>" + config.HTTP_SCHEME + "://" + config.APP_DOMAIN + "/accept/" + _props['invite']['code'] + "</a></p> \
                <p>If the link does not respond, you can copy and paste it in your browser to access the activation page.</p> \
                <p>Regards<br /><br />" + config.CO_TEAM + "</p> \
                <p><small><strong>Powered by " + config.SIGN_OFF + "</strong></small></p>"

           
            print('Message sent sucessfully')
            # return jsonify({"info": "Mail sent successfully!"}), 200
            return send_mail(_props['to'],_props['subject'],message)    
            #return send_mail(email_from, _props['to'], _props['subject'], message)
           
    except Exception as e:
        return jsonify({"error": "Failed: " + str(e)}), 406

def send_user_activation(props=None):

    try:
        _props = json.loads(props)
        if 'to' in _props \
            and 'subject' in _props \
            and 'activation' in _props \
            and 'names' in _props['activation'] \
            and 'code' in _props['activation']:

            # email_from = config.SMTP_USERNAME
            # if 'from' in _props and _props['from'] != "":
            #     email_from = _props['from']

            message = "<p>Dear " + _props['activation']['names'] + ",</p> \
                <p>An account has been created for you in the " + config.APP_NAME + ".</p> \
                <p>Use the following link to activate your account by choosing a password.</p> \
                <p><a href='" + config.HTTP_SCHEME + "://" + config.APP_DOMAIN + "/activate/" + _props['activation']['code'] + "' target='mails'>" + config.HTTP_SCHEME + "://" + config.APP_DOMAIN + "/activate/" + _props['activation']['code'] + "</a></p> \
                <p>If the link does not respond, you can copy and paste it in your browser to access the activation page.</p> \
                <p>Regards<br /><br />" + config.CO_TEAM + "</p> \
                <p><small><strong>Powered by " + config.SIGN_OFF + "</strong></small></p>"

            #return send_mail(email_from, _props['to'],_props['subject'], message)
            return send_mail(_props['to'],_props['subject'],message)

    except Exception as e:
        return jsonify({"error": "Failed: " + str(e)}), 406

def password_reset_request(props=None):

    try:
        _props = json.loads(props)
        if 'to' in _props \
            and 'subject' in _props \
            and 'reset' in _props \
            and 'names' in _props['reset'] \
            and 'code' in _props['reset']:

            # email_from = config.SMTP_USERNAME
            # if 'from' in _props and _props['from'] != "":
            #     email_from = _props['from']

            message = "<p>Dear " + _props['reset']['names'] + ",</p> \
                <p>A password request has been received for your account. If you did not send it, please ignore this email. If you did send the request, use the code below to reset your password from the app.</p> \
                <p>Reset code: <code><strong>" + _props['reset']['code'] + "</strong></code></p>\
                <p>PS: The reset code can only be used once and within 48 hours.</p>\
                <p>Regards<br /><br />" + config.CO_TEAM + "</p> \
                <p><small><strong>Powered by " + config.SIGN_OFF + "</strong></small></p>"

           # return send_mail(email_from, _props['to'], _props['subject'], message)
            return send_mail(_props['to'],_props['subject'],message)

    except Exception as e:
        return jsonify({"error": "Failed: " + str(e)}), 406
