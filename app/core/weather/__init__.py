import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import WeatherUpdate

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


def weather_obj(weather=False, real_id=False):
    _weather = {}
    if weather:
        if real_id:
            _weather["real_id"] = weather.id

        _weather["id"] = str(weather.uid)
        _weather["city"] = weather.city
        _weather["narration"] = weather.narration
        _weather["temp"] = weather.temp
        _weather["icon_url"] = weather.icon_url
        _weather["status"] = status_name(weather.status)
        _weather["created"] = weather.created_at
        _weather["modified"] = weather.modified_at

    return _weather

def fetch_all():
    response = []

    weathers = db\
        .session\
        .query(WeatherUpdate)\
        .filter(WeatherUpdate.status > config.STATUS_DELETED)\
        .order_by(WeatherUpdate.created_at.desc())\
        .all()

    for weather in weathers:
        response.append(weather_obj(weather))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        weather = db\
            .session\
            .query(WeatherUpdate)\
            .filter(WeatherUpdate.uid == uid)\
            .filter(WeatherUpdate.status > config.STATUS_DELETED)\
            .first()
        if weather:
            response = weather_obj(weather, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        weather = db\
            .session\
            .query(WeatherUpdate)\
            .filter(WeatherUpdate.id == id)\
            .filter(WeatherUpdate.status > config.STATUS_DELETED)\
            .first()
        if weather:
            response = weather_obj(weather)

    return jsonify(response)

def fetch_by_city(city=None):
    response = []

    weathers = db\
        .session\
        .query(WeatherUpdate)\
        .filter(WeatherUpdate.city == city)\
        .filter(WeatherUpdate.status > config.STATUS_DELETED)\
        .order_by(WeatherUpdate.created_at.desc())\
        .all()
    for weather in weathers:
        response.append(weather_obj(weather))

    return jsonify(response)

def add_new(weather_data=None, return_obj=False):

    try:
        data = json.loads(weather_data)
        if data:

            _city = None
            if 'city' in data:
                _city = data['city']

            _narration = None
            if 'narration' in data:
                _narration = data['narration']

            _temp = None
            if 'temp' in data:
                _temp = data['temp']

            _icon_url = None
            if 'icon_url' in data:
                _icon_url = data['icon_url']

            if _city and _narration:
                new_weather = WeatherUpdate(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    city = _city,
                    narration = _narration,
                    temp = _temp,
                    icon_url = _icon_url
                )
                db.session.add(new_weather)
                db.session.commit()
                if new_weather:
                    if return_obj:
                        return jsonify(fetch_one(new_weather.uid)), 200

                    return jsonify({"info": "Weather update added successfully!"}), 200

        return jsonify({"error": "Weather update not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
