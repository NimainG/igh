import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import InvoicePayment

from app.core import user
from app.core import invoice as payment_status_name

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def get_next_payment_serial():
    _serial = config.PAYMENT_SERIAL_START

    _latest_serial = db\
        .session\
        .query(InvoicePayment)\
        .filter(InvoicePayment.status > config.STATUS_DELETED)\
        .order_by(InvoicePayment.serial.desc())\
        .first()
    if _latest_serial:
        _serial = _latest_serial.serial + 1

    return _serial

def payment_obj(payment=False, real_id=False):
    _payment = {}
    if payment:
        if real_id:
            _payment["real_id"] = payment.id

        _payment["id"] = str(payment.uid)
        _payment["serial"] = payment.serial
        _payment["amount"] = payment.amount
        _payment["narrative"] = payment.narrative
        _payment["payment_status"] = payment_status_name(payment.payment_status)
        _payment["created_by"] = creator_detail(payment.creator_id)
        _payment["status"] = status_name(payment.status)
        _payment["created"] = payment.created_at
        _payment["modified"] = payment.modified_at

    return _payment

def fetch_all():
    response = []

    payments = db\
        .session\
        .query(InvoicePayment)\
        .filter(InvoicePayment.status > config.STATUS_DELETED)\
        .all()

    for payment in payments:
        response.append(payment_obj(payment))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        payment = db\
            .session\
            .query(InvoicePayment)\
            .filter(InvoicePayment.uid == uid)\
            .filter(InvoicePayment.status > config.STATUS_DELETED)\
            .first()
        if payment:
            response = payment_obj(payment, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        payment = db\
            .session\
            .query(InvoicePayment)\
            .filter(InvoicePayment.id == id)\
            .filter(InvoicePayment.status > config.STATUS_DELETED)\
            .first()
        if payment:
            response = payment_obj(payment)

    return jsonify(response)

def fetch_by_invoice_id(invoice_id=0):
    response = []
    if invoice_id:
        payments = db\
            .session\
            .query(InvoicePayment)\
            .filter(InvoicePayment.invoice_id == invoice_id)\
            .filter(InvoicePayment.status > config.STATUS_DELETED)\
            .all()
        for payment in payments:
            response.append(payment_obj(payment))

    return jsonify(response)

def add_new(payment_data=None, return_obj=False):

    try:
        data = json.loads(payment_data)
        if data:

            if 'amount' in data and 'narrative' in data \
                and 'invoice_id' in data and 'creator_id' in data:

                _amount = data['amount']
                _narrative = data['narrative']
                _invoice_id = data['invoice_id']
                _payment_status = data['payment_status'] if 'payment_status' in data else config.PAYMENT_STATUS_PENDING

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _amount and _narrative and _invoice_id and _creator:
                    new_payment = InvoicePayment(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        serial = get_next_payment_serial(),
                        amount = _amount,
                        narrative = _narrative,
                        payment_status = _payment_status,
                        invoice_id = _invoice_id,
                        creator_id = _creator
                    )
                    db.session.add(new_payment)
                    db.session.commit()
                    if new_payment:
                        if return_obj:
                            return jsonify(fetch_one(new_payment.uid)), 200

                        return jsonify({"info": "Payment added successfully!"}), 200

        return jsonify({"error": "Payment not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(payment_id, payment_data=None, return_obj=False):

    try:
        data = json.loads(payment_data)
        if data:

            if valid_uuid(payment_id) and 'amount' in data and 'narrative' in data:

                _amount = data['amount']
                _narrative = data['narrative']
                _payment_status = data['payment_status'] if 'payment_status' in data else config.PAYMENT_STATUS_PENDING

                if _amount and _narrative:
                    _payment = InvoicePayment\
                        .query\
                        .filter(InvoicePayment.uid == payment_id)\
                        .first()
                    if _payment:
                        _payment.amount = _amount
                        _payment.narrative = _narrative
                        _payment.payment_status = _payment_status
                        db.session.commit()

                        return jsonify({"info": "Payment edited successfully!"}), 200

        return jsonify({"error": "Payment not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
