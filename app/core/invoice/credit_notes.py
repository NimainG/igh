import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CreditNote

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def get_next_credit_note_serial():
    _serial = config.CREDIT_NOTE_SERIAL_START

    _latest_serial = db\
        .session\
        .query(CreditNote)\
        .filter(CreditNote.status > config.STATUS_DELETED)\
        .order_by(CreditNote.serial.desc())\
        .first()
    if _latest_serial:
        _serial = _latest_serial.serial + 1

    return _serial

def credit_note_obj(credit_note=False, real_id=False):
    _credit_note = {}
    if credit_note:
        if real_id:
            _credit_note["real_id"] = credit_note.id

        _credit_note["id"] = str(credit_note.uid)
        _credit_note["serial"] = credit_note.serial
        _credit_note["amount"] = credit_note.amount
        _credit_note["narrative"] = credit_note.narrative
        _credit_note["created_by"] = creator_detail(credit_note.creator_id)
        _credit_note["status"] = status_name(credit_note.status)
        _credit_note["created"] = credit_note.created_at
        _credit_note["modified"] = credit_note.modified_at

    return _credit_note

def fetch_all():
    response = []

    credit_notes = db\
        .session\
        .query(CreditNote)\
        .filter(CreditNote.status > config.STATUS_DELETED)\
        .all()

    for credit_note in credit_notes:
        response.append(credit_note_obj(credit_note))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        credit_note = db\
            .session\
            .query(CreditNote)\
            .filter(CreditNote.uid == uid)\
            .filter(CreditNote.status > config.STATUS_DELETED)\
            .first()
        if credit_note:
            response = credit_note_obj(credit_note, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        credit_note = db\
            .session\
            .query(CreditNote)\
            .filter(CreditNote.id == id)\
            .filter(CreditNote.status > config.STATUS_DELETED)\
            .first()
        if credit_note:
            response = credit_note_obj(credit_note)

    return jsonify(response)

def fetch_by_serial(serial=0):
    response = {}
    if serial:
        credit_note = db\
            .session\
            .query(CreditNote)\
            .filter(CreditNote.serial == serial)\
            .filter(CreditNote.status > config.STATUS_DELETED)\
            .first()
        if credit_note:
            response = credit_note_obj(credit_note)

    return jsonify(response)

def fetch_by_invoice_id(invoice_id=0):
    response = {}
    if invoice_id:
        credit_note = db\
            .session\
            .query(CreditNote)\
            .filter(CreditNote.invoice_id == invoice_id)\
            .filter(CreditNote.status > config.STATUS_DELETED)\
            .first()
        if credit_note:
            response = credit_note_obj(credit_note)

    return jsonify(response)

def add_new(credit_note_data=None, return_obj=False):

    try:
        data = json.loads(credit_note_data)
        if data:

            if 'amount' in data and 'narrative' in data \
                and 'invoice_id' in data and 'creator_id' in data:

                _amount = data['amount']
                _narrative = data['narrative']
                _invoice_id = data['invoice_id']

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _amount and _narrative and _invoice_id and _creator:
                    new_credit_note = CreditNote(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        serial = get_next_credit_note_serial(),
                        amount = _amount,
                        narrative = _narrative,
                        invoice_id = _invoice_id,
                        creator_id = _creator
                    )
                    db.session.add(new_credit_note)
                    db.session.commit()
                    if new_credit_note:
                        if return_obj:
                            return jsonify(fetch_one(new_credit_note.uid)), 200

                        return jsonify({"info": "Credit note added successfully!"}), 200

        return jsonify({"error": "Credit note not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(credit_note_id, credit_note_data=None, return_obj=False):

    try:
        data = json.loads(credit_note_data)
        if data:

            if valid_uuid(credit_note_id) and 'amount' in data and 'narrative' in data:

                _amount = data['amount']
                _narrative = data['narrative']

                if _amount and _narrative:
                    _credit_note = CreditNote\
                        .query\
                        .filter(CreditNote.uid == credit_note_id)\
                        .first()
                    if _credit_note:
                        _credit_note.amount = _amount
                        _credit_note.narrative = _narrative
                        db.session.commit()

                        return jsonify({"info": "Credit note edited successfully!"}), 200

        return jsonify({"error": "Credit note not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(credit_note_id):

    try:

        if valid_uuid(credit_note_id) :
            _credit_note = CreditNote\
                .query\
                .filter(CreditNote.uid == credit_note_id)\
                .first()
            if _credit_note:
                _credit_note.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Credit note deactivated successfully!"}), 200

        return jsonify({"error": "Credit note not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
