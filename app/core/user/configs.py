# fetch_config_user - uuid
# fetch_customer_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import UserConfig

from app.core import user
from app.core import alert

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def temp_metric_name(temp_metric=None):
    _temp_metric_name = ""

    if temp_metric == config.METRIC_TEMP_C:
        _temp_metric_name = "C"

    if temp_metric == config.METRIC_TEMP_F:
        _temp_metric_name = "F"

    return(_temp_metric_name)

def liquid_metric_name(liquid_metric=None):
    _liquid_metric_name = ""

    if liquid_metric == config.METRIC_LIQUID_L:
        _liquid_metric_name = "L"

    if liquid_metric == config.METRIC_LIQUID_GAL:
        _liquid_metric_name = "GAL"

    return(_liquid_metric_name)

def length_metric_name(length_metric=None):
    _length_metric_name = ""

    if length_metric == config.METRIC_LENGTH_F:
        _length_metric_name = "F"

    if length_metric == config.METRIC_LENGTH_M:
        _length_metric_name = "M"

    return(_length_metric_name)

def config_obj(user_config=False, real_id=False):
    _config = {}
    if user_config:
        if real_id:
            _config["real_id"] = user_config.id

        _config["id"] = str(user_config.uid)
        _config["user"] = user.fetch_by_id(user_config.user_id).get_json()
        _config["temp_metric"] = temp_metric_name(user_config.temp_metric)
        _config["liquid_metric"] = liquid_metric_name(user_config.liquid_metric)
        _config["length_metric"] = length_metric_name(user_config.length_metric)
        _config["created_by"] = creator_detail(user_config.creator_id)
        _config["status"] = status_name(user_config.status)
        _config["created"] = user_config.created_at
        _config["modified"] = user_config.modified_at

    return _config

def fetch_all():
    response = []

    user_configs = db\
        .session\
        .query(UserConfig)\
        .filter(UserConfig.status > config.STATUS_DELETED)\
        .all()

    for user_config in user_configs:
        response.append(config_obj(user_config))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        user_config = db\
            .session\
            .query(UserConfig)\
            .filter(UserConfig.uid == uid)\
            .filter(UserConfig.status > config.STATUS_DELETED)\
            .first()
        if user_config:
            response = config_obj(user_config, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        user_config = db\
            .session\
            .query(UserConfig)\
            .filter(UserConfig.id == id)\
            .filter(UserConfig.status > config.STATUS_DELETED)\
            .first()
        if user_config:
            response = config_obj(user_config)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        user_config = db\
            .session\
            .query(UserConfig)\
            .filter(UserConfig.user_id == user_id)\
            .filter(UserConfig.status > config.STATUS_DELETED)\
            .first()
        if user_config:
            response = config_obj(user_config)

    return jsonify(response)

def add_new(config_data=None, return_obj=False):

    try:
        data = json.loads(config_data)
        if data:

            _temp_metric = None
            if 'temp_metric' in data:
                _temp_metric = data['temp_metric']

            _liquid_metric = None
            if 'liquid_metric' in data:
                _liquid_metric = data['liquid_metric']

            _length_metric = None
            if 'length_metric' in data:
                _length_metric = data['length_metric']

            if _temp_metric and _liquid_metric and _length_metric and data['user_id'] \
                and data['creator_id']:
                new_config = UserConfig(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    temp_metric = _temp_metric,
                    liquid_metric = _liquid_metric,
                    length_metric = _length_metric,
                    user_id = data['user_id'],
                    creator_id = data['creator_id']
                )
                db.session.add(new_config)
                db.session.commit()
                if new_config:
                    if return_obj:
                        return jsonify(fetch_one(new_config.uid)), 200

                    return jsonify({"info": "Config added successfully!"}), 200

        return jsonify({"error": "Config not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(config_id, config_data=None, return_obj=False):

    try:
        data = json.loads(config_data)
        if data:

            if valid_uuid(config_id):

                _config = UserConfig\
                    .query\
                    .filter(UserConfig.uid == config_id)\
                    .first()
                if _config:

                    if 'temp_metric' in data:
                        _config.temp_metric = data['temp_metric']

                    if 'liquid_metric' in data:
                        _config.liquid_metric = data['liquid_metric']

                    if 'length_metric' in data:
                        _config.length_metric = data['length_metric']

                    db.session.commit()

                    return jsonify({"info": "Config edited successfully!"}), 200

        return jsonify({"error": "Config not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(config_id):

    try:

        if valid_uuid(config_id) :
            _config = UserConfig\
                .query\
                .filter(UserConfig.uid == config_id)\
                .first()
            if _config:
                _config.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Config deactivated successfully!"}), 200

        return jsonify({"error": "Config not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
