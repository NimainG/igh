# fetch_customer_user_user - uuid
# fetch_customer_user_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import CustomerUser

from app.core import user
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def customer_user_obj(customer_user=False, real_id=False):
    _customer_user = {}
    if customer_user:
        if real_id:
            _customer_user["real_id"] = customer_user.id

        _customer_user["id"] = str(customer_user.uid)
        # _customer_user["id_number"] = customer_user.id_number
        _customer_user["employee_id"] = customer_user.employee_id
        _customer_user["customer"] = customer.fetch_by_id(customer_user.customer_id).get_json()
        _customer_user["user"] = user.fetch_by_id(customer_user.user_id).get_json()
        _customer_user["created_by"] = creator_detail(customer_user.creator_id)
        _customer_user["status"] = status_name(customer_user.status)
        _customer_user["created"] = customer_user.created_at
        _customer_user["modified"] = customer_user.modified_at
    print('customer',_customer_user)
    return _customer_user
    

def fetch_all():
    response = []

    customer_users = db\
        .session\
        .query(CustomerUser)\
        .filter(CustomerUser.status > config.STATUS_DELETED)\
        .all()

    for customer_user in customer_users:
        response.append(customer_user_obj(customer_user))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer_user = db\
            .session\
            .query(CustomerUser)\
            .filter(CustomerUser.uid == uid)\
            .filter(CustomerUser.status > config.STATUS_DELETED)\
            .first()
        if customer_user:
            response = customer_user_obj(customer_user, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer_user = db\
            .session\
            .query(CustomerUser)\
            .filter(CustomerUser.id == id)\
            .filter(CustomerUser.status > config.STATUS_DELETED)\
            .first()
        if customer_user:
            response = customer_user_obj(customer_user)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        customer_user = db\
            .session\
            .query(CustomerUser)\
            .filter(CustomerUser.user_id == user_id)\
            .filter(CustomerUser.status > config.STATUS_DELETED)\
            .first()
        if customer_user:
            response = customer_user_obj(customer_user)

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = {}
    if customer_id:
        customer_user = db\
            .session\
            .query(CustomerUser)\
            .filter(CustomerUser.customer_id == customer_id)\
            .filter(CustomerUser.status > config.STATUS_DELETED)\
            .first()
        if customer_user:
            response = customer_user_obj(customer_user)

    return jsonify(response)

def add_new(customer_user_data=None, return_obj=False):

    try:
        data = json.loads(customer_user_data)
        if data:

            # _id_number = None
            # if 'id_number' in data:
            #     _id_number = data['id_number']

            _employee_id = None
            if 'employee_id' in data:
                _employee_id = data['employee_id']

            # _customer_id = None
            # if valid_uuid(data['customer_id']):
            #     _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']
            _customer_id = None
            if 'customer_id' in data:
                _customer_id = data['customer_id']
            _user_id = None
            if valid_uuid(data['user_id']):
                _user_id = user.fetch_one(data['user_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _user_id and _creator:
                new_customer_user = CustomerUser(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                  #  id_number = _id_number,
                    employee_id = _employee_id,
                    customer_id = _customer_id,
                    user_id = _user_id,
                    creator_id = _creator
                )
                db.session.add(new_customer_user)
                db.session.commit()
                if new_customer_user:
                    if return_obj:
                        return jsonify(fetch_one(new_customer_user.uid)), 200

                    return jsonify({"info": "Customer user added successfully!"}), 200

        return jsonify({"error": "Customer user not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(customer_user_id, customer_user_data=None, return_obj=False):

    try:
        data = json.loads(customer_user_data)
        if data:

            if valid_uuid(customer_user_id):

                _id_number = None
                if 'id_number' in data:
                    _id_number = data['id_number']

                _employee_id = None
                if 'employee_id' in data:
                    _employee_id = data['employee_id']

                _customer_user = CustomerUser\
                    .query\
                    .filter(CustomerUser.uid == customer_user_id)\
                    .first()
                if _customer_user:
                    _customer_user.id_number = _id_number
                    _customer_user.employee_id = _employee_id
                    db.session.commit()

                    return jsonify({"info": "Customer user edited successfully!"}), 200

        return jsonify({"error": "Customer user not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_user_id):

    try:

        if valid_uuid(customer_user_id) :
            _customer_user = CustomerUser\
                .query\
                .filter(CustomerUser.uid == customer_user_id)\
                .first()
            if _customer_user:
                _customer_user.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer user deactivated successfully!"}), 200

        return jsonify({"error": "Customer user not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
