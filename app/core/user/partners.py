# fetch_partner_user_user - uuid
# fetch_partner_user_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import PartnerUser

from app.core import user
from app.core import partner

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def partner_user_obj(partner_user=False, real_id=False):
    _partner_user = {}
    if partner_user:
        if real_id:
            _partner_user["real_id"] = partner_user.id

        _partner_user["id"] = str(partner_user.uid)
        _partner_user["id_number"] = partner_user.id_number
        _partner_user["employee_id"] = partner_user.employee_id
        _partner_user["partner"] = partner.fetch_by_id(partner_user.partner_id).get_json()
        _partner_user["user"] = user.fetch_by_id(partner_user.user_id).get_json()
        _partner_user["created_by"] = creator_detail(partner_user.creator_id)
        _partner_user["status"] = status_name(partner_user.status)
        _partner_user["created"] = partner_user.created_at
        _partner_user["modified"] = partner_user.modified_at

    return _partner_user

def fetch_all():
    response = []

    partner_users = db\
        .session\
        .query(PartnerUser)\
        .filter(PartnerUser.status > config.STATUS_DELETED)\
        .all()

    for partner_user in partner_users:
        response.append(partner_user_obj(partner_user))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        partner_user = db\
            .session\
            .query(PartnerUser)\
            .filter(PartnerUser.uid == uid)\
            .filter(PartnerUser.status > config.STATUS_DELETED)\
            .first()
        if partner_user:
            response = partner_user_obj(partner_user, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        partner_user = db\
            .session\
            .query(PartnerUser)\
            .filter(PartnerUser.id == id)\
            .filter(PartnerUser.status > config.STATUS_DELETED)\
            .first()
        if partner_user:
            response = partner_user_obj(partner_user)

    return jsonify(response)

def fetch_by_user(user_id=0):
    response = {}
    if user_id:
        partner_user = db\
            .session\
            .query(PartnerUser)\
            .filter(PartnerUser.user_id == user_id)\
            .filter(PartnerUser.status > config.STATUS_DELETED)\
            .first()
        if partner_user:
            response = partner_user_obj(partner_user)

    return jsonify(response)

def fetch_by_partner(partner_id=0):
    response = {}
    if partner_id:
        partner_user = db\
            .session\
            .query(PartnerUser)\
            .filter(PartnerUser.partner_id == partner_id)\
            .filter(PartnerUser.status > config.STATUS_DELETED)\
            .first()
        if partner_user:
            response = partner_user_obj(partner_user)

    return jsonify(response)

def add_new(partner_user_data=None, return_obj=False):

    try:
        data = json.loads(partner_user_data)
        if data:

            _id_number = None
            if 'id_number' in data:
                _id_number = data['id_number']

            _employee_id = None
            if 'employee_id' in data:
                _employee_id = data['employee_id']

            _partner_id = None
            if valid_uuid(data['partner_id']):
                _partner_id = partner.fetch_one(data['partner_id'], True).get_json()['real_id']

            _user_id = None
            if valid_uuid(data['user_id']):
                _user_id = user.fetch_one(data['user_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _user_id and _partner_id and _creator:
                new_partner_user = PartnerUser(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    id_number = _id_number,
                    employee_id = _employee_id,
                    partner_id = _partner_id,
                    user_id = _user_id,
                    creator_id = _creator
                )
                db.session.add(new_partner_user)
                db.session.commit()
                if new_partner_user:
                    if return_obj:
                        return jsonify(fetch_one(new_partner_user.uid)), 200

                    return jsonify({"info": "Partner user added successfully!"}), 200

        return jsonify({"error": "Partner user not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(partner_user_id, partner_user_data=None, return_obj=False):

    try:
        data = json.loads(partner_user_data)
        if data:

            if valid_uuid(partner_user_id):

                _id_number = None
                if 'id_number' in data:
                    _id_number = data['id_number']

                _employee_id = None
                if 'employee_id' in data:
                    _employee_id = data['employee_id']

                _partner_user = PartnerUser\
                    .query\
                    .filter(PartnerUser.uid == partner_user_id)\
                    .first()
                if _partner_user:
                    _partner_user.id_number = _id_number
                    _partner_user.employee_id = _employee_id
                    db.session.commit()

                    return jsonify({"info": "Partner user edited successfully!"}), 200

        return jsonify({"error": "Partner user not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(partner_user_id):

    try:

        if valid_uuid(partner_user_id) :
            _partner_user = PartnerUser\
                .query\
                .filter(PartnerUser.uid == partner_user_id)\
                .first()
            if _partner_user:
                _partner_user.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Partner user deactivated successfully!"}), 200

        return jsonify({"error": "Partner user not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
