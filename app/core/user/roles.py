import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Role

from app.core import user
from app.core import customer
from app.core import partner

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def role_obj(role=False, real_id=False):
    _role = {}
    if role:
        initials = role.name.split()

        if real_id:
            _role["real_id"] = role.id

        _role["id"] = str(role.uid)
        _role["name"] = role.name
        _role["initials"] = "".join([initial[0] for initial in initials])
        _role["description"] = role.description
        _role["realm"] = user.realms.realm_name(role.user_realm)
        _role["super_admin"] = role.super_admin
        _role["customer"] = customer.fetch_by_id(role.customer_id).get_json()
        _role["partner"] = partner.fetch_by_id(role.partner_id).get_json()
        _role["created_by"] = creator_detail(role.creator_id)
        _role["status"] = status_name(role.status)
        _role["created"] = role.created_at
        _role["modified"] = role.modified_at

    return _role

def fetch_all():
    response = []

    roles = db\
        .session\
        .query(Role)\
        .filter(Role.status > config.STATUS_DELETED)\
        .all()

    for role in roles:
        response.append(role_obj(role))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        role = db\
            .session\
            .query(Role)\
            .filter(Role.uid == uid)\
            .filter(Role.status > config.STATUS_DELETED)\
            .first()
        if role:
            response = role_obj(role, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        role = db\
            .session\
            .query(Role)\
            .filter(Role.id == id)\
            .filter(Role.status > config.STATUS_DELETED)\
            .first()
        if role:
            response = role_obj(role)

    return jsonify(response)

def fetch_by_name(name=None, real_id=False):
    response = {}
    if name:
        role = db\
            .session\
            .query(Role)\
            .filter(Role.name == name)\
            .filter(Role.status > config.STATUS_DELETED)\
            .first()
        if role:
            response = role_obj(role, real_id)

    return jsonify(response)

def fetch_by_user_realm(user_realm=0):
    response = []
    if user_realm:
        roles = db\
            .session\
            .query(Role)\
            .filter(Role.user_realm == user_realm)\
            .filter(Role.status > config.STATUS_DELETED)\
            .all()
        for role in roles:
            response.append(role_obj(role))

    return jsonify(response)

def fetch_by_name_realm(name=None, user_realm=0):
    response = []
    if name and user_realm:
        roles = db\
            .session\
            .query(Role)\
            .filter(Role.name == name)\
            .filter(Role.user_realm == user_realm)\
            .filter(Role.status > config.STATUS_DELETED)\
            .all()
        for role in roles:
            response.append(role_obj(role))

    return jsonify(response)

def fetch_by_customer(customer_id=0):
    response = {}
    if customer_id:
        role = db\
            .session\
            .query(Role)\
            .filter(Role.customer_id == customer_id)\
            .filter(Role.status > config.STATUS_DELETED)\
            .first()
        if role:
            response = role_obj(role)

    return jsonify(response)

def fetch_by_partner(partner_id=0):
    response = {}
    if partner_id:
        role = db\
            .session\
            .query(Role)\
            .filter(Role.partner_id == partner_id)\
            .filter(Role.status > config.STATUS_DELETED)\
            .first()
        if role:
            response = role_obj(role)

    return jsonify(response)

def add_new(role_data=None, return_obj=False, real_id=False):

    try:
        data = json.loads(role_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _description = None
            if 'description' in data:
                _description = data['description']

            _user_realm = None
            if 'user_realm' in data:
                _user_realm = data['user_realm']

            _super_admin = None
            if 'super_admin' in data:
                _super_admin = data['super_admin']

            _customer_id = None
            if 'customer_id' in data and valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _partner_id = None
            if 'partner_id' in data and valid_uuid(data['partner_id']):
                _partner_id = partner.fetch_one(data['partner_id'], True).get_json()['real_id']

            _creator = None
            if 'creator_id' in data and valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _user_realm and len(fetch_by_name_realm(_name, _user_realm).get_json()) == 0:
                new_role = Role(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    description = _description,
                    user_realm = _user_realm,
                    super_admin = _super_admin,
                    customer_id = _customer_id,
                    partner_id = _partner_id,
                    creator_id = _creator
                )
                db.session.add(new_role)
                db.session.commit()
                if new_role:
                    role_id = new_role.uid
                    if return_obj:
                        return fetch_one(role_id, real_id), 200

                    return jsonify({"info": "Role added successfully!"}), 200

        return jsonify({"error": "Role not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(role_id, role_data=None, return_obj=False):

    try:
        data = json.loads(role_data)
        if data:

            if valid_uuid(role_id):
                _role = Role\
                    .query\
                    .filter(Role.uid == role_id)\
                    .first()

                if _role:
                    if 'name' in data:
                        _role.name = data['name']

                    if 'description' in data:
                        _role.description = data['description']

                    if 'user_realm' in data:
                        _role.user_realm = data['user_realm']

                    if 'super_admin' in data:
                        _role.super_admin = data['super_admin']

                    db.session.commit()

                    return jsonify({"info": "Role edited successfully!"}), 200

        return jsonify({"error": "Role not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(role_id):

    try:

        if valid_uuid(role_id):
            _role = Role\
                .query\
                .filter(Role.uid == role_id)\
                .first()
            if _role:
                _role.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Role activated successfully!"}), 200

        return jsonify({"error": "Role not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(role_id):

    try:

        if valid_uuid(role_id) :
            _role = Role\
                .query\
                .filter(Role.uid == role_id)\
                .first()
            if _role:
                _role.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Role deactivated successfully!"}), 200

        return jsonify({"error": "Role not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
