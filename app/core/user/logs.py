# fetch_log_user - uuid
# fetch_customer_user - uuid
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import UserLog

from app.core import user
from app.core import customer
from app.core import partner

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def log_obj(log=False, real_id=False):
    _log = {}
    if log:
        if real_id:
            _log["real_id"] = log.id

        _log["id"] = str(log.uid)
        _log["action"] = log.action
        _log["ref"] = log.ref
        _log["ref_id"] = log.ref_id
        _log["customer_id"] = customer.fetch_by_id(log.customer_id).get_json()['id']
        _log["partner_id"] = partner.fetch_by_id(log.partner_id).get_json()['id']
        _log["created_by"] = creator_detail(log.creator_id)
        _log["status"] = status_name(log.status)
        _log["created"] = log.created_at
        _log["modified"] = log.modified_at

    return _log

def fetch_all():
    response = []

    logs = db\
        .session\
        .query(UserLog)\
        .filter(UserLog.status > config.STATUS_DELETED)\
        .order_by(UserLog.created_at.desc())\
        .all()

    for log in logs:
        response.append(log_obj(log))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        log = db\
            .session\
            .query(UserLog)\
            .filter(UserLog.uid == uid)\
            .filter(UserLog.status > config.STATUS_DELETED)\
            .first()
        if log:
            response = log_obj(log, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        log = db\
            .session\
            .query(UserLog)\
            .filter(UserLog.id == id)\
            .filter(UserLog.status > config.STATUS_DELETED)\
            .first()
        if log:
            response = log_obj(log)

    return jsonify(response)

def fetch_by_ref_id(ref_id=None):
    response = []
    if ref_id:
        logs = db\
            .session\
            .query(UserLog)\
            .filter(UserLog.ref_id == ref_id)\
            .filter(UserLog.status > config.STATUS_DELETED)\
            .order_by(UserLog.created_at.desc())\
            .all()
        for log in logs:
            response.append(log_obj(log))

    return jsonify(response)

def fetch_by_customer(customer_id=None):
    response = []
    if customer_id:
        logs = db\
            .session\
            .query(UserLog)\
            .filter(UserLog.customer_id == customer_id)\
            .filter(UserLog.status > config.STATUS_DELETED)\
            .order_by(UserLog.created_at.desc())\
            .all()
        for log in logs:
            response.append(log_obj(log))

    return jsonify(response)

def fetch_by_partner(partner_id=None):
    response = []
    if partner_id:
        logs = db\
            .session\
            .query(UserLog)\
            .filter(UserLog.partner_id == partner_id)\
            .filter(UserLog.status > config.STATUS_DELETED)\
            .order_by(UserLog.created_at.desc())\
            .all()
        for log in logs:
            response.append(log_obj(log))

    return jsonify(response)

def add_new(log_data=None, return_obj=False):

    try:
        data = json.loads(log_data)
        if data:

            _action = None
            if 'action' in data:
                _action = data['action']

            _ref = None
            if 'ref' in data:
                _ref = data['ref']

            _ref_id = None
            if 'ref_id' in data:
                _ref_id = data['ref_id']

            _customer_id = None
            if 'customer_id' in data and valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _partner_id = None
            if 'partner_id' in data and valid_uuid(data['partner_id']):
                _partner_id = partner.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if 'creator_id' in data and valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _action and ref and ref_id and _creator_id:
                new_log = UserLog(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    action = _action,
                    ref = _ref,
                    ref_id = _ref_id,
                    customer_id = _customer_id,
                    partner_id = _partner_id,
                    creator_id = _creator_id
                )
                db.session.add(new_log)
                db.session.commit()
                if new_log:
                    if return_obj:
                        return jsonify(fetch_one(new_log.uid)), 200

                    return jsonify({"info": "User log added successfully!"}), 200

        return jsonify({"error": "User log not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(log_id):

    try:

        if valid_uuid(log_id) :
            _log = UserLog\
                .query\
                .filter(UserLog.uid == log_id)\
                .first()
            if _log:
                _log.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "User log deactivated successfully!"}), 200

        return jsonify({"error": "User log not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
