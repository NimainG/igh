import os
import boto3

from botocore.exceptions import ClientError
from werkzeug.utils import secure_filename

from dotenv import load_dotenv

from app.config import activateConfig

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'staging')

def uploadFile(file, location, prefName=None):
    filename = None

    if file:
        filename = secure_filename(file.filename)
        if prefName is not None:
            filenameParts = filename.split(".")
            filename = prefName + "." + filenameParts[len(filenameParts)-1]

        if not os.path.exists(config.UPLOAD_FOLDER + location):
            os.makedirs(config.UPLOAD_FOLDER + location)

        file_path = os.path.join(config.UPLOAD_FOLDER + location, filename)

        file.save(file_path)

    return filename
