import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ProductionStage

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def production_stage_obj(production_stage=False, real_id=False):
    _production_stage = {}
    if production_stage:
        if real_id:
            _production_stage["real_id"] = production_stage.id

        _production_stage["id"] = str(production_stage.uid)
        _production_stage["name"] = production_stage.name
        _production_stage["description"] = production_stage.description
        _production_stage["created_by"] = creator_detail(production_stage.creator_id)
        _production_stage["status"] = status_name(production_stage.status)
        _production_stage["created"] = production_stage.created_at
        _production_stage["modified"] = production_stage.modified_at

    return _production_stage

def fetch_all():
    response = []

    production_stages = db\
        .session\
        .query(ProductionStage)\
        .filter(ProductionStage.status > config.STATUS_DELETED)\
        .all()

    for production_stage in production_stages:
        response.append(production_stage_obj(production_stage))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        production_stage = db\
            .session\
            .query(ProductionStage)\
            .filter(ProductionStage.uid == uid)\
            .filter(ProductionStage.status > config.STATUS_DELETED)\
            .first()
        if production_stage:
            response = production_stage_obj(production_stage, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        production_stage = db\
            .session\
            .query(ProductionStage)\
            .filter(ProductionStage.id == id)\
            .filter(ProductionStage.status > config.STATUS_DELETED)\
            .first()
        if production_stage:
            response = production_stage_obj(production_stage)

    return jsonify(response)

def add_new(production_stage_data=None, return_obj=False):

    try:
        data = json.loads(production_stage_data)
        if data:

            if 'name' in data and 'creator_id' in data:

                _name = data['name']
                _description = data['description'] if 'description' in data else None

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name and _creator:
                    new_production_stage = ProductionStage(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        description = _description,
                        creator_id = _creator
                    )
                    db.session.add(new_production_stage)
                    db.session.commit()
                    if new_production_stage:
                        if return_obj:
                            return jsonify(fetch_one(new_production_stage.uid)), 200

                        return jsonify({"info": "Production stage added successfully!"}), 200

        return jsonify({"error": "Production stage not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(production_stage_id, production_stage_data=None, return_obj=False):

    try:
        data = json.loads(production_stage_data)
        if data:

            if valid_uuid(production_stage_id) and  'name' in data:

                _name = data['name']
                _description = data['description'] if 'description' in data else None

                if _name:
                    _production_stage = ProductionStage\
                        .query\
                        .filter(ProductionStage.uid == production_stage_id)\
                        .first()
                    if _production_stage:
                        _production_stage.name = _name
                        _production_stage.description = _description
                        db.session.commit()

                        return jsonify({"info": "Production stage edited successfully!"}), 200

        return jsonify({"error": "Production stage not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(production_stage_id):

    try:

        if valid_uuid(production_stage_id) :
            _production_stage = ProductionStage\
                .query\
                .filter(ProductionStage.uid == production_stage_id)\
                .first()
            if _production_stage:
                _production_stage.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Production stage deactivated successfully!"}), 200

        return jsonify({"error": "Production stage not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
