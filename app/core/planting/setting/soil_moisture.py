import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import SoilMoistureSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def soil_moisture_obj(soil_moisture=False, real_id=False):
    _soil_moisture = {}
    if soil_moisture:
        if real_id:
            _soil_moisture["real_id"] = soil_moisture.id

        _soil_moisture["id"] = str(soil_moisture.uid)
        _soil_moisture["min"] = soil_moisture.min
        _soil_moisture["max"] = soil_moisture.max
        _soil_moisture["shield_setting_id"] = setting.fetch_by_id(soil_moisture.shield_planting_setting_id).get_json()['uid']
        _soil_moisture["created_by"] = creator_detail(soil_moisture.creator_id)
        _soil_moisture["status"] = status_name(soil_moisture.status)
        _soil_moisture["created"] = soil_moisture.created_at
        _soil_moisture["modified"] = soil_moisture.modified_at

    return _module

def fetch_all():
    response = []

    soil_moistures = db\
        .session\
        .query(SoilMoistureSetting)\
        .filter(SoilMoistureSetting.status > config.STATUS_DELETED)\
        .all()

    for soil_moisture in soil_moistures:
        response.append(soil_moisture_obj(soil_moisture))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        soil_moisture = db\
            .session\
            .query(SoilMoistureSetting)\
            .filter(SoilMoistureSetting.uid == uid)\
            .filter(SoilMoistureSetting.status > config.STATUS_DELETED)\
            .first()
        if soil_moisture:
            response = soil_moisture_obj(soil_moisture, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        soil_moisture = db\
            .session\
            .query(SoilMoistureSetting)\
            .filter(SoilMoistureSetting.id == id)\
            .filter(SoilMoistureSetting.status > config.STATUS_DELETED)\
            .first()
        if soil_moisture:
            response = soil_moisture_obj(soil_moisture)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        soil_moistures = db\
            .session\
            .query(SoilMoistureSetting)\
            .filter(SoilMoistureSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(SoilMoistureSetting.status > config.STATUS_DELETED)\
            .all()
        for soil_moisture in soil_moistures:
            response.append(soil_moisture_obj(soil_moisture))

    return jsonify(response)

def add_new(soil_moisture_data=None, return_obj=False):

    try:
        data = json.loads(soil_moisture_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_soil_moisture = SoilMoistureSetting(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_soil_moisture)
                db.session.commit()
                if new_soil_moisture:
                    if return_obj:
                        return jsonify(fetch_one(new_soil_moisture.uid)), 200

                    return jsonify({"info": "Soil moisture setting added successfully!"}), 200

        return jsonify({"error": "Soil moisture setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(soil_moisture_id, soil_moisture_data=None, return_obj=False):

    try:
        data = json.loads(soil_moisture_data)
        if data:
            if valid_uuid(soil_moisture_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _soil_moisture = SoilMoistureSetting\
                    .query\
                    .filter(SoilMoistureSetting.uid == soil_moisture_id)\
                    .first()
                if _soil_moisture:
                    _soil_moisture.min = _min
                    _soil_moisture.max = _max
                    db.session.commit()

                    return jsonify({"info": "Soil moisture setting edited successfully!"}), 200

        return jsonify({"error": "Soil moisture setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(soil_moisture_id):

    try:

        if valid_uuid(soil_moisture_id) :
            _soil_moisture = SoilMoistureSetting\
                .query\
                .filter(SoilMoistureSetting.uid == soil_moisture_id)\
                .first()
            if _soil_moisture:
                _soil_moisture.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Soil moisture setting deactivated successfully!"}), 200

        return jsonify({"error": "Soil moisture setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
