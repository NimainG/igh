import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import PotassiumSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def potassium_obj(potassium=False, real_id=False):
    _potassium = {}
    if potassium:
        if real_id:
            _potassium["real_id"] = potassium.id

        _potassium["id"] = str(potassium.uid)
        _potassium["min"] = potassium.min
        _potassium["max"] = potassium.max
        _potassium["shield_setting_id"] = setting.fetch_by_id(potassium.shield_planting_setting_id).get_json()['uid']
        _potassium["created_by"] = creator_detail(potassium.creator_id)
        _potassium["status"] = status_name(potassium.status)
        _potassium["created"] = potassium.created_at
        _potassium["modified"] = potassium.modified_at

    return _module

def fetch_all():
    response = []

    potassiums = db\
        .session\
        .query(PotassiumSetting)\
        .filter(PotassiumSetting.status > config.STATUS_DELETED)\
        .all()

    for potassium in potassiums:
        response.append(potassium_obj(potassium))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        potassium = db\
            .session\
            .query(PotassiumSetting)\
            .filter(PotassiumSetting.uid == uid)\
            .filter(PotassiumSetting.status > config.STATUS_DELETED)\
            .first()
        if potassium:
            response = potassium_obj(potassium, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        potassium = db\
            .session\
            .query(PotassiumSetting)\
            .filter(PotassiumSetting.id == id)\
            .filter(PotassiumSetting.status > config.STATUS_DELETED)\
            .first()
        if potassium:
            response = potassium_obj(potassium)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        potassiums = db\
            .session\
            .query(PotassiumSetting)\
            .filter(PotassiumSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(PotassiumSetting.status > config.STATUS_DELETED)\
            .all()
        for potassium in potassiums:
            response.append(potassium_obj(potassium))

    return jsonify(response)

def add_new(potassium_data=None, return_obj=False):

    try:
        data = json.loads(potassium_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_potassium = PotassiumSetting(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_potassium)
                db.session.commit()
                if new_potassium:
                    if return_obj:
                        return jsonify(fetch_one(new_potassium.uid)), 200

                    return jsonify({"info": "Potassium setting added successfully!"}), 200

        return jsonify({"error": "Potassium setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(potassium_id, potassium_data=None, return_obj=False):

    try:
        data = json.loads(potassium_data)
        if data:
            if valid_uuid(potassium_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _potassium = PotassiumSetting\
                    .query\
                    .filter(PotassiumSetting.uid == potassium_id)\
                    .first()
                if _potassium:
                    _potassium.min = _min
                    _potassium.max = _max
                    db.session.commit()

                    return jsonify({"info": "Potassium setting edited successfully!"}), 200

        return jsonify({"error": "Potassium setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(potassium_id):

    try:

        if valid_uuid(potassium_id) :
            _potassium = PotassiumSetting\
                .query\
                .filter(PotassiumSetting.uid == potassium_id)\
                .first()
            if _potassium:
                _potassium.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Potassium setting deactivated successfully!"}), 200

        return jsonify({"error": "Potassium setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
