import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Co2Setting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def co_2_obj(co_2=False, real_id=False):
    _co_2 = {}
    if co_2:
        if real_id:
            _co_2["real_id"] = co_2.id

        _co_2["id"] = str(co_2.uid)
        _co_2["min"] = co_2.min
        _co_2["max"] = co_2.max
        _co_2["shield_setting_id"] = setting.fetch_by_id(co_2.shield_planting_setting_id).get_json()['uid']
        _co_2["created_by"] = creator_detail(co_2.creator_id)
        _co_2["status"] = status_name(co_2.status)
        _co_2["created"] = co_2.created_at
        _co_2["modified"] = co_2.modified_at

    return _module

def fetch_all():
    response = []

    co_2s = db\
        .session\
        .query(Co2Setting)\
        .filter(Co2Setting.status > config.STATUS_DELETED)\
        .all()

    for co_2 in co_2s:
        response.append(co_2_obj(co_2))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        co_2 = db\
            .session\
            .query(Co2Setting)\
            .filter(Co2Setting.uid == uid)\
            .filter(Co2Setting.status > config.STATUS_DELETED)\
            .first()
        if co_2:
            response = co_2_obj(co_2, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        co_2 = db\
            .session\
            .query(Co2Setting)\
            .filter(Co2Setting.id == id)\
            .filter(Co2Setting.status > config.STATUS_DELETED)\
            .first()
        if co_2:
            response = co_2_obj(co_2)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        co_2s = db\
            .session\
            .query(Co2Setting)\
            .filter(Co2Setting.shield_planting_setting_id == shield_setting_id)\
            .filter(Co2Setting.status > config.STATUS_DELETED)\
            .all()
        for co_2 in co_2s:
            response.append(co_2_obj(co_2))

    return jsonify(response)

def add_new(co_2_data=None, return_obj=False):

    try:
        data = json.loads(co_2_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_co_2 = Co2Setting(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_co_2)
                db.session.commit()
                if new_co_2:
                    if return_obj:
                        return jsonify(fetch_one(new_co_2.uid)), 200

                    return jsonify({"info": "CO2 setting added successfully!"}), 200

        return jsonify({"error": "CO2 setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(co_2_id, co_2_data=None, return_obj=False):

    try:
        data = json.loads(co_2_data)
        if data:
            if valid_uuid(co_2_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _co_2 = Co2Setting\
                    .query\
                    .filter(Co2Setting.uid == co_2_id)\
                    .first()
                if _co_2:
                    _co_2.min = _min
                    _co_2.max = _max
                    db.session.commit()

                    return jsonify({"info": "CO2 setting edited successfully!"}), 200

        return jsonify({"error": "CO2 setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(co_2_id):

    try:

        if valid_uuid(co_2_id) :
            _co_2 = Co2Setting\
                .query\
                .filter(Co2Setting.uid == co_2_id)\
                .first()
            if _co_2:
                _co_2.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "CO2 setting deactivated successfully!"}), 200

        return jsonify({"error": "CO2 setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
