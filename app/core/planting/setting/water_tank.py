import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import WaterTankSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def water_tank_obj(water_tank=False, real_id=False):
    _water_tank = {}
    if water_tank:
        if real_id:
            _water_tank["real_id"] = water_tank.id

        _water_tank["id"] = str(water_tank.uid)
        _water_tank["capacity_litres"] = water_tank.capacity_litres
        _water_tank["irrigation_time_min"] = water_tank.irrigation_time_min
        _water_tank["irrigation_time_max"] = water_tank.irrigation_time_max
        _water_tank["irrigation_start_time"] = water_tank.irrigation_start_time
        _water_tank["irrigation_end_time"] = water_tank.irrigation_end_time
        _water_tank["irrigation_interval"] = water_tank.irrigation_interval
        _water_tank["shield_setting_id"] = setting.fetch_by_id(water_tank.shield_planting_setting_id).get_json()['uid']
        _water_tank["created_by"] = creator_detail(water_tank.creator_id)
        _water_tank["status"] = status_name(water_tank.status)
        _water_tank["created"] = water_tank.created_at
        _water_tank["modified"] = water_tank.modified_at

    return _module

def fetch_all():
    response = []

    water_tanks = db\
        .session\
        .query(WaterTankSetting)\
        .filter(WaterTankSetting.status > config.STATUS_DELETED)\
        .all()

    for water_tank in water_tanks:
        response.append(water_tank_obj(water_tank))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        water_tank = db\
            .session\
            .query(WaterTankSetting)\
            .filter(WaterTankSetting.uid == uid)\
            .filter(WaterTankSetting.status > config.STATUS_DELETED)\
            .first()
        if water_tank:
            response = water_tank_obj(water_tank, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        water_tank = db\
            .session\
            .query(WaterTankSetting)\
            .filter(WaterTankSetting.id == id)\
            .filter(WaterTankSetting.status > config.STATUS_DELETED)\
            .first()
        if water_tank:
            response = water_tank_obj(water_tank)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        water_tanks = db\
            .session\
            .query(WaterTankSetting)\
            .filter(WaterTankSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(WaterTankSetting.status > config.STATUS_DELETED)\
            .all()
        for water_tank in water_tanks:
            response.append(water_tank_obj(water_tank))

    return jsonify(response)

def add_new(water_tank_data=None, return_obj=False):

    try:
        data = json.loads(water_tank_data)
        if data:

            _capacity_litres = 0
            if 'capacity_litres' in data:
                _capacity_litres = data['capacity_litres']

            _irrigation_time_min = 0
            if 'irrigation_time_min' in data:
                _irrigation_time_min = data['irrigation_time_min']

            _irrigation_time_max = 0
            if 'irrigation_time_max' in data:
                _irrigation_time_max = data['irrigation_time_max']

            _irrigation_start_time = None
            if 'irrigation_start_time' in data:
                _irrigation_start_time = data['irrigation_start_time']

            _irrigation_end_time = None
            if 'irrigation_end_time' in data:
                _irrigation_end_time = data['irrigation_end_time']

            _irrigation_interval = None
            if 'irrigation_interval' in data:
                _irrigation_interval = data['irrigation_interval']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_water_tank = WaterTankSetting(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    capacity_litres = _capacity_litres,
                    irrigation_time_min = _irrigation_time_min,
                    irrigation_time_max = _irrigation_time_max,
                    irrigation_start_time = _irrigation_start_time,
                    irrigation_end_time = _irrigation_end_time,
                    irrigation_interval = _irrigation_interval,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_water_tank)
                db.session.commit()
                if new_water_tank:
                    if return_obj:
                        return jsonify(fetch_one(new_water_tank.uid)), 200

                    return jsonify({"info": "Water tank setting added successfully!"}), 200

        return jsonify({"error": "Water tank setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(water_tank_id, water_tank_data=None, return_obj=False):

    try:
        data = json.loads(water_tank_data)
        if data:
            if valid_uuid(water_tank_id):

                _capacity_litres = 0
                if 'capacity_litres' in data:
                    _capacity_litres = data['capacity_litres']

                _irrigation_time_min = 0
                if 'irrigation_time_min' in data:
                    _irrigation_time_min = data['irrigation_time_min']

                _irrigation_time_max = 0
                if 'irrigation_time_max' in data:
                    _irrigation_time_max = data['irrigation_time_max']

                _irrigation_start_time = None
                if 'irrigation_start_time' in data:
                    _irrigation_start_time = data['irrigation_start_time']

                _irrigation_end_time = None
                if 'irrigation_end_time' in data:
                    _irrigation_end_time = data['irrigation_end_time']

                _irrigation_interval = None
                if 'irrigation_interval' in data:
                    _irrigation_interval = data['irrigation_interval']

                _water_tank = WaterTankSetting\
                    .query\
                    .filter(WaterTankSetting.uid == water_tank_id)\
                    .first()
                if _water_tank:
                    _water_tank.capacity_litres = _capacity_litres
                    _water_tank.irrigation_time_min = _irrigation_time_min
                    _water_tank.irrigation_time_max = _irrigation_time_max
                    _water_tank.irrigation_start_time = _irrigation_start_time
                    _water_tank.irrigation_end_time = _irrigation_end_time
                    _water_tank.irrigation_interval = _irrigation_interval
                    db.session.commit()

                    return jsonify({"info": "Water tank setting edited successfully!"}), 200

        return jsonify({"error": "Water tank setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(water_tank_id):

    try:

        if valid_uuid(water_tank_id) :
            _water_tank = WaterTankSetting\
                .query\
                .filter(WaterTankSetting.uid == water_tank_id)\
                .first()
            if _water_tank:
                _water_tank.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Water tank setting deactivated successfully!"}), 200

        return jsonify({"error": "Water tank setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
