import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import PhosphorusSetting

from app.core import user
from app.core.planting import setting

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def phosphorus_obj(phosphorus=False, real_id=False):
    _phosphorus = {}
    if phosphorus:
        if real_id:
            _phosphorus["real_id"] = phosphorus.id

        _phosphorus["id"] = str(phosphorus.uid)
        _phosphorus["min"] = phosphorus.min
        _phosphorus["max"] = phosphorus.max
        _phosphorus["shield_setting_id"] = setting.fetch_by_id(phosphorus.shield_planting_setting_id).get_json()['uid']
        _phosphorus["created_by"] = creator_detail(phosphorus.creator_id)
        _phosphorus["status"] = status_name(phosphorus.status)
        _phosphorus["created"] = phosphorus.created_at
        _phosphorus["modified"] = phosphorus.modified_at

    return _module

def fetch_all():
    response = []

    phosphoruss = db\
        .session\
        .query(PhosphorusSetting)\
        .filter(PhosphorusSetting.status > config.STATUS_DELETED)\
        .all()

    for phosphorus in phosphoruss:
        response.append(phosphorus_obj(phosphorus))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        phosphorus = db\
            .session\
            .query(PhosphorusSetting)\
            .filter(PhosphorusSetting.uid == uid)\
            .filter(PhosphorusSetting.status > config.STATUS_DELETED)\
            .first()
        if phosphorus:
            response = phosphorus_obj(phosphorus, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        phosphorus = db\
            .session\
            .query(PhosphorusSetting)\
            .filter(PhosphorusSetting.id == id)\
            .filter(PhosphorusSetting.status > config.STATUS_DELETED)\
            .first()
        if phosphorus:
            response = phosphorus_obj(phosphorus)

    return jsonify(response)

def fetch_by_shield_setting(shield_setting_id=0):
    response = []
    if shield_setting_id:
        phosphoruss = db\
            .session\
            .query(PhosphorusSetting)\
            .filter(PhosphorusSetting.shield_planting_setting_id == shield_setting_id)\
            .filter(PhosphorusSetting.status > config.STATUS_DELETED)\
            .all()
        for phosphorus in phosphoruss:
            response.append(phosphorus_obj(phosphorus))

    return jsonify(response)

def add_new(phosphorus_data=None, return_obj=False):

    try:
        data = json.loads(phosphorus_data)
        if data:

            _min = 0
            if 'min' in data:
                _min = data['min']

            _max = 0
            if 'max' in data:
                _max = data['max']

            _shield_planting_setting_id = None
            if valid_uuid(data['shield_setting_id']):
                _shield_planting_setting_id = setting.fetch_one(data['shield_setting_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_planting_setting_id and _creator_id:
                new_phosphorus = PhosphorusSetting(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    min = _min,
                    max = _max,
                    shield_planting_setting_id = _shield_planting_setting_id,
                    creator_id = _creator_id
                )
                db.session.add(new_phosphorus)
                db.session.commit()
                if new_phosphorus:
                    if return_obj:
                        return jsonify(fetch_one(new_phosphorus.uid)), 200

                    return jsonify({"info": "Phosphorus setting added successfully!"}), 200

        return jsonify({"error": "Phosphorus setting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(phosphorus_id, phosphorus_data=None, return_obj=False):

    try:
        data = json.loads(phosphorus_data)
        if data:
            if valid_uuid(phosphorus_id):

                _min = 0
                if 'min' in data:
                    _min = data['min']

                _max = 0
                if 'max' in data:
                    _max = data['max']

                _phosphorus = PhosphorusSetting\
                    .query\
                    .filter(PhosphorusSetting.uid == phosphorus_id)\
                    .first()
                if _phosphorus:
                    _phosphorus.min = _min
                    _phosphorus.max = _max
                    db.session.commit()

                    return jsonify({"info": "Phosphorus setting edited successfully!"}), 200

        return jsonify({"error": "Phosphorus setting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(phosphorus_id):

    try:

        if valid_uuid(phosphorus_id) :
            _phosphorus = PhosphorusSetting\
                .query\
                .filter(PhosphorusSetting.uid == phosphorus_id)\
                .first()
            if _phosphorus:
                _phosphorus.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Phosphorus setting deactivated successfully!"}), 200

        return jsonify({"error": "Phosphorus setting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
