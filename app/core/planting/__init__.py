import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Planting

from app.core import user
from app.core.customer import shields as customer_shields
from app.core.planting import planting_production_stages

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def planting_obj(planting=False, real_id=False):
    _planting = {}
    if planting:
        if real_id:
            _planting["real_id"] = planting.id

        _planting["id"] = str(planting.uid)
        _planting["customer_shield"] = customer_shields.fetch_by_id(planting.customer_shield_id).get_json()
        _planting["current_production_stage"] = planting_production_stages.fetch_by_id(planting.code).get_json()
        _planting["production_stages"] = planting_production_stages.fetch_by_planting_id(planting.id).get_json()
        _planting["created_by"] = creator_detail(planting.creator_id)
        _planting["status"] = status_name(planting.status)
        _planting["created"] = planting.created_at
        _planting["modified"] = planting.modified_at

    return _module

def fetch_all():
    response = []

    plantings = db\
        .session\
        .query(Planting)\
        .filter(Planting.status > config.STATUS_DELETED)\
        .all()

    for planting in plantings:
        response.append(planting_obj(planting))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        planting = db\
            .session\
            .query(Planting)\
            .filter(Planting.uid == uid)\
            .filter(Planting.status > config.STATUS_DELETED)\
            .first()
        if planting:
            response = planting_obj(planting, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        planting = db\
            .session\
            .query(Planting)\
            .filter(Planting.id == id)\
            .filter(Planting.status > config.STATUS_DELETED)\
            .first()
        if planting:
            response = planting_obj(planting)

    return jsonify(response)

def fetch_by_customer_shield_id(customer_shield_id=0):
    response = {}
    if customer_shield_id:
        planting = db\
            .session\
            .query(Planting)\
            .filter(Planting.customer_shield_id == customer_shield_id)\
            .filter(Planting.status > config.STATUS_DELETED)\
            .first()
        if planting:
            response = planting_obj(planting)

    return jsonify(response)

def add_new(planting_data=None, return_obj=False):

    try:
        data = json.loads(planting_data)
        if data:

            if 'customer_shield_id' in data and 'production_stage_id' in data and 'creator_id' in data:

                _customer_shield_id = None
                if valid_uuid(data['customer_shield_id']):
                    _customer_shield_id = customer_shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

                _production_stage_id = None
                if valid_uuid(data['production_stage_id']):
                    _production_stage_id = planting_production_stages.fetch_one(data['production_stage_id'], True).get_json()['real_id']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _customer_shield_id and _production_stage_id and _creator:
                    new_planting = Planting(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        customer_shield_id = _customer_shield_id,
                        current_production_stage_id = _production_stage_id,
                        creator_id = _creator
                    )
                    db.session.add(new_planting)
                    db.session.commit()
                    if new_planting:
                        if return_obj:
                            return jsonify(fetch_one(new_planting.uid)), 200

                        return jsonify({"info": "Planting added successfully!"}), 200

        return jsonify({"error": "Planting not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(planting_id, planting_data=None, return_obj=False):

    try:
        data = json.loads(planting_data)
        if data:
            if valid_uuid(planting_id) and 'customer_shield_id' in data and 'production_stage_id' in data:

                _customer_shield_id = None
                if valid_uuid(data['customer_shield_id']):
                    _customer_shield_id = customer_shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

                _production_stage_id = None
                if valid_uuid(data['production_stage_id']):
                    _production_stage_id = planting_production_stages.fetch_one(data['production_stage_id'], True).get_json()['real_id']

                if _customer_shield_id and _production_stage_id and _module_id:
                    _planting = Planting\
                        .query\
                        .filter(Planting.uid == planting_id)\
                        .first()
                    if _planting:
                        _planting.customer_shield_id = _customer_shield_id
                        _planting.current_production_stage_id = _production_stage_id
                        db.session.commit()

                        return jsonify({"info": "Planting edited successfully!"}), 200

        return jsonify({"error": "Planting not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(planting_id):

    try:

        if valid_uuid(planting_id) :
            _planting = Planting\
                .query\
                .filter(Planting.uid == planting_id)\
                .first()
            if _planting:
                _planting.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Planting deactivated successfully!"}), 200

        return jsonify({"error": "Planting not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
