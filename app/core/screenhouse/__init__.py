import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Screenhouse

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def screenhouse_obj(screenhouse=False, real_id=False):
    _screenhouse = {}
    if screenhouse:
        if real_id:
            _screenhouse["real_id"] = screenhouse.id

        _screenhouse["id"] = str(screenhouse.uid)
        _screenhouse["screenhouse_ref"] = screenhouse.screenhouse_ref
        _screenhouse["size"] = screenhouse.size
        _screenhouse["info_link"] = screenhouse.info_link
        _screenhouse["created_by"] = creator_detail(screenhouse.creator_id)
        _screenhouse["status"] = status_name(screenhouse.status)
        _screenhouse["created"] = screenhouse.created_at.strftime('%d %B %Y')
        _screenhouse["modified"] = screenhouse.modified_at

    return _screenhouse

def fetch_all():
    response = []

    screenhouses = db\
        .session\
        .query(Screenhouse)\
        .filter(Screenhouse.status > config.STATUS_DELETED)\
        .all()

    for screenhouse in screenhouses:
        response.append(screenhouse_obj(screenhouse))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        screenhouse = db\
            .session\
            .query(Screenhouse)\
            .filter(Screenhouse.uid == uid)\
            .filter(Screenhouse.status > config.STATUS_DELETED)\
            .first()
        if screenhouse:
            response = screenhouse_obj(screenhouse, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        screenhouse = db\
            .session\
            .query(Screenhouse)\
            .filter(Screenhouse.id == id)\
            .filter(Screenhouse.status > config.STATUS_DELETED)\
            .first()
        if screenhouse:
            response = screenhouse_obj(screenhouse)

    return jsonify(response)

def add_new(screenhouse_data=None, return_obj=False):

    try:
        data = json.loads(screenhouse_data)
        if data:

            if 'screenhouse_ref' in data and 'creator_id' in data:

                _screenhouse_ref = data['screenhouse_ref']
                _size = data['size']  if 'size' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _screenhouse_ref and _creator:
                    new_screenhouse = Screenhouse(
                        uid = uuid.uuid4(),
                        
                        status = config.STATUS_ACTIVE,
                        screenhouse_ref = _screenhouse_ref,
                        size = _size,
                        info_link = _info_link,
                        creator_id = _creator
                    )
                    db.session.add(new_screenhouse)
                    db.session.commit()
                    if new_screenhouse:
                        if return_obj:
                            return jsonify(fetch_one(new_screenhouse.uid)), 200

                        return jsonify({"info": "Screenhouse added successfully!"}), 200

        return jsonify({"error": "Screenhouse not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(screenhouse_id, screenhouse_data=None, return_obj=False):

    try:
        data = json.loads(screenhouse_data)
        if data:

            if valid_uuid(screenhouse_id):

                _size = data['size']  if 'size' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                if _name:
                    _screenhouse = Screenhouse\
                        .query\
                        .filter(Screenhouse.uid == str(screenhouse_id))\
                        .first()
                    if _screenhouse:
                        _screenhouse.size = _size
                        _screenhouse.info_link = _info_link
                        db.session.commit()

                        return jsonify({"info": "Screenhouse edited successfully!"}), 200

        return jsonify({"error": "Screenhouse not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def activate(screenhouse_id):

    try:

        if valid_uuid(screenhouse_id):
            _screenhouse= Screenhouse\
                .query\
                .filter(Screenhouse.uid == screenhouse_id)\
                .first()
            if _screenhouse:
                _screenhouse.status = config.STATUS_ACTIVE
                db.session.commit()

                return jsonify({"info": "Screenhouse activated successfully!"}), 200

        return jsonify({"error": "Screenhouse not activated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(screenhouse_id):

    try:

        if valid_uuid(screenhouse_id) :
            _screenhouse = Screenhouse\
                .query\
                .filter(Screenhouse.uid == screenhouse_id)\
                .first()
            if _screenhouse:
                _screenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Screenhouse deactivated successfully!"}), 200

        return jsonify({"error": "Screenhouse not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
