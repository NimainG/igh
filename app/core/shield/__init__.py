import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv
from datetime import datetime
from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Shield

from app.core import user
from app.core.shield import types as shield_types
from app.core.shield import spears as shield_spears

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shield_obj(shield=False, real_id=False):
    _shield = {}
    if shield:
        if real_id:
            _shield["real_id"] = shield.id

        _shield["id"] = str(shield.uid)
        _shield["serial"] = shield.serial
        _shield["mac_address"] = shield.mac_address
        _shield["type"] = shield_types.fetch_by_id(shield.type_id).get_json()
        _shield["spears"] = shield_spears.fetch_by_shield_id(shield.id).get_json()
        _shield["created_by"] = creator_detail(shield.creator_id)
        _shield["status"] = status_name(shield.status)
        _shield["created"] = shield.created_at.strftime('%d %B %Y')
        _shield["modified"] = shield.modified_at.strftime('%d %B %Y')

    return _shield

def fetch_all():
    response = []

    shields = db\
        .session\
        .query(Shield)\
        .filter(Shield.status > config.STATUS_DELETED)\
        .all()

    for shield in shields:
        response.append(shield_obj(shield))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.uid == uid)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.id == id)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def fetch_by_type_id(type_id=0):
    response = {}
    if type_id:
        shield = db\
            .session\
            .query(Shield)\
            .filter(Shield.type_id == type_id)\
            .filter(Shield.status > config.STATUS_DELETED)\
            .first()
        if shield:
            response = shield_obj(shield)

    return jsonify(response)

def add_new(shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:

            if 'serial' in data and 'mac_address' in data and 'shield_type_id' \
            in data and 'creator_id' in data:

                _serial = data['serial']
                _mac_address = data['mac_address']

                _shield_type = None
                _creator = None

                if valid_uuid(data['shield_type_id']):
                    _shield_type = shield_types.fetch_one(data['shield_type_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _serial and mac_address and _creator:
                    new_shield = Shield(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        serial = _serial,
                        mac_address = _mac_address,
                        type_id = _shield_type,
                        creator_id = _creator
                    )
                    db.session.add(new_shield)
                    db.session.commit()
                    if new_shield:
                        if return_obj:
                            return jsonify(fetch_one(new_shield.uid)), 200

                        return jsonify({"info": "Shield added successfully!"}), 200

        return jsonify({"error": "Shield not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shield_id, shield_data=None, return_obj=False):

    try:
        data = json.loads(shield_data)
        if data:

            if valid_uuid(shield_id) and 'serial' in data and 'mac_address' \
            in data and 'shield_type_id' in data:

                _serial = data['serial']
                _mac_address = data['mac_address']

                _shield_type = None

                if valid_uuid(data['shield_type_id']):
                    _shield_type = shield_types.fetch_one(data['shield_type_id'], True).get_json()['real_id']

                if _serial and _mac_address:
                    _shield = Shield\
                        .query\
                        .filter(Shield.uid == str(shield_id))\
                        .first()
                    if _shield:
                        _shield.size = _size
                        _shield.serial = _serial
                        _shield.mac_address = _mac_address
                        _shield.type_id = _shield_type
                        db.session.commit()

                        return jsonify({"info": "Shield edited successfully!"}), 200

        return jsonify({"error": "Shield not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shield_id):

    try:

        if valid_uuid(shield_id) :
            _shield = Shield\
                .query\
                .filter(Shield.uid == shield_id)\
                .first()
            if _shield:
                _shield.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield deactivated successfully!"}), 200

        return jsonify({"error": "Shield not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
