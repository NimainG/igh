import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ShieldType

from app.core import user
from app.core import media

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def placement_name(placement_id):
    _placement = '-'

    if placement_id:

        if placement_id == config.SHIELD_INTERNAL:
            _placement = "Internal"

        if placement_id == config.SHIELD_EXTERNAL:
            _placement = "External"

    return _placement

def shield_type_obj(shield_type=False, real_id=False):
    _shield_type = {}
    if shield_type:
        if real_id:
            _shield_type["real_id"] = shield_type.id

        _shield_type["id"] = str(shield_type.uid)
        _shield_type["name"] = shield_type.name
        _shield_type["photo"] = media.fetch_by_id(shield_type.id).get_json()
        _shield_type["description"] = shield_type.description
        _shield_type["placement"] = placement_name(shield_type.placement)
        _shield_type["info_link"] = shield_type.info_link
        _shield_type["created_by"] = creator_detail(shield_type.creator_id)
        _shield_type["status"] = status_name(shield_type.status)
        _shield_type["created"] = shield_type.created_at.strftime('%d %B %Y')
        _shield_type["modified"] = shield_type.modified_at

    return _shield_type

def fetch_all():
    response = []

    shield_types = db\
        .session\
        .query(ShieldType)\
        .filter(ShieldType.status > config.STATUS_DELETED)\
        .all()

    for shield_type in shield_types:
        response.append(shield_type_obj(shield_type))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield_type = db\
            .session\
            .query(ShieldType)\
            .filter(ShieldType.uid == uid)\
            .filter(ShieldType.status > config.STATUS_DELETED)\
            .first()
        if shield_type:
            response = shield_type_obj(shield_type, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shield_type = db\
            .session\
            .query(ShieldType)\
            .filter(ShieldType.id == id)\
            .filter(ShieldType.status > config.STATUS_DELETED)\
            .first()
        if shield_type:
            response = shield_type_obj(shield_type)

    return jsonify(response)

def add_new(shield_type_data=None, return_obj=False):

    try:
        data = json.loads(shield_type_data)
        if data:

            if 'name' in data and 'placement' in data and 'creator_id' in data:

                _name = data['name']
                _placement = data['placement']
                _size = data['size'] if 'size' in data else None
                _description = data['description'] if 'description' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _photo_media = None
                _creator = None

                if valid_uuid(data['photo_media_id']):
                    _photo_media = media.fetch_one(data['photo_media_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _shield_type_ref and _name and _customer and _creator:
                    new_shield_type = ShieldType(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        photo_url = _photo_media,
                        description = _description,
                        placement = _placement,
                        info_link = _info_link,
                        creator_id = _creator
                    )
                    db.session.add(new_shield_type)
                    db.session.commit()
                    if new_shield_type:
                        if return_obj:
                            return jsonify(fetch_one(new_shield_type.uid)), 200

                        return jsonify({"info": "Shield type added successfully!"}), 200

        return jsonify({"error": "Shield type not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shield_type_id, shield_type_data=None, return_obj=False):

    try:
        data = json.loads(shield_type_data)
        if data:

            if valid_uuid(shield_type_id) and  'name' in data and 'placement' \
            in data:

                _name = data['name']
                _placement = data['placement']
                _size = data['size'] if 'size' in data else None
                _description = data['description'] if 'description' in data else None
                _info_link = data['info_link'] if 'info_link' in data else None

                _photo_media = None

                if valid_uuid(data['cover_photo']):
                    _photo_media = media.fetch_one(data['cover_photo'], True).get_json()['real_id']

                if _name:
                    _shield_type = ShieldType\
                        .query\
                        .filter(ShieldType.uid == shield_type_id)\
                        .first()
                    if _shield_type:
                        _shield_type.name = _name
                        _shield_type.photo_url = _photo_media
                        _shield_type.description = _description
                        _shield_type.placement = _placement
                        _shield_type.info_link = _info_link
                        db.session.commit()

                        return jsonify({"info": "Shield type edited successfully!"}), 200

        return jsonify({"error": "Shield type not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shield_type_id):

    try:

        if valid_uuid(shield_type_id) :
            _shield_type = ShieldType\
                .query\
                .filter(ShieldType.uid == shield_type_id)\
                .first()
            if _shield_type:
                _shield_type.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield type deactivated successfully!"}), 200

        return jsonify({"error": "Shield type not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
