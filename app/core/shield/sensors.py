# shield_sensor_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import ShieldSensor

from app.core import user
from app.core import shield
from app.core.shield import sensor_types

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shield_sensor_obj(shield_sensor=False, real_id=False):
    _shield_sensor = {}
    if shield_sensor:
        if real_id:
            _shield_sensor["real_id"] = shield_sensor.id

        _shield_sensor["id"] = str(shield_sensor.uid)
        _shield_sensor["shield"] = shield.fetch_by_id(shield_sensor.shield_id).get_json()
        _shield_sensor["sensor_type"] = sensor_types.fetch_by_id(shield_sensor.shield_id).get_json()
        _shield_sensor["created_by"] = creator_detail(shield_sensor.creator_id)
        _shield_sensor["status"] = status_name(shield_sensor.status)
        _shield_sensor["created"] = shield_sensor.created_at
        _shield_sensor["modified"] = shield_sensor.modified_at

    return _shield_sensor

def fetch_all():
    response = []

    shield_sensors = db\
        .session\
        .query(ShieldSensor)\
        .filter(ShieldSensor.status > config.STATUS_DELETED)\
        .all()

    for shield_sensor in shield_sensors:
        response.append(shield_sensor_obj(shield_sensor))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shield_sensor = db\
            .session\
            .query(ShieldSensor)\
            .filter(ShieldSensor.uid == uid)\
            .filter(ShieldSensor.status > config.STATUS_DELETED)\
            .first()
        if shield_sensor:
            response = shield_sensor_obj(shield_sensor, real_id)

    return jsonify(response)

def fetch_by_type_id(sensor_type_id):
    response = {}
    if sensor_type_id:
        shield_sensor = db\
            .session\
            .query(ShieldSensor)\
            .filter(ShieldSensor.sensor_type_id == sensor_type_id)\
            .filter(ShieldSensor.status > config.STATUS_DELETED)\
            .first()
        if shield_sensor:
            response = shield_sensor_obj(shield_sensor)

    return jsonify(response)

def fetch_by_shield_id(shield_id):
    response = {}
    if shield_id:
        shield_sensor = db\
            .session\
            .query(ShieldSensor)\
            .filter(ShieldSensor.shield_id == shield_id)\
            .filter(ShieldSensor.status > config.STATUS_DELETED)\
            .first()
        if shield_sensor:
            response = shield_sensor_obj(shield_sensor)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shield_sensor = db\
            .session\
            .query(ShieldSensor)\
            .filter(ShieldSensor.id == id)\
            .filter(ShieldSensor.status > config.STATUS_DELETED)\
            .first()
        if shield_sensor:
            response = shield_sensor_obj(shield_sensor)

    return jsonify(response)

def add_new(shield_sensor_data=None, return_obj=False):

    try:
        data = json.loads(shield_sensor_data)
        if data:

            if 'shield_id' in data and 'sensor_type_id' in data and 'creator_id' in data :

                _shield = None
                _sensor_type = None
                _creator = None

                if valid_uuid(data['shield_id']):
                    _shield = shield.fetch_one(data['shield_id'], True).get_json()['real_id']

                if valid_uuid(data['sensor_type_id']):
                    _sensor_type = shield.fetch_one(data['sensor_type_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _type and _customer and _creator:
                    new_shield_sensor = ShieldSensor(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        shield_id = _shield,
                        sensor_type_id = _sensor_type,
                        creator_id = _creator
                    )
                    db.session.add(new_shield_sensor)
                    db.session.commit()
                    if new_shield_sensor:
                        if return_obj:
                            return jsonify(fetch_one(new_shield_sensor.uid)), 200

                        return jsonify({"info": "Shield sensor added successfully!"}), 200

        return jsonify({"error": "Shield sensor not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shield_sensor_id):

    try:
        if valid_uuid(shield_sensor_id) :
            _shield_sensor = ShieldSensor\
                .query\
                .filter(ShieldSensor.uid == shield_sensor_id)\
                .first()
            if _shield_sensor:
                _shield_sensor.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield sensor deactivated successfully!"}), 200

        return jsonify({"error": "Shield sensor not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
