import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ShieldSpear

from app.core import user
from app.core import spear

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def device_obj(device=False, real_id=False):
    _device = {}
    if device:
        if real_id:
            _device["real_id"] = device.id

        _device["id"] = str(device.uid)
        _device["spear"] = spear.fetch_by_id(device.spear_id).get_json()
        _device["created_by"] = creator_detail(device.creator_id)
        _device["status"] = status_name(device.status)
        _device["created"] = device.created_at
        _device["modified"] = device.modified_at

    return _device

def fetch_all():
    response = []

    devices = db\
        .session\
        .query(ShieldSpear)\
        .filter(ShieldSpear.status > config.STATUS_DELETED)\
        .all()

    for device in devices:
        response.append(device_obj(device))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        device = db\
            .session\
            .query(ShieldSpear)\
            .filter(ShieldSpear.uid == uid)\
            .filter(ShieldSpear.status > config.STATUS_DELETED)\
            .first()
        if device:
            response = device_obj(device, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        device = db\
            .session\
            .query(ShieldSpear)\
            .filter(ShieldSpear.id == id)\
            .filter(ShieldSpear.status > config.STATUS_DELETED)\
            .first()
        if device:
            response = device_obj(device)

    return jsonify(response)

def fetch_by_shield_id(shield_id=0):
    response = {}
    if shield_id:
        device = db\
            .session\
            .query(ShieldSpear)\
            .filter(ShieldSpear.shield_id == shield_id)\
            .filter(ShieldSpear.status > config.STATUS_DELETED)\
            .first()
        if device:
            response = device_obj(device)

    return jsonify(response)

def add_new(device_data=None, return_obj=False):

    try:
        data = json.loads(device_data)
        if data:

            if 'shield_id' in data and 'spear_id' in data and 'creator_id' in data:

                _shield_id = data['spear_id']
                _spear_id = data['spear_id']

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _shield_id and _spear_id and _creator:
                    new_device = ShieldSpear(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        shield_id = _shield_id,
                        spear_id = _spear_id,
                        creator_id = _creator
                    )
                    db.session.add(new_device)
                    db.session.commit()
                    if new_device:
                        if return_obj:
                            return jsonify(fetch_one(new_device.uid)), 200

                        return jsonify({"info": "Shield spear added successfully!"}), 200

        return jsonify({"error": "Shield spear not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(device_id, device_data=None, return_obj=False):

    try:
        data = json.loads(device_data)
        if data:

            if valid_uuid(device_id) and 'shield_id' in data and 'spear_id' in data:

                _shield_id = data['spear_id']
                _spear_id = data['spear_id']

                if _shield_id and _spear_id:
                    _device = ShieldSpear\
                        .query\
                        .filter(ShieldSpear.uid == device_id)\
                        .first()
                    if _device:
                        _device.name = _name
                        _device.shield_id = _shield_id
                        _device.spear_id = _spear_id
                        db.session.commit()

                        return jsonify({"info": "Shield spear edited successfully!"}), 200

        return jsonify({"error": "Shield spear not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(device_id):

    try:

        if valid_uuid(device_id) :
            _device = ShieldSpear\
                .query\
                .filter(ShieldSpear.uid == device_id)\
                .first()
            if _device:
                _device.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shield spear deactivated successfully!"}), 200

        return jsonify({"error": "Shield spear not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
