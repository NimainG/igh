import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import SoilType

from app.core import user
from app.core import media

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def soil_obj(soil=False, real_id=False):
    _soil = {}
    if soil:
        if real_id:
            _soil["real_id"] = soil.id

        _soil["id"] = str(soil.uid)
        _soil["name"] = soil.name
        _soil["description"] = soil.description
        _soil["photo"] = media.fetch_by_id(soil.photo_media_id).get_json()
        _soil["created_by"] = creator_detail(soil.creator_id)
        _soil["status"] = status_name(soil.status)
        _soil["created"] = soil.created_at
        _soil["modified"] = soil.modified_at

    return _soil

def fetch_all():
    response = []

    soils = db\
        .session\
        .query(SoilType)\
        .filter(SoilType.status > config.STATUS_DELETED)\
        .all()

    for soil in soils:
        response.append(soil_obj(soil))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        soil = db\
            .session\
            .query(SoilType)\
            .filter(SoilType.uid == uid)\
            .filter(SoilType.status > config.STATUS_DELETED)\
            .first()
        if soil:
            response = soil_obj(soil, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        soil = db\
            .session\
            .query(SoilType)\
            .filter(SoilType.id == id)\
            .filter(SoilType.status > config.STATUS_DELETED)\
            .first()
        if soil:
            response = soil_obj(soil)

    return jsonify(response)

def add_new(soil_data=None, return_obj=False):

    try:
        data = json.loads(soil_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _description = None
            if 'description' in data:
                _description = data['description']

            _photo_media_id = None
            if valid_uuid(data['photo_media']):
                _photo_media_id = media.fetch_one(data['photo_media'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _description and _creator_id:
                new_soil = SoilType(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    description = _description,
                    photo_media_id = _photo_media,
                    creator_id = _creator_id
                )
                db.session.add(new_soil)
                db.session.commit()
                if new_soil:
                    if return_obj:
                        return jsonify(fetch_one(new_soil.uid)), 200

                    return jsonify({"info": "Soil added successfully!"}), 200


        return jsonify({"error": "Soil not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(soil_id, soil_data=None, return_obj=False):

    try:
        data = json.loads(soil_data)
        if data:

            if valid_uuid(soil_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _description = None
                if 'description' in data:
                    _description = data['description']

                _photo_media_id = None
                if valid_uuid(data['photo_media']):
                    _photo_media_id = media.fetch_one(data['photo_media'], True).get_json()['real_id']

                if _name:
                    _soil = SoilType\
                        .query\
                        .filter(SoilType.uid == str(soil_id))\
                        .first()
                    if _soil:
                        _soil.name = _name
                        _soil.description = _description
                        _soil.photo_media_id = _photo_media_id
                        db.session.commit()

                        return jsonify({"info": "Soil edited successfully!"}), 200

        return jsonify({"error": "Soil not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(soil_id):

    try:

        if valid_uuid(soil_id) :
            _soil = SoilType\
                .query\
                .filter(SoilType.uid == soil_id)\
                .first()
            if _soil:
                _soil.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Soil deactivated successfully!"}), 200

        return jsonify({"error": "Soil not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
