import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Service

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def service_obj(service=False, real_id=False):
    _service = {}
    if service:
        if real_id:
            _service["real_id"] = service.id

        _service["id"] = str(service.uid)
        _service["name"] = service.name
        _service["link"] = service.link
        _service["description"] = service.description
        _service["created_by"] = creator_detail(service.creator_id)
        _service["status"] = status_name(service.status)
        _service["created"] = service.created_at
        _service["modified"] = service.modified_at

    return _service

def fetch_all():
    response = []

    services = db\
        .session\
        .query(Service)\
        .filter(Service.status > config.STATUS_DELETED)\
        .all()

    for service in services:
        response.append(service_obj(service))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        service = db\
            .session\
            .query(Service)\
            .filter(Service.uid == uid)\
            .filter(Service.status > config.STATUS_DELETED)\
            .first()
        if service:
            response = service_obj(service, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        service = db\
            .session\
            .query(Service)\
            .filter(Service.id == id)\
            .filter(Service.status > config.STATUS_DELETED)\
            .first()
        if service:
            response = service_obj(service)

    return jsonify(response)

def add_new(service_data=None, return_obj=False):

    try:
        data = json.loads(service_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _link = None
            if 'link' in data:
                _link = data['link']

            _description = None
            if 'description' in data:
                _description = data['description']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _link and _creator_id:
                new_service = Service(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    link = _link,
                    description = _description,
                    creator_id = _creator_id
                )
                db.session.add(new_service)
                db.session.commit()
                if new_service:
                    if return_obj:
                        return jsonify(fetch_one(new_service.uid)), 200

                    return jsonify({"info": "Service added successfully!"}), 200


        return jsonify({"error": "Service not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(service_id, service_data=None, return_obj=False):

    try:
        data = json.loads(service_data)
        if data:

            if valid_uuid(service_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _link = None
                if 'link' in data:
                    _link = data['link']

                _description = None
                if 'description' in data:
                    _description = data['description']

                if _name and _link:
                    _service = Service\
                        .query\
                        .filter(Service.uid == str(service_id))\
                        .first()
                    if _service:
                        _service.name = _name
                        _service.link = _link
                        _service.description = _description
                        db.session.commit()

                        return jsonify({"info": "Service edited successfully!"}), 200

        return jsonify({"error": "Service not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(service_id):

    try:

        if valid_uuid(service_id) :
            _service = Service\
                .query\
                .filter(Service.uid == service_id)\
                .first()
            if _service:
                _service.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Service deactivated successfully!"}), 200

        return jsonify({"error": "Service not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
