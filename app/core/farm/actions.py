import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import FarmActions

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def farm_action_obj(farm_action=False, real_id=False):
    _farm_action = {}
    if farm_action:
        if real_id:
            _farm_action["real_id"] = farm_action.id

        _farm_action["id"] = str(farm_action.uid)
        _farm_action["name"] = farm_action.name
        _farm_action["description"] = farm_action.description
        _farm_action["created_by"] = creator_detail(farm_action.creator_id)
        _farm_action["status"] = status_name(farm_action.status)
        _farm_action["created"] = farm_action.created_at
        _farm_action["modified"] = farm_action.modified_at

    return _farm_action

def fetch_all():
    response = []

    farm_actions = db\
        .session\
        .query(FarmActions)\
        .filter(FarmActions.status > config.STATUS_DELETED)\
        .all()

    for farm_action in farm_actions:
        response.append(farm_action_obj(farm_action))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        farm_action = db\
            .session\
            .query(FarmActions)\
            .filter(FarmActions.uid == uid)\
            .filter(FarmActions.status > config.STATUS_DELETED)\
            .first()
        if farm_action:
            response = farm_action_obj(farm_action, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        farm_action = db\
            .session\
            .query(FarmActions)\
            .filter(FarmActions.id == id)\
            .filter(FarmActions.status > config.STATUS_DELETED)\
            .first()
        if farm_action:
            response = farm_action_obj(farm_action)

    return jsonify(response)

def add_new(farm_action_data=None, return_obj=False):

    try:
        data = json.loads(farm_action_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _description = None
            if 'description' in data:
                _description = data['description']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _description and _creator_id:
                new_farm_action = FarmActions(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    description = _description,
                    creator_id = _creator
                )
                db.session.add(new_farm_action)
                db.session.commit()
                if new_farm_action:
                    if return_obj:
                        return jsonify(fetch_one(new_farm_action.uid)), 200

                    return jsonify({"info": "Farm actions added successfully!"}), 200

        return jsonify({"error": "Farm actions not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(farm_action_id, farm_action_data=None, return_obj=False):

    try:
        data = json.loads(farm_action_data)
        if data:

            if valid_uuid(farm_action_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _description = None
                if 'description' in data:
                    _description = data['description']

                if _name:
                    _farm_action = FarmActions\
                        .query\
                        .filter(FarmActions.uid == str(farm_action_id))\
                        .first()
                    if _farm_action:
                        _farm_action.name = _name
                        _farm_action.description = _description
                        db.session.commit()

                        return jsonify({"info": "Farm actions edited successfully!"}), 200

        return jsonify({"error": "Farm actions not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(farm_action_id):

    try:

        if valid_uuid(farm_action_id) :
            _farm_action = FarmActions\
                .query\
                .filter(FarmActions.uid == farm_action_id)\
                .first()
            if _farm_action:
                _farm_action.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "FarmActions deactivated successfully!"}), 200

        return jsonify({"error": "FarmActions not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
