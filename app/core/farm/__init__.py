# farm_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Farm

from app.core import user
from app.core import media

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def farm_obj(farm=False, real_id=False):
    _farm = {}
    if farm:
        if real_id:
            _farm["real_id"] = farm.id

        _farm["id"] = str(farm.uid)
        _farm["farm_ref"] = farm.farm_ref
        _farm["name"] = farm.name
        _farm["address"] = farm.address
        _farm["state"] = farm.state
        _farm["city"] = farm.city
        _farm["zip_code"] = farm.zip_code
        _farm["latitude"] = farm.latitude
        _farm["longitude"] = farm.longitude
        _farm["farm_area"] = farm.farm_area
        _farm["cover_photo_media"] = media.fetch_by_id(farm.cover_photo_media_id).get_json()
        _farm["created_by"] = creator_detail(farm.creator_id)
        _farm["status"] = status_name(farm.status)
        _farm["created"] = farm.created_at
        _farm["modified"] = farm.modified_at

    return _farm

def fetch_all():
    response = []

    farms = db\
        .session\
        .query(Farm)\
        .filter(Farm.status > config.STATUS_DELETED)\
        .all()

    for farm in farms:
        response.append(farm_obj(farm))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        farm = db\
            .session\
            .query(Farm)\
            .filter(Farm.uid == uid)\
            .filter(Farm.status > config.STATUS_DELETED)\
            .first()
        if farm:
            response = farm_obj(farm, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        farm = db\
            .session\
            .query(Farm)\
            .filter(Farm.id == id)\
            .filter(Farm.status > config.STATUS_DELETED)\
            .first()
        if farm:
            response = farm_obj(farm)

    return jsonify(response)

def add_new(farm_data=None, return_obj=False):

    try:
        data = json.loads(farm_data)
        if data:

            _farm_ref = None
            if 'farm_ref' in data:
                _farm_ref = data['farm_ref']

            _name = None
            if 'name' in data:
                _name = data['name']

            _address = None
            if 'address' in data:
                _address = data['address']

            _state = None
            if 'state' in data:
                _state = data['state']

            _city = None
            if 'city' in data:
                _city = data['city']

            _zip_code = None
            if 'zip_code' in data:
                _zip_code = data['zip_code']

            _latitude = None
            if 'latitude' in data:
                _latitude = data['latitude']

            _longitude = None
            if 'longitude' in data:
                _longitude = data['longitude']

            _farm_area = None
            if 'farm_area' in data:
                _farm_area = data['farm_area']

            _cover_photo_media_id = None
            if valid_uuid(data['cover_photo_media']):
                _cover_photo_media_id = media.fetch_one(data['cover_photo_media'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _customer_id and _creator_id:

                new_farm = Farm(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    farm_ref = _farm_ref,
                    name = _name,
                    address = _address,
                    state = _state,
                    city = _city,
                    zip_code = _zip_code,
                    latitude = _latitude,
                    longitude = _longitude,
                    farm_area = _farm_area,
                    cover_photo_media_id = _cover_photo_media_id,
                    creator_id = _creator_id
                )
                db.session.add(new_farm)
                db.session.commit()
                if new_farm:
                    if return_obj:
                        return jsonify(fetch_one(new_farm.uid)), 200

                    return jsonify({"info": "Farm added successfully!"}), 200

        return jsonify({"error": "Farm not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(farm_id, farm_data=None, return_obj=False):

    try:
        data = json.loads(farm_data)
        if data:

            if valid_uuid(farm_id):

                _farm_ref = None
                if 'farm_ref' in data:
                    _farm_ref = data['farm_ref']

                _name = None
                if 'name' in data:
                    _name = data['name']

                _address = None
                if 'address' in data:
                    _address = data['address']

                _state = None
                if 'state' in data:
                    _state = data['state']

                _city = None
                if 'city' in data:
                    _city = data['city']

                _zip_code = None
                if 'zip_code' in data:
                    _zip_code = data['zip_code']

                _latitude = None
                if 'latitude' in data:
                    _latitude = data['latitude']

                _longitude = None
                if 'longitude' in data:
                    _longitude = data['longitude']

                _farm_area = None
                if 'farm_area' in data:
                    _farm_area = data['farm_area']

                _cover_photo_media_id = None
                if valid_uuid(data['cover_photo_media']):
                    _cover_photo_media_id = media.fetch_one(data['cover_photo_media'], True).get_json()['real_id']

                _farm = Farm\
                    .query\
                    .filter(Farm.uid == str(farm_id))\
                    .first()
                if _farm:
                    _farm.farm_ref = _farm_ref
                    _farm.name = _name
                    _farm.address = _address
                    _farm.state = _state
                    _farm.city = _city
                    _farm.zip_code = _zip_code
                    _farm.latitude = _latitude
                    _farm.longitude = _longitude
                    _farm.farm_area = _farm_area
                    _farm.cover_photo_media_id = _cover_photo_media_id
                    db.session.commit()

                    return jsonify({"info": "Farm edited successfully!"}), 200

        return jsonify({"error": "Farm not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(farm_id):

    try:
        if valid_uuid(farm_id) :
            _farm = Farm\
                .query\
                .filter(Farm.uid == farm_id)\
                .first()
            if _farm:
                _farm.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Farm deactivated successfully!"}), 200

        return jsonify({"error": "Farm not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
