import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerGreenhouseScreenhouse

from app.core import customer
from app.core import user
from app.core.misc import soils as soil_types

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_screenhouse_obj(greenhouse_screenhouse=False, real_id=False):
    _greenhouse_screenhouse = {}
    if greenhouse_screenhouse:
        if real_id:
            _greenhouse_screenhouse["real_id"] = greenhouse_screenhouse.id

        _greenhouse_screenhouse["soil_type"] = soil_types.fetch_by_id(greenhouse_screenhouse.soil_type_id).get_json()
        _greenhouse_screenhouse["customer_farm_greenhouse"] = customer.farm_greenhouses.fetch_by_id(greenhouse_screenhouse.customer_farm_greenhouse_id).get_json()['uid']
        _greenhouse_screenhouse["customer_screenhouse"] = customer.screenhouses.fetch_by_id(greenhouse_screenhouse.customer_screenhouse_id).get_json()['uid']
        _greenhouse_screenhouse["created_by"] = creator_detail(greenhouse_screenhouse.creator_id)
        _greenhouse_screenhouse["status"] = status_name(greenhouse_screenhouse.status)
        _greenhouse_screenhouse["created"] = greenhouse_screenhouse.created_at
        _greenhouse_screenhouse["modified"] = greenhouse_screenhouse.modified_at

    return _greenhouse_screenhouse

def fetch_all():
    response = []

    greenhouse_screenhouses = db\
        .session\
        .query(CustomerGreenhouseScreenhouse)\
        .filter(CustomerGreenhouseScreenhouse.status > config.STATUS_DELETED)\
        .all()

    for greenhouse_screenhouse in greenhouse_screenhouses:
        response.append(greenhouse_screenhouse_obj(greenhouse_screenhouse))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse_screenhouse = db\
            .session\
            .query(CustomerGreenhouseScreenhouse)\
            .filter(CustomerGreenhouseScreenhouse.uid == uid)\
            .filter(CustomerGreenhouseScreenhouse.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_screenhouse:
            response = greenhouse_screenhouse_obj(greenhouse_screenhouse, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse_screenhouse = db\
            .session\
            .query(CustomerGreenhouseScreenhouse)\
            .filter(CustomerGreenhouseScreenhouse.id == id)\
            .filter(CustomerGreenhouseScreenhouse.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_screenhouse:
            response = greenhouse_screenhouse_obj(greenhouse_screenhouse)

    return jsonify(response)

def fetch_by_customer_farm_greenhouse(customer_farm_greenhouse_id=None):
    response = []
    if customer_farm_greenhouse_id:
        greenhouse_screenhouses = db\
            .session\
            .query(CustomerGreenhouseScreenhouse)\
            .filter(CustomerGreenhouseScreenhouse.customer_farm_greenhouse_id == customer_farm_greenhouse_id)\
            .filter(CustomerGreenhouseScreenhouse.status > config.STATUS_DELETED)\
            .all()
        for greenhouse_screenhouse in greenhouse_screenhouses:
            response = greenhouse_screenhouse_obj(greenhouse_screenhouse)

    return jsonify(response)

def fetch_by_customer_screenhouse(customer_screenhouse_id=None):
    response = []
    if customer_screenhouse_id:
        greenhouse_screenhouses = db\
            .session\
            .query(CustomerGreenhouseScreenhouse)\
            .filter(CustomerGreenhouseScreenhouse.customer_screenhouse_id == customer_screenhouse_id)\
            .filter(CustomerGreenhouseScreenhouse.status > config.STATUS_DELETED)\
            .all()
        for greenhouse_screenhouse in greenhouse_screenhouses:
            response = greenhouse_screenhouse_obj(greenhouse_screenhouse)

    return jsonify(response)

def add_new(greenhouse_screenhouse_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_screenhouse_data)
        if data:

            _customer_farm_greenhouse_id = None
            if valid_uuid(data['customer_farm_greenhouse_id']):
                _customer_farm_greenhouse_id = customer.farm_greenhouses.fetch_one(data['customer_farm_greenhouse_id'], True).get_json()['real_id']

            _customer_screenhouse_id = None
            if valid_uuid(data['customer_screenhouse_id']):
                _customer_screenhouse_id = customer.screenhouses.fetch_one(data['customer_screenhouse_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_farm_greenhouse_id and _customer_shield_id and _creator_id:
                new_greenhouse_screenhouse = CustomerGreenhouseScreenhouse(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    customer_farm_greenhouse_id = _customer_farm_greenhouse_id,
                    customer_screenhouse_id = _customer_screenhouse_id,
                    creator_id = _creator_id
                )
                db.session.add(new_greenhouse_screenhouse)
                db.session.commit()
                if new_greenhouse_screenhouse:
                    if return_obj:
                        return jsonify(fetch_one(new_greenhouse_screenhouse.uid)), 200

                    return jsonify({"info": "Customer greenhouse screenhouse added successfully!"}), 200

        return jsonify({"error": "Customer greenhouse screenhouse not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(greenhouse_screenhouse_id):

    try:
        if valid_uuid(greenhouse_screenhouse_id) :
            _greenhouse_screenhouse = CustomerGreenhouseScreenhouse\
                .query\
                .filter(CustomerGreenhouseScreenhouse.uid == greenhouse_screenhouse_id)\
                .first()
            if _greenhouse_screenhouse:
                _greenhouse_screenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer greenhouse screenhouse deactivated successfully!"}), 200

        return jsonify({"error": "Customer greenhouse screenhouse not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
