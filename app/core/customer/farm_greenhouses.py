import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerFarmGreenhouse

from app.core import customer
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def farm_greenhouse_obj(farm_greenhouse=False, real_id=False):
    _farm_greenhouse = {}
    if farm_greenhouse:
        if real_id:
            _farm_greenhouse["real_id"] = farm_greenhouse.id

        _farm_greenhouse["id"] = str(farm_greenhouse.uid)
        _farm_greenhouse["customer_farm"] = customer.farms.fetch_by_id(farm_greenhouse.customer_farm_id).get_json()['uid']
        _farm_greenhouse["customer_greenhouse"] = customer.greenhouses.fetch_by_id(farm_greenhouse.customer_greenhouse_id).get_json()['uid']
        _farm_greenhouse["created_by"] = creator_detail(farm_greenhouse.creator_id)
        _farm_greenhouse["status"] = status_name(farm_greenhouse.status)
        _farm_greenhouse["created"] = farm_greenhouse.created_at
        _farm_greenhouse["modified"] = farm_greenhouse.modified_at

    return _farm_greenhouse

def fetch_all():
    response = []

    farm_greenhouses = db\
        .session\
        .query(CustomerFarmGreenhouse)\
        .filter(CustomerFarmGreenhouse.status > config.STATUS_DELETED)\
        .all()

    for farm_greenhouse in farm_greenhouses:
        response.append(farm_greenhouse_obj(farm_greenhouse))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        farm_greenhouse = db\
            .session\
            .query(CustomerFarmGreenhouse)\
            .filter(CustomerFarmGreenhouse.uid == uid)\
            .filter(CustomerFarmGreenhouse.status > config.STATUS_DELETED)\
            .first()
        if farm_greenhouse:
            response = farm_greenhouse_obj(farm_greenhouse, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        farm_greenhouse = db\
            .session\
            .query(CustomerFarmGreenhouse)\
            .filter(CustomerFarmGreenhouse.id == id)\
            .filter(CustomerFarmGreenhouse.status > config.STATUS_DELETED)\
            .first()
        if farm_greenhouse:
            response = farm_greenhouse_obj(farm_greenhouse)

    return jsonify(response)

def fetch_by_customer_farm(customer_farm_id=None):
    response = []
    if customer_farm_id:
        farm_greenhouses = db\
            .session\
            .query(CustomerFarmGreenhouse)\
            .filter(CustomerFarmGreenhouse.customer_farm_id == customer_farm_id)\
            .filter(CustomerFarmGreenhouse.status > config.STATUS_DELETED)\
            .all()
        for farm_greenhouse in farm_greenhouses:
            response = farm_greenhouse_obj(farm_greenhouse)

    return jsonify(response)

def add_new(farm_greenhouse_data=None, return_obj=False):

    try:
        data = json.loads(farm_greenhouse_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _customer_farm_id = None
            if valid_uuid(data['customer_farm_id']):
                _customer_farm_id = customer.farms.fetch_one(data['customer_farm_id'], True).get_json()['real_id']

            _customer_greenhouse_id = None
            if valid_uuid(data['customer_greenhouse_id']):
                _customer_greenhouse_id = customer.greenhouses.fetch_one(data['customer_greenhouse_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_farm_id and _customer_greenhouse_id and _creator_id:
                new_farm_greenhouse = CustomerFarmGreenhouse(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    customer_farm_id = _customer_farm_id,
                    customer_greenhouse_id = _customer_greenhouse_id,
                    creator_id = _creator_id
                )
                db.session.add(new_farm_greenhouse)
                db.session.commit()
                if new_farm_greenhouse:
                    if return_obj:
                        return jsonify(fetch_one(new_farm_greenhouse.uid)), 200

                    return jsonify({"info": "Customer farm greenhouse added successfully!"}), 200

        return jsonify({"error": "Customer farm greenhouse not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(farm_greenhouse_id, farm_greenhouse_data=None, return_obj=False):

    try:
        data = json.loads(farm_greenhouse_data)
        if data:

            if valid_uuid(farm_greenhouse_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _farm_greenhouse = CustomerFarmGreenhouse\
                    .query\
                    .filter(CustomerFarmGreenhouse.uid == str(farm_greenhouse_id))\
                    .first()
                if _farm_greenhouse:
                    _farm_greenhouse.name = _name
                    db.session.commit()

                    return jsonify({"info": "Customer farm greenhouse edited successfully!"}), 200

        return jsonify({"error": "Customer farm greenhouse not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(farm_greenhouse_id):

    try:
        if valid_uuid(farm_greenhouse_id) :
            _farm_greenhouse = CustomerFarmGreenhouse\
                .query\
                .filter(CustomerFarmGreenhouse.uid == farm_greenhouse_id)\
                .first()
            if _farm_greenhouse:
                _farm_greenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer farm greenhouse deactivated successfully!"}), 200

        return jsonify({"error": "Customer farm greenhouse not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
