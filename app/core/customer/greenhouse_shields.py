import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerGreenhouseShield

from app.core import customer
from app.core import user
from app.core.misc import soils as soil_types

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_shield_obj(greenhouse_shield=False, real_id=False):
    _greenhouse_shield = {}
    if greenhouse_shield:
        if real_id:
            _greenhouse_shield["real_id"] = greenhouse_shield.id

        _greenhouse_shield["id"] = str(greenhouse_shield.uid)
        _greenhouse_shield["latitude"] = greenhouse_shield.latitude
        _greenhouse_shield["longitude"] = greenhouse_shield.longitude
        _greenhouse_shield["growth_area"] = greenhouse_shield.growth_area
        _greenhouse_shield["soil_type"] = soil_types.fetch_by_id(greenhouse_shield.soil_type_id).get_json()
        _greenhouse_shield["customer_farm_greenhouse"] = customer.farm_greenhouses.fetch_by_id(greenhouse_shield.customer_farm_greenhouse_id).get_json()['uid']
        _greenhouse_shield["customer_shield"] = customer.shields.fetch_by_id(greenhouse_shield.customer_shield_id).get_json()['uid']
        _greenhouse_shield["created_by"] = creator_detail(greenhouse_shield.creator_id)
        _greenhouse_shield["status"] = status_name(greenhouse_shield.status)
        _greenhouse_shield["created"] = greenhouse_shield.created_at
        _greenhouse_shield["modified"] = greenhouse_shield.modified_at

    return _greenhouse_shield

def fetch_all():
    response = []

    greenhouse_shields = db\
        .session\
        .query(CustomerGreenhouseShield)\
        .filter(CustomerGreenhouseShield.status > config.STATUS_DELETED)\
        .all()

    for greenhouse_shield in greenhouse_shields:
        response.append(greenhouse_shield_obj(greenhouse_shield))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse_shield = db\
            .session\
            .query(CustomerGreenhouseShield)\
            .filter(CustomerGreenhouseShield.uid == uid)\
            .filter(CustomerGreenhouseShield.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_shield:
            response = greenhouse_shield_obj(greenhouse_shield, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse_shield = db\
            .session\
            .query(CustomerGreenhouseShield)\
            .filter(CustomerGreenhouseShield.id == id)\
            .filter(CustomerGreenhouseShield.status > config.STATUS_DELETED)\
            .first()
        if greenhouse_shield:
            response = greenhouse_shield_obj(greenhouse_shield)

    return jsonify(response)

def fetch_by_customer_farm_greenhouse(customer_farm_greenhouse_id=None):
    response = []
    if customer_farm_greenhouse_id:
        greenhouse_shields = db\
            .session\
            .query(CustomerGreenhouseShield)\
            .filter(CustomerGreenhouseShield.customer_farm_greenhouse_id == customer_farm_greenhouse_id)\
            .filter(CustomerGreenhouseShield.status > config.STATUS_DELETED)\
            .all()
        for greenhouse_shield in greenhouse_shields:
            response = greenhouse_shield_obj(greenhouse_shield)

    return jsonify(response)

def fetch_by_customer_shield(customer_shield_id=None):
    response = []
    if customer_shield_id:
        greenhouse_shields = db\
            .session\
            .query(CustomerGreenhouseShield)\
            .filter(CustomerGreenhouseShield.customer_shield_id == customer_shield_id)\
            .filter(CustomerGreenhouseShield.status > config.STATUS_DELETED)\
            .all()
        for greenhouse_shield in greenhouse_shields:
            response = greenhouse_shield_obj(greenhouse_shield)

    return jsonify(response)

def add_new(greenhouse_shield_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_shield_data)
        if data:

            _latitude = None
            if 'latitude' in data:
                _latitude = data['latitude']

            _longitude = None
            if 'longitude' in data:
                _longitude = data['longitude']

            _growth_area = None
            if 'growth_area' in data:
                _growth_area = data['growth_area']

            _soil_type_id = None
            if valid_uuid(data['soil_type_id']):
                _soil_type_id = soil_types.fetch_one(data['soil_type_id'], True).get_json()['real_id']

            _customer_farm_greenhouse_id = None
            if valid_uuid(data['customer_farm_greenhouse_id']):
                _customer_farm_greenhouse_id = customer.farm_greenhouses.fetch_one(data['customer_farm_greenhouse_id'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_farm_greenhouse_id and _customer_shield_id and _creator_id:
                new_greenhouse_shield = CustomerGreenhouseShield(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    latitude = _latitude,
                    longitude = _longitude,
                    growth_area = _growth_area,
                    soil_type_id = _soil_type_id,
                    customer_farm_greenhouse_id = _customer_farm_greenhouse_id,
                    customer_shield_id = _customer_shield_id,
                    creator_id = _creator_id
                )
                db.session.add(new_greenhouse_shield)
                db.session.commit()
                if new_greenhouse_shield:
                    if return_obj:
                        return jsonify(fetch_one(new_greenhouse_shield.uid)), 200

                    return jsonify({"info": "Customer greenhouse shield added successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shield not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(greenhouse_shield_id, greenhouse_shield_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_shield_data)
        if data:

            if valid_uuid(greenhouse_shield_id):

                _latitude = None
                if 'latitude' in data:
                    _latitude = data['latitude']

                _longitude = None
                if 'longitude' in data:
                    _longitude = data['longitude']

                _growth_area = None
                if 'growth_area' in data:
                    _growth_area = data['growth_area']

                _soil_type_id = None
                if valid_uuid(data['soil_type_id']):
                    _soil_type_id = soil_types.fetch_one(data['soil_type_id'], True).get_json()['real_id']

                _greenhouse_shield = CustomerGreenhouseShield\
                    .query\
                    .filter(CustomerGreenhouseShield.uid == str(greenhouse_shield_id))\
                    .first()
                if _greenhouse_shield:
                    _greenhouse_shield.latitude = _latitude
                    _greenhouse_shield.longitude = _longitude
                    _greenhouse_shield.growth_area = _growth_area
                    _greenhouse_shield.growth_area = _growth_area
                    _greenhouse_shield.soil_type_id = _soil_type_id
                    db.session.commit()

                    return jsonify({"info": "Customer greenhouse shield edited successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shield not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(greenhouse_shield_id):

    try:
        if valid_uuid(greenhouse_shield_id) :
            _greenhouse_shield = CustomerGreenhouseShield\
                .query\
                .filter(CustomerGreenhouseShield.uid == greenhouse_shield_id)\
                .first()
            if _greenhouse_shield:
                _greenhouse_shield.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer greenhouse shield deactivated successfully!"}), 200

        return jsonify({"error": "Customer greenhouse shield not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
