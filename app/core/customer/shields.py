# customer_shield_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerShield

from app.core import customer
from app.core import shield
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def customer_shield_obj(customer_shield=False, real_id=False):
    _customer_shield = {}
    if customer_shield:
        if real_id:
            _customer_shield["real_id"] = customer_shield.id

        _customer_shield["id"] = str(customer_shield.uid)
        _customer_shield["shield"] = shield.fetch_by_id(customer_shield.shield_id).get_json()
        _customer_shield["customer"] = customer.fetch_by_id(customer_shield.customer_id).get_json()['uid']
        _customer_shield["created_by"] = creator_detail(customer_shield.creator_id)
        _customer_shield["status"] = status_name(customer_shield.status)
        _customer_shield["created"] = customer_shield.created_at
        _customer_shield["modified"] = customer_shield.modified_at

    return _customer_shield

def fetch_all():
    response = []

    customer_shields = db\
        .session\
        .query(CustomerShield)\
        .filter(CustomerShield.status > config.STATUS_DELETED)\
        .all()

    for customer_shield in customer_shields:
        response.append(customer_shield_obj(customer_shield))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer_shield = db\
            .session\
            .query(CustomerShield)\
            .filter(CustomerShield.uid == uid)\
            .filter(CustomerShield.status > config.STATUS_DELETED)\
            .first()
        if customer_shield:
            response = customer_shield_obj(customer_shield, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer_shield = db\
            .session\
            .query(CustomerShield)\
            .filter(CustomerShield.id == id)\
            .filter(CustomerShield.status > config.STATUS_DELETED)\
            .first()
        if customer_shield:
            response = customer_shield_obj(customer_shield)

    return jsonify(response)

def fetch_by_customer(customer_id=None):
    response = []
    if customer_id:
        customer_shields = db\
            .session\
            .query(CustomerShield)\
            .filter(CustomerShield.customer_id == customer_id)\
            .filter(CustomerShield.status > config.STATUS_DELETED)\
            .all()
        for customer_shield in customer_shields:
            response.append(customer_shield_obj(customer_shield))

    return jsonify(response)

def add_new(customer_shield_data=None, return_obj=False):

    try:
        data = json.loads(customer_shield_data)
        if data:

            _shield_id = None
            if valid_uuid(data['shield_id']):
                _shield_id = shield.fetch_one(data['shield_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _shield_id and _customer_id and _creator_id:
                new_customer_shield = CustomerShield(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    shield_id = _shield,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_customer_shield)
                db.session.commit()
                if new_customer_shield:
                    if return_obj:
                        return jsonify(fetch_one(new_customer_shield.uid)), 200

                    return jsonify({"info": "Customer shield added successfully!"}), 200

        return jsonify({"error": "Customer shield not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_shield_id):

    try:
        if valid_uuid(customer_shield_id) :
            _customer_shield = CustomerShield\
                .query\
                .filter(CustomerShield.uid == customer_shield_id)\
                .first()
            if _customer_shield:
                _customer_shield.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer shield deactivated successfully!"}), 200

        return jsonify({"error": "Customer shield not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
