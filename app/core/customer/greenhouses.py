# customer_greenhouse_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerGreenhouse

from app.core import customer
from app.core import greenhouse
from app.core import user


load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def customer_greenhouse_obj(customer_greenhouse=False, real_id=False):
    _customer_greenhouse = {}
    if customer_greenhouse:
        if real_id:
            _customer_greenhouse["real_id"] = customer_greenhouse.id

        _customer_greenhouse["id"] = str(customer_greenhouse.uid)
        _customer_greenhouse["greenhouse"] = greenhouse.fetch_by_id(customer_greenhouse.greenhouse_id).get_json()
        _customer_greenhouse["customer"] = customer.fetch_by_id(customer_greenhouse.customer_id).get_json()['uid']
        _customer_greenhouse["created_by"] = creator_detail(customer_greenhouse.creator_id)
        _customer_greenhouse["status"] = status_name(customer_greenhouse.status)
        _customer_greenhouse["created"] = customer_greenhouse.created_at
        _customer_greenhouse["modified"] = customer_greenhouse.modified_at

    return _customer_greenhouse

def fetch_all():
    response = []

    customer_greenhouses = db\
        .session\
        .query(CustomerGreenhouse)\
        .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
        .all()

    for customer_greenhouse in customer_greenhouses:
        response.append(customer_greenhouse_obj(customer_greenhouse))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer_greenhouse = db\
            .session\
            .query(CustomerGreenhouse)\
            .filter(CustomerGreenhouse.uid == uid)\
            .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
            .first()
        if customer_greenhouse:
            response = customer_greenhouse_obj(customer_greenhouse, real_id)

    return jsonify(response)

def fetch_by_customer(customer_id):
    response = {}
    if customer_id:
        customer_greenhouse = db\
            .session\
            .query(CustomerGreenhouse)\
            .filter(CustomerGreenhouse.customer_id == customer_id)\
            .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
            .first()
        if customer_greenhouse:
            response = customer_greenhouse_obj(customer_greenhouse)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer_greenhouse = db\
            .session\
            .query(CustomerGreenhouse)\
            .filter(CustomerGreenhouse.id == id)\
            .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
            .first()
        if customer_greenhouse:
            response = customer_greenhouse_obj(customer_greenhouse)

    return jsonify(response)

def add_new(customer_greenhouse_data=None, return_obj=False):

    try:
        data = json.loads(customer_greenhouse_data)
        if data:

            _greenhouse_id = None
            if valid_uuid(data['greenhouse_id']):
                _greenhouse_id = greenhouse.fetch_one(data['greenhouse_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _greenhouse_id and _customer_id and _creator_id:
                new_customer_greenhouse = CustomerGreenhouse(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    greenhouse_id = _greenhouse_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_customer_greenhouse)
                db.session.commit()
                if new_customer_greenhouse:
                    if return_obj:
                        return jsonify(fetch_one(new_customer_greenhouse.uid)), 200

                    return jsonify({"info": "Customer greenhouse added successfully!"}), 200

        return jsonify({"error": "Customer greenhouse not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_greenhouse_id):

    try:
        if valid_uuid(customer_greenhouse_id) :
            _customer_greenhouse = CustomerGreenhouse\
                .query\
                .filter(CustomerGreenhouse.uid == customer_greenhouse_id)\
                .first()
            if _customer_greenhouse:
                _customer_greenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer greenhouse deactivated successfully!"}), 200

        return jsonify({"error": "Customer greenhouse not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
