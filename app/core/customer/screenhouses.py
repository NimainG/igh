# customer_screenhouse_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerScreenhouse

from app.core import customer
from app.core import screenhouse
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def customer_screenhouse_obj(customer_screenhouse=False, real_id=False):
    _customer_screenhouse = {}
    if customer_screenhouse:
        if real_id:
            _customer_screenhouse["real_id"] = customer_screenhouse.id

        _customer_screenhouse["id"] = str(customer_screenhouse.uid)
        _customer_screenhouse["screenhouse"] = screenhouse.fetch_by_id(customer_screenhouse.screenhouse_id).get_json()
        _customer_screenhouse["customer"] = customer.fetch_by_id(customer_screenhouse.customer_id).get_json()['uid']
        _customer_screenhouse["created_by"] = creator_detail(customer_screenhouse.creator_id)
        _customer_screenhouse["status"] = status_name(customer_screenhouse.status)
        _customer_screenhouse["created"] = customer_screenhouse.created_at
        _customer_screenhouse["modified"] = customer_screenhouse.modified_at

    return _customer_screenhouse

def fetch_all():
    response = []

    customer_screenhouses = db\
        .session\
        .query(CustomerScreenhouse)\
        .filter(CustomerScreenhouse.status > config.STATUS_DELETED)\
        .all()

    for customer_screenhouse in customer_screenhouses:
        response.append(customer_screenhouse_obj(customer_screenhouse))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer_screenhouse = db\
            .session\
            .query(CustomerScreenhouse)\
            .filter(CustomerScreenhouse.uid == uid)\
            .filter(CustomerScreenhouse.status > config.STATUS_DELETED)\
            .first()
        if customer_screenhouse:
            response = customer_screenhouse_obj(customer_screenhouse, real_id)

    return jsonify(response)


def fetch_by_customer(customer_id):
    response = {}
    if customer_id:
        customer_screenhouse = db\
            .session\
            .query(CustomerScreenhouse)\
            .filter(CustomerScreenhouse.customer_id == customer_id)\
            .filter(CustomerScreenhouse.status > config.STATUS_DELETED)\
            .first()
        if customer_screenhouse:
            response = customer_screenhouse_obj(customer_screenhouse)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer_screenhouse = db\
            .session\
            .query(CustomerScreenhouse)\
            .filter(CustomerScreenhouse.id == id)\
            .filter(CustomerScreenhouse.status > config.STATUS_DELETED)\
            .first()
        if customer_screenhouse:
            response = customer_screenhouse_obj(customer_screenhouse)

    return jsonify(response)

def add_new(customer_screenhouse_data=None, return_obj=False):

    try:
        data = json.loads(customer_screenhouse_data)
        if data:

            _screenhouse_id = None
            if valid_uuid(data['screenhouse_id']):
                _screenhouse_id = screenhouse.fetch_one(data['screenhouse_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _screenhouse_id and _customer_id and _creator_id:
                new_customer_screenhouse = CustomerScreenhouse(
                    uid = uuid.uuid4(),
                    
                    status = config.STATUS_ACTIVE,
                    screenhouse_id = _screenhouse_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_customer_screenhouse)
                db.session.commit()
                if new_customer_screenhouse:
                    if return_obj:
                        return jsonify(fetch_one(new_customer_screenhouse.uid)), 200

                    return jsonify({"info": "Customer screenhouse added successfully!"}), 200

        return jsonify({"error": "Customer screenhouse not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_screenhouse_id):

    try:
        if valid_uuid(customer_screenhouse_id) :
            _customer_screenhouse = CustomerScreenhouse\
                .query\
                .filter(CustomerScreenhouse.uid == customer_screenhouse_id)\
                .first()
            if _customer_screenhouse:
                _customer_screenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer screenhouse deactivated successfully!"}), 200

        return jsonify({"error": "Customer screenhouse not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
