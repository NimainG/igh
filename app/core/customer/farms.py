# customer_farm_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import CustomerFarm

from app.core import customer
from app.core import farm
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def customer_farm_obj(customer_farm=False, real_id=False):
    _customer_farm = {}
    if customer_farm:
        if real_id:
            _customer_farm["real_id"] = customer_farm.id

        _customer_farm["id"] = str(customer_farm.uid)
        _customer_farm["farm"] = farm.fetch_by_id(customer_farm.farm_id).get_json()
        _customer_farm["created_by"] = creator_detail(customer_farm.creator_id)
        _customer_farm["status"] = status_name(customer_farm.status)
        _customer_farm["created"] = customer_farm.created_at
        _customer_farm["modified"] = customer_farm.modified_at

    return _customer_farm

def fetch_all():
    response = []

    customer_farms = db\
        .session\
        .query(CustomerFarm)\
        .filter(CustomerFarm.status > config.STATUS_DELETED)\
        .all()

    for customer_farm in customer_farms:
        response.append(customer_farm_obj(customer_farm))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer_farm = db\
            .session\
            .query(CustomerFarm)\
            .filter(CustomerFarm.uid == uid)\
            .filter(CustomerFarm.status > config.STATUS_DELETED)\
            .first()
        if customer_farm:
            response = customer_farm_obj(customer_farm, real_id)

    return jsonify(response)

def fetch_by_customer(customer_id):
    response = {}
    if customer_id:
        customer_farm = db\
            .session\
            .query(CustomerFarm)\
            .filter(CustomerFarm.customer_id == customer_id)\
            .filter(CustomerFarm.status > config.STATUS_DELETED)\
            .first()
        if customer_farm:
            response = customer_farm_obj(customer_farm)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer_farm = db\
            .session\
            .query(CustomerFarm)\
            .filter(CustomerFarm.id == id)\
            .filter(CustomerFarm.status > config.STATUS_DELETED)\
            .first()
        if customer_farm:
            response = customer_farm_obj(customer_farm)

    return jsonify(response)

def add_new(customer_farm_data=None, return_obj=False):

    try:
        data = json.loads(customer_farm_data)
        if data:

            _farm_id = None
            if valid_uuid(data['farm_id']):
                _farm_id = farm.fetch_one(data['farm_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _farm_id and _customer_id and _creator_id:
                new_customer_farm = CustomerFarm(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    farm_id = _farm_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_customer_farm)
                db.session.commit()
                if new_customer_farm:
                    if return_obj:
                        return jsonify(fetch_one(new_customer_farm.uid)), 200

                    return jsonify({"info": "Customer farm added successfully!"}), 200

        return jsonify({"error": "Customer farm not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_farm_id):

    try:
        if valid_uuid(customer_farm_id) :
            _customer_farm = CustomerFarm\
                .query\
                .filter(CustomerFarm.uid == customer_farm_id)\
                .first()
            if _customer_farm:
                _customer_farm.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer farm deactivated successfully!"}), 200

        return jsonify({"error": "Customer farm not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
