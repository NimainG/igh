import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Customer, IghUser, CustomerUser

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def get_igh_contact(id):
    _contact = {}

    if id:
        _igh_contact = IghUser\
            .query\
            .filter(IghUser.id == id)\
            .first()
        if _igh_contact:
            _contact = {
                "employee_id": _igh_contact.employee_id,
                "user": person_info(_igh_contact.user_id).get_json()
            }

    return jsonify(_contact)

def get_customer_contact(id):
    _contact = {}

    if id:
        _customer_contact = CustomerUser\
            .query\
            .filter(CustomerUser.id == id)\
            .first()
        if _customer_contact:
            _contact = {
                "employee_id": _customer_contact.employee_id,
                "user": person_info(_customer_contact.user_id).get_json()
            }

    return jsonify(_contact)


def customer_obj(customer=False, real_id=False):
    _customer = {}
    if customer:
        if real_id:
            _customer["real_id"] = customer.id

        _customer["id"] = str(customer.uid)
        _customer["name"] = customer.name
        _customer["billing_address"] = customer.billing_address
        _customer["shipping_address"] = customer.shipping_address
        _customer["igh_contact"] = get_igh_contact(customer.igh_contact_id).get_json()
        _customer["contact_person"] = get_customer_contact(customer.contact_person_id).get_json()
        _customer["created_by"] = creator_detail(customer.creator_id)
        _customer["status"] = status_name(customer.status)
        _customer["created"] = customer.created_at
        _customer["modified"] = customer.modified_at

    return _customer

def fetch_all():
    response = []

    customers = db\
        .session\
        .query(Customer)\
        .filter(Customer.status > config.STATUS_DELETED)\
        .all()

    for customer in customers:
        response.append(customer_obj(customer))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        customer = db\
            .session\
            .query(Customer)\
            .filter(Customer.uid == uid)\
            .filter(Customer.status > config.STATUS_DELETED)\
            .first()
        if customer:
            response = customer_obj(customer, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        customer = db\
            .session\
            .query(Customer)\
            .filter(Customer.id == id)\
            .filter(Customer.status > config.STATUS_DELETED)\
            .first()
        if customer:
            response = customer_obj(customer)

    return jsonify(response)

def add_new(customer_data=None, return_obj=False):

    try:
        data = json.loads(customer_data)
        if data:

            if 'name' in data and 'billing_address' in data and 'shipping_address'\
             in data and 'igh_contact_id' in data and 'creator_id' in data:

                _name = data['name']
                _billing_address = data['billing_address']
                _shipping_address = data['shipping_address']
                _igh_contact = None
                _creator = None

                if valid_uuid(data['igh_contact_id']):
                    _igh_contact = user.fetch_igh_user(data['igh_contact_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name and _billing_address and _shipping_address and \
                    _igh_contact and _creator:
                    new_customer = Customer(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        billing_address = _billing_address,
                        shipping_address = _shipping_address,
                        igh_contact_id = _igh_contact,
                        creator_id = _creator
                    )
                    db.session.add(new_customer)
                    db.session.commit()
                    if new_customer:
                        if return_obj:
                            return jsonify(fetch_one(new_customer.uid)), 200

                        return jsonify({"info": "Customer added successfully!"}), 200

        return jsonify({"error": "Customer not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(customer_id, customer_data=None, return_obj=False):

    try:
        data = json.loads(customer_data)
        if data:

            if valid_uuid(customer_id) and 'name' in data and 'billing_address' \
            in data and 'shipping_address' in data and 'igh_contact_id' in data:

                _name = data['name']
                _billing_address = data['billing_address']
                _shipping_address = data['shipping_address']
                _igh_contact = None

                if valid_uuid(data['igh_contact_id']):
                    _igh_contact = user.fetch_igh_user(data['igh_contact_id'], True).get_json()['real_id']

                if _name and _billing_address and _shipping_address and _igh_contact:
                    _customer = Customer\
                        .query\
                        .filter(Customer.uid == str(customer_id))\
                        .first()
                    if _customer:
                        _customer.name = _name
                        _customer.billing_address = _billing_address
                        _customer.shipping_address = _shipping_address
                        _customer.igh_contact_id = _igh_contact
                        db.session.commit()

                        return jsonify({"info": "Customer edited successfully!"}), 200

        return jsonify({"error": "Customer not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(customer_id):

    try:

        if valid_uuid(customer_id) :
            _customer = Customer\
                .query\
                .filter(Customer.uid == customer_id)\
                .first()
            if _customer:
                _customer.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Customer deactivated successfully!"}), 200

        return jsonify({"error": "Customer not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
