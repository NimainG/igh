import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import PestPhoto

from app.core import media
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def pest_photo_obj(pest_photo=False, real_id=False):
    _pest_photo = {}
    if pest_photo:
        if real_id:
            _pest_photo["real_id"] = pest_photo.id

        _pest_photo["id"] = str(pest_photo.uid)
        _pest_photo["photo"] = media.fetch_by_id(pest_photo.photo_media_id).get_json()
        _pest_photo["created_by"] = creator_detail(pest_photo.creator_id)
        _pest_photo["status"] = status_name(pest_photo.status)
        _pest_photo["created"] = pest_photo.created_at
        _pest_photo["modified"] = pest_photo.modified_at

    return _pest_photo

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        pest_photo = db\
            .session\
            .query(PestPhoto)\
            .filter(PestPhoto.uid == uid)\
            .filter(PestPhoto.status > config.STATUS_DELETED)\
            .first()
        if pest_photo:
            response = pest_photo_obj(pest_photo, real_id)

    return jsonify(response)

def fetch_by_pest(pest_id):
    response = {}
    if pest_id:
        pest_photo = db\
            .session\
            .query(PestPhoto)\
            .filter(PestPhoto.pest_id == pest_id)\
            .filter(PestPhoto.status > config.STATUS_DELETED)\
            .first()
        if pest_photo:
            response = pest_photo_obj(pest_photo)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        pest_photo = db\
            .session\
            .query(PestPhoto)\
            .filter(PestPhoto.id == id)\
            .filter(PestPhoto.status > config.STATUS_DELETED)\
            .first()
        if pest_photo:
            response = pest_photo_obj(pest_photo)

    return jsonify(response)

def add_new(pest_photo_data=None, return_obj=False):

    try:
        data = json.loads(pest_photo_data)
        if data:

            if 'photo_media_id' in data and 'pest_id' in data and 'creator_id' in data :

                _photo_media = None
                _pest_id = data['pest_id']
                _creator = None

                if valid_uuid(data['photo_media_id']):
                    _photo_media = media.fetch_one(data['photo_media_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _photo_media and _pest_id and _creator:
                    new_pest_photo = PestPhoto(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        photo_media_id = _photo_media,
                        pest_id = _pest_id,
                        creator_id = _creator
                    )
                    db.session.add(new_pest_photo)
                    db.session.commit()
                    if new_pest_photo:
                        if return_obj:
                            return jsonify(fetch_one(new_pest_photo.uid)), 200

                        return jsonify({"info": "Disease photo added successfully!"}), 200

        return jsonify({"error": "Disease photo not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(pest_photo_id):

    try:
        if valid_uuid(pest_photo_id) :
            _pest_photo = PestPhoto\
                .query\
                .filter(PestPhoto.uid == pest_photo_id)\
                .first()
            if _pest_photo:
                _pest_photo.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Disease photo deactivated successfully!"}), 200

        return jsonify({"error": "Disease photo not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
