import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import SupplyMovement

from app.core import supply
from app.core import media
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def movement_obj(movement=False, real_id=False):
    _movement = {}
    if movement:
        if real_id:
            _movement["real_id"] = movement.id

        _movement["id"] = str(movement.uid)
        _movement["movement"] = movement.movement
        _movement["supply"] = supply.fetch_by_id(movement.supplies_id).get_json()['uid']
        _movement["created_by"] = creator_detail(movement.creator_id)
        _movement["status"] = status_name(movement.status)
        _movement["created"] = movement.created_at
        _movement["modified"] = movement.modified_at

    return _movement

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        movement = db\
            .session\
            .query(SupplyMovement)\
            .filter(SupplyMovement.uid == uid)\
            .filter(SupplyMovement.status > config.STATUS_DELETED)\
            .first()
        if movement:
            response = movement_obj(movement, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        movement = db\
            .session\
            .query(SupplyMovement)\
            .filter(SupplyMovement.id == id)\
            .filter(SupplyMovement.status > config.STATUS_DELETED)\
            .first()
        if movement:
            response = movement_obj(movement)

    return jsonify(response)

def fetch_by_supply(supply_id):
    response = []
    if supply_id:
        movements = db\
            .session\
            .query(SupplyMovement)\
            .filter(SupplyMovement.supply_id == supply_id)\
            .filter(SupplyMovement.status > config.STATUS_DELETED)\
            .all()
        for movement in movements:
            response.append(movement_obj(movement))

    return jsonify(response)

def add_new(movement_data=None, return_obj=False):

    try:
        data = json.loads(movement_data)
        if data:

            _movement = None
            if 'movement' in data and data['movement']:
                _movement = data['movement']

            _supply_id = None
            if valid_uuid(data['supply_id']):
                _supply_id = supply.fetch_one(data['supply_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _movement and _supply_id and _creator_id:
                new_movement = SupplyMovement(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    movement = _movement,
                    supply_id = _supply_id,
                    creator_id = _creator_id
                )
                db.session.add(new_movement)
                db.session.commit()
                if new_movement:
                    if return_obj:
                        return jsonify(fetch_one(new_movement.uid)), 200

                    return jsonify({"info": "Supply movement added successfully!"}), 200

        return jsonify({"error": "Supply movement not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(movement_id):

    try:
        if valid_uuid(movement_id) :
            _movement = SupplyMovement\
                .query\
                .filter(SupplyMovement.uid == movement_id)\
                .first()
            if _movement:
                _movement.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Supply movement deactivated successfully!"}), 200

        return jsonify({"error": "Supply movement not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
