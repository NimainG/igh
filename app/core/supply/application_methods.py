import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import SupplyApplicationMethod

from app.core import user
from app.core import supply

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def application_method_obj(application_method=False, real_id=False):
    _application_method = {}
    if application_method:
        if real_id:
            _application_method["real_id"] = application_method.id

        _application_method["id"] = str(application_method.uid)
        _application_method["name"] = application_method.name
        _application_method["description"] = application_method.description
        _application_method["supply"] = supply.fetch_by_id(application_method.supply_id).get_json()['uid']
        _application_method["created_by"] = creator_detail(application_method.creator_id)
        _application_method["status"] = status_name(application_method.status)
        _application_method["created"] = application_method.created_at
        _application_method["modified"] = application_method.modified_at

    return _application_method

def fetch_all():
    response = []

    application_methods = db\
        .session\
        .query(SupplyApplicationMethod)\
        .filter(SupplyApplicationMethod.status > config.STATUS_DELETED)\
        .all()

    for application_method in application_methods:
        response.append(application_method_obj(application_method))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        application_method = db\
            .session\
            .query(SupplyApplicationMethod)\
            .filter(SupplyApplicationMethod.uid == uid)\
            .filter(SupplyApplicationMethod.status > config.STATUS_DELETED)\
            .first()
        if application_method:
            response = application_method_obj(application_method, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        application_method = db\
            .session\
            .query(SupplyApplicationMethod)\
            .filter(SupplyApplicationMethod.id == id)\
            .filter(SupplyApplicationMethod.status > config.STATUS_DELETED)\
            .first()
        if application_method:
            response = application_method_obj(application_method)

    return jsonify(response)

def fetch_by_supply(supply_id=None):
    response = []

    application_methods = db\
        .session\
        .query(SupplyApplicationMethod)\
        .filter(SupplyApplicationMethod.supply_id == supply_id)\
        .filter(SupplyApplicationMethod.status > config.STATUS_DELETED)\
        .all()

    for application_method in application_methods:
        response.append(application_method_obj(application_method))

    return jsonify(response)

def add_new(application_method_data=None, return_obj=False):

    try:
        data = json.loads(application_method_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _description = None
            if 'description' in data:
                _description = data['description']

            _supply_id = None
            if valid_uuid(data['supply_id']):
                _supply_id = supply.fetch_one(data['supply_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _supply_id and _creator_id:
                new_application_method = SupplyApplicationMethod(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    description = _description,
                    supply_id = _supply_id,
                    creator_id = _creator_id
                )
                db.session.add(new_application_method)
                db.session.commit()
                if new_application_method:
                    if return_obj:
                        return jsonify(fetch_one(new_application_method.uid)), 200

                    return jsonify({"info": "Supply application method added successfully!"}), 200

        return jsonify({"error": "Supply application method not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(application_method_id, application_method_data=None):

    try:
        data = json.loads(application_method_data)
        if data:

            if valid_uuid(application_method_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _description = None
                if 'description' in data:
                    _description = data['description']

                if _name and _supply_id:
                    _application_method = SupplyApplicationMethod\
                        .query\
                        .filter(SupplyApplicationMethod.uid == str(application_method_id))\
                        .first()
                    if _application_method:
                        _application_method.name = _name
                        _application_method.description = _description
                        _application_method.supply_id = _supply_id
                        db.session.commit()

                        return jsonify({"info": "Supply application method edited successfully!"}), 200

        return jsonify({"error": "Supply application method not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(application_method_id):

    try:

        if valid_uuid(application_method_id) :
            _application_method = SupplyApplicationMethod\
                .query\
                .filter(SupplyApplicationMethod.uid == application_method_id)\
                .first()
            if _application_method:
                _application_method.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Supply application method deactivated successfully!"}), 200

        return jsonify({"error": "Supply application method not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
