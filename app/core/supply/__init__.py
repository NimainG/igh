import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Supply

from app.core import user
from app.core import supply
from app.core import supplier
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def commodity_name(commodity_id=None):
    _commodity_name = ""

    if commodity_name == config.COMMODITY_FERTILIZERS:
        _commodity_name = "Fertilizer"

    if commodity_name == config.COMMODITY_AGROCHEMICALS:
        _commodity_name = "Agrochemical"

    return _commodity_name


def supply_obj(supply=False, real_id=False):
    _supply = {}
    if supply:
        if real_id:
            _supply["real_id"] = supply.id

        _supply["id"] = str(supply.uid)
        _supply["name"] = supply.name
        _supply["commodity"] = commodity_name(supply.commodity)
        _supply["metric"] = supply.metric
        _supply["quantity"] = supply.quantity
        _supply["minimum"] = supply.minimum
        _supply["notes"] = supply.notes
        _supply["active_component"] = supply.components.fetch_by_id(supply.active_component_id).get_json()
        _supply["supplier_commodity"] = supplier.commodities.fetch_by_id(supply.supplier_commodity_id).get_json()
        _supply["customer"] = customer.fetch_by_id(supply.customer_id).get_json()
        _supply["created_by"] = creator_detail(supply.creator_id)
        _supply["status"] = status_name(supply.status)
        _supply["created"] = supply.created_at
        _supply["modified"] = supply.modified_at

    return _supply

def fetch_all():
    response = []

    supplys = db\
        .session\
        .query(Supply)\
        .filter(Supply.status > config.STATUS_DELETED)\
        .all()

    for supply in supplys:
        response.append(supply_obj(supply))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        supply = db\
            .session\
            .query(Supply)\
            .filter(Supply.uid == uid)\
            .filter(Supply.status > config.STATUS_DELETED)\
            .first()
        if supply:
            response = supply_obj(supply, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        supply = db\
            .session\
            .query(Supply)\
            .filter(Supply.id == id)\
            .filter(Supply.status > config.STATUS_DELETED)\
            .first()
        if supply:
            response = supply_obj(supply)

    return jsonify(response)

def fetch_by_active_component(active_component_id=None):
    response = []

    supplys = db\
        .session\
        .query(Supply)\
        .filter(Supply.active_component_id == active_component_id)\
        .filter(Supply.status > config.STATUS_DELETED)\
        .all()

    for supply in supplys:
        response.append(supply_obj(supply))

    return jsonify(response)

def fetch_by_supplier_commodity(supplier_commodity_id=None):
    response = []

    supplys = db\
        .session\
        .query(Supply)\
        .filter(Supply.supplier_commodity_id == supplier_commodity_id)\
        .filter(Supply.status > config.STATUS_DELETED)\
        .all()

    for supply in supplys:
        response.append(supply_obj(supply))

    return jsonify(response)

def fetch_by_customer(customer_id=None):
    response = []

    supplys = db\
        .session\
        .query(Supply)\
        .filter(Supply.customer_id == customer_id)\
        .filter(Supply.status > config.STATUS_DELETED)\
        .all()

    for supply in supplys:
        response.append(supply_obj(supply))

    return jsonify(response)

def add_new(supply_data=None, return_obj=False):

    try:
        data = json.loads(supply_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _commodity = None
            if 'commodity' in data:
                _commodity = data['commodity']

            _metric = None
            if 'metric' in data:
                _metric = data['metric']

            _quantity = None
            if 'quantity' in data:
                _quantity = data['quantity']

            _minimum = None
            if 'minimum' in data:
                _minimum = data['minimum']

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _active_component_id = None
            if valid_uuid(data['active_component_id']):
                _active_component_id = supply.components.fetch_one(data['active_component_id'], True).get_json()['real_id']

            _supplier_commodity_id = None
            if valid_uuid(data['supplier_commodity_id']):
                _supplier_commodity_id = supplier.commodities.fetch_one(data['supplier_commodity_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _commodity and active_component_id and \
                supplier_commodity_id and _creator_id:
                new_supply = Supply(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    commodity = _commodity,
                    metric = _metric,
                    quantity = _quantity,
                    minimum = _minimum,
                    notes = _notes,
                    active_component_id = _active_component_id,
                    supplier_commodity_id = _supplier_commodity_id,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_supply)
                db.session.commit()
                if new_supply:
                    if return_obj:
                        return jsonify(fetch_one(new_supply.uid)), 200

                    return jsonify({"info": "Supply added successfully!"}), 200

        return jsonify({"error": "Supply not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(supply_id, supply_data=None, return_obj=False):

    try:
        data = json.loads(supply_data)
        if data:

            if valid_uuid(supply_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _commodity = None
                if 'commodity' in data:
                    _commodity = data['commodity']

                _metric = None
                if 'metric' in data:
                    _metric = data['metric']

                _quantity = None
                if 'quantity' in data:
                    _quantity = data['quantity']

                _minimum = None
                if 'minimum' in data:
                    _minimum = data['minimum']

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                _active_component_id = None
                if valid_uuid(data['active_component_id']):
                    _active_component_id = supply.components.fetch_one(data['active_component_id'], True).get_json()['real_id']

                _supplier_commodity_id = None
                if valid_uuid(data['supplier_commodity_id']):
                    _supplier_commodity_id = supplier.commodities.fetch_one(data['supplier_commodity_id'], True).get_json()['real_id']

                _customer_id = None
                if valid_uuid(data['customer_id']):
                    _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

                if _name and _commodity and active_component_id and supplier_commodity_id:
                    _supply = Supply\
                        .query\
                        .filter(Supply.uid == str(supply_id))\
                        .first()
                    if _supply:
                        _supply.name = _name
                        _supply.commodity = _commodity
                        _supply.metric = _metric
                        _supply.quantity = _quantity
                        _supply.minimum = _minimum
                        _supply.notes = _notes
                        _supply.active_component_id = _active_component_id
                        _supply.supplier_commodity_id = _supplier_commodity_id
                        _supply.customer_id = _customer_id
                        db.session.commit()

                        return jsonify({"info": "Supply edited successfully!"}), 200

        return jsonify({"error": "Supply not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(supply_id):

    try:

        if valid_uuid(supply_id) :
            _supply = Supply\
                .query\
                .filter(Supply.uid == supply_id)\
                .first()
            if _supply:
                _supply.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Supply deactivated successfully!"}), 200

        return jsonify({"error": "Supply not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
