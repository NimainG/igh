import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import SupplyPhoto

from app.core import supply
from app.core import media
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def supply_photo_obj(supply_photo=False, real_id=False):
    _supply_photo = {}
    if supply_photo:
        if real_id:
            _supply_photo["real_id"] = supply_photo.id

        _supply_photo["id"] = str(supply_photo.uid)
        _supply_photo["photo"] = media.fetch_by_id(supply_photo.photo_media_id).get_json()
        _supply_photo["supply"] = supply.fetch_by_id(supply_photo.supplies_id).get_json()['uid']
        _supply_photo["created_by"] = creator_detail(supply_photo.creator_id)
        _supply_photo["status"] = status_name(supply_photo.status)
        _supply_photo["created"] = supply_photo.created_at
        _supply_photo["modified"] = supply_photo.modified_at

    return _supply_photo

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        supply_photo = db\
            .session\
            .query(SupplyPhoto)\
            .filter(SupplyPhoto.uid == uid)\
            .filter(SupplyPhoto.status > config.STATUS_DELETED)\
            .first()
        if supply_photo:
            response = supply_photo_obj(supply_photo, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        supply_photo = db\
            .session\
            .query(SupplyPhoto)\
            .filter(SupplyPhoto.id == id)\
            .filter(SupplyPhoto.status > config.STATUS_DELETED)\
            .first()
        if supply_photo:
            response = supply_photo_obj(supply_photo)

    return jsonify(response)

def fetch_by_supply(supply_id):
    response = []
    if supply_id:
        supply_photos = db\
            .session\
            .query(SupplyPhoto)\
            .filter(SupplyPhoto.supply_id == supply_id)\
            .filter(SupplyPhoto.status > config.STATUS_DELETED)\
            .all()
        for supply_photo in supply_photos:
            response.append(supply_photo_obj(supply_photo))

    return jsonify(response)

def add_new(supply_photo_data=None, return_obj=False):

    try:
        data = json.loads(supply_photo_data)
        if data:

            _photo_media_id = None
            if valid_uuid(data['photo_media_id']):
                _photo_media_id = media.fetch_one(data['photo_media_id'], True).get_json()['real_id']

            _supply_id = None
            if valid_uuid(data['supply_id']):
                _supply_id = supply.fetch_one(data['supply_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _photo_media_id and _supply_id and _creator_id:
                new_supply_photo = SupplyPhoto(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    photo_media_id = _photo_media_id,
                    supply_id = _supply_id,
                    creator_id = _creator_id
                )
                db.session.add(new_supply_photo)
                db.session.commit()
                if new_supply_photo:
                    if return_obj:
                        return jsonify(fetch_one(new_supply_photo.uid)), 200

                    return jsonify({"info": "Supply photo added successfully!"}), 200

        return jsonify({"error": "Supply photo not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(supply_photo_id):

    try:
        if valid_uuid(supply_photo_id) :
            _supply_photo = SupplyPhoto\
                .query\
                .filter(SupplyPhoto.uid == supply_photo_id)\
                .first()
            if _supply_photo:
                _supply_photo.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Supply photo deactivated successfully!"}), 200

        return jsonify({"error": "Supply photo not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
