import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Spear

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def spear_obj(spear=False, real_id=False):
    _spear = {}
    if spear:
        if real_id:
            _spear["real_id"] = spear.id

        _spear["id"] = str(spear.uid)
        _spear["serial"] = spear.serial
        _spear["mac_address"] = spear.mac_address
        _spear["created_by"] = creator_detail(spear.creator_id)
        _spear["status"] = status_name(spear.status)
        _spear["created"] = spear.created_at
        _spear["modified"] = spear.modified_at

    return _spear

def fetch_all():
    response = []

    spears = db\
        .session\
        .query(Spear)\
        .filter(Spear.status > config.STATUS_DELETED)\
        .all()

    for spear in spears:
        response.append(spear_obj(spear))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        spear = db\
            .session\
            .query(Spear)\
            .filter(Spear.uid == uid)\
            .filter(Spear.status > config.STATUS_DELETED)\
            .first()
        if spear:
            response = spear_obj(spear, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        spear = db\
            .session\
            .query(Spear)\
            .filter(Spear.id == id)\
            .filter(Spear.status > config.STATUS_DELETED)\
            .first()
        if spear:
            response = spear_obj(spear)

    return jsonify(response)

def add_new(spear_data=None, return_obj=False):

    try:
        data = json.loads(spear_data)
        if data:

            if 'serial' in data and 'mac_address' in data and 'creator_id' in data:

                _serial = data['serial']
                _mac_address = data['mac_address']

                _creator = None

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _serial and _mac_address and _creator:
                    new_spear = Spear(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        serial = _serial,
                        mac_address = _mac_address,
                        creator_id = _creator
                    )
                    db.session.add(new_spear)
                    db.session.commit()
                    if new_spear:
                        if return_obj:
                            return jsonify(fetch_one(new_spear.uid)), 200

                        return jsonify({"info": "Spear added successfully!"}), 200

        return jsonify({"error": "Spear not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(spear_id, spear_data=None, return_obj=False):

    try:
        data = json.loads(spear_data)
        if data:

            if valid_uuid(spear_id) and 'serial' in data and 'mac_address' in data:

                _serial = data['serial']
                _mac_address = data['mac_address']

                if _serial and _mac_address:
                    _spear = Spear\
                        .query\
                        .filter(Spear.uid == str(spear_id))\
                        .first()
                    if _spear:
                        # _spear.size = _size
                        _spear.serial = _serial
                        _spear.mac_address = _mac_address
                        db.session.commit()

                        return jsonify({"info": "Spear edited successfully!"}), 200

        return jsonify({"error": "Spear not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(spear_id):

    try:

        if valid_uuid(spear_id) :
            _spear = Spear\
                .query\
                .filter(Spear.uid == spear_id)\
                .first()
            if _spear:
                _spear.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Spear deactivated successfully!"}), 200

        return jsonify({"error": "Spear not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
