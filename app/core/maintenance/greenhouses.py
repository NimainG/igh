import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import GreenhouseMaintenance

from app.core import user
from app.core import maintenance
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouse_obj(greenhouse=False, real_id=False):
    _greenhouse = {}
    if greenhouse:
        if real_id:
            _greenhouse["real_id"] = greenhouse.id

        _greenhouse["id"] = str(greenhouse.uid)
        _greenhouse["notes"] = greenhouse.notes
        _greenhouse["customer_greenhouse"] = customer.greenhouses.fetch_by_id(greenhouse.customer_greenhouse_id).get_json()
        _greenhouse["customer"] = customer.fetch_by_id(greenhouse.customer_id).get_json()
        _greenhouse["issues"] = maintenance.greenhouse_issues.fetch_by_greenhouse_maintenance(greenhouse.id).get_json()
        _greenhouse["created_by"] = creator_detail(greenhouse.creator_id)
        _greenhouse["status"] = status_name(greenhouse.status)
        _greenhouse["created"] = greenhouse.created_at
        _greenhouse["modified"] = greenhouse.modified_at

    return _greenhouse

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        greenhouse = db\
            .session\
            .query(GreenhouseMaintenance)\
            .filter(GreenhouseMaintenance.uid == uid)\
            .filter(GreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        greenhouse = db\
            .session\
            .query(GreenhouseMaintenance)\
            .filter(GreenhouseMaintenance.id == id)\
            .filter(GreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse)

    return jsonify(response)

def fetch_by_customer_greenhouse(id=0):
    response = {}
    if id:
        greenhouse = db\
            .session\
            .query(GreenhouseMaintenance)\
            .filter(GreenhouseMaintenance.customer_greenhouse_id == id)\
            .filter(GreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        greenhouse = db\
            .session\
            .query(GreenhouseMaintenance)\
            .filter(GreenhouseMaintenance.maintenance_id == id)\
            .filter(GreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        greenhouse = db\
            .session\
            .query(GreenhouseMaintenance)\
            .filter(GreenhouseMaintenance.customer_id == id)\
            .filter(GreenhouseMaintenance.status > config.STATUS_DELETED)\
            .first()
        if greenhouse:
            response = greenhouse_obj(greenhouse)

    return jsonify(response)

def add_new(greenhouse_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _customer_greenhouse_id = None
            if valid_uuid(data['customer_greenhouse_id']):
                _customer_greenhouse_id = customer.greenhouses.fetch_one(data['customer_greenhouse_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_greenhouse_id and _maintenance_id and _customer_id and _creator:
                new_greenhouse = GreenhouseMaintenance(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    customer_greenhouse_id = _customer_greenhouse_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_greenhouse)
                db.session.commit()
                if new_greenhouse:
                    if return_obj:
                        return jsonify(fetch_one(new_greenhouse.uid)), 200

                    return jsonify({"info": "Greenhouse maintenance added successfully!"}), 200

        return jsonify({"error": "Greenhouse maintenance not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(greenhouse_id, greenhouse_data=None, return_obj=False):

    try:
        data = json.loads(greenhouse_data)
        if data:

            if valid_uuid(greenhouse_id):

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                if _notes:
                    _greenhouse = GreenhouseMaintenance\
                        .query\
                        .filter(GreenhouseMaintenance.uid == str(greenhouse_id))\
                        .first()
                    if _greenhouse:
                        _greenhouse.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Greenhouse maintenance edited successfully!"}), 200

        return jsonify({"error": "Greenhouse maintenance not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(greenhouse_id):

    try:
        if valid_uuid(greenhouse_id) :
            _greenhouse = GreenhouseMaintenance\
                .query\
                .filter(GreenhouseMaintenance.uid == greenhouse_id)\
                .first()
            if _greenhouse:
                _greenhouse.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Greenhouse maintenance deactivated successfully!"}), 200

        return jsonify({"error": "Greenhouse maintenance not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
