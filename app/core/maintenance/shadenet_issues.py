import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import ShadenetMaintenanceIssue

from app.core import user
from app.core import maintenance
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shadenet_issue_obj(shadenet_issue=False, real_id=False):
    _shadenet_issue = {}
    if shadenet_issue:
        if real_id:
            _shadenet_issue["real_id"] = shadenet_issue.id

        _shadenet_issue["id"] = str(shadenet_issue.uid)
        _shadenet_issue["issue"] = maintenance.issues.fetch_by_id(shadenet_issue.issue_id).get_json()
        _shadenet_issue["shadenet_maintenance"] = maintenance.shadenets.fetch_by_id(shadenet_issue.shadenet_maintenance_id).get_json()['uid']
        _shadenet_issue["maintenance"] = maintenance.fetch_by_id(shadenet_issue.maintenance_id).get_json()['uid']
        _shadenet_issue["customer"] = customer.fetch_by_id(shadenet_issue.customer_id).get_json()['uid']
        _shadenet_issue["created_by"] = creator_detail(shadenet_issue.creator_id)
        _shadenet_issue["status"] = status_name(shadenet_issue.status)
        _shadenet_issue["created"] = shadenet_issue.created_at
        _shadenet_issue["modified"] = shadenet_issue.modified_at

    return _shadenet_issue

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shadenet_issue = db\
            .session\
            .query(ShadenetMaintenanceIssue)\
            .filter(ShadenetMaintenanceIssue.uid == uid)\
            .filter(ShadenetMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shadenet_issue:
            response = shadenet_issue_obj(shadenet_issue, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shadenet_issue = db\
            .session\
            .query(ShadenetMaintenanceIssue)\
            .filter(ShadenetMaintenanceIssue.id == id)\
            .filter(ShadenetMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shadenet_issue:
            response = shadenet_issue_obj(shadenet_issue)

    return jsonify(response)

def fetch_by_shadenet_maintenance(id=0):
    response = {}
    if id:
        shadenet_issue = db\
            .session\
            .query(ShadenetMaintenanceIssue)\
            .filter(ShadenetMaintenanceIssue.shadenet_maintenance_id == id)\
            .filter(ShadenetMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shadenet_issue:
            response = shadenet_issue_obj(shadenet_issue)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        shadenet_issue = db\
            .session\
            .query(ShadenetMaintenanceIssue)\
            .filter(ShadenetMaintenanceIssue.maintenance_id == id)\
            .filter(ShadenetMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shadenet_issue:
            response = shadenet_issue_obj(shadenet_issue)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        shadenet_issue = db\
            .session\
            .query(ShadenetMaintenanceIssue)\
            .filter(ShadenetMaintenanceIssue.customer_id == id)\
            .filter(ShadenetMaintenanceIssue.status > config.STATUS_DELETED)\
            .first()
        if shadenet_issue:
            response = shadenet_issue_obj(shadenet_issue)

    return jsonify(response)

def add_new(shadenet_issue_data=None, return_obj=False):

    try:
        data = json.loads(shadenet_issue_data)
        if data:

            _issue_id = None
            if valid_uuid(data['issue_id']):
                _issue_id = customer.shadenet_issues.fetch_one(data['issue_id'], True).get_json()['real_id']

            _shadenet_maintenance_id = None
            if valid_uuid(data['shadenet_maintenance_id']):
                _shadenet_maintenance_id = maintenance.fetch_one(data['shadenet_maintenance_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _issue_id and _shadenet_maintenance_id and _maintenance_id and _customer_id and _creator:
                new_shadenet_issue = ShadenetMaintenanceIssue(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    issue_id = _issue_id,
                    shadenet_maintenance_id = _shadenet_maintenance_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_shadenet_issue)
                db.session.commit()
                if new_shadenet_issue:
                    if return_obj:
                        return jsonify(fetch_one(new_shadenet_issue.uid)), 200

                    return jsonify({"info": "Shadenet maintenance issue added successfully!"}), 200

        return jsonify({"error": "Shadenet maintenance issue not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shadenet_issue_id, shadenet_issue_data=None, return_obj=False):

    try:
        data = json.loads(shadenet_issue_data)
        if data:

            if valid_uuid(shadenet_issue_id):

            _issue_id = None
                if valid_uuid(data['issue_id']):
                    _issue_id = customer.shadenet_issues.fetch_one(data['issue_id'], True).get_json()['real_id']

                _shadenet_maintenance_id = None
                if valid_uuid(data['shadenet_maintenance_id']):
                    _shadenet_maintenance_id = maintenance.fetch_one(data['shadenet_maintenance_id'], True).get_json()['real_id']

                if _issue_id and _shadenet_maintenance_id:
                    _shadenet_issue = ShadenetMaintenanceIssue\
                        .query\
                        .filter(ShadenetMaintenanceIssue.uid == str(shadenet_issue_id))\
                        .first()
                    if _shadenet_issue:
                        _shadenet_issue.issue_id = _issue_id
                        _shadenet_issue.shadenet_maintenance_id = _shadenet_maintenance_id
                        db.session.commit()

                        return jsonify({"info": "Shadenet maintenance issue edited successfully!"}), 200

        return jsonify({"error": "Shadenet maintenance issue not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shadenet_issue_id):

    try:
        if valid_uuid(shadenet_issue_id) :
            _shadenet_issue = ShadenetMaintenanceIssue\
                .query\
                .filter(ShadenetMaintenanceIssue.uid == shadenet_issue_id)\
                .first()
            if _shadenet_issue:
                _shadenet_issue.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shadenet maintenance issue deactivated successfully!"}), 200

        return jsonify({"error": "Shadenet maintenance issue not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
