import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import PestDisease

from app.core import user
from app.core import maintenance
from app.core import planting
from app.core import customer
from app.core import pest
from app.core import disease

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def disease_obj(disease=False, real_id=False):
    _disease = {}
    if disease:
        if real_id:
            _disease["real_id"] = disease.id

        _disease["id"] = str(disease.uid)
        _disease["notes"] = disease.notes
        _disease["maintenance_id"] = maintenance.fetch_by_id(disease.maintenance_id).get_json()['uid']
        _disease["pest"] = pest.fetch_by_id(disease.pest_id).get_json()
        _disease["disease"] = disease.fetch_by_id(disease.disease_id).get_json()
        _disease["production_stage"] = planting.production_stages.fetch_by_id(disease.production_stage_id).get_json()
        _disease["variety"] = planting.varieties.fetch_by_id(disease.variety_id).get_json()
        _disease["plant"] = planting.plants.fetch_by_id(disease.plant_id).get_json()
        _disease["customer"] = customer.fetch_by_id(disease.customer_id).get_json()
        _disease["customer_greenhouse"] = customer.greenhouses.fetch_by_id(disease.customer_greenhouse_id).get_json()
        _disease["customer_shield"] = customer.shields.fetch_by_id(disease.customer_shields_id).get_json()
        _disease["created_by"] = creator_detail(disease.creator_id)
        _disease["status"] = status_name(disease.status)
        _disease["created"] = disease.created_at
        _disease["modified"] = disease.modified_at

    return _disease

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        disease = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.uid == uid)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .first()
        if disease:
            response = disease_obj(disease, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        disease = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .first()
        if disease:
            response = disease_obj(disease)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        disease = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.maintenance_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .first()
        if disease:
            response = disease_obj(disease)

    return jsonify(response)

def fetch_by_production_stage(id=0):
    response = []
    if id:
        diseases = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.production_stage_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .all()
        for disease in diseases:
            response.append(disease_obj(disease))

    return jsonify(response)

def fetch_by_variety(id=0):
    response = []
    if id:
        diseases = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.variety_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .all()
        for disease in diseases:
            response.append(disease_obj(disease))

    return jsonify(response)

def fetch_by_plant(id=0):
    response = []
    if id:
        diseases = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.plant_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .all()
        for disease in diseases:
            response.append(disease_obj(disease))

    return jsonify(response)

def fetch_by_customer_greenhouse(id=0):
    response = []
    if id:
        diseases = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.customer_greenhouse_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .all()
        for disease in diseases:
            response.append(disease_obj(disease))

    return jsonify(response)

def fetch_by_customer_shield(id=0):
    response = []
    if id:
        diseases = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.customer_shield_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .all()
        for disease in diseases:
            response.append(disease_obj(disease))

    return jsonify(response)

def fetch_by_disease(id=0):
    response = []
    if id:
        diseases = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.disease_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .all()
        for disease in diseases:
            response.append(disease_obj(disease))

    return jsonify(response)

def fetch_by_customer(id=0):
    response = []
    if id:
        diseases = db\
            .session\
            .query(PestDisease)\
            .filter(PestDisease.customer_id == id)\
            .filter(PestDisease.status > config.STATUS_DELETED)\
            .all()
        for disease in diseases:
            response.append(disease_obj(disease))

    return jsonify(response)

def add_new(disease_data=None, return_obj=False):

    try:
        data = json.loads(disease_data)
        if data:

            _notes = None
            if 'notes' in data and data['notes']:
                _notes = data['notes']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _pest_id = None
            if valid_uuid(data['pest_id']):
                _pest_id = pest.fetch_one(data['pest_id'], True).get_json()['real_id']

            _disease_id = None
            if valid_uuid(data['disease_id']):
                _disease_id = disease.fetch_one(data['disease_id'], True).get_json()['real_id']

            _production_stage_id = None
            if valid_uuid(data['production_stage_id']):
                _production_stage_id = planting.production_stages.fetch_one(data['production_stage_id'], True).get_json()['real_id']

            _variety_id = None
            if valid_uuid(data['variety_id']):
                _variety_id = planting.varieties.fetch_one(data['variety_id'], True).get_json()['real_id']

            _plant_id = None
            if valid_uuid(data['plant_id']):
                _plant_id = planting.plant.fetch_one(data['plant_id'], True).get_json()['real_id']

            _customer_greenhouse_id = None
            if valid_uuid(data['customer_greenhouse_id']):
                _customer_greenhouse_id = customer.greenhouses.fetch_one(data['customer_greenhouse_id'], True).get_json()['real_id']

            _customer_shield_id = None
            if valid_uuid(data['customer_shield_id']):
                _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _maintenance_id and _production_stage_id and \
                _variety_id and _plant_id and _customer_id and _creator:
                new_disease = PestDisease(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    maintenance_id = _maintenance_id,
                    production_stage_id = _production_stage_id,
                    variety_id = _variety_id,
                    plant_id = _plant_id,
                    customer_greenhouse_id = _customer_greenhouse_id,
                    customer_shield_id = _customer_shield_id,
                    pest_id = _pest_id,
                    disease_id = _disease_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_disease)
                db.session.commit()
                if new_disease:
                    if return_obj:
                        return jsonify(fetch_one(new_disease.uid)), 200

                    return jsonify({"info": "Disease added successfully!"}), 200

        return jsonify({"error": "Disease not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(disease_id, disease_data=None, return_obj=False):

    try:
        data = json.loads(disease_data)
        if data:

            if valid_uuid(disease_id):

                _notes = None
                if 'notes' in data and data['notes']:
                    _notes = data['notes']

                _pest_id = None
                if valid_uuid(data['pest_id']):
                    _pest_id = pest.fetch_one(data['pest_id'], True).get_json()['real_id']

                _disease_id = None
                if valid_uuid(data['disease_id']):
                    _disease_id = disease.fetch_one(data['disease_id'], True).get_json()['real_id']

                _production_stage_id = None
                if valid_uuid(data['production_stage_id']):
                    _production_stage_id = planting.production_stages.fetch_one(data['production_stage_id'], True).get_json()['real_id']

                _variety_id = None
                if valid_uuid(data['variety_id']):
                    _variety_id = planting.varieties.fetch_one(data['variety_id'], True).get_json()['real_id']

                _plant_id = None
                if valid_uuid(data['plant_id']):
                    _plant_id = planting.plant.fetch_one(data['plant_id'], True).get_json()['real_id']

                _customer_greenhouse_id = None
                if valid_uuid(data['customer_greenhouse_id']):
                    _customer_greenhouse_id = customer.greenhouses.fetch_one(data['customer_greenhouse_id'], True).get_json()['real_id']

                _customer_shield_id = None
                if valid_uuid(data['customer_shield_id']):
                    _customer_shield_id = customer.shields.fetch_one(data['customer_shield_id'], True).get_json()['real_id']

                _customer_id = None
                if valid_uuid(data['customer_id']):
                    _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

                if _maintenance_id and _production_stage_id and \
                    _variety_id and _plant_id and _customer_id:
                    _disease = PestDisease\
                        .query\
                        .filter(PestDisease.uid == str(disease_id))\
                        .first()
                    if _disease:
                        _disease.notes = _notes
                        _disease.production_stage_id = _production_stage_id
                        _disease.variety_id = _variety_id
                        _disease.plant_id = _plant_id
                        _disease.customer_greenhouse_id = _customer_greenhouse_id
                        _disease.customer_shield_id = _customer_shield_id
                        _disease.pest_id = _pest_id
                        _disease.disease_id = _disease_id
                        db.session.commit()

                        return jsonify({"info": "Disease edited successfully!"}), 200

        return jsonify({"error": "Disease not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(disease_id):

    try:
        if valid_uuid(disease_id) :
            _disease = PestDisease\
                .query\
                .filter(PestDisease.uid == disease_id)\
                .first()
            if _disease:
                _disease.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Disease deactivated successfully!"}), 200

        return jsonify({"error": "Disease not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
