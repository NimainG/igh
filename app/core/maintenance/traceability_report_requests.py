import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import TraceabilityReportRequest

from app.core import user
from app.core import maintenance
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def trr_obj(trr=False, real_id=False):
    _trr = {}
    if trr:
        if real_id:
            _trr["real_id"] = trr.id

        _trr["id"] = str(trr.uid)
        _trr["notes"] = trr.notes
        _trr["crop"] = maintenance.traceability_report_crop.fetch_by_traceability_report_request(trr.id).get_json()
        _trr["maintenance"] = maintenance.fetch_by_id(trr.maintenance_id).get_json()['uid']
        _trr["customer"] = customer.fetch_by_id(trr.customer_id).get_json()['uid']
        _trr["created_by"] = creator_detail(trr.creator_id)
        _trr["status"] = status_name(trr.status)
        _trr["created"] = trr.created_at
        _trr["modified"] = trr.modified_at

    return _trr

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        trr = db\
            .session\
            .query(TraceabilityReportRequest)\
            .filter(TraceabilityReportRequest.uid == uid)\
            .filter(TraceabilityReportRequest.status > config.STATUS_DELETED)\
            .first()
        if trr:
            response = trr_obj(trr, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        trr = db\
            .session\
            .query(TraceabilityReportRequest)\
            .filter(TraceabilityReportRequest.id == id)\
            .filter(TraceabilityReportRequest.status > config.STATUS_DELETED)\
            .first()
        if trr:
            response = trr_obj(trr)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        trr = db\
            .session\
            .query(TraceabilityReportRequest)\
            .filter(TraceabilityReportRequest.maintenance_id == id)\
            .filter(TraceabilityReportRequest.status > config.STATUS_DELETED)\
            .first()
        if trr:
            response = trr_obj(trr)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        trr = db\
            .session\
            .query(TraceabilityReportRequest)\
            .filter(TraceabilityReportRequest.customer_id == id)\
            .filter(TraceabilityReportRequest.status > config.STATUS_DELETED)\
            .first()
        if trr:
            response = trr_obj(trr)

    return jsonify(response)

def add_new(trr_data=None, return_obj=False):

    try:
        data = json.loads(trr_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _maintenance_id and _customer_id and _creator:

                if _name and _applies_to and _creator:
                    new_trr = TraceabilityReportRequest(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        notes = _notes,
                        maintenance_id = _maintenance_id,
                        customer_id = _customer_id,
                        creator_id = _creator
                    )
                    db.session.add(new_trr)
                    db.session.commit()
                    if new_trr:
                        if return_obj:
                            return jsonify(fetch_one(new_trr.uid)), 200

                        return jsonify({"info": "Traceability report request added successfully!"}), 200

        return jsonify({"error": "Traceability report request not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(trr_id, trr_data=None, return_obj=False):

    try:
        data = json.loads(trr_data)
        if data:

            if valid_uuid(trr_id) :

                _notes
                if 'notes' in data:
                    _notes = data['notes']

                if _notes:
                    _trr = TraceabilityReportRequest\
                        .query\
                        .filter(TraceabilityReportRequest.uid == str(trr_id))\
                        .first()
                    if _trr:
                        _trr.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Traceability report request edited successfully!"}), 200

        return jsonify({"error": "Traceability report request not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(trr_id):

    try:
        if valid_uuid(trr_id) :
            _trr = TraceabilityReportRequest\
                .query\
                .filter(TraceabilityReportRequest.uid == trr_id)\
                .first()
            if _trr:
                _trr.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Traceability report request deactivated successfully!"}), 200

        return jsonify({"error": "Traceability report request not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
