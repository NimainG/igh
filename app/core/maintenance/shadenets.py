import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import ShadenetMaintenance

from app.core import user
from app.core import maintenance
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def shadenet_obj(shadenet=False, real_id=False):
    _shadenet = {}
    if shadenet:
        if real_id:
            _shadenet["real_id"] = shadenet.id

        _shadenet["id"] = str(shadenet.uid)
        _shadenet["notes"] = shadenet.notes
        _shadenet["customer_shadenet"] = customer.shadenets.fetch_by_id(shadenet.customer_shadenet_id).get_json()
        _shadenet["customer"] = customer.fetch_by_id(shadenet.customer_id).get_json()
        _shadenet["issues"] = maintenance.shadenet_issues.fetch_by_shadenet_maintenance(shadenet.id).get_json()
        _shadenet["created_by"] = creator_detail(shadenet.creator_id)
        _shadenet["status"] = status_name(shadenet.status)
        _shadenet["created"] = shadenet.created_at
        _shadenet["modified"] = shadenet.modified_at

    return _shadenet

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        shadenet = db\
            .session\
            .query(ShadenetMaintenance)\
            .filter(ShadenetMaintenance.uid == uid)\
            .filter(ShadenetMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shadenet:
            response = shadenet_obj(shadenet, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        shadenet = db\
            .session\
            .query(ShadenetMaintenance)\
            .filter(ShadenetMaintenance.id == id)\
            .filter(ShadenetMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shadenet:
            response = shadenet_obj(shadenet)

    return jsonify(response)

def fetch_by_customer_shadenet(id=0):
    response = {}
    if id:
        shadenet = db\
            .session\
            .query(ShadenetMaintenance)\
            .filter(ShadenetMaintenance.customer_shadenet_id == id)\
            .filter(ShadenetMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shadenet:
            response = shadenet_obj(shadenet)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        shadenet = db\
            .session\
            .query(ShadenetMaintenance)\
            .filter(ShadenetMaintenance.maintenance_id == id)\
            .filter(ShadenetMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shadenet:
            response = shadenet_obj(shadenet)

    return jsonify(response)

def fetch_by_customer(id=0):
    response = {}
    if id:
        shadenet = db\
            .session\
            .query(ShadenetMaintenance)\
            .filter(ShadenetMaintenance.customer_id == id)\
            .filter(ShadenetMaintenance.status > config.STATUS_DELETED)\
            .first()
        if shadenet:
            response = shadenet_obj(shadenet)

    return jsonify(response)

def add_new(shadenet_data=None, return_obj=False):

    try:
        data = json.loads(shadenet_data)
        if data:

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _customer_shadenet_id = None
            if valid_uuid(data['customer_shadenet_id']):
                _customer_shadenet_id = customer.shadenets.fetch_one(data['customer_shadenet_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _customer_shadenet_id and _maintenance_id and _customer_id and _creator:
                new_shadenet = ShadenetMaintenance(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    notes = _notes,
                    customer_shadenet_id = _customer_shadenet_id,
                    maintenance_id = _maintenance_id,
                    customer_id = _customer_id,
                    creator_id = _creator
                )
                db.session.add(new_shadenet)
                db.session.commit()
                if new_shadenet:
                    if return_obj:
                        return jsonify(fetch_one(new_shadenet.uid)), 200

                    return jsonify({"info": "Shadenet maintenance added successfully!"}), 200

        return jsonify({"error": "Shadenet maintenance not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(shadenet_id, shadenet_data=None, return_obj=False):

    try:
        data = json.loads(shadenet_data)
        if data:

            if valid_uuid(shadenet_id):

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                if _notes:
                    _shadenet = ShadenetMaintenance\
                        .query\
                        .filter(ShadenetMaintenance.uid == str(shadenet_id))\
                        .first()
                    if _shadenet:
                        _shadenet.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Shadenet maintenance edited successfully!"}), 200

        return jsonify({"error": "Shadenet maintenance not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(shadenet_id):

    try:
        if valid_uuid(shadenet_id) :
            _shadenet = ShadenetMaintenance\
                .query\
                .filter(ShadenetMaintenance.uid == shadenet_id)\
                .first()
            if _shadenet:
                _shadenet.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Shadenet maintenance deactivated successfully!"}), 200

        return jsonify({"error": "Shadenet maintenance not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
