import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import MaintenancePhoto

from app.core import media
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def maintenance_photo_obj(maintenance_photo=False, real_id=False):
    _maintenance_photo = {}
    if maintenance_photo:
        if real_id:
            _maintenance_photo["real_id"] = maintenance_photo.id

        _maintenance_photo["id"] = str(maintenance_photo.uid)
        _maintenance_photo["photo"] = media.fetch_by_id(maintenance_photo.photo_media_id).get_json()
        _maintenance_photo["created_by"] = creator_detail(maintenance_photo.creator_id)
        _maintenance_photo["status"] = status_name(maintenance_photo.status)
        _maintenance_photo["created"] = maintenance_photo.created_at
        _maintenance_photo["modified"] = maintenance_photo.modified_at

    return _maintenance_photo

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        maintenance_photo = db\
            .session\
            .query(MaintenancePhoto)\
            .filter(MaintenancePhoto.uid == uid)\
            .filter(MaintenancePhoto.status > config.STATUS_DELETED)\
            .first()
        if maintenance_photo:
            response = maintenance_photo_obj(maintenance_photo, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        maintenance_photo = db\
            .session\
            .query(MaintenancePhoto)\
            .filter(MaintenancePhoto.id == id)\
            .filter(MaintenancePhoto.status > config.STATUS_DELETED)\
            .first()
        if maintenance_photo:
            response = maintenance_photo_obj(maintenance_photo)

    return jsonify(response)

def fetch_by_maintenance(maintenance_id):
    response = {}
    if maintenance_id:
        maintenance_photo = db\
            .session\
            .query(MaintenancePhoto)\
            .filter(MaintenancePhoto.maintenance_id == maintenance_id)\
            .filter(MaintenancePhoto.status > config.STATUS_DELETED)\
            .first()
        if maintenance_photo:
            response = maintenance_photo_obj(maintenance_photo)

    return jsonify(response)

def add_new(maintenance_photo_data=None, return_obj=False):

    try:
        data = json.loads(maintenance_photo_data)
        if data:

            if 'photo_media_id' in data and 'maintenance_id' in data and 'creator_id' in data :

                _photo_media = None
                _maintenance_id = data['maintenance_id']
                _creator = None

                if valid_uuid(data['photo_media_id']):
                    _photo_media = media.fetch_one(data['photo_media_id'], True).get_json()['real_id']

                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _photo_media and _disease_id and _creator:
                    new_maintenance_photo = MaintenancePhoto(
                        uid = uuid.uuid4(),
                        revision = 0,
                        status = config.STATUS_ACTIVE,
                        photo_media_id = _photo_media,
                        maintenance_id = _maintenance_id,
                        creator_id = _creator
                    )
                    db.session.add(new_maintenance_photo)
                    db.session.commit()
                    if new_maintenance_photo:
                        if return_obj:
                            return jsonify(fetch_one(new_maintenance_photo.uid)), 200

                        return jsonify({"info": "Maintenance photo added successfully!"}), 200

        return jsonify({"error": "Maintenance photo not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(maintenance_photo_id):

    try:
        if valid_uuid(maintenance_photo_id) :
            _maintenance_photo = MaintenancePhoto\
                .query\
                .filter(MaintenancePhoto.uid == maintenance_photo_id)\
                .first()
            if _maintenance_photo:
                _maintenance_photo.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Maintenance photo deactivated successfully!"}), 200

        return jsonify({"error": "Maintenance photo not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
