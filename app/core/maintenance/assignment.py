import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import MaintenanceAssignment

from app.core import user
from app.core import maintenance

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def assignment_obj(assignment=False, real_id=False):
    _assignment = {}
    if assignment:
        if real_id:
            _assignment["real_id"] = assignment.id

        _assignment["id"] = str(assignment.uid)
        _assignment["customer_rating"] = assignment.customer_rating
        _assignment["scheduled"] = assignment.scheduled
        _assignment["igh_user"] = user.fetch_realm_user(config.USER_IGH, assignment.igh_user_id).get_json()
        _assignment["maintenance"] = maintenance.fetch_by_id(assignment.maintenance_id).get_json()['uid']
        _assignment["created_by"] = creator_detail(assignment.creator_id)
        _assignment["status"] = status_name(assignment.status)
        _assignment["created"] = assignment.created_at
        _assignment["modified"] = assignment.modified_at

    return _assignment

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        assignment = db\
            .session\
            .query(MaintenanceAssignment)\
            .filter(MaintenanceAssignment.uid == uid)\
            .filter(MaintenanceAssignment.status > config.STATUS_DELETED)\
            .first()
        if assignment:
            response = assignment_obj(assignment, real_id)

    return jsonify(response)

def fetch_all():
    response = []

    assignment = db\
        .session\
        .query(MaintenanceAssignment)\
        .filter(MaintenanceAssignment.status > config.STATUS_DELETED)\
        .all()

    for assign_maintain in assignment:
        response.append(assignment_obj(assign_maintain))

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        assignment = db\
            .session\
            .query(MaintenanceAssignment)\
            .filter(MaintenanceAssignment.id == id)\
            .filter(MaintenanceAssignment.status > config.STATUS_DELETED)\
            .first()
        if assignment:
            response = assignment_obj(assignment)

    return jsonify(response)

def fetch_by_igh_user(id=0):
    response = {}
    if id:
        assignment = db\
            .session\
            .query(MaintenanceAssignment)\
            .filter(MaintenanceAssignment.igh_user_id == id)\
            .filter(MaintenanceAssignment.status > config.STATUS_DELETED)\
            .first()
        if assignment:
            response = assignment_obj(assignment)

    return jsonify(response)

def fetch_by_maintenance(id=0):
    response = {}
    if id:
        assignment = db\
            .session\
            .query(MaintenanceAssignment)\
            .filter(MaintenanceAssignment.maintenance_id == id)\
            .filter(MaintenanceAssignment.status > config.STATUS_DELETED)\
            .first()
        if assignment:
            response = assignment_obj(assignment)

    return jsonify(response)

def add_new(assignment_data=None, return_obj=False):

    try:
        data = json.loads(assignment_data)
        if data:

            _customer_rating = None
            if 'customer_rating' in data:
                _customer_rating = data['customer_rating']

            _scheduled = None
            if 'scheduled' in data:
                _scheduled = data['scheduled']

            _igh_user_id = None
            if valid_uuid(data['igh_user_id']):
                _igh_user_id = user.fetch_one(data['igh_user_id'], True).get_json()['real_id']

            _maintenance_id = None
            if valid_uuid(data['maintenance_id']):
                _maintenance_id = maintenance.fetch_one(data['maintenance_id'], True).get_json()['real_id']

            _creator = None
            if valid_uuid(data['creator_id']):
                _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _scheduled and _igh_user_id and _maintenance_id and _creator:
                new_assignment = MaintenanceAssignment(
                    uid = uuid.uuid4(),
                    status = config.STATUS_ACTIVE,
                    customer_rating = _customer_rating,
                    scheduled = _scheduled,
                    igh_user_id = _igh_user_id,
                    maintenance_id = _maintenance_id,
                    creator_id = _creator
                )
                db.session.add(new_assignment)
                db.session.commit()
                if new_assignment:
                    if return_obj:
                        return jsonify(fetch_one(new_assignment.uid)), 200

                    return jsonify({"info": "Maintenance assignment added successfully!"}), 200

        return jsonify({"error": "Maintenance assignment not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(assignment_id, assignment_data=None, return_obj=False):

    try:
        data = json.loads(assignment_data)
        if data:

            if valid_uuid(assignment_id):

                _customer_rating = None
                if 'customer_rating' in data:
                    _customer_rating = data['customer_rating']

                _scheduled = None
                if 'scheduled' in data:
                    _scheduled = data['scheduled']

                _igh_user_id = None
                if valid_uuid(data['igh_user_id']):
                    _igh_user_id = user.fetch_one(data['igh_user_id'], True).get_json()['real_id']

                if _scheduled and _igh_user_id:
                    _assignment = MaintenanceAssignment\
                        .query\
                        .filter(MaintenanceAssignment.uid == str(assignment_id))\
                        .first()
                    if _assignment:
                        _assignment.customer_rating = _customer_rating
                        _assignment.scheduled = _scheduled
                        _assignment.igh_user_id = _igh_user_id
                        db.session.commit()

                        return jsonify({"info": "Maintenance assignment edited successfully!"}), 200

        return jsonify({"error": "Maintenance assignment not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(assignment_id):

    try:
        if valid_uuid(assignment_id) :
            _assignment = MaintenanceAssignment\
                .query\
                .filter(MaintenanceAssignment.uid == assignment_id)\
                .first()
            if _assignment:
                _assignment.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Maintenance assignment deactivated successfully!"}), 200

        return jsonify({"error": "Maintenance assignment not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
