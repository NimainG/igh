import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import Supplier

from app.core import user
from app.core import customer

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def supplier_obj(supplier=False, real_id=False):
    _supplier = {}
    if supplier:
        if real_id:
            _supplier["real_id"] = supplier.id

        _supplier["id"] = str(supplier.uid)
        _supplier["name"] = supplier.name
        _supplier["notes"] = supplier.notes
        _supplier["customer"] = customer.fetch_by_id(supplier.customer_id).get_json()
        _supplier["created_by"] = creator_detail(supplier.creator_id)
        _supplier["status"] = status_name(supplier.status)
        _supplier["created"] = supplier.created_at
        _supplier["modified"] = supplier.modified_at

    return _supplier

def fetch_all():
    response = []

    suppliers = db\
        .session\
        .query(Supplier)\
        .filter(Supplier.status > config.STATUS_DELETED)\
        .all()

    for supplier in suppliers:
        response.append(supplier_obj(supplier))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        supplier = db\
            .session\
            .query(Supplier)\
            .filter(Supplier.uid == uid)\
            .filter(Supplier.status > config.STATUS_DELETED)\
            .first()
        if supplier:
            response = supplier_obj(supplier, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        supplier = db\
            .session\
            .query(Supplier)\
            .filter(Supplier.id == id)\
            .filter(Supplier.status > config.STATUS_DELETED)\
            .first()
        if supplier:
            response = supplier_obj(supplier)

    return jsonify(response)

def fetch_by_customer(customer_id=None):
    response = []

    suppliers = db\
        .session\
        .query(Supplier)\
        .filter(Supplier.customer_id == customer_id)\
        .filter(Supplier.status > config.STATUS_DELETED)\
        .all()

    for supplier in suppliers:
        response.append(supplier_obj(supplier))

    return jsonify(response)

def add_new(supplier_data=None, return_obj=False):

    try:
        data = json.loads(supplier_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _customer_id = None
            if valid_uuid(data['customer_id']):
                _customer_id = customer.fetch_one(data['customer_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _creator_id:
                new_supplier = Supplier(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    notes = _notes,
                    customer_id = _customer_id,
                    creator_id = _creator_id
                )
                db.session.add(new_supplier)
                db.session.commit()
                if new_supplier:
                    if return_obj:
                        return jsonify(fetch_one(new_supplier.uid)), 200

                    return jsonify({"info": "Supplier added successfully!"}), 200

        return jsonify({"error": "Supplier not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(supplier_id, supplier_data=None, return_obj=False):

    try:
        data = json.loads(supplier_data)
        if data:

            if valid_uuid(supplier_id):

                _name = None
                if 'name' in data:
                    _name = data['name']

                _notes = None
                if 'notes' in data:
                    _notes = data['notes']

                if _name:
                    _supplier = Supplier\
                        .query\
                        .filter(Supplier.uid == str(supplier_id))\
                        .first()
                    if _supplier:
                        _supplier.name = _name
                        _supplier.notes = _notes
                        db.session.commit()

                        return jsonify({"info": "Supplier edited successfully!"}), 200

        return jsonify({"error": "Supplier not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(supplier_id):

    try:

        if valid_uuid(supplier_id) :
            _supplier = Supplier\
                .query\
                .filter(Supplier.uid == supplier_id)\
                .first()
            if _supplier:
                _supplier.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Supplier deactivated successfully!"}), 200

        return jsonify({"error": "Supplier not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
