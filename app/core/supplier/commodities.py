import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import SupplierCommodities

from app.core import user
from app.core import supply
from app.core import supplier

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def supplier_commodity_obj(supplier_commodity=False, real_id=False):
    _supplier_commodity = {}
    if supplier_commodity:
        if real_id:
            _supplier_commodity["real_id"] = supplier_commodity.id

        _supplier_commodity["id"] = str(supplier_commodity.uid)
        _supplier_commodity["name"] = supplier_commodity.name
        _supplier_commodity["type"] = supply.commodity_name(supplier_commodity.type)
        _supplier_commodity["notes"] = supplier_commodity.notes
        _supplier_commodity["commodity"] = supplier.commodity.fetch_by_id(supplier_commodity.commodity_id).get_json()['uid']
        _supplier_commodity["supplier"] = supplier.fetch_by_id(supplier_commodity.supplier_id).get_json()['uid']
        _supplier_commodity["created_by"] = creator_detail(supplier_commodity.creator_id)
        _supplier_commodity["status"] = status_name(supplier_commodity.status)
        _supplier_commodity["created"] = supplier_commodity.created_at
        _supplier_commodity["modified"] = supplier_commodity.modified_at

    return _supplier_commodity

def fetch_all():
    response = []

    supplier_commodities = db\
        .session\
        .query(SupplierCommodities)\
        .filter(SupplierCommodities.status > config.STATUS_DELETED)\
        .all()

    for supplier_commodity in supplier_commodities:
        response.append(supplier_commodity_obj(supplier_commodity))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        supplier_commodity = db\
            .session\
            .query(SupplierCommodities)\
            .filter(SupplierCommodities.uid == uid)\
            .filter(SupplierCommodities.status > config.STATUS_DELETED)\
            .first()
        if supplier_commodity:
            response = supplier_commodity_obj(supplier_commodity, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        supplier_commodity = db\
            .session\
            .query(SupplierCommodities)\
            .filter(SupplierCommodities.id == id)\
            .filter(SupplierCommodities.status > config.STATUS_DELETED)\
            .first()
        if supplier_commodity:
            response = supplier_commodity_obj(supplier_commodity)

    return jsonify(response)

def fetch_by_commodity(commodity_id=None):
    response = []

    commodity_commodities = db\
        .session\
        .query(SupplierCommodities)\
        .filter(SupplierCommodities.commodity_id == commodity_id)\
        .filter(SupplierCommodities.status > config.STATUS_DELETED)\
        .all()
    for supplier_commodity in supplier_commodities:
        response.append(supplier_commodity_obj(supplier_commodity))

    return jsonify(response)

def fetch_by_supplier(supplier_id=None):
    response = []

    supplier_commodities = db\
        .session\
        .query(SupplierCommodities)\
        .filter(SupplierCommodities.supplier_id == supplier_id)\
        .filter(SupplierCommodities.status > config.STATUS_DELETED)\
        .all()
    for supplier_commodity in supplier_commodities:
        response.append(supplier_commodity_obj(supplier_commodity))

    return jsonify(response)

def add_new(supplier_commodity_data=None, return_obj=False):

    try:
        data = json.loads(supplier_commodity_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _type = None
            if 'type' in data:
                _type = data['type']

            _notes = None
            if 'notes' in data:
                _notes = data['notes']

            _commodity_id = None
            if valid_uuid(data['commodity_id']):
                _commodity_id = supplier.commodity.fetch_one(data['commodity_id'], True).get_json()['real_id']

            _supplier_id = None
            if valid_uuid(data['supplier_id']):
                _supplier_id = supplier.fetch_one(data['supplier_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _creator_id:
                new_supplier_commodity = SupplierCommodities(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    type = _type,
                    notes = _notes,
                    commodity_id = _commodity_id,
                    supplier_id = _supplier_id,
                    creator_id = _creator_id
                )
                db.session.add(new_supplier_commodity)
                db.session.commit()
                if new_supplier_commodity:
                    if return_obj:
                        return jsonify(fetch_one(new_supplier_commodity.uid)), 200

                    return jsonify({"info": "Supplier commodity added successfully!"}), 200

        return jsonify({"error": "Supplier commodity not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(supplier_commodity_id):

    try:

        if valid_uuid(supplier_commodity_id) :
            _supplier_commodity = SupplierCommodities\
                .query\
                .filter(SupplierCommodities.uid == supplier_commodity_id)\
                .first()
            if _supplier_commodity:
                _supplier_commodity.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Supplier commodity deactivated successfully!"}), 200

        return jsonify({"error": "Supplier commodity not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
