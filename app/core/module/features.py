import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, person_info, status_name

from app.models import db
from app.models import ModuleFeature

from app.core import user
from app.core import module

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def module_feature_obj(module_feature=False, real_id=False):
    _module_feature = {}
    if module_feature:
        if real_id:
            _module_feature["real_id"] = module_feature.id

        _module_feature["id"] = str(module_feature.uid)
        _module_feature["name"] = module_feature.name
        _module_feature["code"] = module_feature.code
        _module_feature["created_by"] = creator_detail(module_feature.creator_id)
        _module_feature["status"] = status_name(module_feature.status)
        _module_feature["created"] = module_feature.created_at
        _module_feature["modified"] = module_feature.modified_at

    return _module_feature

def fetch_all():
    response = []

    module_features = db\
        .session\
        .query(ModuleFeature)\
        .filter(ModuleFeature.status > config.STATUS_DELETED)\
        .all()

    for module_feature in module_features:
        response.append(module_feature_obj(module_feature))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        module_feature = db\
            .session\
            .query(ModuleFeature)\
            .filter(ModuleFeature.uid == uid)\
            .filter(ModuleFeature.status > config.STATUS_DELETED)\
            .first()
        if module_feature:
            response = module_feature_obj(module_feature, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        module_feature = db\
            .session\
            .query(ModuleFeature)\
            .filter(ModuleFeature.id == id)\
            .filter(ModuleFeature.status > config.STATUS_DELETED)\
            .first()
        if module_feature:
            response = module_feature_obj(module_feature)

    return jsonify(response)

def fetch_by_module(module_id=0, real_id=False):
    response = []
    if module_id:
        module_features = db\
            .session\
            .query(ModuleFeature)\
            .filter(ModuleFeature.module_id == module_id)\
            .filter(ModuleFeature.status > config.STATUS_DELETED)\
            .all()
        for module_feature in module_features:
            response.append(module_feature_obj(module_feature, real_id))

    return jsonify(response)

def add_new(module_feature_data=None, return_obj=False):

    try:
        data = json.loads(module_feature_data)
        if data:

            if 'name' in data and 'code' in data and 'creator_id' in data:

                _name = data['name']
                _code = data['code']

                _module_id = None
                if valid_uuid(data['module_id']):
                    _module_id = module.fetch_one(data['module_id'], True).get_json()['real_id']

                _creator = None
                if valid_uuid(data['creator_id']):
                    _creator = user.fetch_one(data['creator_id'], True).get_json()['real_id']

                if _name and _code and _module_id and _creator:
                    new_module_feature = ModuleFeature(
                        uid = uuid.uuid4(),
                        status = config.STATUS_ACTIVE,
                        name = _name,
                        code = _code,
                        module_id = _module_id,
                        creator_id = _creator
                    )
                    db.session.add(new_module_feature)
                    db.session.commit()
                    if new_module_feature:
                        if return_obj:
                            return fetch_one(new_module_feature.uid), 200

                        return jsonify({"info": "Module feature added successfully!"}), 200

        return jsonify({"error": "Module feature not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def edit(module_feature_id, module_feature_data=None, return_obj=False):

    try:
        data = json.loads(module_feature_data)
        if data:

            if valid_uuid(module_feature_id) and 'name' in data and 'code' in data:

                _name = data['name']
                _code = data['code']

                _module_id = None
                if valid_uuid(data['module_id']):
                    _module_id = module.fetch_one(data['module_id'], True).get_json()['real_id']

                if _name and _code and _module_id:
                    _module_feature = ModuleFeature\
                        .query\
                        .filter(ModuleFeature.uid == module_feature_id)\
                        .first()
                    if _module_feature:
                        _module_feature.name = _name
                        _module_feature.code = _code
                        _module_feature.module_id = _module_id
                        db.session.commit()

                        return jsonify({"info": "Module feature edited successfully!"}), 200

        return jsonify({"error": "Module feature not edited, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(module_feature_id):

    try:

        if valid_uuid(module_feature_id) :
            _module_feature = ModuleFeature\
                .query\
                .filter(ModuleFeature.uid == module_feature_id)\
                .first()
            if _module_feature:
                _module_feature.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Module feature deactivated successfully!"}), 200

        return jsonify({"error": "Module feature not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
