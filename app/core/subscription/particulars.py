# particular_id - Integer
import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import SubscriptionParticular

from app.core import subscription
from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def particular_obj(particular=False, real_id=False):
    _particular = {}
    if particular:
        if real_id:
            _particular["real_id"] = particular.id

        _particular["id"] = str(particular.uid)
        _particular["item"] = particular.item
        _particular["cost"] = particular.cost
        _particular["subscription"] = subscription.fetch_by_id(particular.subscription_id).get_json()['uid']
        _particular["created_by"] = creator_detail(particular.creator_id)
        _particular["status"] = status_name(particular.status)
        _particular["created"] = particular.created_at
        _particular["modified"] = particular.modified_at

    return _particular

def fetch_all():
    response = []

    particulars = db\
        .session\
        .query(SubscriptionParticular)\
        .filter(SubscriptionParticular.status > config.STATUS_DELETED)\
        .all()

    for particular in particulars:
        response.append(particular_obj(particular))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        particular = db\
            .session\
            .query(SubscriptionParticular)\
            .filter(SubscriptionParticular.uid == uid)\
            .filter(SubscriptionParticular.status > config.STATUS_DELETED)\
            .first()
        if particular:
            response = particular_obj(particular, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        particular = db\
            .session\
            .query(SubscriptionParticular)\
            .filter(SubscriptionParticular.id == id)\
            .filter(SubscriptionParticular.status > config.STATUS_DELETED)\
            .first()
        if particular:
            response = particular_obj(particular)

    return jsonify(response)

def fetch_by_subscription(subscription_id=0):
    response = []
    if subscription_id:
        particulars = db\
            .session\
            .query(SubscriptionParticular)\
            .filter(SubscriptionParticular.subscription_id == subscription_id)\
            .filter(SubscriptionParticular.status > config.STATUS_DELETED)\
            .first()
        for particular in particulars:
            response.append(particular_obj(particular))

    return jsonify(response)

def add_new(particular_data=None, return_obj=False):

    try:
        data = json.loads(particular_data)
        if data:

            _item = None
            if 'item' in data:
                _item = data['item']

            _cost = None
            if 'cost' in data:
                _cost = data['cost']

            _subscription_id = None
            if valid_uuid(data['subscription_id']):
                _subscription_id = subscription.fetch_one(data['subscription_id'], True).get_json()['real_id']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _item and _cost and _subscription_id and _creator_id:
                new_particular = SubscriptionParticular(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    item = _item,
                    cost = _cost,
                    subscription_id = _subscription_id,
                    creator_id = _creator_id
                )
                db.session.add(new_particular)
                db.session.commit()
                if new_particular:
                    if return_obj:
                        return jsonify(fetch_one(new_particular.uid)), 200

                    return jsonify({"info": "Subscription particular added successfully!"}), 200

        return jsonify({"error": "Subscription particular not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(particular_id):

    try:
        if valid_uuid(particular_id) :
            _particular = SubscriptionParticular\
                .query\
                .filter(SubscriptionParticular.uid == particular_id)\
                .first()
            if _particular:
                _particular.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Subscription particular deactivated successfully!"}), 200

        return jsonify({"error": "Subscription particular not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
