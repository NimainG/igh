import os
import json
import urllib.parse
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import JSON
from flask import jsonify
from dotenv import load_dotenv

from app.config import activateConfig

from app.helper import valid_uuid, creator_detail, status_name

from app.models import db
from app.models import Subscription

from app.core import user

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def subscription_obj(subscription=False, real_id=False):
    _subscription = {}
    if subscription:
        if real_id:
             _subscription["real_id"] = subscription.id

        _subscription["id"] = str(subscription.uid)
        _subscription["name"] = subscription.name
        _subscription["cylce"] = subscription.cylce
        _subscription["amount"] = subscription.amount
        _subscription["created_by"] = creator_detail(subscription.creator_id)
        _subscription["status"] = status_name(subscription.status)
        _subscription["created"] = subscription.created_at
        _subscription["modified"] = subscription.modified_at

    return _subscription

def fetch_all():
    response = []

    subscriptions = db\
        .session\
        .query(Subscription)\
        .filter(Subscription.status > config.STATUS_DELETED)\
        .all()

    for subscription in subscriptions:
        response.append(subscription_obj(subscription))

    return jsonify(response)

def fetch_one(uid, real_id=False):
    response = {}

    if valid_uuid(uid):
        subscription = db\
            .session\
            .query(Subscription)\
            .filter(Subscription.uid == uid)\
            .filter(Subscription.status > config.STATUS_DELETED)\
            .first()
        if subscription:
            response = subscription_obj(subscription, real_id)

    return jsonify(response)

def fetch_by_id(id=0):
    response = {}
    if id:
        subscription = db\
            .session\
            .query(Subscription)\
            .filter(Subscription.id == id)\
            .filter(Subscription.status > config.STATUS_DELETED)\
            .first()
        if subscription:
            response = subscription_obj(subscription)

    return jsonify(response)

def add_new(subscription_data=None, return_obj=False):

    try:
        data = json.loads(subscription_data)
        if data:

            _name = None
            if 'name' in data:
                _name = data['name']

            _cylce = None
            if 'cylce' in data:
                _cylce = data['cylce']

            _amount = None
            if 'amount' in data:
                _amount = data['amount']

            _creator_id = None
            if valid_uuid(data['creator_id']):
                _creator_id = user.fetch_one(data['creator_id'], True).get_json()['real_id']

            if _name and _cylce and _amount and _creator_id:
                new_subscription = Subscription(
                    uid = uuid.uuid4(),
                    revision = 0,
                    status = config.STATUS_ACTIVE,
                    name = _name,
                    cylce = _cylce,
                    amount = _amount,
                    creator_id = _creator_id
                )
                db.session.add(new_subscription)
                db.session.commit()
                if new_subscription:
                    if return_obj:
                        return jsonify(fetch_one(new_subscription.uid)), 200

                    return jsonify({"info": "Subscription added successfully!"}), 200

        return jsonify({"error": "Subscription not added, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406

def deactivate(subscription_id):

    try:
        if valid_uuid(subscription_id) :
            _subscription = Subscription\
                .query\
                .filter(Subscription.uid == subscription_id)\
                .first()
            if _subscription:
                _subscription.status = config.STATUS_INACTIVE
                db.session.commit()

                return jsonify({"info": "Subscription deactivated successfully!"}), 200

        return jsonify({"error": "Subscription not deactivated, input invalid."}), 405

    except Exception as e:
        return jsonify({"error": str(e)}), 406
