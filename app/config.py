import os
from sqlalchemy import create_engine
from dotenv import load_dotenv
from pathlib import Path
from flask_mail import Mail,Message

load_dotenv()

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'de07bc8e001e3e5c4df'

    APP_ROOT = os.path.dirname(os.path.abspath(__file__))
    APP_ROOT_PATH = Path(APP_ROOT)
    UPLOAD_FOLDER = os.path.join(APP_ROOT_PATH, 'igh/static/files/')

    user = os.environ['POSTGRES_USER'] if 'POSTGRES_USER' in os.environ else 'postgress'
    password = os.environ['POSTGRES_PASSWORD'] if 'POSTGRES_PASSWORD' in os.environ else 'sensors'
    host = os.environ['POSTGRES_HOST'] if 'POSTGRES_HOST' in os.environ else '127.0.0.1'
    database = os.environ['POSTGRES_DB'] if 'POSTGRES_DB' in os.environ else 'igh'
    port = os.environ['POSTGRES_PORT'] if 'POSTGRES_PORT' in os.environ else 5432

    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = f'postgresql+psycopg2://{user}:{password}@{host}:{port}/{database}'

    # Data statuses
    STATUS_ACTIVE = 1
    STATUS_INACTIVE = 0
    STATUS_DELETED = -1

    # Notification types
    NOTIFICATION_ALERT = 1
    NOTIFICATION_USER = 2
    NOTIFICATION_SYSTEM = 99

    # IGH
    COMPANY_NAME = "Synnefa"
    HTTP_SCHEME = "http"
    #HTTP_SCHEME = "https"
    APP_NAME = "Farm Shield"
    SIGN_OFF = "Farm Shield"
    CO_TEAM = "Customer Support"

    # Default Customer User Role
    CUSTOMER_ADMIN = "Customer Admin"
    CUSTOMER_USER = "Customer User"

    # User Realms
    USER_IGH = 1
    USER_CUSTOMER = 2
    USER_PARTNER = 3

    EMPLOYEE_ID_START = 100

    # Supply Commodities
    COMMODITY_FERTILIZERS = 1
    COMMODITY_AGROCHEMICALS = 2

    # Maintenance
    MAINTENANCE_SERIAL_START = 1

    MAINTENANCE_PESTS_DISEASES = 1
    MAINTENANCE_AGRONOMIST_VISIT = 2
    MAINTENANCE_SHIELD_ISSUE = 3
    MAINTENANCE_GREENHOUSE_ISSUE = 4
    MAINTENANCE_SCREENHOUSE_ISSUE = 5
    MAINTENANCE_SHADENET_ISSUE = 6
    MAINTENANCE_TRACEABILITY_REPORT = 7
    MAINTENANCE_OTHERS = 99

    # Alerts
    ALERT_SYSTEM = 1
    ALERT_SHIELD = 2
    ALERT_USER = 3

    # Alert Severity
    ALERT_INFORMATIONAL = 1
    ALERT_WARNING = 2
    ALERT_MINOR = 3
    ALERT_MAJOR = 4
    ALERT_CRITICAL = 5
    ALERT_FATAL = 6

    # Alert modes
    NOTIFICATION_MODE_PROFILE = 1
    NOTIFICATION_MODE_EMAIL = 2
    NOTIFICATION_MODE_SMS = 3
    NOTIFICATION_MODE_ALL = 99

    # Reports
    REPORT_WASTE_POLLUTION_MANAGEMENT = 1
    REPORT_FERTILIZER_APPLICATION = 2
    REPORT_AGROCHEMICAL_APPLICATION = 3
    REPORT_PREHARVEST_CHECKLIST = 4
    REPORT_HARVEST_DETAIL = 5
    REPORT_SCOUTING_REPORT = 6
    REPORT_OTHERS = 7

    # Metrics
    METRIC_TEMP_C = 1
    METRIC_TEMP_F = 2

    METRIC_LIQUID_L = 1
    METRIC_LIQUID_GAL = 2

    METRIC_LENGTH_F = 1
    METRIC_LENGTH_M = 2

    # Invoices & Payments
    INVOICE_SERIAL_START = 10
    CREDIT_NOTE_SERIAL_START = 10
    PAYMENT_SERIAL_START = 10

    PAYMENT_STATUS_PENDING = 1
    PAYMENT_STATUS_PROCESSING = 2
    PAYMENT_STATUS_FAILED = 3
    PAYMENT_STATUS_SUCCESS = 4

    # Subscription & Billing
    BILLING_ONCE = 0
    BILLING_MONTHLY = 1
    BILLING_QUARTERLY = 2
    BILLING_YEARLY = 3


    
    # Mails


    # MAILGUN_API = "https://api.mailgun.net/v3/mg." + IGH_DOMAIN + "/messages"
    MG_IGH_DOMAIN = "mg.illuminumgreenhouses.com"
    MAILGUN_API = "http://sandbox45a4ff91749947279f7a38d895b6b901.mailgun.org"
    #MAILGUN_API_KEY = "pubkey-04dd12cc660aa1351a0660efe29d5840"
    MAILGUN_API_KEY = "5361171dcd6a74cd43ee70f0599d593c-2bf328a5-599b9d1e"
    #MAILGUN_API = "http://sandbox02390c1e9ac9483285a52f1a12558938.mailgun.org"
    #MAILGUN_API_KEY = "532f1ba928a6831cb0c130b53797dbcd-ed4dc7c4-25546095"

    EMAIL_FROM = "Farm Shield<nimain@sinsignal.com>"
    #EMAIL_FROM = "Farm Shield<nimain@sinsignal.com>"
    EMAIL_SERVER = "smtp.mailgun.org"
    EMAIL_SERVER_PORT = "587"
   # SMTP_USERNAME = "postmaster@sandbox02390c1e9ac9483285a52f1a12558938.mailgun.org"
   # SMTP_PASSWORD = "8537b6cf48c6c209eede92f7f45f0d2e-ed4dc7c4-a8716204"

    #SMTP_USERNAME = "postmaster@sandbox45a4ff91749947279f7a38d895b6b901.mailgun.org"
    SMTP_USERNAME = "nimain@sinsignal.com"
    SMTP_PASSWORD = "e30099dc705d4b49673b0064ac791be1-2bf328a5-f3b364b9"

    # Google Maps API
    GOOGLE_API_KEY = "AIzaSyCV4HWI-lCszJXc_X9373Sjh31AN1jbDDU"

    # Device Types
    DEVICE_SHIELD = 1
    DEVICE_SPEAR = 2
    DEVICE_SENSOR = 3

    # Shield placement
    SHIELD_INTERNAL = 1
    SHIELD_EXTERNAL = 2

    # Data sources
    DATA_CSV = 1
    DATA_AT = 2
    DATA_G_SHEET = 3
    DATA_MANUAL = 4
    DATA_MQTT = 5

    @staticmethod
    def init_app(app):
        pass

class DevelopmentConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = True

    IGH_ADMIN_EMAIL = "admin@synnefa.com"

   # IGH_DOMAIN = "illuminumgreenhouses.com"
    IGH_DOMAIN = "127.0.0.1:5000"
    APP_DOMAIN = "" + IGH_DOMAIN
    #MAILS_FROM = "Farm Shield <farmshield@" + IGH_DOMAIN + ">"

    # AfricasTalking
    AT_API_URL = "https://api.sandbox.africastalking.com/version1/messaging"
    AT_USERNAME = "sandbox"
    AT_KEY = "55c97fde3472a33209cbe0bf6de07bc8e001e3e5c4df074d888f44686d86b393"
    AT_SENDER = "sandbox"

class ProductionConfig(Config):
    DEBUG = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    IGH_ADMIN_EMAIL = "admin@illuminumgreenhouses.com"

    IGH_DOMAIN = "127.0.0.1:5000"
    APP_DOMAIN = "" + IGH_DOMAIN
    #MAILS_FROM = "Farm Shield <farmshield@" + IGH_DOMAIN + ">"

    # AfricasTalking
    AT_API_URL = "https://api.africastalking.com/version1/messaging"
    AT_USERNAME = os.environ['AT_USERNAME'] if 'AT_USERNAME' in os.environ else ""
    AT_API_KEY = os.environ['AT_API_KEY'] if 'AT_API_KEY' in os.environ else ""
    AT_SENDER = os.environ['AT_API_SENDER'] if 'AT_API_SENDER' in os.environ else ""

def activateConfig(configRef="development"):
    if configRef == "production":
        return ProductionConfig
    return DevelopmentConfig
