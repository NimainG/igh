# import uuid, os, datetime, smtplib

# from flask import jsonify
# from flask.app import Flask
# from flask_login import login_required, login_user, logout_user, current_user, LoginManager
# from email.utils import parseaddr
# from email.mime.multipart import MIMEMultipart
# from email.mime.text import MIMEText
# from time import strftime

# from sqlalchemy import or_

# from app import db
# import app
# from app.core import user
# #from app.login import LoginMan
# from app.config import Config
# from app.models import Credentials, Users, Addresses
# from app.modules.views import validateEmailAddress
# from flask_mail import Mail

# config = Config()
# db = db.session


# @LoginMan.login_manager.user_loader
# def load_user(user_id):
#     return User(user_id)

# def sendMail(emailFrom, emailTo, emailSubject, message):
#     if '@' in parseaddr(emailFrom)[1] and '@' in parseaddr(emailTo)[1] and emailSubject != "" and message != "":
#         msg = MIMEMultipart('alternative')
#         msg['Subject'] = emailSubject
#         msg['From'] = config.EMAIL_FROM
#         msg['To'] = emailTo
#         msg.attach(MIMEText(message, 'html'))

#         try:
#             instance = smtplib.SMTP(config.EMAIL_SERVER, config.EMAIL_SERVER_PORT)
#             instance.ehlo()
#             instance.starttls()
#             instance.login(config.SMTP_USERNAME, config.SMTP_PASSWORD)
#             instance.sendmail(emailFrom, emailTo, msg.as_string())
#             instance.quit()
#             return "Sent"
#         except Exception as e:
#             return "Failed: " + str(e)

#     return "Failed: Invalid properties"

# def sendAccountActivationMail(userId):
#     theUser = None
#     sendActivationMail = False
#     activityHash = uuid.uuid4()

#     existsExists = db.query(Users, Addresses)\
#                     .join(Addresses, Addresses.id == Users.address)\
#                     .filter(Users.id == userId)\
#                     .filter(Users.status == config.STATE_INACTIVE)
#     if existsExists.count() > 0:
#         theUser = existsExists[0]

#     if theUser is not None:

#         emailAddress = theUser.addresses.email_address
#         userCredentials = db.query(Credentials)\
#                                 .filter(Credentials.user_id == userId)

#         if userCredentials.count() > 0:
#             db.query(Credentials).filter(Credentials.user_id==theUser.users.id).update({Credentials.activation_hash:activityHash.hex},synchronize_session=False)
#             db.commit()
#             sendActivationMail = True
#         else:
#             userCredentials = Credentials(uuid=uuid.uuid4(), user_id=userId, activation_hash=activityHash.hex)
#             db.add(userCredentials)
#             db.commit()
#             sendActivationMail = True

#         if emailAddress is not None and sendActivationMail is True:
#             subject = "Account Activation"
#             message = """\
#                             <!DOCTYPE html>
#                             <html>
#                                 <head>
#                                     <meta charset="utf-8">
#                                     <meta http-equiv="X-UA-Compatible" content="IE=edge">
#                                     <title>""" + config.COMPANY_NAME + """</title>
#                                     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/css/bootstrap.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/AdminLTE.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/skins/_all-skins.min.css'>
#                                 </head>
#                                 <body class="hold-transition skin-purple sidebar-mini">
#                                     <section class="content">
#                                         <p>Dear """ + theUser.users.first_name +""",</p>
#                                         <p>An account has been created for you in the """ + config.APP_NAME + """.</p>
#                                         <p>Use the following link to activate your account by choosing a password.</p>
#                                         <p><a href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/activate/""" + str(activityHash.hex) + """' target='mails'>""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/activate/""" + str(activityHash.hex) + """</a></p>
#                                         <p>If the link does not respond, you can copy and paste it in your browser to access the activation page.</p>
#                                         <p>Regards<br /><br />""" + config.CO_TEAM + """</p>
#                                         <p><small><strong>Powered by """ + config.SIGN_OFF + """</strong></small></p>
#                                     </section>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/plugins/jQuery/jQuery-2.1.4.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/js/bootstrap.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/js/app.min.js'></script>
#                                 </body>
#                             </html>
#                         """

#             return sendMail(config.SMTP_USERNAME, emailAddress, subject, message)
#         else:
#             return "Not sent"
#     else:
#         return "Invalid user"

# def sendAccountReActivationMail(userId):
#     theUser = None
#     sendReActivationMail = False
#     activityHash = uuid.uuid4()

#     existsExists = db.query(Users, Addresses)\
#                     .join(Addresses, Addresses.id == Users.address)\
#                     .filter(Users.id == userId)\
#                     .filter(Users.status == config.STATE_INACTIVE)
#     if existsExists.count() > 0:
#         theUser = existsExists[0]

#     if theUser is not None:
#         emailAddress = theUser.addresses.email_address
#         userCredentials = db.query(Credentials)\
#                                 .filter(Credentials.user_id == userId)

#         if userCredentials.count() > 0:
#             db.query(Credentials).filter(Credentials.user_id==theUser.users.id).update({Credentials.activation_hash:activityHash.hex},synchronize_session=False)
#             db.commit()
#             sendReActivationMail = True

#         if emailAddress is not None and sendReActivationMail is True:
#             subject = "Account Re Activation"
#             message = """\
#                             <!DOCTYPE html>
#                             <html>
#                                 <head>
#                                     <meta charset="utf-8">
#                                     <meta http-equiv="X-UA-Compatible" content="IE=edge">
#                                     <title>""" + config.COMPANY_NAME + """</title>
#                                     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/css/bootstrap.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/AdminLTE.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/skins/_all-skins.min.css'>
#                                 </head>
#                                 <body class="hold-transition skin-purple sidebar-mini">
#                                     <section class="content">
#                                         <p>Dear """ + theUser.users.first_name +""",</p>
#                                         <p>Your """ + config.APP_NAME + """ account has been re-enabled. Use the following link to set a new password.</p>
#                                         <p><a href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/reactivate/""" + str(activityHash.hex) + """' target='mails'>""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/reactivate/""" + str(activityHash.hex) + """</a></p>
#                                         <p>If the link does not respond, you can copy and paste it in your browser to access the re-activate page.</p>
#                                         <p>PS: The password reset link can only be used once and within 48 hours.</p>
#                                         <p>Regards<br /><br />""" + config.CO_TEAM + """</p>
#                                         <p><small><strong>Powered by """ + config.SIGN_OFF + """</strong></small></p>
#                                     </section>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/plugins/jQuery/jQuery-2.1.4.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/js/bootstrap.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/js/app.min.js'></script>
#                                 </body>
#                             </html>
#                         """

#             return sendMail(config.SMTP_USERNAME, emailAddress, subject, message)
#         else:
#             return "Not sent"
#     else:
#         return "Invalid user"

# def passwordResetRequest(emailAddress):
#     responseObj = {}
#     if validateEmailAddress(emailAddress):
#         userAccount = db.query(Addresses, Users)\
#                         .join(Users, Users.address == Addresses.id)\
#                         .filter(Addresses.email_address == emailAddress)\
#                         .filter(Users.status > config.STATE_DELETED)\
#                         .first()
#         if userAccount:
#             resetCode = str(uuid.uuid4())[9:-23].upper()
#             db.query(Credentials)\
#                 .filter(Credentials.user_id == userAccount.users.id)\
#                 .update({
#                     Credentials.validation_hash: resetCode
#                 }, synchronize_session=False)
#             db.commit()

#             subject = "Password Reset Request"
#             message = """\
#                             <!DOCTYPE html>
#                             <html>
#                                 <head>
#                                     <meta charset="utf-8">
#                                     <meta http-equiv="X-UA-Compatible" content="IE=edge">
#                                     <title>""" + config.COMPANY_NAME + """</title>
#                                     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/css/bootstrap.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/AdminLTE.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/skins/_all-skins.min.css'>
#                                 </head>
#                                 <body class="hold-transition skin-purple sidebar-mini">
#                                     <section class="content">
#                                         <p>Dear """ + userAccount.users.first_name + """,</p>
#                                         <p>A password request has been received for your account. If you did not send it, please ignore this email. If you did send the request, use the code below to reset your password from the app.</p>
#                                         <p>Reset code: <code><strong>""" + resetCode + """</strong></code></p>
#                                         <p>PS: The reset code can only be used once and within 48 hours.</p>
#                                         <p>Regards<br /><br />""" + config.CO_TEAM + """</p>
#                                         <p><small><strong>Powered by """ + config.SIGN_OFF + """</strong></small></p>
#                                     </section>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/plugins/jQuery/jQuery-2.1.4.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/js/bootstrap.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/js/app.min.js'></script>
#                                 </body>
#                             </html>
#                         """

#             sendNow = sendMail(config.SMTP_USERNAME, emailAddress, subject, message)
#             #print str(sendNow)

#             if sendNow == "Sent":
#                 responseObj = {
#                     "code": 200,
#                     "body": "An email with password reset code has been sent to the address provided."
#                 }
#             else:
#                 responseObj = {
#                     "code": 400,
#                     "body": "Reset email not sent: " + str(sendNow)
#                 }
#     else:
#         responseObj = {
#             "code": 400,
#             "body": "Email address is invalid"
#         }

#     return responseObj

# def sendWelcomeMail(userId):
#     theUser = None
#     sendActivationMail = False
#     activityHash = uuid.uuid4()

#     existsExists = db.query(Users, Addresses)\
#                     .join(Addresses, Addresses.id == Users.address)\
#                     .filter(Users.id == userId)\
#                     .filter(or_(Users.status == config.STATE_INACTIVE, Users.status == config.STATE_ACTIVE))
#     if existsExists.count() > 0:
#         theUser = existsExists[0]

#     if theUser is not None:

#         emailAddress = theUser.addresses.email_address
#         userCredentials = db.query(Credentials)\
#                                 .filter(Credentials.user_id == userId)

#         if userCredentials.count() > 0:
#             # db.query(Credentials).filter(Credentials.user_id==theUser.users.id).update({Credentials.activation_hash:activityHash.hex},synchronize_session=False)
#             # db.commit()
#             sendActivationMail = True
#         else:
#             userCredentials = Credentials(uuid=uuid.uuid4(), user_id=userId, activation_hash=activityHash.hex)
#             db.add(userCredentials)
#             db.commit()
#             sendActivationMail = True

#         if emailAddress is not None and sendActivationMail is True:
#             subject = "Account Activation"
#             message = """\
#                             <!DOCTYPE html>
#                             <html>
#                                 <head>
#                                     <meta charset="utf-8">
#                                     <meta http-equiv="X-UA-Compatible" content="IE=edge">
#                                     <title>""" + config.COMPANY_NAME + """</title>
#                                     <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/css/bootstrap.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/AdminLTE.min.css'>
#                                     <link rel="stylesheet" href='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/css/skins/_all-skins.min.css'>
#                                 </head>
#                                 <body class="hold-transition skin-purple sidebar-mini">
#                                     <section class="content">
#                                         <p>Dear """ + theUser.users.first_name +""",</p>
#                                         <p>Thank you for installing """ + config.APP_NAME + """ and creating an account.</p>
#                                         <p>""" + config.APP_NAME + """ will help you access and follow content of interest on the internet as well as create page profiles for that which matters to you.</p>
#                                         <p>Regards<br /><br />""" + config.CO_TEAM + """</p>
#                                         <p><small><strong>Powered by """ + config.SIGN_OFF + """</strong></small></p>
#                                     </section>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/plugins/jQuery/jQuery-2.1.4.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/bootstrap/js/bootstrap.min.js'></script>
#                                     <script src='""" + config.HTTP_SCHEME + """://""" + config.APP_DOMAIN + """/static/dist/js/app.min.js'></script>
#                                 </body>
#                             </html>
#                         """

#             return sendMail(config.SMTP_USERNAME, emailAddress, subject, message)
#         else:
#             return "Not sent"
#     else:
#         return "Invalid user"
