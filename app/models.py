import enum
import uuid

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.postgresql import UUID, JSONB
from sqlalchemy.sql import select, func

from datetime import datetime

db = SQLAlchemy()

# BASE
class BaseModel(db.Model):

    '''Base data model for all objects'''

    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    uid = db.Column(UUID(as_uuid=True), default=uuid.uuid4(), index=True, nullable=False)
    status = db.Column(db.Integer, default=1, nullable=False)
    created_at = db.Column(db.DateTime, default=db.func.current_timestamp(), nullable=False)
    modified_at = db.Column(db.DateTime, onupdate=db.func.current_timestamp())

class Media(BaseModel):
    __tablename__ = 'media'
    file_url = db.Column(db.Text)
    caption = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# USERS
class Auth(BaseModel):
    __tablename__ = 'auth'
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    username = db.Column(db.Text)
    password = db.Column(db.Text)
    activation_code = db.Column(db.Text)
    reset_code = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class User(BaseModel):
    __tablename__ = 'users'
    email_address = db.Column(db.String(100), nullable=False)
    phone_number = db.Column(db.String(100), nullable=True)
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    avatar_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=True)

class UserRealm(BaseModel):
    __tablename__ = 'user_realms'
    realm = db.Column(db.Integer) # IGH, Customer or Partner
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=True)

class UserInvite(BaseModel):
    __tablename__ = 'user_invites'
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    accept = db.Column(db.Integer)
    igh = db.Column(db.Integer)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"))
    partner_id = db.Column(db.Integer, db.ForeignKey('partners.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=True)

class Role(BaseModel):
    __tablename__ = 'roles'
    name = db.Column(db.String(45), nullable=False)
    description = db.Column(db.Text)
    user_realm = db.Column(db.Integer) # IGH, Customer or Partner
    super_admin = db.Column(db.Integer, default=0)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"))
    partner_id = db.Column(db.Integer, db.ForeignKey('partners.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=True)

class UserRole(BaseModel):
    __tablename__ = 'user_roles'
    user_realm = db.Column(db.Integer) # IGH, Customer or Partner
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class IghUser(BaseModel):
    __tablename__ = 'igh_users'
    id_number = db.Column(db.String(50))
    employee_id = db.Column(db.String(50))
    rank = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerUser(BaseModel):
    __tablename__ = 'customer_users'
    employee_id = db.Column(db.String(50))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PartnerUser(BaseModel):
    __tablename__ = 'partner_users'
    employee_id = db.Column(db.String(50))
    partner_id = db.Column(db.Integer, db.ForeignKey('partners.id', ondelete="CASCADE"), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class UserConfig(BaseModel):
    __tablename__ = 'user_configs'
    temp_metric = db.Column(db.Integer)
    liquid_metric = db.Column(db.Integer)
    length_metric = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# PERMISSIONS
class Module(BaseModel):
    __tablename__ = 'modules'
    name = db.Column(db.Text)
    user_realm = db.Column(db.Integer) # IGH, Customer or Partner
    icon_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ModuleFeature(BaseModel):
    __tablename__ = 'module_features'
    name = db.Column(db.Text)
    code = db.Column(db.Text)
    module_id = db.Column(db.Integer, db.ForeignKey('modules.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ModulePermission(BaseModel):
    __tablename__ = 'module_permissions'
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id', ondelete="CASCADE"))
    module_feature_id = db.Column(db.Integer, db.ForeignKey('module_features.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# FARMS
class Farm(BaseModel):
    __tablename__ = 'farms'
    farm_ref = db.Column(db.Text)
    name = db.Column(db.String(45), nullable=False)
    address = db.Column(db.Text)
    state = db.Column(db.Text)
    city = db.Column(db.Text)
    pincode = db.Column(db.Text)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    area = db.Column(db.Float)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class FarmActions(BaseModel):
    __tablename__ = 'farm_actions'
    name = db.Column(db.Text)
    description = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# CUSTOMERS
class Customer(BaseModel):
    __tablename__ = 'customers'
    name = db.Column(db.String(45), nullable=False)
    billing_address = db.Column(db.Text)
    shipping_address = db.Column(db.Text)
    igh_contact_id = db.Column(db.Integer, db.ForeignKey('igh_users.id', ondelete="CASCADE"))
    contact_person_id = db.Column(db.Integer, db.ForeignKey('customer_users.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerFarm(BaseModel):
    __tablename__ = 'customer_farms'
    farm_id = db.Column(db.Integer, db.ForeignKey('farms.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerGreenhouse(BaseModel):
    __tablename__ = 'customer_greenhouses'
    greenhouse_id = db.Column(db.Integer, db.ForeignKey('greenhouses.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerFarmGreenhouse(BaseModel):
    __tablename__ = 'customer_farm_greenhouses'
    name = db.Column(db.String(45), nullable=False)
    customer_farm_id = db.Column(db.Integer, db.ForeignKey('customer_farms.id', ondelete="CASCADE"), nullable=False)
    customer_greenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_greenhouses.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerShield(BaseModel):
    __tablename__ = 'customer_shields'
    shield_id = db.Column(db.Integer, db.ForeignKey('shields.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerGreenhouseShield(BaseModel):
    __tablename__ = 'customer_greenhouse_shields'
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    growth_area = db.Column(db.Integer)
    soil_type_id = db.Column(db.Integer, db.ForeignKey('soil_types.id', ondelete="CASCADE"), nullable=False)
    customer_farm_greenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_farm_greenhouses.id', ondelete="CASCADE"))
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerExternalShield(BaseModel):
    __tablename__ = 'customer_external_shields'
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    growth_area = db.Column(db.Integer)
    customer_farm_id = db.Column(db.Integer, db.ForeignKey('customer_farms.id', ondelete="CASCADE"), nullable=False)
    soil_type_id = db.Column(db.Integer, db.ForeignKey('soil_types.id', ondelete="CASCADE"), nullable=False)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerScreenhouse(BaseModel):
    __tablename__ = 'customer_screenhouses'
    screenhouse_id = db.Column(db.Integer, db.ForeignKey('screenhouses.id', ondelete="CASCADE"))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerGreenhouseScreenhouse(BaseModel):
    __tablename__ = 'customer_greenhouse_screenhouses'
    customer_farm_greenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_farm_greenhouses.id', ondelete="CASCADE"))
    customer_screenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_screenhouses.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerShadenet(BaseModel):
    __tablename__ = 'customer_shadenets'
    shadenet_id = db.Column(db.Integer, db.ForeignKey('shadenets.id', ondelete="CASCADE"))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerGreenhouseShadenet(BaseModel):
    __tablename__ = 'customer_greenhouse_shadenets'
    customer_farm_greenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_farm_greenhouses.id', ondelete="CASCADE"))
    customer_shadenet_id = db.Column(db.Integer, db.ForeignKey('customer_shadenets.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class CustomerSubscription(BaseModel):
    __tablename__ = 'customer_subscription'
    starts = db.Column(db.DateTime)
    ends = db.Column(db.DateTime)
    is_active = db.Column(db.Integer)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoices.id', ondelete="CASCADE"))
    sale_type_id = db.Column(db.Integer, db.ForeignKey('sale_types.id', ondelete="CASCADE"))
    subscription_id = db.Column(db.Integer, db.ForeignKey('subscriptions.id', ondelete="CASCADE"))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

# SUBSCRIPTIONS
class SaleType(BaseModel):
    __tablename__ = 'sale_types'
    name = db.Column(db.Text)
    description = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Subscription(BaseModel):
    __tablename__ = 'subscriptions'
    name = db.Column(db.Text)
    cylce = db.Column(db.Integer)
    amount = db.Column(db.Integer)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

class SubscriptionParticular(BaseModel):
    __tablename__ = 'subscription_particulars'
    item = db.Column(db.Text)
    cost = db.Column(db.Integer)
    subscription_id = db.Column(db.Integer, db.ForeignKey('subscriptions.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

# PARTNERS
class Partner(BaseModel):
    __tablename__ = 'partners'
    name = db.Column(db.String(45), nullable=False)
    notes = db.Column(db.Text)
    igh_contact_id = db.Column(db.Integer, db.ForeignKey('igh_users.id', ondelete="CASCADE"), nullable=True)
    contact_person_id = db.Column(db.Integer, db.ForeignKey('partner_users.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# SUPPLIES
class Commodity(BaseModel):
    __tablename__ = 'commodities'
    name = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Supplier(BaseModel):
    __tablename__ = 'suppliers'
    name = db.Column(db.Text, nullable=False)
    notes = db.Column(db.Text)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SupplierCommodities(BaseModel):
    __tablename__ = 'supplier_commodities'
    name = db.Column(db.Text, nullable=False)
    type = db.Column(db.Integer) # Fertilizer or agrochemical
    notes = db.Column(db.Text)
    commodity_id = db.Column(db.Integer, db.ForeignKey('commodities.id', ondelete="CASCADE"), nullable=True)
    supplier_id = db.Column(db.Integer, db.ForeignKey('suppliers.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Supply(BaseModel):
    __tablename__ = 'supplies'
    name = db.Column(db.Text)
    commodity = db.Column(db.Integer) # Fertilizer or agrochemical
    metric = db.Column(db.Text)
    quantity = db.Column(db.Integer, default=0)
    minimum = db.Column(db.Integer, default=0)
    notes = db.Column(db.Text)
    active_component_id = db.Column(db.Integer, db.ForeignKey('supply_components.id', ondelete="CASCADE"), nullable=True)
    supplier_commodity_id = db.Column(db.Integer, db.ForeignKey('supplier_commodities.id', ondelete="CASCADE"), nullable=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SupplyComponents(BaseModel):
    __tablename__ = 'supply_components'
    component = db.Column(db.Text)
    supply_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SupplyPhoto(BaseModel):
    __tablename__ = 'supply_photos'
    photo_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    supplies_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SupplyMovement(BaseModel):
    __tablename__ = 'supply_quantities'
    movement = db.Column(db.Integer)
    supply_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SupplyApplicationMethod(BaseModel):
    __tablename__ = 'supply_application_methods'
    name = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text)
    supply_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# PESTS & DISEASES
class Pest(BaseModel):
    __tablename__ = 'pests'
    name = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PestPhoto(BaseModel):
    __tablename__ = 'pest_photos'
    photo_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    pest_id = db.Column(db.Integer, db.ForeignKey('pests.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Disease(BaseModel):
    __tablename__ = 'diseases'
    name = db.Column(db.Text, nullable=False)
    description = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class DiseasePhoto(BaseModel):
    __tablename__ = 'disease_photos'
    photo_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    disease_id = db.Column(db.Integer, db.ForeignKey('diseases.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# INVENTORY
class GreenhouseType(BaseModel):
    __tablename__ = 'greenhouse_types'
    name = db.Column(db.String(45), nullable=False)
    size = db.Column(db.Text)
    description = db.Column(db.Text)
    info_link = db.Column(db.Text)
    photo_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Greenhouse(BaseModel):
    __tablename__ = 'greenhouses'
    greenhouse_ref = db.Column(db.Text, nullable=False)
    size = db.Column(db.Text)
    info_link = db.Column(db.Text)
    greenhouse_type_id = db.Column(db.Integer, db.ForeignKey('greenhouse_types.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Screenhouse(BaseModel):
    __tablename__ = 'screenhouses'
    screenhouse_ref = db.Column(db.Text, nullable=False)
    size = db.Column(db.Text)
    info_link = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Shadenet(BaseModel):
    __tablename__ = 'shadenets'
    shadenet_ref = db.Column(db.Text, nullable=False)
    size = db.Column(db.Text)
    info_link = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SensorType(BaseModel):
    __tablename__ = 'sensor_types'
    name = db.Column(db.Text)
    description = db.Column(db.Text)
    info_link = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ShieldType(BaseModel):
    __tablename__ = 'shield_types'
    name = db.Column(db.String(45), nullable=False)
    photo_url = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    description = db.Column(db.Text)
    placement = db.Column(db.Integer)
    info_link = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Shield(BaseModel):
    __tablename__ = 'shields'
    serial = db.Column(db.Text)
    mac_address = db.Column(db.Text)
    type_id = db.Column(db.Integer, db.ForeignKey('shield_types.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ShieldSensor(BaseModel):
    __tablename__ = 'shield_sensors'
    shield_id = db.Column(db.Integer, db.ForeignKey('shields.id', ondelete="CASCADE"), nullable=False)
    sensor_type_id = db.Column(db.Integer, db.ForeignKey('sensor_types.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Spear(BaseModel):
    __tablename__ = 'spears'
    serial = db.Column(db.Text)
    mac_address = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ShieldSpear(BaseModel):
    __tablename__ = 'shield_spears'
    shield_id = db.Column(db.Integer, db.ForeignKey('shields.id', ondelete="CASCADE"), nullable=False)
    spear_id = db.Column(db.Integer, db.ForeignKey('spears.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# PLANTING
class Plants(BaseModel):
    __tablename__ = 'plants'
    name = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Variety(BaseModel):
    __tablename__ = 'varieties'
    name = db.Column(db.Text)
    plant_id = db.Column(db.Integer, db.ForeignKey('plants.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ProductionStage(BaseModel):
    __tablename__ = 'production_stages'
    name = db.Column(db.Text)
    description = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Planting(BaseModel):
    __tablename__ = 'planting'
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"), nullable=False)
    current_production_stage_id = db.Column(db.Integer, db.ForeignKey('planting_production_stages.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PlantingProductionStage(BaseModel):
    __tablename__ = 'planting_production_stages'
    planting_id = db.Column(db.Integer, db.ForeignKey('planting.id', ondelete="CASCADE"), nullable=False)
    production_stage_id = db.Column(db.Integer, db.ForeignKey('production_stages.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PlantingCrops(BaseModel):
    __tablename__ = 'planting_crops'
    start_date = db.Column(db.DateTime, nullable=False)
    seedling_tally = db.Column(db.Integer, default=0)
    harvest_estimate = db.Column(db.Integer, default=0)
    planting_period = db.Column(db.Integer, default=0)
    maturity_period = db.Column(db.Integer, default=0)
    production_period = db.Column(db.Integer, default=0)
    harvest_period = db.Column(db.Integer, default=0)
    crop_id = db.Column(db.Integer, db.ForeignKey('varieties.id', ondelete="CASCADE"), nullable=False)
    planting_id = db.Column(db.Integer, db.ForeignKey('planting.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# SHIELD SETTINGS
class ShieldPlantingSetting(BaseModel):
    __tablename__ = 'shield_planting_settings'
    planting_id = db.Column(db.Integer, db.ForeignKey('planting.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class WaterTankSetting(BaseModel):
    __tablename__ = 'water_tank_settings'
    capacity_litres = db.Column(db.Integer, default=0)
    irrigation_time_min = db.Column(db.Integer, default=0)
    irrigation_time_max = db.Column(db.Integer, default=0)
    irrigation_start_time = db.Column(db.Text, nullable=True)
    irrigation_end_time = db.Column(db.Text, nullable=True)
    irrigation_interval = db.Column(db.Text, nullable=True)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Co2Setting(BaseModel):
    __tablename__ = 'co_2_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class AirTemperatureSetting(BaseModel):
    __tablename__ = 'air_temperature_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class AirHumiditySetting(BaseModel):
    __tablename__ = 'air_humidity_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class LightSetting(BaseModel):
    __tablename__ = 'light_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SoilMoistureSetting(BaseModel):
    __tablename__ = 'soil_moisture_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SoilTemperatureSetting(BaseModel):
    __tablename__ = 'soil_temperature_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class NitrogenSetting(BaseModel):
    __tablename__ = 'nitrogen_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PhosphorusSetting(BaseModel):
    __tablename__ = 'phosphorus_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PotassiumSetting(BaseModel):
    __tablename__ = 'potassium_settings'
    min = db.Column(db.Integer, default=0)
    max = db.Column(db.Integer, default=0)
    shield_planting_setting_id = db.Column(db.Integer, db.ForeignKey('shield_planting_settings.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# SUPPORT & MAINTENANCE
class Issue(BaseModel):
    __tablename__ = 'issues'
    name = db.Column(db.Text)
    description = db.Column(db.Text)
    applies_to = db.Column(db.Integer)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Maintenance(BaseModel):
    __tablename__ = 'maintenance'
    serial = db.Column(db.Text, nullable=False)
    type = db.Column(db.Integer)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class MaintenancePhoto(BaseModel):
    __tablename__ = 'maintenance_photos'
    photo_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PestDisease(BaseModel):
    __tablename__ = 'pests_diseases'
    notes = db.Column(db.Integer)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    production_stage_id = db.Column(db.Integer, db.ForeignKey('production_stages.id', ondelete="CASCADE"), nullable=False)
    variety_id = db.Column(db.Integer, db.ForeignKey('varieties.id', ondelete="CASCADE"), nullable=False)
    plant_id = db.Column(db.Integer, db.ForeignKey('plants.id', ondelete="CASCADE"), nullable=False)
    customer_greenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_greenhouses.id', ondelete="CASCADE"), nullable=True)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"), nullable=True)
    pest_id = db.Column(db.Integer, db.ForeignKey('pests.id', ondelete="CASCADE"), nullable=True)
    disease_id = db.Column(db.Integer, db.ForeignKey('diseases.id', ondelete="CASCADE"), nullable=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class AgronomistVisit(BaseModel):
    __tablename__ = 'agronomist_visit'
    visit_date = db.Column(db.Date)
    visit_time = db.Column(db.Text)
    reorder_fertilizer = db.Column(db.Integer, default=0)
    reorder_agrochemical = db.Column(db.Integer, default=0)
    fertilizer_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"))
    fertilizer_main_component_id = db.Column(db.Integer, db.ForeignKey('supply_components.id', ondelete="CASCADE"))
    fertilizer_amount = db.Column(db.Integer)
    agrochemical_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"))
    agrochemical_main_component_id = db.Column(db.Integer, db.ForeignKey('supply_components.id', ondelete="CASCADE"))
    agrochemical_amount = db.Column(db.Integer)
    notes = db.Column(db.Text)
    observations = db.Column(db.Text)
    risk_observations = db.Column(db.Text)
    recommendations = db.Column(db.Text)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ShieldMaintenance(BaseModel):
    __tablename__ = 'shield_maintenance'
    notes = db.Column(db.Text)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ShieldMaintenanceIssue(BaseModel):
    __tablename__ = 'shield_maintenance_issues'
    issue_id = db.Column(db.Integer, db.ForeignKey('issues.id', ondelete="CASCADE"), nullable=False)
    shield_maintenance_id = db.Column(db.Integer, db.ForeignKey('shield_maintenance.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class GreenhouseMaintenance(BaseModel):
    __tablename__ = 'greenhouse_maintenance'
    notes = db.Column(db.Text)
    customer_greenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_greenhouses.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class GreenhouseMaintenanceIssue(BaseModel):
    __tablename__ = 'greenhouse_maintenance_issues'
    issue_id = db.Column(db.Integer, db.ForeignKey('issues.id', ondelete="CASCADE"), nullable=False)
    greenhouse_maintenance_id = db.Column(db.Integer, db.ForeignKey('greenhouse_maintenance.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ScreenhouseMaintenance(BaseModel):
    __tablename__ = 'screenhouse_maintenance'
    notes = db.Column(db.Text)
    customer_screenhouse_id = db.Column(db.Integer, db.ForeignKey('customer_screenhouses.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ScreenhouseMaintenanceIssue(BaseModel):
    __tablename__ = 'screenhouse_maintenance_issues'
    issue_id = db.Column(db.Integer, db.ForeignKey('issues.id', ondelete="CASCADE"), nullable=False)
    screenhouse_maintenance_id = db.Column(db.Integer, db.ForeignKey('screenhouse_maintenance.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ShadenetMaintenance(BaseModel):
    __tablename__ = 'shadenet_maintenance'
    notes = db.Column(db.Text)
    customer_shadenet_id = db.Column(db.Integer, db.ForeignKey('customer_shadenets.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ShadenetMaintenanceIssue(BaseModel):
    __tablename__ = 'shadenet_maintenance_issues'
    issue_id = db.Column(db.Integer, db.ForeignKey('issues.id', ondelete="CASCADE"), nullable=False)
    shadenet_maintenance_id = db.Column(db.Integer, db.ForeignKey('shadenet_maintenance.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityReportRequest(BaseModel):
    __tablename__ = 'traceability_report_requests'
    notes = db.Column(db.Text)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityReportCrop(BaseModel):
    __tablename__ = 'traceability_report_crops'
    crop_planting_id = db.Column(db.Integer, db.ForeignKey('planting_crops.id', ondelete="CASCADE"), nullable=False)
    traceability_report_request_id = db.Column(db.Integer, db.ForeignKey('traceability_report_requests.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class OtherMaintenanceReport(BaseModel):
    __tablename__ = 'other_maintenance_reports'
    notes = db.Column(db.Text)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class MaintenanceAssignment(BaseModel):
    __tablename__ = 'maintenance_assignment'
    customer_rating = db.Column(db.Integer)
    scheduled = db.Column(db.DateTime)
    igh_user_id = db.Column(db.Integer, db.ForeignKey('igh_users.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class MaintenanceClosure(BaseModel):
    __tablename__ = 'maintenance_closures'
    notes = db.Column(db.Text)
    igh_user_id = db.Column(db.Integer, db.ForeignKey('igh_users.id', ondelete="CASCADE"), nullable=False)
    maintenance_id = db.Column(db.Integer, db.ForeignKey('maintenance.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# ALERTS
class Alert(BaseModel):
    __tablename__ = 'alerts'
    type = db.Column(db.Integer)
    severity = db.Column(db.Integer)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

class SystemAlert(BaseModel):
    __tablename__ = 'system_alerts'
    name = db.Column(db.Text)
    detail = db.Column(db.Text)
    trigger = db.Column(db.Text)
    alert_id = db.Column(db.Integer, db.ForeignKey('alerts.id', ondelete="CASCADE"))

class ShieldAlert(BaseModel):
    __tablename__ = 'shield_alerts'
    name = db.Column(db.Text)
    current_read = db.Column(db.Text)
    ideal_read = db.Column(db.Text)
    shield_id = db.Column(db.Integer, db.ForeignKey('shields.id', ondelete="CASCADE"))
    sensor_type_id = db.Column(db.Integer, db.ForeignKey('sensor_types.id', ondelete="CASCADE"))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"))

class UserAlert(BaseModel):
    __tablename__ = 'user_alerts'
    name = db.Column(db.Text)
    detail = db.Column(db.Text)
    trigger = db.Column(db.Text)
    action_text = db.Column(db.Text)
    action_link = db.Column(db.Text)
    alert_id = db.Column(db.Integer, db.ForeignKey('alerts.id', ondelete="CASCADE"))
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

class AlertConfig(BaseModel):
    __tablename__ = 'alert_configs'
    user_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
    preferred_mode = db.Column(db.Integer, nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# REPORTS
class Report(BaseModel):
    __tablename__ = 'reports'
    type = db.Column(db.Integer)
    score = db.Column(db.Integer)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class WastePollutionManagement(BaseModel):
    __tablename__ = 'waste_pollution_management'
    residue = db.Column(db.Text) # Waste/Pollution
    source = db.Column(db.Text)
    eliminated_amount = db.Column(db.Integer)
    reduced_amount = db.Column(db.Integer)
    recycled_amount = db.Column(db.Integer)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class FertilizerApplication(BaseModel):
    __tablename__ = 'fertilizer_application'
    date_time_applied = db.Column(db.DateTime)
    quantity_applied = db.Column(db.Integer)
    supply_application_method_id = db.Column(db.Integer, db.ForeignKey('supply_application_methods.id', ondelete="CASCADE"))
    fertilizer_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"))
    fertilizer_main_component = db.Column(db.Integer, db.ForeignKey('supply_components.id', ondelete="CASCADE"))
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class AgrochemicalApplication(BaseModel):
    __tablename__ = 'agrochemical_application'
    date_time_applied = db.Column(db.DateTime)
    rei = db.Column(db.Integer)
    phi = db.Column(db.Integer)
    re_entry_date = db.Column(db.Date)
    preharvest_interval_expiration_date = db.Column(db.Date)
    pest_id = db.Column(db.Integer, db.ForeignKey('pests.id', ondelete="CASCADE"))
    supply_application_method_id = db.Column(db.Integer, db.ForeignKey('supply_application_methods.id', ondelete="CASCADE"))
    agrochemical_id = db.Column(db.Integer, db.ForeignKey('supplies.id', ondelete="CASCADE"))
    agrochemical_main_component = db.Column(db.Integer, db.ForeignKey('supply_components.id', ondelete="CASCADE"))
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class PreharvestChecklist(BaseModel):
    __tablename__ = 'preharvest_checklist'
    animal_intrusion = db.Column(db.Integer)
    crew_washed_hands = db.Column(db.Integer)
    crew_clothing_is_clean = db.Column(db.Integer)
    crew_without_jewellery = db.Column(db.Integer)
    packing_material_clean = db.Column(db.Integer)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Harvest(BaseModel):
    __tablename__ = 'harvest'
    harvest_date_time = db.Column(db.DateTime)
    kilos_harvested = db.Column(db.Integer)
    kilos_rejected = db.Column(db.Integer)
    kilos_lost_in_transit = db.Column(db.Integer)
    kilos_sold = db.Column(db.Integer)
    kilo_price = db.Column(db.Integer)
    total_income = db.Column(db.Integer)
    variety_id = db.Column(db.Integer, db.ForeignKey('varieties.id', ondelete="CASCADE"))
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class Scouting(BaseModel):
    __tablename__ = 'scouting'
    leaf_miners = db.Column(db.Integer)
    ball_worms = db.Column(db.Integer)
    thrips = db.Column(db.Integer)
    caterpillars = db.Column(db.Integer)
    mites = db.Column(db.Integer)
    scales = db.Column(db.Integer)
    bean_flies = db.Column(db.Integer)
    white_flies = db.Column(db.Integer)
    cut_worms = db.Column(db.Integer)
    other_insects = db.Column(db.Integer)
    aschochtya = db.Column(db.Integer)
    botrytis = db.Column(db.Integer)
    downy = db.Column(db.Integer)
    powdery = db.Column(db.Integer)
    leaf_spots = db.Column(db.Integer)
    halo_blight = db.Column(db.Integer)
    other_diseases = db.Column(db.Integer)
    weeds = db.Column(db.Integer)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class ScoutingAgronomistRecommendation(BaseModel):
    __tablename__ = 'scouting_agronomist_recommendations'
    spray = db.Column(db.Integer)
    agrochem = db.Column(db.Text)
    active_ingredient = db.Column(db.Text)
    agrochem_quantity = db.Column(db.Text)
    water_amount = db.Column(db.Text)
    application_rate = db.Column(db.Text)
    application_method_id = db.Column(db.Integer, db.ForeignKey('supply_application_methods.id', ondelete="CASCADE"))
    application_start_date = db.Column(db.Date)
    scouting_id = db.Column(db.Integer, db.ForeignKey('scouting.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityReport(BaseModel):
    __tablename__ = 'traceability_reports'
    traceability_report_request_id = db.Column(db.Integer, db.ForeignKey('traceability_report_requests.id', ondelete="CASCADE"), nullable=False)
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    plant_id = db.Column(db.Integer, db.ForeignKey('plants.id', ondelete="CASCADE"), nullable=False)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    customer_farm_id = db.Column(db.Integer, db.ForeignKey('customer_farms.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityPlantQuery(BaseModel):
    __tablename__ = 'traceability_plant_queries'
    planting_date = db.Column(db.Integer)
    crop_variety = db.Column(db.Integer)
    traceability_report_id = db.Column(db.Integer, db.ForeignKey('traceability_reports.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityFertilizerQuery(BaseModel):
    __tablename__ = 'traceability_fertilizer_queries'
    name = db.Column(db.Integer)
    component = db.Column(db.Integer)
    application_date = db.Column(db.Integer)
    traceability_report_id = db.Column(db.Integer, db.ForeignKey('traceability_reports.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityAgrochemicalQuery(BaseModel):
    __tablename__ = 'traceability_agrochemical_queries'
    name = db.Column(db.Integer)
    component = db.Column(db.Integer)
    application_date = db.Column(db.Integer)
    traceability_report_id = db.Column(db.Integer, db.ForeignKey('traceability_reports.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityHarvestQuery(BaseModel):
    __tablename__ = 'traceability_harvest_queries'
    harvest_date = db.Column(db.Integer)
    harvest_time = db.Column(db.Integer)
    traceability_report_id = db.Column(db.Integer, db.ForeignKey('traceability_reports.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class TraceabilityIrrigationQuery(BaseModel):
    __tablename__ = 'traceability_irrigation_queries'
    monthly_irrigation = db.Column(db.Integer)
    traceability_report_id = db.Column(db.Integer, db.ForeignKey('traceability_reports.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class OtherReport(BaseModel):
    __tablename__ = 'other_reports'
    notes = db.Column(db.Text)
    farm_action_id = db.Column(db.Integer, db.ForeignKey('farm_actions.id', ondelete="CASCADE"))
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))
    report_id = db.Column(db.Integer, db.ForeignKey('reports.id', ondelete="CASCADE"), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# LOGS
class UserLog(BaseModel):
    __tablename__ = 'user_logs'
    action = db.Column(db.Text)
    ref = db.Column(db.Text)
    ref_id = db.Column(db.Integer)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"))
    partner_id = db.Column(db.Integer, db.ForeignKey('partners.id', ondelete="CASCADE"))
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

# WEATHER
class WeatherUpdate(BaseModel):
    __tablename__ = 'weather_updates'
    city = db.Column(db.Text)
    narration = db.Column(db.Text)
    temp = db.Column(db.Integer)
    icon_url = db.Column(db.Text)

# INVOICES
class Invoice(BaseModel):
    __tablename__ = 'invoices'
    serial = db.Column(db.Integer)
    gross = db.Column(db.Integer)
    tax = db.Column(db.Integer)
    deductions = db.Column(db.Integer)
    net = db.Column(db.Integer)
    fully_paid = db.Column(db.Integer)
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

class InvoiceParticular(BaseModel):
    __tablename__ = 'invoice_particulars'
    item = db.Column(db.Text)
    pax = db.Column(db.Integer)
    amount = db.Column(db.Integer)
    discount = db.Column(db.Integer)
    total = db.Column(db.Integer)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoices.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

class InvoicePayment(BaseModel):
    __tablename__ = 'invoice_payments'
    serial = db.Column(db.Integer)
    amount = db.Column(db.Integer)
    narrative = db.Column(db.Text)
    payment_status = db.Column(db.Integer)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoices.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

class CreditNote(BaseModel):
    __tablename__ = 'credit_notes'
    serial = db.Column(db.Integer)
    amount = db.Column(db.Integer)
    narrative = db.Column(db.Text)
    invoice_id = db.Column(db.Integer, db.ForeignKey('invoices.id', ondelete="CASCADE"), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"))

# DATA
class ShieldData(BaseModel):
    __tablename__ = 'shield_data'
    batch = db.Column(JSONB)
    data_source = db.Column(db.Integer)
    customer_shield_id = db.Column(db.Integer, db.ForeignKey('customer_shields.id', ondelete="CASCADE"))

# MISC
class Service(BaseModel):
    __tablename__ = 'services'
    name = db.Column(db.Text, nullable=False)
    link = db.Column(db.Text)
    description = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)

class SoilType(BaseModel):
    __tablename__ = 'soil_types'
    name = db.Column(db.String(45), nullable=False)
    photo_media_id = db.Column(db.Integer, db.ForeignKey('media.id', ondelete="CASCADE"))
    description = db.Column(db.Text)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.id', ondelete="CASCADE"), nullable=False)
