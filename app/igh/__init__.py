from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid
from werkzeug.exceptions import HTTPException
from werkzeug.security import check_password_hash, generate_password_hash

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig
from app import *
from app.core import alert, customer
from app.core import user
from app.core.user import invites
from app.core.user import auth
from app.core.user import realms
from app.core.user import configs
from app.core.user import user_roles
from app.core.user import igh
from app.core.user import customers
from app.core.user import partners

from app.helper import validateEmailAddress, valid_phone_number, send_account_activation_mail

dashboard = Blueprint('dashboard', __name__, template_folder='templates', static_folder='static',  static_url_path='/igh/static')
dashboard.config = {}
load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def session_setup(user_id):
    _active_user = user.fetch_one(user_id, True).get_json()
    print("active user",_active_user)
    if _active_user:
        # _active_user = user.fetch_by_id(user_id).get_json()
        _active_realm = realms.fetch_by_user(_active_user["real_id"]).get_json()
        _user_configs = configs.fetch_by_user(_active_user["real_id"]).get_json()
        _user_role = user_roles.fetch_by_user(_active_user["real_id"]).get_json()
        _alert_config = alert.fetch_alert_config(_active_user["real_id"])[0].get_json()
        print('user session1',_active_realm)
        print('user session2',_user_configs)
        print('user session3',_user_role)
        print('user session4',_alert_config)
        _staff_info = {}
        if _active_realm[0]['realm'] == config.USER_IGH:
            _staff_info = igh.fetch_by_user(_active_user["real_id"]).get_json()
            # print('user session5',_staff_info)
        if _active_realm[0]['realm'] == config.USER_CUSTOMER:
            _staff_info = customer.fetch_by_user(_active_user["real_id"]).get_json()
            # print('user session6',_staff_info)
        if _active_realm[0]['realm'] == config.USER_PARTNER:
            _staff_info = partners.fetch_by_user(_active_user["real_id"]).get_json()
            # print('user session7',_staff_info)
        if _active_user and _active_realm and _user_configs and _user_role:
            session["profile"] = {
                "user": _active_user,
                "realm": realms.realm_name(_active_realm[0]['realm']),
                "configs": _user_configs,
                "role": _user_role,
                "staff_info": _staff_info,
                "alert_config": _alert_config
            }
           # print('user session8',session["profile"])

def portal_check(session, portal=None):

    if portal is not None:
        known_realm = ""
        if "profile" in session:
            if 'realm' in session['profile'] and session['profile']['realm'] != "":
                known_realm = ""
                if session['profile']['realm'].split(" ")[0] == 'IGH':
                    known_realm = 'admin'

                if session['profile']['realm'].split(" ")[0] == 'Customer':
                    known_realm = 'customer'

                if session['profile']['realm'].split(" ")[0] == 'Partner':
                    known_realm = 'partner'

        if portal != known_realm:
                    return True, redirect("/" + known_realm)

        return False, False

    return True, redirect("/login")

@dashboard.route('/', methods=['POST', 'GET'])
def main():
    if "profile" not in session:
        return redirect("/login")

    if 'realm' in session['profile'] and session['profile']['realm'] != "":
       if session['profile']['realm'] == 'IGH':
            return redirect("/admin")

    if session['profile']['realm'] == 'Customer':
            return redirect("/customer")

    return render_template('error.html')

@dashboard.route('/login', methods=['POST', 'GET'])
def login():
    action_response = {}
    _posted = request.form
    if len(_posted) > 0:
        if 'username' in _posted and _posted['username'].strip() != "" and \
            'password' in _posted and _posted['password'].strip() != "":
            _user = auth.fetch_by_username(_posted['username']).get_json()
            if _user:
                if check_password_hash(_user['password'], _posted['password'].strip()):
                    session_setup(_user["user"]["id"])
                    
                    return redirect("/")

            action_response = {
                "type": "danger",
                "message": "Invalid credentials supplied"
            }

        else:
            action_response = {
                "type": "warning",
                "message": "Email address and password pair required to login"
            }

    return render_template(
        'login.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date":str(datetime.now().year)}
    )

@dashboard.route('/request-invite', methods=['POST', 'GET'])
def request_invite():
    action_response = {}
    _posted = request.form

    return render_template(
        'request-invite.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date":str(datetime.now().year)}
    )

@dashboard.route('/invite/<invite_id>', methods=['POST', 'GET'])
def invite(invite_id):
    action_response = {}
    _posted = request.form

    invite_accepted = 0
    invitation = invites.fetch_one(invite_id, True).get_json()
    if not invitation:
        action_response = {
            "type": "danger",
            "message": "Invite link is invalid."
        }
    else:
        invite_accepted = invitation['accept']

        if invitation['accept'] == 1:
            action_response = {
                "type": "warning",
                "message": "Invite has already been accepted."
            }
        else:
            if len(_posted) > 0:
                if 'username' in _posted and validateEmailAddress(_posted['username'].strip()) and \
                    'password' in _posted and _posted['password'].strip() != "" and \
                    'password2' in _posted and _posted['password2'].strip() != "":

                    if _posted['password'].strip() == _posted['password2'].strip():
                        _user = user.fetch_one(invitation['user']['id'], True).get_json()
                        if _user:
                            _user_auth = auth.fetch_by_user(_user['real_id']).get_json()
                            if _user_auth:
                                if _user_auth['username'] == _posted['username'].strip():
                                    _auth_accept = auth.edit(
                                        _user_auth['id'],
                                        json.dumps({
                                            "password": _posted['password'].strip(),
                                            "activation_code": "-",
                                            "reset_code": "-",
                                        }),
                                        True
                                    )

                                    if _auth_accept[1] == 200:
                                        invites.edit(
                                            invitation['id'],
                                            json.dumps(
                                                {
                                                    "accept": 1
                                                }
                                            )
                                        )
                                        action_response = {
                                            "type": "success",
                                            "message": "Invite accepted and account activated, you may now login"
                                        }
                                    else:
                                        action_response = {
                                            "type": "danger",
                                            "message": "An error occured, try again later."
                                        }
                                else:
                                    action_response = {
                                        "type": "danger",
                                        "message": "Email address provided does not match that in the invite."
                                    }
                            else:
                                action_response = {
                                    "type": "danger",
                                    "message": "Invitation is invalid."
                                }
                        else:
                            action_response = {
                                "type": "danger",
                                "message": "User is invalid."
                            }
                    else:
                        action_response = {
                            "type": "danger",
                            "message": "Passwords provided do not match"
                        }
                else:
                    action_response = {
                        "type": "warning",
                        "message": "Email and password required to activate"
                    }

    return render_template(
        'invite.html',
        ACCEPTED=invite_accepted,
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date":str(datetime.now().year)}
    )

@dashboard.route('/register', methods=['POST', 'GET'])
def register():
    action_response = {}
    _posted = request.form
    if len(_posted) > 0:
        if 'first_name' in _posted and _posted['first_name'].strip() != "" and \
            'last_name' in _posted and _posted['last_name'].strip() != "" \
            'email_address' in _posted and validateEmailAddress(_posted['email_address'].strip()) != "":

            exists = user.auth.fetch_by_username(_posted['email_address']).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please use a different one."
                }
            else:
                _role_id = None
                _default_role = user.roles.fetch_by_name("Customer Admin").get_json()
                if _default_role:
                    _role_id = _default_role['id']
                new_user = user.add_new(
                    json.dumps({
                        "email_address": _posted['email_address'].strip(),
                        "first_name": _posted['first_name'].strip(),
                        "last_name": _posted['last_name'].strip(),
                        "phone_number": _posted['phone_number'].strip(),
                        "user_realm": config.USER_CUSTOMER,
                        "role_id": _role_id
                    })
                )
                print(str(new_user[0].get_json()))
        else:
            action_response = {
                "type": "warning",
                "message": "First name, last name, phone number and email address are required to register."
            }

    return render_template(
        'register.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date":str(datetime.now().year)}
    )

@dashboard.route('/logout', methods=['POST', 'GET'])
def logout():
    del session['profile']
    return redirect("/")

# @dashboard.route('/activate', methods=['POST', 'GET'])
# @dashboard.route('/activate/<activation_code>', methods=['POST', 'GET'])
# def activate(activation_code=None):
#     action_response = {}
#     _posted = request.form
#
#     activation_viable = Auth\
#         .query\
#         .filter(Auth.activation_code == activation_code)\
#         .first()
#
#     if not activation_viable:
#         action_response = {
#             "type": "danger",
#             "message": "Activation code is invalid."
#         }
#
#     else:
#         if len(_posted) > 0:
#             if 'username' in _posted and validateEmailAddress(_posted['username'].strip()) and \
#                 'password' in _posted and _posted['password'].strip() != "" and \
#                 'password2' in _posted and _posted['password2'].strip() != "":
#
#                 if _posted['password'].strip() == _posted['password2'].strip():
#                     activation = auth_activate(activation_code, _posted['username'], _posted['password'])
#
#                     if activation:
#                         action_response = {
#                             "type": "success",
#                             "message": "Account activated, you may login"
#                         }
#                     else:
#                         action_response = {
#                             "type": "danger",
#                             "message": "An error occured, try again later."
#                         }
#                 else:
#                     action_response = {
#                         "type": "danger",
#                         "message": "Passwords provided do not match"
#                     }
#             else:
#                 action_response = {
#                     "type": "warning",
#                     "message": "Email and password required to activate"
#                 }
#
#     return render_template(
#         'activate.html',
#         POSTED=_posted,
#         FORM_ACTION_RESPONSE=action_response,
#         DEVICES_ACTIVE='active',
#         DEVICES_LIST='active',
#         data={"date":str(datetime.now().year)}
#     )

@dashboard.route('/reset-request', methods=['POST', 'GET'])
def reset_request():
    action_response = {}
    _posted = request.form
    if len(_posted) > 0:
        if 'username' in _posted and validateEmailAddress(_posted['username'].strip()):
            activation_code = str(uuid.uuid4()).replace("-", "")
            if auth_request_reset(_posted['username'], activation_code):
                action_response = {
                    "type": "success",
                    "message": "Reset request done, check email."
                }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Invalid email address supplied"
                }
        else:
            action_response = {
                "type": "warning",
                "message": "Email address is required"
            }

    return render_template(
        'reset-request.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date":str(datetime.now().year)}
    )

@dashboard.route('/reset/<reset_code>', methods=['POST', 'GET'])
def reset(reset_code):
    action_response = {}
    _posted = request.form
    if len(_posted) > 0:
        if 'email_address' in _posted and validateEmailAddress(_posted['email_address'].strip()) and \
        'password' in _posted and _posted['password'].strip() != "" and \
            'password2' in _posted and _posted['password2'].strip() != "":
            if _posted['password'].strip() == _posted['password2'].strip():
                if auth_reset_password(_posted['email_address'], _posted['password'], reset_code):
                    action_response = {
                        "type": "success",
                        "message": "Password reset, you may now login."
                    }
                else:
                    action_response = {
                        "type": "danger",
                        "message": "An error occured, try again later."
                    }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Passwords provided do not match"
                }
        else:
            action_response = {
                "type": "warning",
                "message": "Email address and password required to reset"
            }

    return render_template(
        'reset.html',
        POSTED=_posted,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        data={"date":str(datetime.now().year)}
    )
