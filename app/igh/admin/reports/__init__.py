from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.helper import valid_uuid

igh_reports = Blueprint('igh_reports', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_reports.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@igh_reports.route('/daily', methods=['POST', 'GET'])
def daily_reports():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/reports/daily.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@igh_reports.route('/archives', methods=['POST', 'GET'])
def report_archives():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/reports/archives.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@igh_reports.route('/traceability', methods=['POST', 'GET'])
def traceability_reports():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/reports/traceability.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@igh_reports.route('/export', methods=['POST', 'GET'])
def export_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/reports/export.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@igh_reports.route('/create', methods=['POST', 'GET'])
def create_report():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/reports/create.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
