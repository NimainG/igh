from functools import wraps

import json
import os
import re
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.config import activateConfig
from app.core.greenhouse import types
from app.core import greenhouse as greenh
from app.core import screenhouse as screenh
from app.core import shadenet as shadenets
from app.core import shield as shields

from app.core.shield import types as shield_types
from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.helper import valid_uuid

igh_inventory = Blueprint('igh_inventory', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_inventory.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


# @igh_inventory.template_filter('strftime')
# def _jinja2_filter_datetime(date, fmt=None):
#     date = dateutil.parser.parse(date)
#     native = date.replace(tzinfo=None)
#     format='%b %d, %Y'
#     return native.strftime(format) 

# @igh_inventory.template_filter()
# def format_datetime(value, format='medium'):
#     if format == 'full':
#         format="EEEE, d. MMMM y 'at' HH:mm"
#     elif format == 'medium':
#         format="EE dd.MM.y HH:mm"
#     return babel.dates.format_datetime(value, format)




@igh_inventory.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _greenhouse = greenh.fetch_all().get_json()
    _types = types.fetch_all().get_json()
    _device_types = shield_types.fetch_all().get_json()
    _devices = shields.fetch_all().get_json()
    _screenhouse = screenh.fetch_all().get_json()
    _shadenet = shadenets.fetch_all().get_json()

    _actives = []
    _inactives = []

    posted = request.form
    if posted:
        if "action" in posted:
             if posted.get("action") == "activate_greenhouse" and "greenhouse_id" in posted:
                return greenh.activate(posted.get("greenhouse_id"))

             if posted.get("action") == "deactivate_greenhouse" and "greenhouse_id" in posted:
                return greenh.deactivate(posted.get("greenhouse_id"))
            
             if posted.get("action") == "activate_screenhouse" and "screenhouse_id" in posted:
                return screenh.activate(posted.get("screenhouse_id"))

             if posted.get("action") == "deactivate_screenhouse" and "screenhouse_id" in posted:
                return screenh.deactivate(posted.get("screenhouse_id"))

             if posted.get("action") == "activate_shadenet" and "shadenet_id" in posted:
                return shadenets.activate(posted.get("shadenet_id"))

             if posted.get("action") == "deactivate_shadenet" and "shadenet_id" in posted:
                return shadenets.deactivate(posted.get("shadenet_id"))
            
            



    return render_template(
        'admin/inventory/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        GREENH = _greenhouse,
        DEVICES = _devices,
        TYPES= _types,
        DEVICETYPE = _device_types,
        SCREENH = _screenhouse,
        SHADEN = _shadenet,
        ACTIVETBL=_actives,
        INACTIVETBL=_inactives,
        data={"date":str(datetime.now().year)},
        data1={"date":str(datetime.now().year), "utc_date": datetime.utcnow()},
       
    )

@igh_inventory.route('/greenhouse-types', methods=['POST', 'GET'])
def greenhouse_types():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _types = types.fetch_all().get_json()
    _multiple = []

    posted = request.form
    if posted:
        if "action" in posted:


            if posted.get("action") == "greenhouse_types" and "type_id" in posted:
                _greenhouse_types = []

                _type = types\
                    .fetch_one(posted.get("type_id"), True)\
                    .get_json()
                if _type:
                    _greenhouse_types = types\
                        .fetch_by_id(_type["id"])\
                        .get_json()
                    return jsonify(_greenhouse_types)

            if posted.get("action") == "add_types" and "type_name" and "type_size" and "type_desc" and "type_link" in posted:
                return types.add_new(
                    json.dumps({
                        "name": posted.get("type_name"),
                        "size": posted.get("type_size"),
                        "description" : posted.get("type_desc"),
                        "info_link" : posted.get("type_link"),
                        "user_realm": config.USER_IGH,
                        "creator_id": session['profile']['user']['id'],
                    }),
                    True
                )
            if posted.get("action") == "update_types" and "types" in posted:
                _types = json.loads(posted.get("types"))
                for type in _types:
                    type.edit(
                        types["type_id"],
                        json.dumps(
                            {
                                "name": types["type_name"],
                                "size": types["type_name"],
                                "description":types["type_desc"],
                                "info_link":types["type_link"]
                            }
                        )
                    )
                return jsonify(_types)
  
            print(str( _greenhouse_types[0].get_json()))
           

    return render_template(
        'admin/inventory/greenhouse_types.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        TYPES= _types,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )

@igh_inventory.route('/greenhouses/add', methods=['POST', 'GET'])
def greenhouse_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _types = types.fetch_all().get_json()
    print("types",_types)
    _multiple = []    
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
            _greenhouse_type_id = posted.getlist("greenhouse_type_id")
            _size = posted.getlist("size[x]")
            _info_link = posted.getlist("info_link[x]")
            _greenhouse_ref = posted.getlist("greenhouse_ref[x]")
            _creator_id = posted.getlist("creator_id")
            _status = posted.getlist("status")
            print("greenhouse type",_greenhouse_type_id) 
            x = 0
            for _name in _greenhouse_ref:
                _multiple.append({
                    "greenhouse_ref": _greenhouse_ref,
                    "size": _size,
                    "info_link": _info_link,
                    "greenhouse_type_id": _greenhouse_type_id,
                    "creator_id" : _creator_id,
                    "status": _status
                })
                x = x + 1
                print("greenhouse type 1",_greenhouse_type_id)

        else:
            _greenhouse_type_id = posted.getlist("greenhouse_type_id")
            _greenhouse_ref = posted.getlist("greenhouse_ref[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            print("greenhouse type 2",_greenhouse_type_id[0])
            print("greenhouse_ref",_greenhouse_ref[0])
            print("size",_size[0])
            print("created_by",_creator_id)
            print("info link",_info_link)
            print("status",_status)
            x = 0
            _new_greenhouse = greenh.add_new(
                        json.dumps({
                            "greenhouse_ref": _greenhouse_ref[0],
                            "size": _size[0],
                            "info_link": _info_link,
                            "greenhouse_type_id": _greenhouse_type_id[0],
                            "creator_id": _creator_id,
                            "status": _status  
                        }),
                        True
                    )
            print(str(_new_greenhouse[0].get_json()))
            if _new_greenhouse[1] == 200:
                        
              x = x + 1
              
            


    return render_template(
        'admin/inventory/greenhouse_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        MULTIGREEN=_multiple,
        TYPESS = _types,
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_inventory.route('/greenhouses/bulk', methods=['POST', 'GET'])
def greenhouse_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/greenhouse_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )

@igh_inventory.route('/device-types', methods=['POST', 'GET'])
def device_types():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _device_types = shield_types.fetch_all().get_json()
  

    return render_template(
        'admin/inventory/device_types.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        DEVICE = _device_types,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )

@igh_inventory.route('/devices/add', methods=['POST', 'GET'])
def device_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
   
    return render_template(
        'admin/inventory/device_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_inventory.route('/devices/bulk', methods=['POST', 'GET'])
def device_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/device_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_inventory.route('/screenhouses/add', methods=['POST', 'GET'])
def screenhouse_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _screenhouse = []    
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
            _id = posted.getlist("id[x]")
            _screenhouse_ref = posted.getlist("screenhouse_ref[x]")
            _size = posted.getlist("size[x]")
            _info_link = posted.getlist("info_link[x]")
            
            _creator_id = posted.getlist("creator_id")
            _status = posted.getlist("status")
            # _created = posted.getlist("created")
           
            x = 0
            for _name in _screenhouse_ref:
                _screenhouse.append({
                    "id": _id,
                    "screenhouse_ref": _screenhouse_ref,
                    "size": _size,
                    "info_link": _info_link,
                    "creator_id" : _creator_id,
                    # "created" : _created,
                    "status": _status
                })
                x = x + 1
               

        else:
            _id =  posted.getlist("id[]")
            _screenhouse_ref = posted.getlist("screenhouse_ref[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            # _created = posted.getlist("created")
            
            print("id",_id)
            print("screenhouse_ref",_screenhouse_ref[0])
            print("size",_size[0])
            print("created_by",_creator_id)
            print("info link",_info_link[0])
            print("status",_status)
            x = 0
            _new_screenhouse = screenh.add_new(
                        json.dumps({
                            
                            "screenhouse_ref": _screenhouse_ref[0],
                            "size": _size[0],
                            "info_link": _info_link[0],
                            "creator_id": _creator_id,
                            "status": _status,
                            # "created": _created
                        }),
                        True
                    )
            print(str(_new_screenhouse[0].get_json()))
            if _new_screenhouse[1] == 200:
                        
              x = x + 1

    return render_template(
        'admin/inventory/screenhouse_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        SCREENHS =_screenhouse,
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_inventory.route('/screenhouses/bulk', methods=['POST', 'GET'])
def screenhouse_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/screenhouse_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_inventory.route('/shadenets/add', methods=['POST', 'GET'])
def shadenet_add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _shadenet = []    
    posted = request.form
    if posted:
        add_multiple = posted.get("add-multiple")
        if add_multiple:
           
            _shadenet_ref = posted.getlist("shadenet_ref[x]")
            _size = posted.getlist("size[x]")
            _info_link = posted.getlist("info_link[x]")
            
            _creator_id = posted.getlist("creator_id")
            _status = posted.getlist("status")
            # _created = posted.getlist("created")
           
            x = 0
            for _name in _shadenet_ref:
                _shadenet.append({
                    
                    "shadenet_ref": _shadenet_ref,
                    "size": _size,
                    "info_link": _info_link,
                    "creator_id" : _creator_id,
                    # "created" : _created,
                    "status": _status
                })
                x = x + 1
               

        else:
            
            _shadenet_ref = posted.getlist("shadenet_ref[]")
            _size = posted.getlist("size[]")
            _info_link = posted.getlist("info_link[]")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            # _created = posted.getlist("created")
            
            
            print("shadenet_ref",_shadenet_ref[0])
            print("size",_size[0])
            print("created_by",_creator_id)
            print("info link",_info_link[0])
            print("status",_status)
            x = 0
            _new_shadenet = shadenets.add_new(
                        json.dumps({
                            
                            "shadenet_ref": _shadenet_ref[0],
                            "size": _size[0],
                            "info_link": _info_link[0],
                            "creator_id": _creator_id,
                            "status": _status,
                            # "created": _created
                        }),
                        True
                    )
           
            if _new_shadenet[1] == 200:
                
                       
               return render_template('admin/inventory/main.html')

    return render_template(
        'admin/inventory/shadenet_add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        SHADENET = _shadenet,
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_inventory.route('/shadenets/bulk', methods=['POST', 'GET'])
def shadenet_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/inventory/shadenet_bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_INVENTORY_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )



