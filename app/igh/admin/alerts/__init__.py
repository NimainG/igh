from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.core import alert

from app.helper import valid_uuid

igh_alerts = Blueprint('igh_alerts', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_alerts.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@igh_alerts.route('/', methods=['POST', 'GET'])
def alerts():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _alerts = alert.fetch_all().get_json()

    return render_template(
        'admin/alerts/main.html',
        ADMIN_PORTAL=True,
        ALERT = _alerts,
        ADMIN_PORTAL_ALERTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
