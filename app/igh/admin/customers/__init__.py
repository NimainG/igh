from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from datetime import datetime
from app.core import customer as customer_user
from app.core.customer import subscription as customer_sub
from app.core import greenhouse as greenh
from app.core import screenhouse as screenh
from app.core import shadenet as shadenets

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.helper import valid_uuid

from app.helper import validateEmailAddress

from app.core.user import igh

from app.core import user

from flask_wtf import Form

from wtforms.fields import DateField

from app.core.customer import greenhouses
from app.core.customer import screenhouses
from app.core.customer import shadenets as sh

igh_customers = Blueprint('igh_customers', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_customers.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')


class MyForm(Form):
    date = DateField(id='datepick')

_customer = []

@igh_customers.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    _customer = customer_user.fetch_all().get_json()

    return render_template(
        'admin/customers/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        CUSTOMER = _customer,
        data={"date":str(datetime.now().year)}
    )

@igh_customers.route('/add', methods=['POST', 'GET'])
def add():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _assign_memb = igh.fetch_all().get_json()
    _customer_subcription=[]
    _user_customer=[]
    
    form = MyForm()
    action_response = {}
    _posted = request.form
    
    if _posted:
        if 'first_name' in _posted and _posted['first_name'].strip() != "" and \
            'email_address' in _posted and validateEmailAddress(_posted['email_address'].strip()) != "":

            exists = user.auth.fetch_by_username(_posted['email_address']).get_json()
            if exists:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please use a different one."
                }
            _name = _posted.getlist("first_name")
            _email = _posted.getlist("email_address")
            _last = _posted.getlist("last_name")
       
            for name in _name:
                _user_customer.append({
                    "first_name": _name,
                    "email_address": _email,
                    "last_name": _last,
                   
                })
            else:
                _role_id = None
                _default_role = user.roles.fetch_by_name(config.CUSTOMER_USER).get_json()
                _creator_id = session['profile']['user']['id']
                _last = ""
                if _default_role:
                    _role_id = _default_role['id']
                print("Details",_default_role,_creator_id,_role_id,_posted['email_address'].strip())
                new_user = user.add_new(
                    json.dumps({
                        "email_address": _posted['email_address'].strip(),
                        "first_name": _posted['first_name'].strip(),
                        "last_name":_last,
                        "phone_number": _posted['phone_number'].strip(),
                        "user_realm": config.USER_CUSTOMER,
                        "role_id": _default_role,
                        "creator_id": _creator_id
                    })
                )
                print(str(new_user[0].get_json()))

                
                # print(str(_new_customer[0].get_json()))
        else:
            action_response = {
                "type": "warning",
                "message": "First name, last name, phone number and email address are required to register."
            }
     

    if _posted:
        add_multiple = _posted.get("add-multiple")
        if add_multiple:
            _name = _posted.getlist("name")
            _billing_address = _posted.getlist("billing_address")
            _shipping_address = _posted.getlist("shipping_address")
            _igh_contact = _posted.getlist("igh_contact")
            _contact_person = _posted.getlist("contact_person")
            _created_by = _posted.getlist("created_by")
            _created = _posted.getlist("created")
            _status = _posted.getlist("status")
            print("Customer Contact",_contact_person) 
            x = 0
            for name in _name:
                _customer.append({
                    "name": _name,
                    "billing_address": _billing_address,
                    "shipping_address ": _shipping_address ,
                    "igh_contact": _igh_contact,
                    "contact_person":_contact_person,
                    "created_by" : _created_by,
                    "created":_created,
                    "status": _status
                })
                x = x + 1
                print("greenhouse type 1",_name)

        else:
            _igh_contact = _posted.getlist("igh_contact")
            _name = _posted.getlist("name[]")
            _billing_address = _posted.getlist("billing_address[]")
            _shipping_address = _posted.getlist("shipping_address[]")
            _contact_person = _posted.getlist("contact_person")
            _created_by = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            _created = _posted.getlist("created")
            print("customer manager type ",_igh_contact)
            
            print("status",_status)
            x = 0
            _new_customer = customer_user.add_new(
                        json.dumps({
                            "name": _name,
                            "billing_address": _billing_address,
                            "shipping_address": _shipping_address,
                            "contact_person": _contact_person,
                            "igh_contact": _igh_contact,
                            "created_by": _created_by,
                            "status": _status,
                            "created": _created  
                        }),
                        True
                    )
            print(str(_new_customer[0].get_json()))
            if _new_customer[1] == 200:
                        
              x = x + 1
    if _posted:
        add_multiple = _posted.get("add-multiple")
        if add_multiple:
            _starts = _posted.getlist("starts")
            _ends = _posted.getlist("ends")
            _invoice_id = _posted.getlist("invoice_id")
            _sale_type_id = _posted.getlist("sale_type_id")
            _subscription_id = _posted.getlist("subscription_id")
            _customer_id = _posted.getlist("customer_id")
            _creator_id = _posted.getlist("creator_id")
            _status = _posted.getlist("status")
            
          #  x = 0
            for name in _starts:
                _customer_subcription.append({
                    "starts": _starts,
                    "ends": _ends,
                    "invoice_id": _invoice_id ,
                    "sale_type_id": _sale_type_id,
                    "subscription_id":_subscription_id,
                    "customer_id":_customer_id,
                    "creator_id" : _creator_id,
                    "status": _status
                })
          #      x = x + 1
              

        else:
            _starts = _posted.getlist("starts")
            _ends = _posted.getlist("ends")
            _invoice_id = _posted.getlist("invoice_id")
            _sale_type_id = _posted.getlist("sale_type_id")
            _subscription_id = _posted.getlist("subscription_id")
            _customer_id = _posted.getlist("customer_id")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            
            print("customer sub details ",_starts,_ends,_creator_id,_invoice_id,_subscription_id,_sale_type_id,_status)
            
            print("status",_status)
            x = 0
            _new_sub_customer = customer_sub.add_new(
                        json.dumps({
                            "starts": _starts,
                            "ends": _ends,
                            "invoice_id": _invoice_id,
                            "sale_type_id": _sale_type_id,
                            "subscription_id": _subscription_id,
                            "customer_id":_customer_id,
                            "creator_id": _creator_id,
                            "status": _status
                            
                        }),
                        True
                    )
            print(str(_new_sub_customer[0].get_json()))
            if _new_sub_customer[1] == 200:
                        
              x = x + 1


    return render_template(
        'admin/customers/add.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        POSTEDCUSTOMER=_user_customer,
        CUSTOMER_REAL = _customer,
        ASSIGN_MEMB = _assign_memb,
        DEVICES_ACTIVE='active',
        DEVICES_LIST='active',
        CUSTOMER = _customer_subcription,
        form=form,
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
        # data={"date":str(datetime.now().year)}
    )

@igh_customers.route('/add/greenhouses', methods=['POST', 'GET'])
def add_greenhouses():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _greenhouse = greenh.fetch_all().get_json()    
    _customer_greenhouses=[]
    _posted = request.form

    if _posted:
        add_multiple = _posted.get("add-multiple")
        if add_multiple:
            _greenhouse_id = _posted.getlist("greenhouse_id")
            _customer_id = _posted.getlist("customer_id")
            _creator_id = _posted.getlist("creator_id")
            
            _created = _posted.getlist("created")
            _status = _posted.getlist("status")
          
            x = 0
            for name in _greenhouse_id:
                _customer_greenhouses.append({
                    "greenhouse_id": _greenhouse_id,
                    "customer_id": _customer_id,
                    "creator_id ": _creator_id ,
                    "status": _status
                })
                x = x + 1
                

        else:
            _greenhouse_id = _posted.getlist("greenhouse_id")
            _customer_id = _customer['id'] [0]
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            
            
            
            print("_customer_",_customer_id)
            x = 0
            _new_customer = greenhouses.add_new(
                        json.dumps({
                            "greenhouse_id": _greenhouse_id,
                            "customer_id": _customer_id,
                            "creator_id": _creator_id,
                            "status": _status,
                            
                        }),
                        True
                    )
            print(str(_new_customer[0].get_json()))
            if _new_customer[1] == 200:
                        
              x = x + 1

    return render_template(
        'admin/customers/greenhouses.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        CUSTOMER = _customer,
        GREENH = _greenhouse,
        CGREENHOUSES = _customer_greenhouses,
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
        
    )

@igh_customers.route('/add/devices', methods=['POST', 'GET'])
def add_devices():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/customers/devices.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_customers.route('/add/screenhouses', methods=['POST', 'GET'])
def add_screenhouses():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _screenhouse = screenh.fetch_all().get_json()    
    _customer_screenhouses=[]
    _posted = request.form

    if _posted:
        add_multiple = _posted.get("add-multiple")
        if add_multiple:
            _screenhouse_id = _posted.getlist("screenhouse_id")
            _customer_id = _posted.getlist("customer_id")
            _creator_id = _posted.getlist("creator_id")
            
            _created = _posted.getlist("created")
            _status = _posted.getlist("status")
          
            x = 0
            for name in _screenhouse_id:
                _customer_screenhouses.append({
                    "screenhouse_id": _screenhouse_id,
                    "customer_id": _customer_id,
                    "creator_id ": _creator_id ,
                    "status": _status
                })
                x = x + 1
                

        else:
            _greenhouse_id = _posted.getlist("screenhouse_id")
            _customer_id = _posted.getlist("customer_id")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            
            
            
            print("status",_status)
            x = 0
            _new_customer = screenhouses.add_new(
                        json.dumps({
                            "screenhouse_id": _greenhouse_id,
                            "customer_id": _customer_id,
                            "creator_id": _creator_id,
                            "status": _status,
                            
                        }),
                        True
                    )
            print(str(_new_customer[0].get_json()))
            if _new_customer[1] == 200:
                        
              x = x + 1


    return render_template(
        'admin/customers/screenhouses.html',
        ADMIN_PORTAL=True,
        SCREENH = _screenhouse,
        CUSTOMER_SCREEN = _customer_screenhouses,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
         data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_customers.route('/add/shadnets', methods=['POST', 'GET'])
def add_shadnets():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _shadenet = shadenets.fetch_all().get_json()    
    _customer_shadenet=[]
    _posted = request.form

    if _posted:
        add_multiple = _posted.get("add-multiple")
        if add_multiple:
            _shadenet_id = _posted.getlist("shadenet_id")
            _customer_id = _posted.getlist("customer_id")
            _creator_id = _posted.getlist("creator_id")
            
            _created = _posted.getlist("created")
            _status = _posted.getlist("status")
          
            x = 0
            for name in _shadenet_id:
                _customer_shadenet.append({
                    "shadenet_id": _shadenet_id,
                    "customer_id": _customer_id,
                    "creator_id ": _creator_id ,
                    "status": _status
                })
                x = x + 1
                

        else:
            _shadenet_id = _posted.getlist("shadenet_id")
            _customer_id = _posted.getlist("customer_id")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            
            
            
            print("status",_status)
            x = 0
            _new_customer = sh.add_new(
                        json.dumps({
                            "shadenet_id": _shadenet_id,
                            "customer_id": _customer_id,
                            "creator_id": _creator_id,
                            "status": _status,
                            
                        }),
                        True
                    )
            print(str(_new_customer[0].get_json()))
            if _new_customer[1] == 200:
                        
              x = x + 1


    return render_template(
        'admin/customers/shadnets.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        SHADENET = _customer_shadenet,
        SHADENETSALL = _shadenet,
        # data={"date":str(datetime.now().year)}
         data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_customers.route('/add/bulk', methods=['POST', 'GET'])
def add_bulk():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/customers/bulk.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_CUSTOMERS_ACTIVE='active',
        PROFILE=session['profile'],
        # data={"date":str(datetime.now().year)}
         data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )
