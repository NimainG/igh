from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.core.shield import types

from app.models import db, Customer, User, Role, UserRole, Greenhouse, \
    CustomerGreenhouse, Farm, CustomerDevice, Device

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

igh_devices = Blueprint('igh_devices', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_devices.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def form_constructor():

    _type_options = [
        {
            "label": 'Shield',
            "value": config.DEVICE_SHIELD,
        },
        {
            "label": 'Spear',
            "value": config.DEVICE_SPEAR,
        },
        {
            "label": 'Sensor',
            "value": config.DEVICE_SENSOR,
        },
    ]

    _form_fields = [
        {
            "name": "name",
            "title": "Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"Preferred reference",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "device_type",
            "title": "Type",
            "element": [
                "select",
                {
                    "options": _type_options
                }
            ],
            "required": 1,
            "default": config.DEVICE_SHIELD,
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "serial",
            "title": "Serial",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "photo_url",
            "title": "Photo Link",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "info_url",
            "title": "More Info Link",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "description",
            "title": "Description",
            "element": [
                "textarea"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "status",
            "title": "Status",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Active",
                            "value": config.STATUS_ACTIVE,
                        },
                        {
                            "label": "Inactive",
                            "value": config.STATUS_INACTIVE,
                        }
                    ]
                }
            ],
            "required": 0,
            "default":"Active",
            "views": [
                "edit"
            ]
        },
    ]

    return _form_fields

def device_type_name(device_type):
    if device_type == config.DEVICE_SHIELD:
        return "Shield"

    if device_type == config.DEVICE_SPEAR:
        return "Spear"

    if device_type == config.DEVICE_SENSOR:
        return "Sensor"

    return ""

@igh_devices.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    form_action = 'Add'
    action_response = {}

    posted = request.form
    if posted:
        if posted.get("action").strip().lower() == 'add':
            _name = posted.get("name").strip()
            _type = posted.get("device_type").strip()
            _serial = posted.get("serial").strip()
            _photo_url = posted.get("photo_url").strip()
            _info_url = posted.get("info_url").strip()
            _description = posted.get("description").strip()

            if _name is not None and _serial is not None:
                user = User\
                    .query\
                    .filter(User.uid == session["profile"]["id"])\
                    .filter(User.status > config.STATUS_DELETED)\
                    .first()
                if user:
                    _device = Device(
                        uid=uuid.uuid4(),
                        name=_name,
                        type=_type,
                        serial=_serial,
                        photo_url=_photo_url,
                        info_link=_info_url,
                        description=_description,
                        creator_id=user.id
                    )
                    db.session.add(_device)
                    db.session.commit()

                    if _device:
                        action_response = {
                            "type": "success",
                            "message": "Device added successfully."
                        }
            else:
                action_response = {
                    "type": "warning",
                    "message": "Device not added, invalid input."
                }
        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    devices = []
    _devices = db\
        .session\
        .query(Device)\
        .filter(Device.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        devices.append({
            "id": _device.uid,
            "name": _device.name,
            "type": device_type_name(_device.type),
            "serial": _device.serial,
            "photo_url": _device.photo_url,
            "info_url": _device.info_link,
            "description": _device.description,
            "status": "Active" if _device.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.created_at,
            "updated": _device.modified_at,
            "creator": contact_person(_device.creator_id),
        })

    return render_template(
        'admin/devices/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_DEVICES_ACTIVE='active',
        ADMIN_PORTAL_DEVICES_LIST_ACTIVE='active',
        FORM=form_constructor(),
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES=devices,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@igh_devices.route('/<device_id>/edit', methods=['POST', 'GET'])
def edit(device_id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    form_action = 'Edit'
    action_form = form_constructor()
    action_response = {}

    if device_id != "":
        device = Device\
            .query\
            .filter(Device.uid == device_id)\
            .filter(Device.status > config.STATUS_DELETED)\
            .first()
        if device:
            posted = request.form
            if posted:
                if posted.get("action").strip().lower() == 'edit':
                    _name = posted.get("name").strip()
                    _type = posted.get("device_type").strip()
                    _serial = posted.get("serial").strip()
                    _photo_url = posted.get("photo_url").strip()
                    _info_url = posted.get("info_url").strip()
                    _description = posted.get("description").strip()
                    _status = posted.get("status").strip()

                    if _name is not None and _serial is not None:
                        device.name = _name
                        device.type = _type
                        device.serial = _serial
                        device.photo_url = _photo_url
                        device.info_link = _info_url
                        device.description = _description
                        device.status = _status

                        db.session.commit()

                        action_response = {
                            "type": "success",
                            "message": "Device edited successfully."
                        }

                    else:
                        action_response = {
                            "type": "warning",
                            "message": "Device not added, invalid input."
                        }

                else:
                    action_response = {
                        "type": "danger",
                        "message": "Invalid input."
                    }

            for elem in action_form:
                if elem["name"] == "name":
                    elem["default"] = device.name

                if elem["name"] == "device_type":
                    elem["default"] = device.type

                if elem["name"] == "serial":
                    elem["default"] = device.serial

                if elem["name"] == "photo_url":
                    elem["default"] = device.photo_url

                if elem["name"] == "description":
                    elem["default"] = device.description

                if elem["name"] == "info_url":
                    elem["default"] = device.info_link

                if elem["name"] == "status":
                    elem["default"] = device.status

        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    devices = []
    _devices = db\
        .session\
        .query(Device)\
        .filter(Device.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        devices.append({
            "id": _device.uid,
            "name": _device.name,
            "type": device_type_name(_device.type),
            "serial": _device.serial,
            "photo_url": _device.photo_url,
            "info_url": _device.info_link,
            "description": _device.description,
            "status": "Active" if _device.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.created_at,
            "updated": _device.modified_at,
            "creator": contact_person(_device.creator_id),
        })

    return render_template(
        'admin/devices/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_DEVICES_ACTIVE='active',
        ADMIN_PORTAL_DEVICES_LIST_ACTIVE='active',
        FORM=action_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        DEVICES=devices,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@igh_devices.route('/<device_id>/delete', methods=['POST', 'GET'])
def delete(device_id):
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    action_response = {}

    if device_id != "":
        device = Device\
            .query\
            .filter(Device.uid == device_id)\
            .filter(Device.status > config.STATUS_DELETED)\
            .first()
        if device:
            device.status=config.STATUS_DELETED
            db.session.commit()

            action_response = {
                "type": "success",
                "message": device.name + " deleted successfully."
            }
        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    devices = []
    _devices = db\
        .session\
        .query(Device)\
        .filter(Device.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        devices.append({
            "id": _device.uid,
            "name": _device.name,
            "type": device_type_name(_device.type),
            "serial": _device.serial,
            "photo_url": _device.photo_url,
            "info_url": _device.info_link,
            "description": _device.description,
            "status": "Active" if _device.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.created_at,
            "updated": _device.modified_at,
            "creator": contact_person(_device.creator_id),
        })

    return render_template(
        'admin/devices/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_DEVICES_ACTIVE='active',
        FORM_ACTION_RESPONSE=action_response,
        DEVICES=devices,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )
