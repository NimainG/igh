from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.models import db, Notification, NotificationConfig, User

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

igh_notifications = Blueprint('igh_notifications', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_notifications.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def notifications_form_constructor():

    _user_options = []
    _users = db\
        .session\
        .query(User)\
        .filter(User.status > config.STATUS_DELETED)\
        .all()
    for _user in _users:
        _user_options.append({
            "label": _user.first_name + ' ' + _user.last_name,
            "value": _user.uid
        })

    _form_fields = [
        {
            "name": "post_mode",
            "title": "Select Mode",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Post on profile",
                            "value": config.NOTIFICATION_MODE_PROFILE,
                        },
                        {
                            "label": "Send on mail",
                            "value": config.NOTIFICATION_MODE_EMAIL,
                        },
                        {
                            "label": "Post to profile and email",
                            "value": config.NOTIFICATION_MODE_ALL,
                        }
                    ]
                }
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "mode"
            ]
        }
    ]

    return _form_fields

def type_name(type=0):
    if type > 0:
        if type == config.NOTIFICATION_ALERT:
            return "Alert"

        if type == config.NOTIFICATION_USER:
            return "User"

        if type == config.NOTIFICATION_SYSTEM:
            return "System"

    return "General"

def mode_name(mode=0):
    if mode > 0:
        if mode == config.NOTIFICATION_MODE_PROFILE:
            return "Post on profile"

        if mode == config.NOTIFICATION_MODE_EMAIL:
            return "Send on email"

        if mode == config.NOTIFICATION_MODE_ALL:
            return "Post on profile and email"

    return "General"

def status_name(status=0):
    if status == 0:
        return "Not Seen"

    if status == 1:
        return "Seen"

    if status == 2:
        return "Interacted"

    return "Unknown"

@igh_notifications.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    form_action = 'Mode'
    action_response = {}
    notification_config = None
    _config_form = notifications_form_constructor()

    user = User\
        .query\
        .filter(User.uid == session["profile"]["id"])\
        .filter(User.status > config.STATUS_DELETED)\
        .first()
    if user:
        notification_config = NotificationConfig\
            .query\
            .filter(NotificationConfig.user_id == user.id)\
            .filter(NotificationConfig.status > config.STATUS_DELETED)\
            .first()

    posted = request.form
    if posted:
        if posted.get("action").strip().lower() == 'mode':
            _post_mode = posted.get("post_mode").strip()

            if _post_mode is not None:
                if notification_config:
                    notification_config.preferred_mode = _post_mode
                else:
                    _notification_config = NotificationConfig(
                        uid=uuid.uuid4(),
                        user_id=user.id,
                        preferred_mode=_post_mode,
                        creator_id=user.id
                    )
                    db.session.add(_notification_config)

                db.session.commit()

                action_response = {
                    "type": "success",
                    "message": mode_name(int(_post_mode)) + " notification configuration saved successfully."
                }
            else:
                action_response = {
                    "type": "warning",
                    "message":  mode_name(int(_post_mode)) + " notification configuration not saved, invalid input."
                }
        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    if notification_config:
        for elem in _config_form:
            if elem["name"] == "post_mode":
                elem["default"] = notification_config.preferred_mode

    notifications = []
    _notifications = db\
        .session\
        .query(Notification)\
        .filter(Notification.status > config.STATUS_DELETED)\
        .order_by(Notification.id.desc())\
        .all()
    for _notification in _notifications:
        notifications.append({
            "id": _notification.uid,
            "title": _notification.title,
            "type": type_name(_notification.type),
            "recipient": _notification.recipient,
            "action_link": _notification.action_link,
            "mode": mode_name(_notification.mode),
            "status": status_name(_notification.notification_status),
            "created": _notification.created_at,
            "modified": _notification.modified_at,
            "creator": contact_person(_notification.creator_id),
        })

    return render_template(
        'admin/notifications/main.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_NOTIFICATIONS_ACTIVE='active',
        ADMIN_PORTAL_NOTIFICATIONS_LIST_ACTIVE='active',
        FORM=_config_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        NOTIFICATIONS=notifications,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )
