from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from app.core import maintenance
from app.core.user import igh

from app.config import activateConfig

from app.igh import portal_check
from app.igh.admin import contact_person

from app.igh import session_setup

from app.helper import valid_uuid

from app.core.maintenance import assignment as assignmembers

igh_maintenance = Blueprint('igh_maintenance', __name__,  static_folder='../../static', template_folder='.../../templates')
igh_maintenance.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@igh_maintenance.route('/', methods=['POST', 'GET'])
def alerts():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _maintenance = maintenance.fetch_all().get_json()

    return render_template(
        'admin/maintenance/main.html',
        ADMIN_PORTAL=True,
        MAINTAIN = _maintenance,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@igh_maintenance.route('/request', methods=['POST', 'GET'])
def request_ype():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm
    _assign_memb = igh.fetch_all().get_json()
    _maintain_request = []
   
    _posted = request.form


    if _posted:
        add_multiple = _posted.get("add-multiple")
        if add_multiple:
            _serial = _posted.getlist("serial")
            _type = _posted.getlist("type")
            _customer_id = _posted.getlist("customer_id")
            _assignment = _posted.getlist("assignment")
            _subscription_id = _posted.getlist("subscription_id")
            _detail = _posted.getlist("detail")
            _creator_id = _posted.getlist("creator_id")
            _status = _posted.getlist("status")
            
          #  x = 0
            for name in _serial:
                _maintain_request.append({
                    "serial": _serial,
                    "type": _type,
                    "customer_id": _customer_id ,
                    "assignment": _assignment,
                    "subscription_id":_subscription_id,
                    "detail":_detail,
                    "creator_id" : _creator_id,
                    "status": _status
                })
          #      x = x + 1
              

        else:
            _serial = _posted.getlist("serial")
            _type = _posted.getlist("type")
            _customer_id = _posted.getlist("customer_id")
            _assignment = _posted.getlist("assignment")
            _subscription_id = _posted.getlist("subscription_id")
            _detail = _posted.getlist("detail")
            _creator_id = session['profile']['user']['id']
            _status = config.STATUS_ACTIVE
            
            print("customer sub details ",_serial,_type,_creator_id,_customer_id,_subscription_id,_assignment,_status)
            
            print("status",_status)
            x = 0
            _new_maintain = maintenance.add_new(
                        json.dumps({
                            "serial": _serial,
                            "type": _type,
                            "customer_id": _customer_id,
                            "assignment": _assignment,
                            "subscription_id": _subscription_id,
                            "detail":_detail,
                            "creator_id": _creator_id,
                            "status": _status
                            
                        }),
                        True
                    )
            print(str(_new_maintain[0].get_json()))
            if _new_maintain[1] == 200:
                        
              x = x + 1


    return render_template(
        'admin/maintenance/request.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        MAINTAINS = _maintain_request,
        ASSIGNMEMB = _assign_memb,
        # data={"date":str(datetime.now().year)}
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@igh_maintenance.route('/request/type', methods=['POST', 'GET'])
def type():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/maintenance/type.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@igh_maintenance.route('/request/pests-diseases', methods=['POST', 'GET'])
def pests_diseases():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/maintenance/pests_diseases.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@igh_maintenance.route('/request/photos', methods=['POST', 'GET'])
def photos():
    redirect, realm = portal_check(session, 'admin')
    if redirect:
        return realm

    return render_template(
        'admin/maintenance/photos.html',
        ADMIN_PORTAL=True,
        ADMIN_PORTAL_MAINTENANCE_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
