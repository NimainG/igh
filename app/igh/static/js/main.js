$(function(){

  $('.btn-period').on("click", function(){
      var selected = $(this);
      selected.closest('.btn-group').find('.btn-period.active').removeClass("active");
      selected.addClass("active");
  })

  $("input[name='temp_metric']").on("change", function(){
    var realm = document.location.href.split("/")[3];
    var temp_metric = $(this).val()

    var formData = new FormData();

    formData.append("metric", true);
    formData.append("temp", temp_metric);

    $.ajax({
       url : '/' + realm + '/settings/my-profile',
       type : 'POST',
       data : formData,
       processData: false,
       contentType: false,
       success: false
    });

  });

  $("input[name='liquid_metric']").on("change", function(){
    var realm = document.location.href.split("/")[3];
    var liquid_metric = $(this).val()

    var formData = new FormData();

    formData.append("metric", true);
    formData.append("liquid", liquid_metric);

    $.ajax({
       url : '/' + realm + '/settings/my-profile',
       type : 'POST',
       data : formData,
       processData: false,
       contentType: false,
       success: false
    });
  });

  $("input[name='length_metric']").on("change", function(){
    var realm = document.location.href.split("/")[3];
    var length_metric = $(this).val()

    var formData = new FormData();

    formData.append("metric", true);
    formData.append("length", length_metric);

    $.ajax({
       url : '/' + realm + '/settings/my-profile',
       type : 'POST',
       data : formData,
       processData: false,
       contentType: false,
       success: false
    });

  });

});

function confirm_delete(link, entity) {
  if (confirm("Deleting " + entity + ". This action is irreversible!")) {
    document.location.href = link;
  }
}

function confirm_save(entity) {
  if (confirm("Saving " + entity + " information...")) {
    return true;
  } else {
    return false;
  }
}

function plot_bar_chart(chart_elem, data) {
  var _chart_container = $(chart_elem)
  var _chart_elem = chart_elem + ' .chart-data';

  var _data = {
    labels: data.labels,
    series: data.series
  };
  var _options = {
    showPoint: true,
    lineSmooth: false,
    axisX: {
      showGrid: false,
      showLabel: true,
    },
    axisY: {
      offset: 60,
      labelInterpolationFnc: function(value) {
        return value + data.metric;
      },
    }
  };

  _chart_container.find('.y-axis .axis-label span').html(data.axis.y_axis);
  _chart_container.find('.x-axis .axis-label span').html(data.axis.x_axis);

  var chart = new Chartist.Bar(_chart_elem, _data, _options);
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(event) { chart.update(); });
}

function plot_line_chart(chart_elem, data) {
  var _chart_container = $(chart_elem)
  var _chart_elem = chart_elem + ' .chart-data';

  var _data = {
    labels: data.labels,
    series: data.series
  };
  var _options = {
    showPoint: true,
    lineSmooth: false,
    axisX: {
      showGrid: false,
      showLabel: true,
    },
    axisY: {
      offset: 60,
      labelInterpolationFnc: function(value) {
        return value + data.metric;
      },
    }
  };

  _chart_container.find('.y-axis .axis-label span').html(data.axis.y_axis);
  _chart_container.find('.x-axis .axis-label span').html(data.axis.x_axis);

  var chart = new Chartist.Line(_chart_elem, _data, _options);
  $('a[data-toggle="tab"]').on('shown.bs.tab', function(event) { chart.update(); });
}

function plot_total_irrigation() {
  var farm, duration, month, year, axis_lables;
  var _elem = $("#total-farms .irrigation-report");

  farm = _elem.find("#select_farm").val()
  duration = _elem.find(".chart-periods .btn-period.active").attr('rel')
  month = _elem.find(".chart-periods .month-select").val()
  year = _elem.find(".chart-periods .year-select").val()

  $.post("/customer/query_data", {request: "irrigation", farm: farm, duration: duration, month: month, year:year}, function(resp){
    plot_bar_chart('#all_farms_water', resp)
  });
}

function plot_farm_irrigation() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .irrigation-report");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find(".chart-periods .btn-period.active").attr('rel')
  month = _elem.find(".month-select").val()
  year = _elem.find(".year-select").val()

  $.post("/customer/query_data", {request: "irrigation", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference < 0) {
      _elem.find('.label-difference').removeClass('label-success').removeClass('label-default').addClass("label-danger")
      _elem.find('.label-difference').html(resp.difference + ' <i class="fa fa-long-arrow-down"></i>');
    }
    if (resp.difference == 0) {
      _elem.find('.label-difference').removeClass('label-success').removeClass('label-danger').addClass("label-default")
      _elem.find('.label-difference').html(resp.difference);
    }
    if (resp.difference > 0) {
      _elem.find('.label-difference').removeClass('label-default').removeClass('label-danger').addClass("label-success")
      _elem.find('.label-difference').html('+1' + resp.difference + ' <i class="fa fa-long-arrow-up"></i>');
    }
    plot_bar_chart('#detailed_farm_water', resp)
  });
}

function plot_farm_co2() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .co2-read");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find(".chart-periods .btn-period.active").attr('rel')
  month = _elem.find(".month-select").val()
  year = _elem.find(".year-select").val()

  $.post("/customer/query_data", {request: "co2", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference < 0) {
      _elem.find('.label-difference').removeClass('label-success').removeClass('label-default').addClass("label-danger")
      _elem.find('.label-difference').html(resp.difference + ' <i class="fa fa-long-arrow-down"></i>');
    }
    if (resp.difference == 0) {
      _elem.find('.label-difference').removeClass('label-success').removeClass('label-danger').addClass("label-default")
      _elem.find('.label-difference').html(resp.difference);
    }
    if (resp.difference > 0) {
      _elem.find('.label-difference').removeClass('label-default').removeClass('label-danger').addClass("label-success")
      _elem.find('.label-difference').html('+1' + resp.difference + ' <i class="fa fa-long-arrow-up"></i>');
    }
    plot_bar_chart('#detailed_farm_co2', resp)
  });
}

function plot_farm_air_readings_temp() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .air-readings");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find("#air-temp .chart-periods .btn-period.active").attr('rel')
  month = _elem.find("#air-temp .month-select").val()
  year = _elem.find("#air-temp .year-select").val()

  $.post("/customer/query_data", {request: "air_temp", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference < 0) {
      _elem.find('#air-temp .label-difference').removeClass('label-success').removeClass('label-default').addClass("label-danger")
      _elem.find('#air-temp .label-difference').html(resp.difference + ' <i class="fa fa-long-arrow-down"></i>');
    }
    if (resp.difference == 0) {
      _elem.find('#air-temp .label-difference').removeClass('label-success').removeClass('label-danger').addClass("label-default")
      _elem.find('#air-temp .label-difference').html(resp.difference);
    }
    if (resp.difference > 0) {
      _elem.find('#air-temp .label-difference').removeClass('label-default').removeClass('label-danger').addClass("label-success")
      _elem.find('#air-temp .label-difference').html('+1' + resp.difference + ' <i class="fa fa-long-arrow-up"></i>');
    }
    plot_bar_chart('#detailed_farm_air_readings_temp', resp)
  });
}

function plot_farm_air_readings_humidity() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .air-readings");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find("#air-humidity .chart-periods .btn-period.active").attr('rel')
  month = _elem.find("#air-humidity .month-select").val()
  year = _elem.find("#air-humidity .year-select").val()

  $.post("/customer/query_data", {request: "air_water", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference < 0) {
      _elem.find('#air-humidity .label-difference').removeClass('label-success').removeClass('label-default').addClass("label-danger")
      _elem.find('#air-humidity .label-difference').html(resp.difference + ' <i class="fa fa-long-arrow-down"></i>');
    }
    if (resp.difference == 0) {
      _elem.find('#air-humidity .label-difference').removeClass('label-success').removeClass('label-danger').addClass("label-default")
      _elem.find('#air-humidity .label-difference').html(resp.difference);
    }
    if (resp.difference > 0) {
      _elem.find('#air-humidity .label-difference').removeClass('label-default').removeClass('label-danger').addClass("label-success")
      _elem.find('#air-humidity .label-difference').html('+1' + resp.difference + ' <i class="fa fa-long-arrow-up"></i>');
    }
    plot_bar_chart('#detailed_farm_air_readings_humidity', resp)
  });
}

function plot_farm_light() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .light-read");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find(".chart-periods .btn-period.active").attr('rel')
  month = _elem.find(".month-select").val()
  year = _elem.find(".year-select").val()

  $.post("/customer/query_data", {request: "light", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference < 0) {
      _elem.find('.label-difference').removeClass('label-success').removeClass('label-default').addClass("label-danger")
      _elem.find('.label-difference').html(resp.difference + ' <i class="fa fa-long-arrow-down"></i>');
    }
    if (resp.difference == 0) {
      _elem.find('.label-difference').removeClass('label-success').removeClass('label-danger').addClass("label-default")
      _elem.find('.label-difference').html(resp.difference);
    }
    if (resp.difference > 0) {
      _elem.find('.label-difference').removeClass('label-default').removeClass('label-danger').addClass("label-success")
      _elem.find('.label-difference').html('+1' + resp.difference + ' <i class="fa fa-long-arrow-up"></i>');
    }
    plot_bar_chart('#detailed_farm_light', resp)
  });
}

function plot_farm_soil_readings_temp() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .soil-read");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find("#soil-temp .chart-periods .btn-period.active").attr('rel')
  month = _elem.find("#soil-temp .month-select").val()
  year = _elem.find("#soil-temp .year-select").val()

  $.post("/customer/query_data", {request: "soil_temp", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference < 0) {
      _elem.find('#soil-temp .label-difference').removeClass('label-success').removeClass('label-default').addClass("label-danger")
      _elem.find('#soil-temp .label-difference').html(resp.difference + ' <i class="fa fa-long-arrow-down"></i>');
    }
    if (resp.difference == 0) {
      _elem.find('#soil-temp .label-difference').removeClass('label-success').removeClass('label-danger').addClass("label-default")
      _elem.find('#soil-temp .label-difference').html(resp.difference);
    }
    if (resp.difference > 0) {
      _elem.find('#soil-temp .label-difference').removeClass('label-default').removeClass('label-danger').addClass("label-success")
      _elem.find('#soil-temp .label-difference').html('+1' + resp.difference + ' <i class="fa fa-long-arrow-up"></i>');
    }
    plot_bar_chart('#detailed_farm_soil_readings_temp', resp)
  });
}

function plot_farm_soil_readings_humidity() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .soil-read");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find("#soil-humidity .chart-periods .btn-period.active").attr('rel')
  month = _elem.find("#soil-humidity .month-select").val()
  year = _elem.find("#soil-humidity .year-select").val()

  $.post("/customer/query_data", {request: "soil_water", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference < 0) {
      _elem.find('#soil-humidity .label-difference').removeClass('label-success').removeClass('label-default').addClass("label-danger")
      _elem.find('#soil-humidity .label-difference').html(resp.difference + ' <i class="fa fa-long-arrow-down"></i>');
    }
    if (resp.difference == 0) {
      _elem.find('#soil-humidity .label-difference').removeClass('label-success').removeClass('label-danger').addClass("label-default")
      _elem.find('#soil-humidity .label-difference').html(resp.difference);
    }
    if (resp.difference > 0) {
      _elem.find('#soil-humidity .label-difference').removeClass('label-default').removeClass('label-danger').addClass("label-success")
      _elem.find('#soil-humidity .label-difference').html('+1' + resp.difference + ' <i class="fa fa-long-arrow-up"></i>');
    }
    plot_bar_chart('#detailed_farm_soil_readings_humidity', resp)
  });
}

function plot_farm_npk() {
  var duration, month, year, axis_lables, gh;
  var _elem = $("#current-farm-greenhouses .npk-read");

  gh = $("#current-farm-greenhouses #select_greenhouse").val()
  duration = _elem.find(".chart-periods .btn-period.active").attr('rel')
  month = _elem.find(".month-select").val()
  year = _elem.find(".year-select").val()

  $.post("/customer/query_data", {request: "npk", greenhouse: gh, duration: duration, month: month, year:year}, function(resp){
    if (resp.difference != undefined && resp.difference.length > 0) {
      if (resp.difference[0] < 0) {
        _elem.find('.label-difference-n').attr('style','background-color: #d70206;').html('N: ' + resp.difference[0] + ' <i class="fa fa-long-arrow-down"></i>');
      }
      if (resp.difference[0] == 0) {
        _elem.find('.label-difference-n').attr('style','background-color: #d70206;').html('N: ' + resp.difference[0]);
      }
      if (resp.difference[0] > 0) {
        _elem.find('.label-difference-n').attr('style','background-color: #d70206;').html('N:  +1' + resp.difference[0] + ' <i class="fa fa-long-arrow-up"></i>');
      }

      if (resp.difference[1] < 0) {
        _elem.find('.label-difference-p').attr('style','background-color: #f05b4f;').html('P: ' + resp.difference[1] + ' <i class="fa fa-long-arrow-down"></i>');
      }
      if (resp.difference[1] == 0) {
        _elem.find('.label-difference-p').attr('style','background-color: #f05b4f;').html('P: ' + resp.difference[1]);
      }
      if (resp.difference[1] > 0) {
        _elem.find('.label-difference-p').attr('style','background-color: #f05b4f;').html('P:  +1' + resp.difference[1] + ' <i class="fa fa-long-arrow-up"></i>');
      }

      if (resp.difference[2] < 0) {
        _elem.find('.label-difference-k').attr('style','background-color: #f4c63d;').html('K: ' + resp.difference[2] + ' <i class="fa fa-long-arrow-down"></i>');
      }
      if (resp.difference[2] == 0) {
        _elem.find('.label-difference-k').attr('style','background-color: #f4c63d;').html('K: ' + resp.difference[2]);
      }
      if (resp.difference[2] > 0) {
        _elem.find('.label-difference-k').attr('style','background-color: #f4c63d;').html('K:  +1' + resp.difference[2] + ' <i class="fa fa-long-arrow-up"></i>');
      }
      plot_line_chart('#detailed_farm_npk', resp)
    }
  });
}

function switch_farm(farm_id){
  $.post("/customer/", {action: 'swap-farm', farm_id: farm_id}, function(){
    document.location.href = document.location.href;
  });
}
