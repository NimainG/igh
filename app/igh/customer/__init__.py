from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import datetime as dt
import random
import uuid
import calendar

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime
from dateutil.relativedelta import relativedelta
from sqlalchemy import extract

from app.config import activateConfig

from app.core import customer as _customer
from app.core.customer import farms as _farms
from app.core.customer import greenhouses as _greenhouses
from app.core.customer import shields as _shields
from app.core.customer import screenhouses as _screenhouses
from app.core.customer import shadenets as _shadenets
from app.core.customer import farm_greenhouses as _farm_greehouses

from app.igh import portal_check

from app.helper import valid_uuid, valid_date

customer = Blueprint('customer', __name__, template_folder='../templates', static_folder='../static')
customer.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@customer.app_template_filter()
def numberFormat(value):
    return format(int(value), ',d')

@customer.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    this_farm = None

    data_date = request.args.get('dt')

    posted = request.form
    if posted and 'greenhouse_id' in posted and valid_uuid(posted.get('greenhouse_id')):
        session['greenhouse'] = posted.get('greenhouse_id')

    if posted and 'farm' in posted and valid_uuid(posted.get('farm')):
        session['farm'] = posted.get('farm')

    if posted and 'action' in posted and posted.get('action') == "swap-farm":
        if posted and 'farm_id' in posted and valid_uuid(posted.get('farm_id')):
            session['farm'] = posted.get('farm_id')
            return jsonify({"swapped": 1})

    # Anchor date should change on demand
    anchor_date = valid_date(data_date) if valid_date(data_date) else datetime(2020, 3, 30, 0, 0, 0)

    farms = _farms.fetch_by_customer(_this_org["real_id"]).get_json()
    greenhouses = _greenhouses.fetch_by_customer(_this_org["real_id"]).get_json()
    shields = _shields.fetch_by_customer(_this_org["real_id"]).get_json()

    farm_greenhouses = _farm_greehouses.fetch_by_customer(this_farm).get_json() if this_farm else []

    devices = []
    users = []
    irrigation_total = 0

    greenhouse_devices = []
    greenhouse = []
    the_farm = {}

    return render_template(
        'customers/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_DASHBOARD_ACTIVE='active',
        ORG=_this_org,
        FARMS=farms,
        GREENHOUSES=len(greenhouses),
        GREENHOUSE=greenhouse,
        GREENHOUSE_DEVICES=greenhouse_devices,
        DEVICES=len(devices),
        CUSTOMER_DEVICES=devices,
        FARM_GREENHOUSES=farm_greenhouses,
        FARM=the_farm,
        IRRIGATION_TOTAL=irrigation_total,
        USER=users,
        TEMP_CHART=[],
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "utc_date": datetime.utcnow()}
    )

@customer.route('/query_data', methods=['POST', 'GET'])
def query_data():
    data = {
        "labels": [],
        "series": [],
        "axis": {
            "y_axis": "Readings",
            "x_axis": "",
        },
        "metric": ""
    }

    return jsonify(data)
