from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime


from app.config import activateConfig

from app.core import customer as _customer
from app.core.customer import farms as _farms

from app.igh import portal_check

customer_settings = Blueprint('customer_settings', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_settings.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def users_form_constructor():

    _user_role_options = []
    _user_roles = db\
        .session\
        .query(Role)\
        .filter(Role.name.like('Customer%'))\
        .filter(Role.status > config.STATUS_DELETED)\
        .all()
    for _user_role in _user_roles:
        _user_role_options.append({
            "label": _user_role.name,
            "value": _user_role.uid
        })

    _form_fields = [
        {
            "name": "first_name",
            "title": "First Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "last_name",
            "title": "Last Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "id_number",
            "title": "ID Number",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"ID Number",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "phone_number",
            "title": "Phone Number",
            "element": [
                "tel"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "email_address",
            "title": "Email Address",
            "element": [
                "email"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "user_role",
            "title": "User Role",
            "element": [
                "select",
                {
                    "options": _user_role_options
                }
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "status",
            "title": "Status",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Active",
                            "value": config.STATUS_ACTIVE,
                        },
                        {
                            "label": "Inactive",
                            "value": config.STATUS_INACTIVE,
                        }
                    ]
                }
            ],
            "required": 0,
            "default":"Active",
            "views": [
                "edit"
            ]
        },
    ]

    return _form_fields

def notifications_form_constructor():

    _user_options = []
    _users = db\
        .session\
        .query(User)\
        .filter(User.status > config.STATUS_DELETED)\
        .all()
    for _user in _users:
        _user_options.append({
            "label": _user.first_name + ' ' + _user.last_name,
            "value": _user.uid
        })

    _form_fields = [
        {
            "name": "post_mode",
            "title": "Select Mode",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Post on profile",
                            "value": config.NOTIFICATION_MODE_PROFILE,
                        },
                        {
                            "label": "Send on mail",
                            "value": config.NOTIFICATION_MODE_EMAIL,
                        },
                        {
                            "label": "Post to profile and email",
                            "value": config.NOTIFICATION_MODE_ALL,
                        }
                    ]
                }
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "mode"
            ]
        }
    ]

    return _form_fields

def get_co_details(org_id):
    org = {}
    if valid_uuid(org_id):
        _this_co = _customer\
            .query\
            .filter(_customer.uid == org_id)\
            .filter(_customer.status > config.STATUS_DELETED)\
            .first()
        if _this_co:
            org["real_id"] = _this_co.id
            org["id"] = _this_co.uid
            org["name"] = _this_co.name
            org["billing_address"] = _this_co.billing_address
            org["shipping_address"] = _this_co.shipping_address
            org["billing_cycle"] = _this_co.billing_cycle
            org["billing_cycle"] = _this_co.billing_cycle
            org["igh_contact"] = contact_person(_this_co.igh_contact_id)
            org["added_by"] = contact_person(_this_co.creator_id)

    return org

def get_users():
    users = []
    _users = db\
        .session\
        .query(User, UserRole, Role, CustomerUser, Customer)\
        .join(UserRole, UserRole.user_id == User.id)\
        .join(Role, Role.id == UserRole.role_id)\
        .join(CustomerUser, CustomerUser.user_id == User.id)\
        .join(Customer, Customer.id == CustomerUser.customer_id)\
        .filter(Role.name.like('Customer%'))\
        .filter(Customer.uid == session["profile"]["org_id"])\
        .filter(User.status > config.STATUS_DELETED)\
        .filter(UserRole.status > config.STATUS_DELETED)\
        .filter(Role.status > config.STATUS_DELETED)\
        .all()
    for _user in _users:
        users.append({
            "id": _user.User.uid,
            "name": _user.User.first_name + " " + _user.User.last_name,
            "phone_number": _user.User.phone_number,
            "email_address": _user.User.email_address,
            "user_role": _user.Role.name,
            "status": "Active" if _user.User.status == config.STATUS_ACTIVE else "Inactive",
            "created": _user.User.created_at,
            "updated": _user.User.modified_at,
            "creator": contact_person(_user.User.creator_id),
        })

    return users

def type_name(type=0):
    if type > 0:
        if type == config.NOTIFICATION_ALERT:
            return "Alert"

        if type == config.NOTIFICATION_USER:
            return "User"

        if type == config.NOTIFICATION_SYSTEM:
            return "System"

    return "General"

def mode_name(mode=0):
    if mode > 0:
        if mode == config.NOTIFICATION_MODE_PROFILE:
            return "Post on profile"

        if mode == config.NOTIFICATION_MODE_EMAIL:
            return "Send on email"

        if mode == config.NOTIFICATION_MODE_ALL:
            return "Post on profile and email"

    return "General"

def status_name(status=0):
    if status == 0:
        return "Not Seen"

    if status == 1:
        return "Seen"

    if status == 2:
        return "Interacted"

    return "Unknown"

@customer_settings.route('/profile', methods=['POST', 'GET'])
def profile():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    action_response = {}

    _org_farms = _farms.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    posted = request.form

    # if posted:
    #     _org_name = posted.get("org_name").strip()
    #     _phone_number = posted.get("phone_number").strip()
    #     _email_address = posted.get("email_address").strip()
    #     _billing_address = posted.get("billing_address").strip()
    #     _shipping_address = posted.get("shipping_address").strip()
    #     _org_id = posted.get("org_id").strip()
    #
    #     if _org_id is not None and _org_name is not None \
    #         and _email_address is not None and _phone_number:
    #
    #         _customer = Customer\
    #             .query\
    #             .filter(Customer.uid == _org_id)\
    #             .first()
    #         if _customer:
    #             _customer.name = _org_name
    #             _customer.billing_address = _billing_address
    #             _customer.shipping_address = _shipping_address
    #
    #             session['profile']['org'] = _org_name
    #             session['profile']['org_billing_address'] = _billing_address
    #             session['profile']['org_shipping_address'] = _shipping_address
    #
    #             db.session.commit()
    #
    #             return jsonify({
    #                 "type": "success",
    #                 "message": "Customer details edited successfully."
    #             })
    #
    #         # exists = User\
    #         #     .query\
    #         #     .filter(User.email_address == _email_address)\
    #         #     .filter(User.status > config.STATUS_DELETED)\
    #         #     .first()
    #         # if not exists:
    #         #     role = Role\
    #         #         .query\
    #         #         .filter(Role.uid == _user_role)\
    #         #         .filter(Role.name.like('Customer%'))\
    #         #         .filter(Role.status > config.STATUS_DELETED)\
    #         #         .first()
    #         #
    #         #     user = User\
    #         #         .query\
    #         #         .filter(User.uid == session["profile"]["id"])\
    #         #         .filter(User.status > config.STATUS_DELETED)\
    #         #         .first()
    #         #     if user and role:
    #         #         _user = User(
    #         #             uid=uuid.uuid4(),
    #         #             first_name=_first_name,
    #         #             last_name=_last_name,
    #         #             email_address=_email_address,
    #         #             phone_number=_phone_number,
    #         #             creator_id=user.id
    #         #         )
    #         #         db.session.add(_user)
    #         #         db.session.commit()
    #         #
    #         #         if _user:
    #         #             _user_role = UserRole(
    #         #                 uid=uuid.uuid4(),
    #         #                 user_id=_user.id,
    #         #                 role_id=role.id,
    #         #                 creator_id=user.id
    #         #             )
    #         #             db.session.add(_user_role)
    #         #             db.session.commit()
    #         #
    #         #             _new_customer_user = CustomerUser(
    #         #                 uid=uuid.uuid4(),
    #         #                 id_number=_id_number,
    #         #                 customer_id=this_co["real_id"],
    #         #                 user_id=_user.id,
    #         #                 creator_id=user.id
    #         #             )
    #         #             db.session.add(_new_customer_user)
    #         #             db.session.commit()
    #         #
    #         #             activation_code = str(uuid.uuid4()).replace("-", "")
    #         #             _user_auth = Auth(
    #         #                 uid=uuid.uuid4(),
    #         #                 user_id=_user.id,
    #         #                 username=_email_address,
    #         #                 activation_code=activation_code,
    #         #                 creator_id=user.id
    #         #             )
    #         #             db.session.add(_user_auth)
    #         #             db.session.commit()
    #         #
    #         #             dispatch_resp = "not sent"
    #         #             _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
    #         #             if _dispatch.status_code == 200:
    #         #                 _obj = json.loads(_dispatch.content)
    #         #                 if "message" in _obj:
    #         #                     dispatch_resp = "sent"
    #         #
    #         #             action_response = {
    #         #                 "type": "success",
    #         #                 "message": "User added successfully, activation email " + dispatch_resp + "."
    #         #             }
    #         # else:
    #         #     action_response = {
    #         #         "type": "danger",
    #         #         "message": "Email address already in use, please try a different one."
    #         #     }
    #     else:
    #         action_response = {
    #             "type": "warning",
    #             "message": "User not added, invalid input."
    #         }
    #
        # if posted.get("action").strip().lower() == 'edit':
        #     _first_name = posted.get("first_name").strip()
        #     _last_name = posted.get("last_name").strip()
        #     _email_address = posted.get("email_address").strip()
        #     _phone_number = posted.get("phone_number").strip()
        #     _user_role = posted.get("user_role").strip()
        #     _id_number = posted.get("id_number").strip()
        #
        #     if _first_name is not None and _last_name is not None \
        #         and _email_address is not None and _phone_number is not None \
        #         and _user_role is not None:
        #
        #         exists = User\
        #             .query\
        #             .filter(User.email_address == _email_address)\
        #             .filter(User.status > config.STATUS_DELETED)\
        #             .first()
        #         if not exists:
        #             role = Role\
        #                 .query\
        #                 .filter(Role.uid == _user_role)\
        #                 .filter(Role.name.like('Customer%'))\
        #                 .filter(Role.status > config.STATUS_DELETED)\
        #                 .first()
        #
        #             user = User\
        #                 .query\
        #                 .filter(User.uid == session["profile"]["id"])\
        #                 .filter(User.status > config.STATUS_DELETED)\
        #                 .first()
        #             if user and role:
        #                 _user = User(
        #                     uid=uuid.uuid4(),
        #                     first_name=_first_name,
        #                     last_name=_last_name,
        #                     email_address=_email_address,
        #                     phone_number=_phone_number,
        #                     creator_id=user.id
        #                 )
        #                 db.session.add(_user)
        #                 db.session.commit()
        #
        #                 if _user:
        #                     _user_role = UserRole(
        #                         uid=uuid.uuid4(),
        #                         user_id=_user.id,
        #                         role_id=role.id,
        #                         creator_id=user.id
        #                     )
        #                     db.session.add(_user_role)
        #                     db.session.commit()
        #
        #                     _new_customer_user = CustomerUser(
        #                         uid=uuid.uuid4(),
        #                         id_number=_id_number,
        #                         customer_id=this_co["real_id"],
        #                         user_id=_user.id,
        #                         creator_id=user.id
        #                     )
        #                     db.session.add(_new_customer_user)
        #                     db.session.commit()
        #
        #                     activation_code = str(uuid.uuid4()).replace("-", "")
        #                     _user_auth = Auth(
        #                         uid=uuid.uuid4(),
        #                         user_id=_user.id,
        #                         username=_email_address,
        #                         activation_code=activation_code,
        #                         creator_id=user.id
        #                     )
        #                     db.session.add(_user_auth)
        #                     db.session.commit()
        #
        #                     dispatch_resp = "not sent"
        #                     _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
        #                     if _dispatch.status_code == 200:
        #                         _obj = json.loads(_dispatch.content)
        #                         if "message" in _obj:
        #                             dispatch_resp = "sent"
        #
        #                     action_response = {
        #                         "type": "success",
        #                         "message": "User added successfully, activation email " + dispatch_resp + "."
        #                     }
        #         else:
        #             action_response = {
        #                 "type": "danger",
        #                 "message": "Email address already in use, please try a different one."
        #             }
        #     else:
        #         action_response = {
        #             "type": "warning",
        #             "message": "User not added, invalid input."
        #         }
        # else:
        #     action_response = {
        #         "type": "danger",
        #         "message": "Invalid input."
        #     }

    users = []

    return render_template(
        'customers/settings/profile.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_PROFILE_ACTIVE='active',
        USERS=users,
        FARMS=_org_farms,
        FARM={},
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/users', methods=['POST', 'GET'])
def users():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    form_action = 'Add'
    action_response = {}

    this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    posted = request.form
    if posted:
        if posted.get("action").strip().lower() == 'add':
            _first_name = posted.get("first_name").strip()
            _last_name = posted.get("last_name").strip()
            _email_address = posted.get("email_address").strip()
            _phone_number = posted.get("phone_number").strip()
            _user_role = posted.get("user_role").strip()
            _id_number = posted.get("id_number").strip()

            if _first_name is not None and _last_name is not None \
                and _email_address is not None and _phone_number is not None \
                and _user_role is not None:

                exists = User\
                    .query\
                    .filter(User.email_address == _email_address)\
                    .filter(User.status > config.STATUS_DELETED)\
                    .first()
                if not exists:
                    role = Role\
                        .query\
                        .filter(Role.uid == _user_role)\
                        .filter(Role.name.like('Customer%'))\
                        .filter(Role.status > config.STATUS_DELETED)\
                        .first()

                    user = User\
                        .query\
                        .filter(User.uid == session["profile"]["id"])\
                        .filter(User.status > config.STATUS_DELETED)\
                        .first()
                    if user and role:
                        _user = User(
                            uid=uuid.uuid4(),
                            first_name=_first_name,
                            last_name=_last_name,
                            email_address=_email_address,
                            phone_number=_phone_number,
                            creator_id=user.id
                        )
                        db.session.add(_user)
                        db.session.commit()

                        if _user:
                            _user_role = UserRole(
                                uid=uuid.uuid4(),
                                user_id=_user.id,
                                role_id=role.id,
                                creator_id=user.id
                            )
                            db.session.add(_user_role)
                            db.session.commit()

                            _new_customer_user = CustomerUser(
                                uid=uuid.uuid4(),
                                id_number=_id_number,
                                customer_id=this_co["real_id"],
                                user_id=_user.id,
                                creator_id=user.id
                            )
                            db.session.add(_new_customer_user)
                            db.session.commit()

                            activation_code = str(uuid.uuid4()).replace("-", "")
                            _user_auth = Auth(
                                uid=uuid.uuid4(),
                                user_id=_user.id,
                                username=_email_address,
                                activation_code=activation_code,
                                creator_id=user.id
                            )
                            db.session.add(_user_auth)
                            db.session.commit()

                            dispatch_resp = "not sent"
                            _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
                            if _dispatch.status_code == 200:
                                _obj = json.loads(_dispatch.content)
                                if "message" in _obj:
                                    dispatch_resp = "sent"

                            action_response = {
                                "type": "success",
                                "message": "User added successfully, activation email " + dispatch_resp + "."
                            }
                else:
                    action_response = {
                        "type": "danger",
                        "message": "Email address already in use, please try a different one."
                    }
            else:
                action_response = {
                    "type": "warning",
                    "message": "User not added, invalid input."
                }
        else:
            action_response = {
                "type": "danger",
                "message": "Invalid input."
            }

    users = []

    return render_template(
        'customers/settings/users/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        FORM=[],
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        USERS=users,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/users/add', methods=['POST', 'GET'])
def add_users():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    form_action = 'Add'
    action_response = {}

    this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    posted = request.form
    if posted:
        _first_name = posted.get("first_name")
        _last_name = posted.get("last_name")
        _email_address = posted.get("email_address")
        _phone_number = posted.get("phone_number")
        _user_role = posted.get("user_role")
        _id_number = posted.get("id_number")

        if _first_name is not None and _last_name is not None \
            and _email_address is not None and _phone_number is not None \
            and _user_role is not None:

            exists = User\
                .query\
                .filter(User.email_address == _email_address)\
                .filter(User.status > config.STATUS_DELETED)\
                .first()
            if not exists:
                role = Role\
                    .query\
                    .filter(Role.uid == _user_role)\
                    .filter(Role.name.like('Customer%'))\
                    .filter(Role.status > config.STATUS_DELETED)\
                    .first()

                user = User\
                    .query\
                    .filter(User.uid == session["profile"]["id"])\
                    .filter(User.status > config.STATUS_DELETED)\
                    .first()
                if user and role:
                    _user = User(
                        uid=uuid.uuid4(),
                        first_name=_first_name,
                        last_name=_last_name,
                        email_address=_email_address,
                        phone_number=_phone_number,
                        creator_id=user.id
                    )
                    db.session.add(_user)
                    db.session.commit()

                    if _user:
                        _user_role = UserRole(
                            uid=uuid.uuid4(),
                            user_id=_user.id,
                            role_id=role.id,
                            creator_id=user.id
                        )
                        db.session.add(_user_role)
                        db.session.commit()

                        _new_customer_user = CustomerUser(
                            uid=uuid.uuid4(),
                            id_number=_id_number,
                            customer_id=this_co["real_id"],
                            user_id=_user.id,
                            creator_id=user.id
                        )
                        db.session.add(_new_customer_user)
                        db.session.commit()

                        activation_code = str(uuid.uuid4()).replace("-", "")
                        _user_auth = Auth(
                            uid=uuid.uuid4(),
                            user_id=_user.id,
                            username=_email_address,
                            activation_code=activation_code,
                            creator_id=user.id
                        )
                        db.session.add(_user_auth)
                        db.session.commit()

                        dispatch_resp = "not sent"
                        _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
                        if _dispatch.status_code == 200:
                            _obj = json.loads(_dispatch.content)
                            if "message" in _obj:
                                dispatch_resp = "sent"

                        action_response = {
                            "type": "success",
                            "message": "User added successfully, activation email " + dispatch_resp + "."
                        }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please try a different one."
                }
        else:
            action_response = {
                "type": "warning",
                "message": "Emails not sent out, invalid input."
            }

    users = []

    return render_template(
        'customers/settings/users/add.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        USERS=users,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )

@customer_settings.route('/users/bulk', methods=['POST', 'GET'])
def bulk_users():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    form_action = 'Add'
    action_response = {}

    this_co = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()

    posted = request.form
    if posted:
        _first_name = posted.get("first_name")
        _last_name = posted.get("last_name")
        _email_address = posted.get("email_address")
        _phone_number = posted.get("phone_number")
        _user_role = posted.get("user_role")
        _id_number = posted.get("id_number")

        if _first_name is not None and _last_name is not None \
            and _email_address is not None and _phone_number is not None \
            and _user_role is not None:

            exists = User\
                .query\
                .filter(User.email_address == _email_address)\
                .filter(User.status > config.STATUS_DELETED)\
                .first()
            if not exists:
                role = Role\
                    .query\
                    .filter(Role.uid == _user_role)\
                    .filter(Role.name.like('Customer%'))\
                    .filter(Role.status > config.STATUS_DELETED)\
                    .first()

                user = User\
                    .query\
                    .filter(User.uid == session["profile"]["id"])\
                    .filter(User.status > config.STATUS_DELETED)\
                    .first()
                if user and role:
                    _user = User(
                        uid=uuid.uuid4(),
                        first_name=_first_name,
                        last_name=_last_name,
                        email_address=_email_address,
                        phone_number=_phone_number,
                        creator_id=user.id
                    )
                    db.session.add(_user)
                    db.session.commit()

                    if _user:
                        _user_role = UserRole(
                            uid=uuid.uuid4(),
                            user_id=_user.id,
                            role_id=role.id,
                            creator_id=user.id
                        )
                        db.session.add(_user_role)
                        db.session.commit()

                        _new_customer_user = CustomerUser(
                            uid=uuid.uuid4(),
                            id_number=_id_number,
                            customer_id=this_co["real_id"],
                            user_id=_user.id,
                            creator_id=user.id
                        )
                        db.session.add(_new_customer_user)
                        db.session.commit()

                        activation_code = str(uuid.uuid4()).replace("-", "")
                        _user_auth = Auth(
                            uid=uuid.uuid4(),
                            user_id=_user.id,
                            username=_email_address,
                            activation_code=activation_code,
                            creator_id=user.id
                        )
                        db.session.add(_user_auth)
                        db.session.commit()

                        dispatch_resp = "not sent"
                        _dispatch = send_account_activation_mail(activation_code, _first_name, _email_address)
                        if _dispatch.status_code == 200:
                            _obj = json.loads(_dispatch.content)
                            if "message" in _obj:
                                dispatch_resp = "sent"

                        action_response = {
                            "type": "success",
                            "message": "User added successfully, activation email " + dispatch_resp + "."
                        }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Email address already in use, please try a different one."
                }
        else:
            action_response = {
                "type": "warning",
                "message": "Emails not sent out, invalid input."
            }

    users = []

    return render_template(
        'customers/settings/users/bulk.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USERS_ACTIVE='active',
        USERS=users,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )

@customer_settings.route('/roles', methods=['POST', 'GET'])
def all_roles():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/settings/roles.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        # PROFILE=session['profile'],
        ROLES=[],
        MODULES=[],
        ACTIVE_ROLES=[],
        INACTIVE_ROLES=[],
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/roles/edit', methods=['POST', 'GET'])
def edit_role():
    # redirect, realm = portal_check(session, 'customer')
    # if redirect:
    #     return realm

    return render_template(
        'customers/settings/roles-edit.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
       # PROFILE=session['profile'],
        ROLES=[],
        MODULES=[],
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/roles/view', methods=['POST', 'GET'])
def view_role():
    # redirect, realm = portal_check(session, 'customer')
    # if redirect:
    #     return realm

    return render_template(
        'customers/settings/role-view.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        # PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_settings.route('/activity-log', methods=['POST', 'GET'])
def activity_log():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/settings/activity-logs.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SETTINGS_ACTIVE='active',
        CUSTOMER_PORTAL_SETTINGS_USER_ROLES_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
