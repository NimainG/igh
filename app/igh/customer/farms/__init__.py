from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.core import user as _user
from app.core import customer as _customer
from app.core.customer import farms as _farms

from app.igh import portal_check

customer_farms = Blueprint('customer_farms', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_farms.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouses(farm_id):
    _response = []
    _ghs = db\
        .session\
        .query(CustomerGreenhouse, Greenhouse)\
        .join(Greenhouse, Greenhouse.id == CustomerGreenhouse.greenhouse_id)\
        .filter(CustomerGreenhouse.farm_id == farm_id)\
        .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
        .filter(Greenhouse.status > config.STATUS_DELETED)\
        .all()
    for _gh in _ghs:
        _response.append({
            "id": _gh.Greenhouse.uid,
            "name": _gh.Greenhouse.name,
            "description": _gh.Greenhouse.description,
            "more": _gh.Greenhouse.info_link,
            "status": "Active" if _gh.Greenhouse.status == config.STATUS_ACTIVE else "Inactive",
            "created": _gh.Greenhouse.created_at,
            "updated": _gh.Greenhouse.modified_at,
            "creator": contact_person(_gh.Greenhouse.creator_id),
        })

    return _response

def devices(farm_id):
    _response = []
    _devices = db\
        .session\
        .query(CustomerDevice, Device)\
        .join(Device, Device.id == CustomerDevice.device_id)\
        .filter(CustomerDevice.farm_id == farm_id)\
        .filter(CustomerDevice.status > config.STATUS_DELETED)\
        .filter(Device.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        _response.append({
            "id": _device.Device.uid,
            "name": _device.Device.name,
            "serial": _device.Device.serial,
            "photo_url": _device.Device.photo_url,
            "description": _device.Device.description,
            "more": _device.Device.info_link,
            "status": "Active" if _device.Device.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.Device.created_at,
            "updated": _device.Device.modified_at,
            "creator": contact_person(_device.Device.creator_id),
        })

    return _response

def farm_form_constructor():

    _user_options = []
    _users = db\
        .session\
        .query(User)\
        .filter(User.status > config.STATUS_DELETED)\
        .all()
    for _user in _users:
        _user_options.append({
            "label": _user.first_name + ' ' + _user.last_name,
            "value": _user.uid
        })

    _form_fields = [
        {
            "name": "name",
            "title": "Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "address",
            "title": "Address",
            "element": [
                "textarea"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "latitude",
            "title": "Latitude",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "longitude",
            "title": "Longitude",
            "element": [
                "text"
            ],
            "required": 0,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "status",
            "title": "Status",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Active",
                            "value": config.STATUS_ACTIVE,
                        },
                        {
                            "label": "Inactive",
                            "value": config.STATUS_INACTIVE,
                        }
                    ]
                }
            ],
            "required": 0,
            "default":"Active",
            "views": [
                "edit"
            ]
        },
    ]

    return _form_fields

@customer_farms.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = this_org()
    _this_user = this_user()

    form_action = 'Add'
    action_response = {}

    farms = []
    _farms = db\
        .session\
        .query(Farm)\
        .filter(Farm.customer_id == _this_org.id)\
        .filter(Farm.status > config.STATUS_DELETED)\
        .all()
    for _farm in _farms:
        _contact_person = False
        _farm_contact_person = CustomerUser\
            .query\
            .filter(CustomerUser.id == _farm.contact_person_id)\
            .filter(User.status > config.STATUS_DELETED)\
            .first()
        if _farm_contact_person:
            _contact_person = contact_person(_farm_contact_person.user_id)

        farms.append({
            "id": _farm.uid,
            "name": _farm.name,
            "customer": _this_org.name,
            "contact_person": "",
            "address": _farm.address,
            "latitude": _farm.latitude,
            "longitude": _farm.longitude,
            "greenhouses": greenhouses(_farm.id),
            "devices": devices(_farm.id),
            "status": "Active" if _farm.status == config.STATUS_ACTIVE else "Inactive",
            "created": _farm.created_at,
            "updated": _farm.modified_at,
            "creator": contact_person(_farm.creator_id),
        })

    return render_template(
        'customers/farms/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        FORM=farm_form_constructor(),
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        FARMS=farms,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@customer_farms.route('/add', methods=['POST', 'GET'])
def add():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = _customer.fetch_one(session["profile"]["staff_info"]["customer"]["id"], True).get_json()
    _this_user = _user.fetch_one(session["profile"]["user"]["id"]).get_json()

    form_action = 'Edit'
    action_form = []
    action_response = {}

    posted = request.form
    if posted:
        _name = posted.get("farm_ref").strip()
        _address = posted.get("farm_address").strip()
        _state = posted.get("state").strip()
        _city = posted.get("city").strip()
        _pincode = posted.get("pincode").strip()
        _latitude = posted.get("latitude").strip()
        _longitude = posted.get("longitude").strip()
        _total_farm_area = posted.get("total_farm_area").strip()
        _status = posted.get("status").strip()
        _this_customer = _this_org

        if _this_customer and _name:
            action_response = {
                "type": "warning",
                "message": "Farm not added, invalid input."
            }

            # user = User\
            #     .query\
            #     .filter(User.uid == session["profile"]["id"])\
            #     .filter(User.status > config.STATUS_DELETED)\
            #     .first()
            # if user:
            #     # _farm = Farm(
            #     #     uid=uuid.uuid4(),
            #     #     name=_name,
            #     #     customer_id=_this_customer.id,
            #     #     contact_person_id=_this_user.CustomerUser.id,
            #     #     address=_address,
            #     #     state=_state,
            #     #     city=_city,
            #     #     pincode=_pincode,
            #     #     latitude=_latitude,
            #     #     longitude=_longitude,
            #     #     area=_total_farm_area,
            #     #     status=_status,
            #     #     creator_id=user.id
            #     # )
            #     # db.session.add(_farm)
            #     # db.session.commit()
            #
            #     if _farm:
            #         action_response = {
            #             "type": "success",
            #             "message": "Farm added successfully."
            #         }
        else:
            action_response = {
                "type": "warning",
                "message": "Farm not added, invalid input."
            }

    farms = []
    # _farms = db\
    #     .session\
    #     .query(Farm)\
    #     .filter(Farm.customer_id == _this_org.id)\
    #     .filter(Farm.status > config.STATUS_DELETED)\
    #     .all()
    # for _farm in _farms:
    #     _contact_person = False
    #     _farm_contact_person = CustomerUser\
    #         .query\
    #         .filter(CustomerUser.id == _farm.contact_person_id)\
    #         .filter(User.status > config.STATUS_DELETED)\
    #         .first()
    #     if _farm_contact_person:
    #         _contact_person = contact_person(_farm_contact_person.user_id)
    #     farms.append({
    #         "id": _farm.uid,
    #         "uid": _farm.uid,
    #         "name": _farm.name,
    #         "customer": _this_org.name,
    #         "contact_person": "",
    #         "address": _farm.address,
    #         "latitude": _farm.latitude,
    #         "longitude": _farm.longitude,
    #         "farms": greenhouses(_farm.id),
    #         "devices": devices(_farm.id),
    #         "status": "Active" if _farm.status == config.STATUS_ACTIVE else "Inactive",
    #         "created": _farm.created_at,
    #         "updated": _farm.modified_at,
    #         "creator": contact_person(_farm.creator_id),
    #     })
    #
    return render_template(
        'customers/farms/add.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        CUSTOMER_PORTAL_FARMS_LIST_ACTIVE='active',
        FORM=action_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        FARMS=farms,
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year), "today": datetime.now()}
    )

@customer_farms.route('/<farm_id>/edit', methods=['POST', 'GET'])
def edit(farm_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = this_org()

    farm = None

    form_action = 'Edit'
    action_form = []
    action_response = {}

    # if farm_id != "":
    #     farm = Farm\
    #         .query\
    #         .filter(Farm.uid == farm_id)\
    #         .filter(Farm.status > config.STATUS_DELETED)\
    #         .first()
    #     if farm:
    #         posted = request.form
    #         if posted:
    #             _name = posted.get("farm_ref").strip()
    #             _address = posted.get("farm_address").strip()
    #             _state = posted.get("state").strip()
    #             _city = posted.get("city").strip()
    #             _pincode = posted.get("pincode").strip()
    #             _latitude = posted.get("latitude").strip()
    #             _longitude = posted.get("longitude").strip()
    #             _total_farm_area = posted.get("total_farm_area").strip()
    #             _status = posted.get("status").strip()
    #             _this_customer = _this_org
    #
    #             if _this_customer and _name:
    #                 farm.name = _name
    #                 farm.customer_id = _this_customer.id
    #                 farm.address = _address
    #                 farm.state = _state
    #                 farm.city = _city
    #                 farm.pincode = _pincode
    #                 farm.latitude = _latitude
    #                 farm.longitude = _longitude
    #                 farm.area = _total_farm_area
    #                 farm.status = _status
    #
    #                 db.session.commit()
    #
    #                 action_response = {
    #                     "type": "success",
    #                     "message": "Farm edited successfully."
    #                 }
    #
    #             else:
    #                 action_response = {
    #                     "type": "warning",
    #                     "message": "Farm not edited, invalid input."
    #                 }
    #
    #     else:
    #         action_response = {
    #             "type": "danger",
    #             "message": "Invalid input."
    #         }

    farms = []
    # _farms = db\
    #     .session\
    #     .query(Farm)\
    #     .filter(Farm.customer_id == _this_org.id)\
    #     .filter(Farm.status > config.STATUS_DELETED)\
    #     .all()
    #
    # for _farm in _farms:
    #     _contact_person = False
    #     _farm_contact_person = CustomerUser\
    #         .query\
    #         .filter(CustomerUser.id == _farm.contact_person_id)\
    #         .filter(User.status > config.STATUS_DELETED)\
    #         .first()
    #     if _farm_contact_person:
    #         _contact_person = contact_person(_farm_contact_person.user_id)
    #     farms.append({
    #         "id": _farm.uid,
    #         "uid": _farm.uid,
    #         "name": _farm.name,
    #         "customer": _this_org.name,
    #         "contact_person": "",
    #         "address": _farm.address,
    #         "latitude": _farm.latitude,
    #         "longitude": _farm.longitude,
    #         "farms": greenhouses(_farm.id),
    #         "devices": devices(_farm.id),
    #         "status": "Active" if _farm.status == config.STATUS_ACTIVE else "Inactive",
    #         "created": _farm.created_at,
    #         "updated": _farm.modified_at,
    #         "creator": contact_person(_farm.creator_id),
    #     })

    return render_template(
        'customers/farms/edit.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        CUSTOMER_PORTAL_FARMS_LIST_ACTIVE='active',
        FORM=action_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        FARMS=_farms,
        FARM=farm,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@customer_farms.route('/<farm_id>/delete', methods=['POST', 'GET'])
def delete(farm_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = this_org()

    action_response = {}

    # if farm_id != "":
    #     farm = Farm\
    #         .query\
    #         .filter(Farm.uid == farm_id)\
    #         .filter(Farm.status > config.STATUS_DELETED)\
    #         .first()
    #     if farm:
    #         farm.status=config.STATUS_DELETED
    #         db.session.commit()
    #
    #         action_response = {
    #             "type": "success",
    #             "message": farm.name + " deleted successfully."
    #         }
    #     else:
    #         action_response = {
    #             "type": "danger",
    #             "message": "Invalid input."
    #         }

    farms = []
    # _farms = db\
    #     .session\
    #     .query(Farm)\
    #     .filter(Farm.customer_id == _this_org.id)\
    #     .filter(Farm.status > config.STATUS_DELETED)\
    #     .all()
    # for _farm in _farms:
    #     _contact_person = False
    #     _farm_contact_person = CustomerUser\
    #         .query\
    #         .filter(CustomerUser.id == _farm.contact_person_id)\
    #         .filter(User.status > config.STATUS_DELETED)\
    #         .first()
    #     if _farm_contact_person:
    #         _contact_person = contact_person(_farm_contact_person.user_id)
    #     farms.append({
    #         "id": _farm.uid,
    #         "name": _farm.name,
    #         "customer": this_org(),
    #         "address": _farm.address,
    #         "latitude": _farm.latitude,
    #         "longitude": _farm.longitude,
    #         "status": "Active" if _farm.status == config.STATUS_ACTIVE else "Inactive",
    #         "created": _farm.created_at,
    #         "updated": _farm.modified_at,
    #         "creator": contact_person(_farm.creator_id),
    #     })

    return render_template(
        'customers/farms/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_FARMS_ACTIVE='active',
        FORM_ACTION_RESPONSE=action_response,
        FARMS=farms,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )
