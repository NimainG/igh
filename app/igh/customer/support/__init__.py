from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.igh import portal_check

from app.helper import valid_uuid, valid_date

customer_support = Blueprint('customer_support', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_support.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@customer_support.route('/', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/support/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_support.route('/request', methods=['POST', 'GET'])
def request():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/support/request.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_support.route('/request/type', methods=['POST', 'GET'])
def type():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/support/type.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_support.route('/request/detail', methods=['POST', 'GET'])
def detail():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/support/detail.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_support.route('/request/photos', methods=['POST', 'GET'])
def photos():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/support/photos.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_SUPPORT_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
