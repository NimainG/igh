from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.models import db, Customer, User, Role, UserRole, Greenhouse, \
    CustomerGreenhouse, Farm, CustomerDevice, Device, CustomerUser, CustomerGreenhouseDevice

from app.config import activateConfig

from app.igh.authenticate import portal_check
from app.igh.admin import contact_person
from app.igh.customer import this_org, this_user

customer_greenhouses = Blueprint('customer_greenhouses', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_greenhouses.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

def greenhouses(farm_id):
    _response = []
    _ghs = db\
        .session\
        .query(CustomerGreenhouse, Greenhouse)\
        .join(Greenhouse, Greenhouse.id == CustomerGreenhouse.greenhouse_id)\
        .filter(CustomerGreenhouse.farm_id == farm_id)\
        .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
        .filter(Greenhouse.status > config.STATUS_DELETED)\
        .all()
    for _gh in _ghs:
        _response.append({
            "id": _gh.Greenhouse.uid,
            "name": _gh.Greenhouse.name,
            "description": _gh.Greenhouse.description,
            "more": _gh.Greenhouse.info_link,
            "status": "Active" if _gh.Greenhouse.status == config.STATUS_ACTIVE else "Inactive",
            "created": _gh.Greenhouse.created_at,
            "updated": _gh.Greenhouse.modified_at,
            "creator": contact_person(_gh.Greenhouse.creator_id),
        })

    return _response

def devices(farm_id):
    _response = []
    _devices = db\
        .session\
        .query(CustomerDevice, Device)\
        .join(Device, Device.id == CustomerDevice.device_id)\
        .filter(CustomerDevice.farm_id == farm_id)\
        .filter(CustomerDevice.status > config.STATUS_DELETED)\
        .filter(Device.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        _response.append({
            "id": _device.Device.uid,
            "name": _device.Device.name,
            "serial": _device.Device.serial,
            "photo_url": _device.Device.photo_url,
            "description": _device.Device.description,
            "more": _device.Device.info_link,
            "status": "Active" if _device.Device.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.Device.created_at,
            "updated": _device.Device.modified_at,
            "creator": contact_person(_device.Device.creator_id),
        })

    return _response

def greenhouse_form_constructor():
    _this_org = this_org()

    _farms_options = []
    _farms = db\
        .session\
        .query(Farm)\
        .filter(Farm.customer_id == _this_org.id)\
        .filter(Farm.status > config.STATUS_DELETED)\
        .all()
    for _farm in _farms:
        _farms_options.append({
            "label": _farm.name,
            "value": _farm.uid
        })

    _greenhouses_options = []
    _greenhouses = db\
        .session\
        .query(Greenhouse)\
        .filter(Greenhouse.status > config.STATUS_DELETED)\
        .all()
    for _greenhouse in _greenhouses:
        _greenhouses_options.append({
            "label": _greenhouse.name,
            "value": _greenhouse.uid
        })

    _form_fields = [
        {
            "name": "name",
            "title": "Name",
            "element": [
                "text"
            ],
            "required": 1,
            "default":"",
            "placeholder":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "farm_id",
            "title": "Farm",
            "element": [
                "select",
                {
                    "options": _farms_options
                }
            ],
            "required": 1,
            "default":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "greenhouse_id",
            "title": "Greenhouse Type",
            "element": [
                "select",
                {
                    "options": _greenhouses_options
                }
            ],
            "required": 1,
            "default":"",
            "views": [
                "add",
                "edit"
            ]
        },
        {
            "name": "status",
            "title": "Status",
            "element": [
                "select",
                {
                    "options": [
                        {
                            "label": "Active",
                            "value": config.STATUS_ACTIVE,
                        },
                        {
                            "label": "Inactive",
                            "value": config.STATUS_INACTIVE,
                        }
                    ]
                }
            ],
            "required": 0,
            "default":"Active",
            "views": [
                "edit"
            ]
        },
    ]

    return _form_fields

def devices_in_greenhouse(greenhouse_id):
    _response = []
    _devices = db\
        .session\
        .query(CustomerGreenhouseDevice, CustomerDevice, CustomerGreenhouse, Greenhouse, Device, Customer)\
        .join(CustomerDevice, CustomerDevice.id == CustomerGreenhouseDevice.customer_device_id)\
        .join(CustomerGreenhouse, CustomerGreenhouse.id == CustomerGreenhouseDevice.customer_greenhouse_id)\
        .join(Greenhouse, Greenhouse.id == CustomerGreenhouse.greenhouse_id)\
        .join(Device, Device.id == CustomerDevice.device_id)\
        .join(Customer, Customer.id == CustomerGreenhouseDevice.customer_id)\
        .filter(CustomerGreenhouse.id == greenhouse_id)\
        .filter(CustomerDevice.status > config.STATUS_DELETED)\
        .all()
    for _device in _devices:
        _response.append({
            "id": _device.CustomerDevice.uid,
            "device_ref": _device.Device.name,
            "serial": _device.Device.serial,
            "description": _device.Device.description,
            "status": "Active" if _device.CustomerDevice.status == config.STATUS_ACTIVE else "Inactive",
            "created": _device.CustomerDevice.created_at,
            "creator": contact_person(_device.CustomerDevice.creator_id),
        })

    return _response

@customer_greenhouses.route('/', methods=['POST', 'GET'])
def farm_greenhouses():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = this_org()
    _this_user = this_user()

    form_action = 'Add'
    action_response = {}
    posted = request.form

    greenhouses = []
    customer_info = {}

    _this_customer = _this_org

    if _this_customer:
        customer_info = {
            "id": _this_customer.uid,
            "name": _this_customer.name,
        }

        if posted:
            if posted.get("action").strip().lower() == 'add':
                _name = posted.get("name").strip()
                _greenhouse_id = posted.get("greenhouse_id").strip()
                _farm_id = posted.get("farm_id").strip()

                if _name is not None and _greenhouse_id is not None and _farm_id is not None:
                    _this_farm = Farm\
                        .query\
                        .filter(Farm.uid == _farm_id)\
                        .filter(Farm.status > config.STATUS_DELETED)\
                        .first()

                    _this_greenhouse = Greenhouse\
                        .query\
                        .filter(Greenhouse.uid == _greenhouse_id)\
                        .filter(Greenhouse.status > config.STATUS_DELETED)\
                        .first()

                    if _this_farm and _this_greenhouse:
                        _new_greenhouse = CustomerGreenhouse(
                            uid=uuid.uuid4(),
                            name=_name,
                            customer_id=_this_customer.id,
                            farm_id=_this_farm.id,
                            greenhouse_id=_this_greenhouse.id,
                            creator_id=_this_user.User.id
                        )
                        db.session.add(_new_greenhouse)
                        db.session.commit()

                    if _this_greenhouse:
                        action_response = {
                            "type": "success",
                            "message": "Greenhouse added successfully."
                        }
                else:
                    action_response = {
                        "type": "warning",
                        "message": "Greenhouse not added, invalid input."
                    }
            else:
                action_response = {
                    "type": "danger",
                    "message": "Invalid input."
                }

        _greenhouses = db\
            .session\
            .query(CustomerGreenhouse, Greenhouse, Farm)\
            .join(Greenhouse, CustomerGreenhouse.greenhouse_id == Greenhouse.id)\
            .join(Farm, CustomerGreenhouse.farm_id == Farm.id)\
            .filter(CustomerGreenhouse.customer_id == _this_customer.id)\
            .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
            .all()
        for _greenhouse in _greenhouses:
            greenhouses.append({
                "id": _greenhouse.CustomerGreenhouse.uid,
                "name": _greenhouse.CustomerGreenhouse.name,
                "greenhouse_ref": _greenhouse.Greenhouse.name,
                "farm": _greenhouse.Farm.name,
                "devices": devices_in_greenhouse(_greenhouse.CustomerGreenhouse.id),
                "status": "Active" if _greenhouse.CustomerGreenhouse.status == config.STATUS_ACTIVE else "Inactive",
                "created": _greenhouse.CustomerGreenhouse.created_at,
                "creator": contact_person(_greenhouse.CustomerGreenhouse.creator_id),
            })

    return render_template(
        'customers/greenhouses/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_GREENHOUSES_ACTIVE='active',
        FORM=greenhouse_form_constructor(),
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        CUSTOMER=customer_info,
        GREENHOUSES=greenhouses,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@customer_greenhouses.route('/<greenhouse_id>/edit', methods=['POST', 'GET'])
def edit_greenhouse(greenhouse_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = this_org()
    _this_user = this_user()

    form_action = 'Edit'
    action_form = greenhouse_form_constructor()
    action_response = {}
    posted = request.form

    greenhouses = []
    customer_info = {}

    _this_customer = _this_org

    _this_greenhouse = db\
        .session\
        .query(Greenhouse, CustomerGreenhouse)\
        .join(CustomerGreenhouse, CustomerGreenhouse.greenhouse_id == Greenhouse.id)\
        .filter(CustomerGreenhouse.uid == greenhouse_id)\
        .filter(Greenhouse.status > config.STATUS_DELETED)\
        .first()

    if _this_customer and _this_greenhouse:
        customer_info = {
            "id": _this_customer.uid,
            "name": _this_customer.name,
        }

        if posted:
            if posted.get("action").strip().lower() == 'edit':
                _name = posted.get("name").strip()
                _farm_id = posted.get("farm_id").strip()
                _greenhouse_id = posted.get("greenhouse_id").strip()
                _status = posted.get("status").strip()

                if _name is not None and _farm_id is not None and _greenhouse_id is not None:

                    _selected_farm = Farm\
                        .query\
                        .filter(Farm.uid == _farm_id)\
                        .filter(Farm.status > config.STATUS_DELETED)\
                        .first()

                    _selected_greenhouse = Greenhouse\
                        .query\
                        .filter(Greenhouse.uid == _greenhouse_id)\
                        .filter(Greenhouse.status > config.STATUS_DELETED)\
                        .first()
                    if _selected_farm and _selected_greenhouse:
                        _this_greenhouse.CustomerGreenhouse.name = _name
                        _this_greenhouse.CustomerGreenhouse.farm_id = _selected_farm.id
                        _this_greenhouse.CustomerGreenhouse.greenhouse_id = _selected_greenhouse.id
                        _this_greenhouse.CustomerGreenhouse.status = _status

                        db.session.commit()

                        action_response = {
                            "type": "success",
                            "message": "Greenhouse edited successfully."
                        }

                else:
                    action_response = {
                        "type": "warning",
                        "message": "Greenhouse not edited, invalid input."
                    }

            else:
                action_response = {
                    "type": "danger",
                    "message": "Invalid input."
                }

        for elem in action_form:
            if elem["name"] == "name":
                elem["default"] = _this_greenhouse.CustomerGreenhouse.name

            if elem["name"] == "farm_id":
                _f = Farm\
                    .query\
                    .filter(Farm.id == _this_greenhouse.CustomerGreenhouse.farm_id)\
                    .filter(Farm.status > config.STATUS_DELETED)\
                    .first()
                if _f:
                    elem["default"] = _f.uid

            if elem["name"] == "greenhouse_id":
                _g_h = Greenhouse\
                    .query\
                    .filter(Greenhouse.id == _this_greenhouse.CustomerGreenhouse.greenhouse_id)\
                    .filter(Greenhouse.status > config.STATUS_DELETED)\
                    .first()
                if _g_h:
                    elem["default"] = _g_h.uid

            if elem["name"] == "status":
                elem["default"] = _this_greenhouse.CustomerGreenhouse.status


        _greenhouses = db\
            .session\
            .query(CustomerGreenhouse, Greenhouse, Farm)\
            .join(Greenhouse, CustomerGreenhouse.greenhouse_id == Greenhouse.id)\
            .join(Farm, CustomerGreenhouse.farm_id == Farm.id)\
            .filter(CustomerGreenhouse.customer_id == _this_customer.id)\
            .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
            .all()
        for _greenhouse in _greenhouses:
            greenhouses.append({
                "id": _greenhouse.CustomerGreenhouse.uid,
                "name": _greenhouse.CustomerGreenhouse.name,
                "greenhouse_ref": _greenhouse.Greenhouse.name,
                "farm": _greenhouse.Farm.name,
                "status": "Active" if _greenhouse.CustomerGreenhouse.status == config.STATUS_ACTIVE else "Inactive",
                "created": _greenhouse.CustomerGreenhouse.created_at,
                "creator": contact_person(_greenhouse.CustomerGreenhouse.creator_id),
            })

    return render_template(
        'customers/greenhouses/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_GREENHOUSES_ACTIVE='active',
        FORM=action_form,
        FORM_ACTION=form_action,
        FORM_ACTION_RESPONSE=action_response,
        CUSTOMER=customer_info,
        GREENHOUSES=greenhouses,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )

@customer_greenhouses.route('/<greenhouse_id>/delete', methods=['POST', 'GET'])
def delete_greenhouse(greenhouse_id):
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    _this_org = this_org()
    _this_user = this_user()

    action_response = {}
    customer_info = {}

    greenhouses = []
    customer_info = {}

    _this_customer = _this_org
    if _this_customer:
        customer_info = {
            "id": _this_customer.uid,
            "name": _this_customer.name,
        }

    _this_greenhouse = db\
        .session\
        .query(Greenhouse, CustomerGreenhouse)\
        .join(CustomerGreenhouse, CustomerGreenhouse.greenhouse_id == Greenhouse.id)\
        .filter(CustomerGreenhouse.uid == greenhouse_id)\
        .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
        .filter(Greenhouse.status > config.STATUS_DELETED)\
        .first()

    if _this_greenhouse:
        _this_greenhouse.CustomerGreenhouse.status=config.STATUS_DELETED
        db.session.commit()

        action_response = {
            "type": "success",
            "message": _this_greenhouse.CustomerGreenhouse.name + " deleted successfully."
        }
    else:
        action_response = {
            "type": "danger",
            "message": "Invalid input."
        }

    _greenhouses = db\
        .session\
        .query(CustomerGreenhouse, Greenhouse, Farm)\
        .join(Greenhouse, CustomerGreenhouse.greenhouse_id == Greenhouse.id)\
        .join(Farm, CustomerGreenhouse.farm_id == Farm.id)\
        .filter(CustomerGreenhouse.customer_id == _this_customer.id)\
        .filter(CustomerGreenhouse.status > config.STATUS_DELETED)\
        .all()
    for _greenhouse in _greenhouses:
        greenhouses.append({
            "id": _greenhouse.CustomerGreenhouse.uid,
            "name": _greenhouse.CustomerGreenhouse.name,
            "greenhouse_ref": _greenhouse.Greenhouse.name,
            "farm": _greenhouse.Farm.name,
            "status": "Active" if _greenhouse.CustomerGreenhouse.status == config.STATUS_ACTIVE else "Inactive",
            "created": _greenhouse.CustomerGreenhouse.created_at,
            "creator": contact_person(_greenhouse.CustomerGreenhouse.creator_id),
        })

    return render_template(
        'customers/greenhouses/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_GREENHOUSES_ACTIVE='active',
        FORM_ACTION_RESPONSE=action_response,
        CUSTOMER=customer_info,
        GREENHOUSES=greenhouses,
        PROFILE=session['profile'],
        SERVICES=session['services'],
        data={"date":str(datetime.now().year)}
    )
