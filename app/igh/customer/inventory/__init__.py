from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.core import customer as _customer
from app.core.customer import farms as _farms

from app.igh import portal_check

customer_inventory = Blueprint('customer_inventory', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_inventory.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@customer_inventory.route('/product-inventory', methods=['POST', 'GET'])
def product_inventory():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/inventory/product-inventory.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_inventory.route('/activate-screenhouse', methods=['POST', 'GET'])
def activate_screenhouse():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/inventory/activate-screenhouse.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_inventory.route('/activate-shadenet', methods=['POST', 'GET'])
def activate_shadenet():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/inventory/activate-shadenet.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_inventory.route('/activated-products', methods=['POST', 'GET'])
def active_product():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/inventory/active-products.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )

@customer_inventory.route('/fertilizers-agrochems', methods=['POST', 'GET'])
def fertilizers_agrochems():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/inventory/fertilizers-agrochems.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_PRODUCTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
