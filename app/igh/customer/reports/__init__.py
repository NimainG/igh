from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.igh import portal_check

from app.helper import valid_uuid, valid_date

customer_reports = Blueprint('customer_reports', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_reports.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@customer_reports.route('/daily-work', methods=['POST', 'GET'])
def main():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/reports/daily-work.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_REPORTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
