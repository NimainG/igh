from functools import wraps

import json
import os
import urllib.parse
import requests
import importlib
import uuid

from werkzeug.exceptions import HTTPException

from dotenv import load_dotenv
from flask import Flask
from flask import jsonify
from flask import redirect
from flask import render_template
from flask import session
from flask import url_for
from flask import Blueprint
from flask import request
from authlib.integrations.flask_client import OAuth
from six.moves.urllib.parse import urlencode
from datetime import datetime

from app.config import activateConfig

from app.core import customer as _customer
from app.core.customer import farms as _farms

from app.igh import portal_check

customer_alerts = Blueprint('customer_alerts', __name__,  static_folder='../../static', template_folder='.../../templates')
customer_alerts.config = {}

load_dotenv()

config = activateConfig(os.getenv('ENV') or 'production')

@customer_alerts.route('/', methods=['POST', 'GET'])
def alerts():
    redirect, realm = portal_check(session, 'customer')
    if redirect:
        return realm

    return render_template(
        'customers/alerts/main.html',
        CUSTOMER_PORTAL=True,
        CUSTOMER_PORTAL_ALERTS_ACTIVE='active',
        PROFILE=session['profile'],
        data={"date":str(datetime.now().year)}
    )
